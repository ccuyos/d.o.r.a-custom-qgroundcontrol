'''
Title: GUIConstants.py
Date: 5/18/2023
Description: Lists all the GUI constants and enums used for GUI creation
'''

# Spacing sizes #
smallSpace = 0
mediumSpace = 1
largeSpace = 2
extremeSpace = 3
customSpace = 4

smallSize = 20
mediumSize = 50
largeSize = 100
extremeSize = 500