function PlotSensors(data, time, want, dT)
timeData_raw = data(:, time) * dT;
wantData_raw = data(:, want);
want_valid_idx = find(isnan(wantData_raw) ~= 1);
timeData_true = timeData_raw(want_valid_idx);
wantData_true = wantData_raw(want_valid_idx);
want_valid_idx = find(wantData_true ~= 0);
timeData_true = timeData_true(want_valid_idx);
wantData_true = wantData_true(want_valid_idx);
figure 
plot(timeData_true', wantData_true')
end
