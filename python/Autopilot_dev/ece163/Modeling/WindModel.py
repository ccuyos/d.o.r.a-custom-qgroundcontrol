'''
    Author: Margaret Silva
    This file contains the wind model class and associated functions
'''

import math
import random
from ..Containers import States
from ..Containers import Inputs
from ..Utilities import MatrixMath as MM
from ..Constants import VehiclePhysicalConstants as VPC


class WindModel():

    def __init__(self, dt=VPC.dT, Va=VPC.InitialSpeed, drydenParamters=VPC.DrydenNoWind):
        ''' Inits all necessary components of the wind model '''
        self.windState = States.windState()
        self.dt = dt
        self.Va = Va
        self.drydenParams = drydenParamters
        self.xu = [[0]]
        self.xv = [[0], [0]]
        self.xw = [[0], [0]]
        # init dryden matrices with zeros, then populate them using createDrydenTransferFns()
        self.Phiu = [[0]]
        self.Gammau = [[0]]
        self.Hu = [[0]]
        self.Phiv = [[0, 0], [0, 0]]
        self.Gammav = [[0], [0]]
        self.Hv = [[0, 0]]
        self.Phiw = [[0, 0], [0, 0]]
        self.Gammaw = [[0], [0]]
        self.Hw = [[0, 0]]
        self.CreateDrydenTransferFns(self.dt, self.Va, self.drydenParams)


    def CreateDrydenTransferFns(self, dT, Va, drydenParamters):
        ''' Creates dryden transfer functions internally in discrete form '''

        # handle zero speed corner case
        if math.isclose(Va, 0):
            raise ArithmeticError

        Lu = drydenParamters.Lu
        Lv = drydenParamters.Lv
        Lw = drydenParamters.Lw
        sig_u = drydenParamters.sigmau
        sig_v = drydenParamters.sigmav
        sig_w = drydenParamters.sigmaw

        # matrices for axis u
        if math.isclose(Lu, 0):     # prevent divide by zero error
            self.Phiu = [[1]]       # value of x_u remains unchanged
            self.Gammau = [[0]]     # gusts go to zero
            self.Hu = [[1]]
        else: 
            self.Phiu = [[math.exp(-(Va/Lu)*dT)]]
            self.Gammau = MM.scalarMultiply(Lu/Va, 
                                        [[1 - math.exp(-(Va/Lu)*dT)]])
            self.Hu = [[sig_u*math.sqrt((2*Va)/(math.pi*Lu))]]

        # matrices for axis v
        if math.isclose(Lv, 0):     # prevent divide by zero error
            self.Phiv = [[1, 0], [0, 1]]
            self.Gammav = [[0], [0]]
            self.Hv = [[1, 1]]
        else: 
            self.Phiv = MM.scalarMultiply(math.exp(-1.0*(Va/Lv)*dT),
                                [[1 - (Va/Lv)*dT, -((Va/Lv)**2)*dT], 
                                    [dT, 1 + (Va/Lv)*dT]])
            self.Gammav = MM.scalarMultiply(math.exp(-1.0*(Va/Lv)*dT),
                        [[dT], 
                        [((Lv/Va)**2) * (math.exp((Va/Lv)*dT) - 1) - ((Lv/Va)*dT)]])
            self.Hv = MM.scalarMultiply(sig_v*math.sqrt((3*Va)/(math.pi*Lv)),
                                        [[1, Va/(math.sqrt(3)*Lv)]])
        
        # matrices for axis w
        if math.isclose(Lw, 0):     # prevent divide by zero error
            self.Phiw = [[1, 0], [0, 1]]
            self.Gammaw = [[0], [0]]
            self.Hw = [[1, 1]]
        else: 
            self.Phiw = MM.scalarMultiply(math.exp(-1.0*(Va/Lw)*dT),
                                [[1 - (Va/Lw)*dT, -((Va/Lw)**2)*dT], 
                                    [dT, 1 + (Va/Lw)*dT]])
            self.Gammaw = MM.scalarMultiply(math.exp(-1.0*(Va/Lw)*dT),
                        [[dT], 
                        [((Lw/Va)**2) * (math.exp((Va/Lw)*dT) - 1) - ((Lw/Va)*dT)]])
            self.Hw = MM.scalarMultiply(sig_w*math.sqrt((3*Va)/(math.pi*Lw)),
                                        [[1, Va/(math.sqrt(3)*Lw)]])


    def Update(self, uu=None, uv=None, uw=None):
        ''' Updates wind gusts and inserts them back into the wind state '''
        if uu == None:
            uu = random.gauss(0, 1)
        if uv == None:
            uv = random.gauss(0, 1)
        if uw == None:
            uw = random.gauss(0, 1)
        
        # update u axis
        self.xu = MM.add(MM.multiply(self.Phiu, self.xu), MM.multiply(self.Gammau, [[uu]]))
        self.windState.Wu = MM.multiply(self.Hu, self.xu)[0][0]
        # update v axis
        self.xv = MM.add(MM.multiply(self.Phiv, self.xv), MM.multiply(self.Gammav, [[uv]]))
        self.windState.Wv = MM.multiply(self.Hv, self.xv)[0][0]
        # update v axis
        self.xw = MM.add(MM.multiply(self.Phiw, self.xw), MM.multiply(self.Gammaw, [[uw]]))
        self.windState.Ww = MM.multiply(self.Hw, self.xw)[0][0]


    def getDrydenTransferFns(self):
        ''' Returns all 9 of the current dryden transfer matrices '''
        return self.Phiu, self.Gammau, self.Hu, self.Phiv, self.Gammav, self.Hv, self.Phiw, \
            self.Gammaw, self.Hw


    def getWind(self):
        ''' Returns the current wind state '''
        return self.windState


    def setWind(self, windState):
        ''' 
            Sets the current wind state to the value passed in 
            Used to inject constant wind and gust values into the model
        '''
        self.windState = windState


    def reset(self):
        ''' 
            Resets wind model code, but does not reset model params. To change model params, use 
            CreateDrydenTransferFns() 
        '''
        self.windState = States.windState()
        self.xu = [[0]]
        self.xv = [[0], [0]]
        self.xw = [[0], [0]]


    def setWindModelParameters(self, Wn=0.0, We=0.0, Wd=0.0, drydenParamters=VPC.DrydenNoWind):
        ''' Injects parameters into wind model '''
        self.windState.Wn = Wn
        self.windState.We = We
        self.windState.Wd = Wd
        # calculate new dryden transfer function matrices
        self.drydenParams = drydenParamters
        self.CreateDrydenTransferFns(self.dt, self.Va, self.drydenParams)
