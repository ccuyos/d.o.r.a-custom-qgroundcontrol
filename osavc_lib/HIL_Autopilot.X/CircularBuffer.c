/* 
 * File:   CircularBuffer.c
 * Author: Margaret Silva
 * Brief: Used to create and handle circular buffers and their operations
 * Created on Jan 22, 2022, 1:22 am
 * Modified on Jan 22, 2022, 1:22 am
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

#include "CircularBuffer.h" // The header file for this source file. 

/*******************************************************************************
 * PRIVATE #DEFINES                                                            *
 ******************************************************************************/


/*******************************************************************************
 * PRIVATE TYPEDEFS                                                            *
 ******************************************************************************/

/*******************************************************************************
 * PRIVATE FUNCTIONS PROTOTYPES                                                 *
 ******************************************************************************/

/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATIONS                                             *
 ******************************************************************************/

/**
 * @Function createBuffer()
 * @param none
 * @return a newly initialized circular buffer
 * @brief used to create a new empty circular buffer
 * @note 
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 1:30 am */
struct CircularBuffer createBuffer()
{
    struct CircularBuffer newBuffer = {.head = 0, .tail = 0};
    return newBuffer;
}

/**
 * @Function bufferEnqueue()
 * @param buffer, a pointer to a circular buffer struct
 *        item, an item to be added to the buffer
 * @return TRUE to indicate success, FALSE to indicate failure
 * @brief used to add a new item to a buffer
 * @note if the buffer is full, this function doesn't execute
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 1:48 am */
uint8_t bufferEnqueue(struct CircularBuffer * buffer, unsigned char item)
{
    if (isBufferFull(buffer)) {
        return FALSE; //failure
    } else {
        buffer->buffer[buffer->tail] = item;
        buffer->tail = (buffer->tail + 1) % BUFFER_LEN;
        return TRUE; //success
    }
}

/**
 * @Function bufferDequeue()
 * @param buffer, a pointer to a circular buffer struct
 *        item, a pointer to which the dequeued item is written to
 * @return TRUE to indicate success, FALSE to indicate failure
 * @brief used to pull an item from the buffer
 * @note if the buffer is empty, this function doesn't execute
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 1:57 am */
uint8_t bufferDequeue(struct CircularBuffer * buffer, unsigned char * item)
{
    if (isBufferEmpty(buffer)) {
        return FALSE;
    } else {
        *item = buffer->buffer[buffer->head];
        buffer->head = (buffer->head + 1) % BUFFER_LEN;
        return TRUE;
    }
}

/**
 * @Function isBufferEmpty()
 * @param buffer, a pointer to a circular buffer struct
 * @return TRUE or FALSE
 * @brief used to check if the indicated buffer is empty or not
 * @note 
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 2:00 am */
uint8_t isBufferEmpty(struct CircularBuffer * buffer)
{
    if (buffer->tail == buffer->head) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * @Function isBufferFull()
 * @param buffer, a pointer to a circular buffer struct
 * @return TRUE or FALSE
 * @brief used to check if the indicated buffer is full or not
 * @note 
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 1:53 am */
uint8_t isBufferFull(struct CircularBuffer * buffer)
{
    if (((buffer->tail + 1) % BUFFER_LEN) == buffer->head) {
        return TRUE; // the buffer is full
    } else {
        return FALSE; // some space remains
    }
}

/**
 * @Function bufferLength()
 * @param buffer, a pointer to a circular buffer struct
 * @return the number of items currently in the buffer
 * @brief used to count the number of items in the given buffer
 * @note 
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 2:03 am */
int bufferLength(struct CircularBuffer * buffer)
{
    int num_items = buffer->tail - buffer->head;
    if (num_items < 0) {
        num_items += BUFFER_LEN;
    }
    return num_items;
}


/**
 * @Function bufferDelete()
 * @param buffer, a pointer to a circular buffer struct
 * @param num_bytes, the number of bytes to be deleted, starting at the tail of the buffer
 * @return TRUE or FALSE, FALSE indicating that an error has occurred
 * @brief used to quickly remove items from the end of the buffer
 * @note will fail if the number of bytes exceeds the current buffer length
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.30 7:23 am */
uint8_t bufferDelete(struct CircularBuffer * buffer, unsigned char num_bytes){
    if (bufferLength(buffer) < num_bytes){
        return FALSE;
    }
    
    buffer->tail -= num_bytes;
    if (buffer->tail < 0){
        // used to correct for wrap around
        buffer->tail += BUFFER_LEN;
    }
    return TRUE;
}

/*******************************************************************************
 * PRIVATE FUNCTION IMPLEMENTATIONS                                            *
 ******************************************************************************/



// test harness
//#define CIRCULAR_BUFFER_TEST
#ifdef CIRCULAR_BUFFER_TEST

int main()
{
    int ledsLit = 0x00;
    char testchar;
    struct CircularBuffer buffer = createBuffer();

    BOARD_Init();

    //createBuffer() tests
    if (buffer.head == 0) {
        ledsLit |= 0b01;
    }
    if (buffer.tail == 0) {
        ledsLit |= 0b10;
    }

    setLEDs(ledsLit); // 2 leds should light
    while (!(buttonStatus() & BTN1_Status)) {
        // keep displaying the led pattern until a button is pressed
    }

    ledsLit = 0x0000;
    // bufferEnqueue/Dequeue tests
    bufferEnqueue(&buffer, 'a');
    bufferEnqueue(&buffer, 'b');
    bufferEnqueue(&buffer, 'c');
    if (buffer.head == 0) {
        ledsLit |= 0b01;
    }
    if (buffer.tail == 3) {
        ledsLit |= 0b10;
    }
    bufferDequeue(&buffer, &testchar);
    if (testchar == 'a') {
        ledsLit |= 0b100;
    }
    bufferDequeue(&buffer, &testchar);
    if (testchar == 'b') {
        ledsLit |= 0b1000;
    }
    bufferDequeue(&buffer, &testchar);
    if (testchar == 'c') {
        ledsLit |= 0b10000;
    }
    if (bufferDequeue(&buffer, &testchar) == FALSE) {
        ledsLit |= 0b100000;
    }
    setLEDs(ledsLit); // 6 leds should light
    while (!(buttonStatus() & BTN2_Status)) {
        // keep displaying the led pattern until a button is pressed
    }

    // test isBufferEmpty(), isBufferFull(), and bufferLength()
    ledsLit = 0x0000;
    if (isBufferEmpty(&buffer)) {
        ledsLit |= 0b01;
    }
    bufferEnqueue(&buffer, 'f');
    if (isBufferEmpty(&buffer) == FALSE) {
        ledsLit |= 0b10;
    }
    while (isBufferFull(&buffer) == FALSE) {
        if (bufferEnqueue(&buffer, 'f') == FALSE) {
            // if the enqueue fails, light leds in certain pattern
            setLEDs(0xF0F0);
            while (1);
        }
        setLEDs(bufferLength(&buffer)); // show the current buffer length
    }
    if (bufferLength(&buffer) == (BUFFER_LEN - 1)) {
        ledsLit |= 0b100;
    }
    bufferDequeue(&buffer, &testchar);
    bufferDequeue(&buffer, &testchar);
    bufferDequeue(&buffer, &testchar);
    if (bufferLength(&buffer) == (BUFFER_LEN - 1 - 3)) {
        ledsLit |= 0b1000;
    }

    setLEDs(ledsLit); // 4 leds should light
    while (!(buttonStatus() & BTN3_Status)) {
        // keep displaying the led pattern until a button is pressed
    }

    while (1);
}
#endif