/* 
 * File:   AutopilotController.c
 * Author: Margaret Silva
 * Brief: Contains all functions needed to run the feedback controller for the 
 *          autopilot and return actuator commands
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/
#include <stdlib.h>
#include "StabilityController.h"
#include "PI.h"
#include "PD.h"
#include "PID.h"
#include "AirframeConstants.h"
#include "math.h"
#include "Board.h"

/*******************************************************************************
 * PRIVATE #DEFINES                                                            *
 ******************************************************************************/
enum altitude_states {
    CLIMBING, HOLDING, DESCENDING
};

#define FLOAT_COMPARISON 0.000001

#define DEBUG_MSG_PRINT_RATE 10

#define RUDDER_MOVING_AVG_LEN 10

/*******************************************************************************
 * STATIC VARIABLES                                                            *
 ******************************************************************************/
static PI_controller course = {// output u is commanded roll
    .dt = DT_s,
    .kp = 0.0,
    .ki = 0.0,
    .trim = 0.0,
    .u_max = MAX_ROLL_ANGLE,
    .u_min = MIN_ROLL_ANGLE
};

static PID_controller roll = {// output u is aileron command
    .dt = DT_s,
    .kp = 0.0,
    .ki = 0.0,
    .kd = 0.0,
    .trim = TRIM_CONTROLS_AILERON,
    .u_max = MAX_AILERON_ANGLE,
    .u_min = MIN_AILERON_ANGLE
};

static PI_controller rudder_sideslip = {// this one is likely to be replaced later on   
    .dt = DT_s, // output u is rudder command
    .kp = 0.0,
    .ki = 0.0,
    .trim = TRIM_CONTROLS_RUDDER,
    .u_max = MAX_RUDDER_ANGLE,
    .u_min = MIN_RUDDER_ANGLE
};

static PI_controller rudder_yaw_damping = {// controller good for irl flight  
    .dt = DT_s, // output u is rudder command
    .kp = 0.0,
    .ki = 0.0,
    .trim = TRIM_CONTROLS_RUDDER,
    .u_max = MAX_RUDDER_ANGLE,
    .u_min = MIN_RUDDER_ANGLE
};

static PI_controller airspeed_throttle = {// output u is throttle command (during const alitutde flight)
    .dt = DT_s,
    .kp = 0.0,
    .ki = 0.0,
    .trim = TRIM_CONTROLS_THROTTLE,
    .u_max = MAX_THROTTLE,
    .u_min = MIN_THROTTLE
};

static PI_controller airspeed_pitch = {// output u is pitch command (during ascent/descent flight)
    .dt = DT_s,
    .kp = 0.0,
    .ki = 0.0,
    .trim = 0.0,
    .u_max = MAX_PITCH_ANGLE,
    .u_min = MAX_PITCH_ANGLE
};

static PI_controller altitude = {// output u is commanded pitch
    .dt = DT_s,
    .kp = 0.0,
    .ki = 0.0,
    .trim = 0.0,
    .u_max = MAX_PITCH_ANGLE,
    .u_min = MIN_PITCH_ANGLE
};

static PD_controller pitch = {// output u in elevator command
    .dt = DT_s,
    .kp = 0.0,
    .kd = 0.0,
    .trim = TRIM_CONTROLS_ELEVATOR,
    .u_max = MAX_ELEVATOR_ANGLE,
    .u_min = MIN_ELEVATOR_ANGLE
};

// statics needed for alternative yaw damping controller
static float rudder_xi = 0;

/*******************************************************************************
 * PRIVATE FUNCTIONS PROTOTYPES                                                 *
 ******************************************************************************/

/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATIONS                                             *
 ******************************************************************************/

/**
 * @Function initController()
 * @param none
 * @return none
 * @brief initializes controllers to have the values saved on the SD card
 * @author Margaret Silva */
void initController(void) {
    // lateral controllers
    PI_init(&course);
    PID_init(&roll);
    PI_init(&rudder_sideslip);
    PI_init(&rudder_yaw_damping);
    // longitudinal controllers
    PI_init(&airspeed_pitch);
    PI_init(&airspeed_throttle);
    PI_init(&altitude);
    PD_init(&pitch);
    rudder_xi = 0;
}

/**
 * @Function updateController()
 * @param current_state, the aircraft's current state
 *        X_c, the aircraft's commanded course angle
 *        h_c, the aircraft's commanded altitude
 *        Va_c, the aircraft's commanded airspeed
 * @return controller outputs calculated by controller
 * @brief updates the controller with the current state estimation and commanded
 *          course and altitude
 * @author Margaret Silva */
struct controller_outputs updateController(struct vehicle_state current_state,
        float X_c, float h_c, float Va_c) {
    // define all variables
    static enum altitude_states long_controller_state = CLIMBING;
    struct controller_outputs actuator_cmds = {0, 0, 0, 0};
    double rc;
    
    // make sure commanded course is in range [-pi, pi]
    if ((X_c - current_state.chi) >= M_PI) {
        current_state.chi = current_state.chi + 2 * M_PI;
    }
    if ((X_c - current_state.chi) <= -M_PI) {
        current_state.chi = current_state.chi - 2 * M_PI;
    }

    // lateral controllers
    PI_update(&course, X_c, current_state.chi);
    PID_update(&roll, course.u, current_state.roll, current_state.p);
    
    // command yaw rate to match what's need for coordinated turn
//    if (fabs(current_state.Va) < 0.1) {
//        rc = g0 * tan(course.u);
//    } else {
//        rc = (sin(course.u) * cos(current_state.pitch) * g0) / current_state.Va;
//    }
//    PI_update(&rudder_yaw_damping, (float) rc, current_state.r);
//    actuator_cmds.d_rudder = rudder_yaw_damping.u;
    
    rudder_xi += DT_s * (-rudder_yaw_damping.ki * rudder_xi + rudder_yaw_damping.kp*current_state.r);
    actuator_cmds.d_rudder = -rudder_yaw_damping.ki * rudder_xi + rudder_yaw_damping.kp * current_state.r;

    actuator_cmds.d_aileron = roll.u;

    // longitudinal controllers
    switch (long_controller_state) {
            // control throttle based on current state
        case DESCENDING:
            actuator_cmds.d_throttle = MIN_THROTTLE;
            if (fabs(h_c + current_state.pd) < ALTITUDE_HOLD_THRESHOLD) {
                long_controller_state = HOLDING;
                PI_reset_integrator(&altitude);
                // set throttle to correct value
                actuator_cmds.d_throttle = TRIM_CONTROLS_THROTTLE;
            }
            break;
        case HOLDING:
            actuator_cmds.d_throttle = TRIM_CONTROLS_THROTTLE;
            if (-current_state.pd < (h_c - ALTITUDE_HOLD_THRESHOLD)) {
                long_controller_state = CLIMBING;
                PI_reset_integrator(&airspeed_pitch);
                // set throttle to correct value
                actuator_cmds.d_throttle = MAX_THROTTLE;
            } else if (-current_state.pd > (h_c + ALTITUDE_HOLD_THRESHOLD)) {
                long_controller_state = DESCENDING;
                PI_reset_integrator(&airspeed_pitch);
                // set throttle to correct value
                actuator_cmds.d_throttle = MIN_THROTTLE;
            }
            break;
        case CLIMBING:
            actuator_cmds.d_throttle = MAX_THROTTLE;
            if (fabs(h_c + current_state.pd) < ALTITUDE_HOLD_THRESHOLD) {
                long_controller_state = HOLDING;
                PI_reset_integrator(&altitude);
                // set throttle to correct value
                actuator_cmds.d_throttle = TRIM_CONTROLS_THROTTLE;
            }
            break;
        default:
            break;
    }

    // command pitch based on altitude
    PI_update(&altitude, h_c, -current_state.pd);
    PD_update(&pitch, altitude.u, current_state.pitch, current_state.q);
    actuator_cmds.d_elevator = pitch.u;

    return actuator_cmds;
}

/**
 * @Function setControllerGains(controller_gains gains)
 * @param gains - controller gains for both lateral and longitudinal controllers
 * @return none
 * @brief updates the controller with the a new set of gains. This function only works for controllers
 *          that match with the default 163 controller structure
 * @author Margaret Silva */
void setControllerGains(float gains[16]) {   
    // roll controller
    roll.kp = gains[0];
    roll.kd = gains[1];
    roll.ki = gains[2];
#ifdef USE_SIDESLIP_CONTROLLER
    // sideslip controller
    rudder_sideslip.kp = gains[3];
    rudder_sideslip.ki = gains[4];
#else
    // yaw damping controller
    rudder_yaw_damping.kp = -gains[3];
    rudder_yaw_damping.ki = -gains[4];
#endif
    // course controller
    course.kp = gains[5];
    course.ki = gains[6];
    // pitch
    pitch.kp = gains[7];
    pitch.kd = gains[8];
    // altitude 
    altitude.kp = gains[9];
    altitude.ki = gains[10];
    // speed
    airspeed_throttle.kp = gains[11];
    airspeed_throttle.ki = gains[12];
    airspeed_pitch.kp = gains[13];
    airspeed_pitch.ki = gains[14];
}

/**
 * @Function getControllerGains()
 * @param none
 * @return current controller gains
 * @brief returns the current controller gains. This only works with the default
 *      controller set up as is
 * @author Margaret Silva */
struct controller_gains getControllerGains(void) {
    struct controller_gains gains;
    // roll controller
    gains.kp_roll = roll.kp;
    gains.kd_roll = roll.kd;
    gains.ki_roll = roll.ki;
#ifdef USE_SIDESLIP_CONTROLLER
    // sideslip controller
    gains.kp_rudder = rudder_sideslip.kp;
    gains.ki_rudder = rudder_sideslip.ki;
#else
    // yaw damping controller
    gains.kp_rudder = rudder_yaw_damping.kp;
    gains.ki_rudder = rudder_yaw_damping.ki;
#endif
    // course controller
    gains.kp_course = course.kp;
    gains.ki_course = course.ki;
    // pitch
    gains.kp_pitch = pitch.kp;
    gains.kd_pitch = pitch.kd;
    // altitude 
    gains.kp_altitude = altitude.kp;
    gains.ki_altitude = altitude.ki;
    // speed
    gains.kp_speedFromThrottle = airspeed_throttle.kp;
    gains.ki_speedFromThrottle = airspeed_throttle.ki;
    gains.kp_speedFromElevator = airspeed_pitch.kp;
    gains.ki_speedFromElevator = airspeed_pitch.ki;
    return gains;
}
