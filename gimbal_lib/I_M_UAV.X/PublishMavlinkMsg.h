/* 
 * File:   PublishMavlinkMsg.h
 * Author: Margaret Silva
 * 
 * This file contains all the mavlink publish functions used in the main file
 *
 * Created on May 26, 2023, 2:42 PM
 */

#ifndef PUBLISHMAVLINKMSG_H
#define	PUBLISHMAVLINKMSG_H


/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/
#include "BOARD.h"
#include "common/mavlink.h"
#include "serial.h"

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
// use this to switch between sending data over serial/over radio
#define GET_CHAR() GetChar()
#define PUT_CHAR(c) PutChar(c)
#define DATA_AVAILABLE() !IsReceiveEmpty()

// buffer defines
#define BUFFER_SIZE 1024

/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

/**
 * @Function publish_controller_output(float output, char controller_type[10])
 * @param none
 * @brief invokes mavlink helper to send controller output over radio
 * @author aaron hunter
 */
void publish_controller_output(float output, char controller_type[10]);

/**
 * @Function publish_status_text(void)
 * @param none
 * @brief invokes mavlink helper to send out status text
 * @author aaron hunter
 */
void publish_status_text(char text[50]);

/**
 * @Function publish_hil_act_controls(uint8_t mode, float outputs[16])
 * @param mode - flag that determines what info is transmitted
 *      outputs - data to be sent over mavlink
 * @brief invokes mavlink helper to generate hil act controls and sends out via the radio
 * @author aaron hunter
 */
void publish_hil_act_controls(uint64_t flags, float outputs[16]);

/**
 * @Function publish_heartbeat(void)
 * @param status - int value that specifies the type of heartbeat
 * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
 * @author aaron hunter
 */
void publish_heartbeat(uint8_t status);

/**
 * @Function publish_euler_angles(float angles[3])
 * @param angles[3] - yaw, pitch, and roll angles received from sim
 * @brief invokes mavlink helper to send actuator commands over radio
 * @author Margaret Silva
 */
void publish_euler_angles(float angles[3]);

/**
 * @Function publish_euler_angles(float angles[3])
 * @param angles[3] - yaw, pitch, and roll angles received from sim
 * @brief invokes mavlink helper to send actuator commands over radio
 * @author Margaret Silva
 */
void publish_euler_angles_redux(float angles[3]);

/**
 * @Function publish_est_and_gimbal_angles(float est_angles[3], float gimbal_angle[3])
 * @param est angles[3] - yaw, pitch, and roll angles received from attitude est
 * @param gimbal angles[3] - yaw, pitch, roll angles from gimbal settings
 * @brief invokes mavlink helper to send actuator commands over radio
 * @author Margaret Silva
 */
void publish_est_and_gimbal_angles(float est_angles[3], float gimbal_angle[3]);

/**
 * @Function publish_reference_cmds_angles(float X_c, float h_c)
 * @param X_c - commanded course
 *        h_c - commanded height
 * @brief invokes mavlink helper to send reference commands over radio
 * @author Margaret Silva
 */
void publish_reference_cmds_angles(float X_c, float h_c);


#endif	/* PUBLISHMAVLINKMSG_H */

