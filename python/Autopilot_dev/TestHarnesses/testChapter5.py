"""This file is a test harness for the module VehiclePerturbationModels. 

It is meant to be run from the Testharnesses directory of the repo with:

python ./TestHarnesses/testChapter5.py (from the root directory) -or-
python testChapter5.py (from inside the TestHarnesses directory)

at which point it will execute various tests on the VehiclePerturbationModels module"""

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Utilities.MatrixMath as mm
import ece163.Utilities.Rotations as Rotations
import ece163.Modeling.VehicleDynamicsModel as VDM
import ece163.Controls.VehiclePerturbationModels as VPM
import ece163.Modeling.WindModel as WM
import ece163.Controls.VehicleTrim as VehicleTrim
import ece163.Containers.Inputs as Inputs
import ece163.Containers.States as States
import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Containers.Linearized as Linearized

"""math.isclose doesn't work well for comparing things near 0 unless we 
use an absolute tolerance, so we make our own isclose:"""
isclose = lambda  a,b : math.isclose(a, b, abs_tol= 1e-12)

def compareVectors(a, b):
	"""A quick tool to compare two vectors"""
	el_close = [isclose(a[i][0], b[i][0]) for i in range(3)]
	return all(el_close)

#of course, you should test your testing tools too:
assert(compareVectors([[0], [0], [-1]],[[1e-13], [0], [-1+1e-9]]))
assert(not compareVectors([[0], [0], [-1]],[[1e-11], [0], [-1]]))
assert(not compareVectors([[1e8], [0], [-1]],[[1e8+1], [0], [-1]]))



failed = []
passed = []
def evaluateTest(test_name, boolean):
	"""evaluateTest prints the output of a test and adds it to one of two 
	global lists, passed and failed, which can be printed later"""
	if boolean:
		print(f"   passed {test_name}")
		passed.append(test_name)
	else:
		print(f"   failed {test_name}")
		failed.append(test_name)
	return boolean


#%% PUT A TEST HERE?
vTrim = VehicleTrim.VehicleTrim()
Vastar = 25.0
Gammastar = math.radians(6.0)
Kappastar = -1.0 / 150.0

check = vTrim.computeTrim(Vastar, Kappastar, Gammastar)
if check:
 print("Optimization successful")
else:
 print("Model converged outside of valid inputs, change parameters and try again")
	   
 
tF = VPM.CreateTransferFunction(
	vTrim.getTrimState(), 
	vTrim.getTrimControls())

print("Testing vehicle perturbation models basic functionality")
cur_test = "VPM functionality test"
expected = Linearized.transferFunctions(Va_trim=25.000000000000004, alpha_trim=0.05643956420442324, beta_trim=0.0, gamma_trim=0.10008261852239951, theta_trim=0.15652218272682275, phi_trim=-0.39967477522059336, a_phi1=22.62885069325777, a_phi2=130.88367819945046, a_beta1=0.7767725000000002,a_beta2=0.15059875000000006, a_theta1=5.294738297989019, a_theta2=99.94742162885468, a_theta3=-36.11238956662997, a_V1=0.30042541657427574, a_V2=11.345223241536509, a_V3=9.760909914307426)
if not evaluateTest(cur_test, tF == expected ):
    print("test failed")
    print("expected:", expected)
    print("returned:", tF)


#%% dThrust_dThrottle() test

print("Testing dThrust_dThrottle")

cur_test = "dThrust_dThrottle corner case test"
result = VPM.dThrust_dThrottle(25, .1279294, 0.010000094)/ VPC.mass
expected = 7.848621*10**(-2)
if not evaluateTest(cur_test, math.isclose(expected, result, abs_tol=10**-7) ):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)
    


#%% dThrust_dVa() test
print("Testing dThrust_dVa")
cur_test = "dThrust_dVa corner case test"
C_D = VPC.CD0 + VPC.CDalpha*.03958321 + VPC.CDdeltaE*.5
result = ((VPC.rho*(25)*VPC.S)/VPC.mass)*(C_D) - VPM.dThrust_dVa(25, .1279294, 0.01)/ VPC.mass
expected = 0.255816539750
if not evaluateTest(cur_test, isclose(expected, result) ):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)
    



#%% Print results:

total = len(passed) + len(failed)
print(f"\n---\nPassed {len(passed)}/{total} tests")
[print("   " + test) for test in passed]

if failed:
	print(f"Failed {len(failed)}/{total} tests:")
	[print("   " + test) for test in failed]
