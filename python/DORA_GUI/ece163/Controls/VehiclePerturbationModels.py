"""
File that contains all the functions required to create the linearized perturbation models from the stability derivatives
and the non-linear model (using numerical gradients). The linearization (both in terms of state space and transfer function)
is done about the trim state that is input to the functions. All models are using perturbations about nominal trim states
and inputs (e.g.: deltaElevator is change about trim elevator angle).
"""

import math
from ece163.Modeling import VehicleAerodynamicsModel
from ece163.Constants import VehiclePhysicalConstants as VPC
from ece163.Containers import States
from ece163.Containers import Inputs
from ece163.Containers import Linearized
from ece163.Utilities import MatrixMath


def CreateTransferFunction(trimState, trimInputs):
	"""
	Function to fill the transfer function parameters used for the successive loop closure from the given trim state
	and trim inputs. Note that these parameters will be later used to generate actual control loops. Vehicle Perturbation
	models are developed using the trim state and inputs. Models for transfer function parameters and state space
	implementations are calculated using constants in VehiclePhysicalParameters and the input trim state and trim
	control inputs. Results are returned as a Linearized.transferFunction class.

	:param trimState: vehicle trim state, as calculated in VehicleTrim code
	:param trimInputs: vehicle trim inputs, as calculated in VehicleTrim code
	:return: transferFunction, from Linearized.transferFunctions class
	"""
	transferFunction = Linearized.transferFunctions()

	trimState.Va = math.hypot(trimState.u, trimState.v, trimState.w)
	trimState.alpha = math.atan2(trimState.w, trimState.u)
	if math.isclose(trimState.Va, 0.0):
		trimState.beta = 0.0
	else:
		trimState.beta = math.asin(trimState.v / trimState.Va)
	transferFunction.Va_trim = trimState.Va
	transferFunction.alpha_trim = trimState.alpha
	transferFunction.beta_trim = trimState.beta
	transferFunction.phi_trim = trimState.roll
	transferFunction.theta_trim = trimState.pitch
	transferFunction.gamma_trim = trimState.pitch - trimState.alpha

	Va = trimState.Va
	transferFunction.a_phi1 = -0.25 * VPC.rho * Va * VPC.S *VPC.b**2 * VPC.Cpp
	transferFunction.a_phi2 = 0.5 * VPC.rho * Va ** 2 * VPC.S * VPC.b * VPC.CpdeltaA

	transferFunction.a_beta1 = -0.5 * VPC.rho * Va * VPC.S * VPC.CYbeta / VPC.mass
	transferFunction.a_beta2 = 0.5 * VPC.rho * Va * VPC.S * VPC.CYdeltaR / VPC.mass

	transferFunction.a_theta1 = -0.25 * VPC.rho * Va *VPC.S * VPC.c**2 * VPC.CMq / VPC.Jyy
	transferFunction.a_theta2 = -0.5 * VPC.rho * Va**2 * VPC.S * VPC.c * VPC.CMalpha / VPC.Jyy
	transferFunction.a_theta3 = 0.5 * VPC.rho * Va**2 * VPC.S * VPC.c * VPC.CMdeltaE / VPC.Jyy

	alpha = trimState.alpha
	transferFunction.a_V1 = VPC.rho * Va * VPC.S * (VPC.CD0 + VPC.CDalpha * alpha + VPC.CDdeltaE *
														 trimInputs.Elevator) / VPC.mass - dThrust_dVa(Va, trimInputs.Throttle) / VPC.mass
	transferFunction.a_V2 = dThrust_dThrottle(Va, trimInputs.Throttle) / VPC.mass
	transferFunction.a_V3 = VPC.g0 * math.cos(transferFunction.gamma_trim)
	return transferFunction

def CreateStateSpace(trimState, trimInputs):
	"""
	Function to fill the state space matrices used for LQR / Pole Placement loop closure from the given trim state
	and trim inputs. Matrices are updated in self.transferFunction. Lateral state is [v, p, r, roll, yaw]^T and
	Longitudinal state is [u, w, q, pitch, height]^T. Height is used rather than down traditionally. Models for state
	space implementations are calculated using constants in VehiclePhysicalParameters and the input trim state and trim
	control inputs. Results are returned as a Linearized.stateSpace class.

	:param trimState: vehicle trim state, as calculated in VehicleTrim code
	:param trimInputs: vehicle trim inputs, as calculated in VehicleTrim code
	:return: stateSpace, linearized model is returned as Linearized.stateSpace class
	"""

	stateSpace = Linearized.stateSpace()

	vAero = VehicleAerodynamicsModel.VehicleAerodynamicsModel()
	vAero.vehicleDynamics.state = trimState

	trimState.Va = math.hypot(trimState.u, trimState.v, trimState.w)
	trimState.alpha = math.atan2(trimState.w, trimState.u)
	if math.isclose(trimState.Va, 0.0):
		trimState.beta = math.copysign(math.pi / 2.0, trimState.u)
	else:
		trimState.beta = math.asin(trimState.v / trimState.Va)
	stateSpace.alpha_trim = trimState.alpha
	stateSpace.beta_trim = trimState.beta
	stateSpace.phi_trim = trimState.roll
	stateSpace.theta_trim = trimState.pitch
	stateSpace.gamma_trim = trimState.pitch - trimState.alpha

	Va = trimState.Va
	alpha = trimState.alpha
	beta = trimState.beta
	phi = trimState.roll
	theta = stateSpace.theta_trim
	p = trimState.p
	q = trimState.q
	r = trimState.r
	u = trimState.u
	v = trimState.v
	w = trimState.w
	dA = trimInputs.Aileron
	dR = trimInputs.Rudder
	# Table 5-2 in Beard
	Yv = 0.25 * VPC.rho * VPC.S * VPC.b * v * (VPC.CYp * p + VPC.CYr * r) / (VPC.mass * Va) + \
			VPC.rho * VPC.S * v * (VPC.CY0 + VPC.CYbeta * beta + VPC.CYdeltaA * dA + VPC.CYdeltaR * dR) / VPC.mass + \
			0.5 * VPC.rho * VPC.S * VPC.CYbeta * math.hypot(u,w) / VPC.mass
	Yp = w + 0.25 * VPC.rho * Va * VPC.S * VPC.b * VPC.CYp / VPC.mass
	Yr = -u + 0.25 * VPC.rho * Va * VPC.S * VPC.b * VPC.CYr / VPC.mass
	YdeltaA = 0.5 * VPC.rho * Va**2 * VPC.S * VPC.CYdeltaA / VPC.mass
	YdeltaR = 0.5 * VPC.rho * Va**2 * VPC.S * VPC.CYdeltaR / VPC.mass
	Lv = 0.25 * VPC.rho * VPC.S * VPC.b**2 * v * (VPC.Cpp * p + VPC.Cpr * r) / Va + \
		 VPC.rho * VPC.S * VPC.b * v * (VPC.Cp0 + VPC.Cpbeta * beta + VPC.CpdeltaA * dA + VPC.CpdeltaR * dR) + \
		0.5 * VPC.rho * VPC.S * VPC.b * VPC.Cpbeta * math.hypot(u,w)
	Lp = VPC.Gamma1 * q + 0.25 * VPC.rho * Va * VPC.S * VPC.b**2 * VPC.Cpp
	Lr = -VPC.Gamma2 * q + 0.25 * VPC.rho * Va * VPC.S * VPC.b**2 * VPC.Cpr
	LdeltaA = 0.5 * VPC.rho * Va**2 * VPC.S * VPC.b * VPC.CpdeltaA
	LdeltaR = 0.5 * VPC.rho * Va**2 * VPC.S * VPC.b * VPC.CpdeltaR
	Nv = 0.25 * VPC.rho * VPC.S * VPC.b**2 * v * (VPC.Crp * p + VPC.Crr * r) / Va + \
		 VPC.rho * VPC.S * VPC.b * v * (VPC.Cr0 + VPC.Crbeta * beta + VPC.CrdeltaA * dA + VPC.CrdeltaR * dR) + \
		0.5 * VPC.rho * VPC.S * VPC.b * VPC.Crbeta * math.hypot(u,w)
	Np = VPC.Gamma7 * q + 0.25 * VPC.rho * Va * VPC.S * VPC.b**2 * VPC.Crp
	Nr = -VPC.Gamma1 * q + 0.25 * VPC.rho * Va * VPC.S * VPC.b**2 * VPC.Crr
	NdeltaA = 0.5 * VPC.rho * Va**2 * VPC.S * VPC.b * VPC.CrdeltaA
	NdeltaR = 0.5 * VPC.rho * Va**2 * VPC.S * VPC.b * VPC.CrdeltaR
	# Equations (5.50) in Beard
	stateSpace.A_lateral[0][0] = Yv
	stateSpace.A_lateral[0][1] = Yp
	stateSpace.A_lateral[0][2] = Yr
	stateSpace.A_lateral[0][3] = VPC.g0 * math.cos(theta) * math.cos(phi)
	stateSpace.A_lateral[0][4] = 0.0

	stateSpace.A_lateral[1][0] = Lv
	stateSpace.A_lateral[1][1] = Lp
	stateSpace.A_lateral[1][2] = Lr
	stateSpace.A_lateral[1][3] = 0.0
	stateSpace.A_lateral[1][4] = 0.0

	stateSpace.A_lateral[2][0] = Nv
	stateSpace.A_lateral[2][1] = Np
	stateSpace.A_lateral[2][2] = Nr
	stateSpace.A_lateral[2][3] = 0.0
	stateSpace.A_lateral[2][4] = 0.0

	stateSpace.A_lateral[3][0] = 0.0
	stateSpace.A_lateral[3][1] = 1.0
	stateSpace.A_lateral[3][2] = math.cos(phi) * math.tan(theta)
	stateSpace.A_lateral[3][0] = q * math.cos(phi) * math.tan(theta) - r * math.sin(phi) * math.tan(theta)
	stateSpace.A_lateral[3][0] = 0.0

	stateSpace.A_lateral[4][0] = 0.0
	stateSpace.A_lateral[4][1] = 0.0
	stateSpace.A_lateral[4][2] = math.cos(phi) / math.cos(theta)
	stateSpace.A_lateral[4][3] = p * math.cos(phi) / math.cos(theta) - r * math.sin(phi) / math.cos(theta)
	stateSpace.A_lateral[4][4] = 0.0

	stateSpace.B_lateral[0][0] = YdeltaA
	stateSpace.B_lateral[0][1] = YdeltaR
	stateSpace.B_lateral[1][0] = LdeltaA
	stateSpace.B_lateral[1][1] = LdeltaR
	stateSpace.B_lateral[2][0] = NdeltaA
	stateSpace.B_lateral[2][1] = NdeltaR
	stateSpace.B_lateral[3][0] = 0.0
	stateSpace.B_lateral[3][1] = 0.0
	stateSpace.B_lateral[4][0] = 0.0
	stateSpace.B_lateral[4][1] = 0.0
	# Table 5-2 in Beard
	Ralpha = [[math.cos(alpha), -math.sin(alpha)], [math.sin(alpha), math.cos(alpha)]]
	dCXZ = MatrixMath.multiply(Ralpha,[[-VPC.CD0, -VPC.CDalpha, -VPC.CDdeltaE, -VPC.CDq], [-VPC.CL0, -VPC.CLalpha, -VPC.CLdeltaE, -VPC.CLq]])
	CX0 = dCXZ[0][0]
	CZ0 = dCXZ[1][0]
	CXalpha = dCXZ[0][1]
	CZalpha = dCXZ[1][1]
	CXdeltaE = dCXZ[0][2]
	CZdeltaE = dCXZ[1][2]
	CXq = dCXZ[0][3]
	CZq = dCXZ[1][3]
	Xu = u * VPC.rho * VPC.S * (CX0 + CXalpha * alpha + CXdeltaE * trimInputs.Elevator) / VPC.mass - \
		 0.5 * VPC.rho * VPC.S * w * CXalpha / VPC.mass + \
		 0.25 * VPC.rho * VPC.S * VPC.c * CXq * u * q / (VPC.mass * Va) + dThrust_du(u, v, w, trimInputs.Throttle) / VPC.mass
	Xw = -q + w * VPC.rho * VPC.S * (CX0 + CXalpha * alpha + CXdeltaE * trimInputs.Elevator) / VPC.mass + \
		 0.5 * VPC.rho * VPC.S * u * CXalpha / VPC.mass + \
		 0.25 * VPC.rho * VPC.S * VPC.c * CXq * w * q / (VPC.mass * Va) + dThrust_dw(u, v, w, trimInputs.Throttle) / VPC.mass
	Xq = -w + 0.25 * VPC.rho * Va * VPC.S * CXq * VPC.c / VPC.mass
	XdeltaE = 0.5 * VPC.rho * Va**2 * VPC.S * CXdeltaE / VPC.mass
	XdeltaT = dThrust_dThrottle(Va, trimInputs.Throttle) / VPC.mass
	Zu = -q + u * VPC.rho * VPC.S * (CZ0 + CZalpha * alpha + CZdeltaE * trimInputs.Elevator) / VPC.mass - \
		 0.5 * VPC.rho * VPC.S * w * CZalpha / VPC.mass + \
		 0.25 * VPC.rho * VPC.S * VPC.c * CZq * u * q / (VPC.mass * Va) + dThrust_dw(u, v, w, trimInputs.Throttle) / VPC.mass
	Zw = w * VPC.rho * VPC.S * (CZ0 + CZalpha * alpha + CZdeltaE * trimInputs.Elevator) / VPC.mass + \
		 0.5 * VPC.rho * VPC.S * u * CZalpha / VPC.mass + \
		 0.25 * VPC.rho * VPC.S * VPC.c * CZq * w * q / (VPC.mass * Va)
	Zq = u + 0.25 * VPC.rho * Va * VPC.S * CZq * VPC.c / VPC.mass
	ZdeltaE = 0.5 * VPC.rho * Va**2 * VPC.S * CZdeltaE / VPC.mass
	Mu = u * VPC.rho * VPC.S * VPC.c * (VPC.CM0 + VPC.CMalpha * alpha + VPC.CMdeltaE * trimInputs.Elevator) / VPC.Jyy -\
		 0.5 * VPC.rho * VPC.S * VPC.c *VPC.CMalpha * w / VPC.Jyy + 0.25 * VPC.rho * VPC.S * VPC.c**2 * VPC.CMq * q * u / (VPC.Jyy * Va)
	Mw = w * VPC.rho * VPC.S * VPC.c * (VPC.CM0 + VPC.CMalpha * alpha + VPC.CMdeltaE * trimInputs.Elevator) / VPC.Jyy + \
		 0.5 * VPC.rho * VPC.S * VPC.c * VPC.CMalpha * u / VPC.Jyy + 0.25 * VPC.rho * VPC.S * VPC.c ** 2 * VPC.CMq * q * w / (VPC.Jyy * Va)
	Mq = 0.25 * VPC.rho * Va * VPC.S * VPC.c ** 2 * VPC.CMq / VPC.Jyy
	MdeltaE = 0.5 * VPC.rho * Va**2 * VPC.S * VPC.c * VPC.CMdeltaE / VPC.Jyy

	stateSpace.A_longitudinal[0][0] = Xu
	stateSpace.A_longitudinal[0][1] = Xw
	stateSpace.A_longitudinal[0][2] = Xq
	stateSpace.A_longitudinal[0][3] = -VPC.g0 * math.cos(theta)
	stateSpace.A_longitudinal[0][4] = 0.0

	stateSpace.A_longitudinal[1][0] = Zu
	stateSpace.A_longitudinal[1][1] = Zw
	stateSpace.A_longitudinal[1][2] = Zq
	stateSpace.A_longitudinal[1][3] = -VPC.g0 * math.sin(theta)
	stateSpace.A_longitudinal[1][4] = 0.0

	stateSpace.A_longitudinal[2][0] = Mu
	stateSpace.A_longitudinal[2][1] = Mw
	stateSpace.A_longitudinal[2][2] = Mq
	stateSpace.A_longitudinal[2][3] = 0.0
	stateSpace.A_longitudinal[2][4] = 0.0

	stateSpace.A_longitudinal[3][0] = 0.0
	stateSpace.A_longitudinal[3][1] = 0.0
	stateSpace.A_longitudinal[3][2] = 1.0
	stateSpace.A_longitudinal[3][3] = 0.0
	stateSpace.A_longitudinal[3][4] = 0.0

	stateSpace.A_longitudinal[4][0] = math.sin(theta)
	stateSpace.A_longitudinal[4][1] = -math.cos(theta)
	stateSpace.A_longitudinal[4][2] = 0.0
	stateSpace.A_longitudinal[4][3] = u * math.cos(theta) + w * math.sin(theta)
	stateSpace.A_longitudinal[4][4] = 0.0

	stateSpace.B_longitudinal[0][0] = XdeltaE
	stateSpace.B_longitudinal[0][1] = XdeltaT
	stateSpace.B_longitudinal[1][0] = ZdeltaE
	stateSpace.B_longitudinal[1][1] = 0.0
	stateSpace.B_longitudinal[2][0] = MdeltaE
	stateSpace.B_longitudinal[2][1] = 0.0
	stateSpace.B_longitudinal[3][0] = 0.0
	stateSpace.B_longitudinal[3][1] = 0.0
	stateSpace.B_longitudinal[4][0] = 0.0
	stateSpace.B_longitudinal[4][1] = 0.0
	return stateSpace


def dThrust_dVa(Va, Throttle, epsilon=0.5):
	"""
	def dThrust_dVa(Va, Throttle, epsilon=0.5):
	Function to calculate the numerical partial derivative of propeller thrust to change in airspeed using the actual
	prop function from complex propeller model (inside the VehicleAerodynamicsModel class)

	:param Va: vehicle trim airspeed [m/s]
	:param Throttle: trim throttle setting [0-1]
	:param epsilon: step to take for perturbation in Velocity
	:return: dTdVa: partial derivative [N-s/m]
	"""
	v = VehicleAerodynamicsModel.VehicleAerodynamicsModel()
	Fx, Mx = v.CalculatePropForces(Va, Throttle)
	Fxplus, Mxplus = v.CalculatePropForces(Va + epsilon, Throttle)
	dTdVa = (Fxplus - Fx) / epsilon
	return dTdVa

def dThrust_dThrottle(Va, Throttle, epsilon=0.01):
	"""
	def dThrust_dThrottle(Va, Throttle, epsilon=0.01):
	Function to calculate the numerical partial derivative of propeller thrust to change in throttle setting using the actual
	prop function from complex propeller model (inside the VehicleAerodynamicsModel class)

	:param Va: vehicle trim airspeed [m/s]
	:param Throttle: trim throttle setting [0-1]
	:param epsilon: step to take for perturbation in throttle setting
	:return: dTdDeltaT: partial derivative [N/PWM]
	"""
	v = VehicleAerodynamicsModel.VehicleAerodynamicsModel()
	Fx, Mx = v.CalculatePropForces(Va, Throttle)
	Fxplus, Mxplus = v.CalculatePropForces(Va, Throttle + epsilon)
	dTdDeltaT = (Fxplus - Fx) / epsilon
	return dTdDeltaT

def dThrust_du(u, v, w, Throttle, epsilon=0.5):
	"""
	def dThrust_du(u, v, w, Throttle, epsilon=0.5):
	Function to calculate the numerical partial derivative of propeller thrust to change in body x-velocity using
	the actual prop function from complex propeller model (inside the VehicleAerodynamicsModel class). Needs all the body
	referenced velocities to compute the change	in airspeed due to the change in x-velocity in trim.

	:param u: trim body x-velocity [m/s]
	:param v: trim body y-velocity [m/s]
	:param w: trim body z-velocity [m/s]
	:param Throttle: trim throttle setting [0-1]
	:param epsilon: step to take for perturbation in velocity, u
	:return: dTdDeltaU: partial derivative [N-s/m]
	"""
	vAero = VehicleAerodynamicsModel.VehicleAerodynamicsModel()
	Va = math.hypot(u, v, w)
	Fx, Mx = vAero.CalculatePropForces(Va, Throttle)
	Va = math.hypot(u + epsilon, v, w)
	Fxplus, Mxplus = vAero.CalculatePropForces(Va, Throttle)
	dTdDeltaU = (Fxplus - Fx) / epsilon
	return dTdDeltaU


def dThrust_dw(u, v, w, Throttle, epsilon=0.5):
	"""
	def dThrust_dw(u, v, w, Throttle, epsilon=0.5):
	Function to calculate the numerical partial derivative of propeller thrust to change in body x-velocity using
	the actual prop function from complex propeller model (inside the VehicleAerodynamicsModel class). Needs all the body
	referenced velocities to compute the change	in airspeed due to the change in x-velocity in trim.

	:param u: trim body x-velocity [m/s]
	:param v: trim body y-velocity [m/s]
	:param w: trim body z-velocity [m/s]
	:param Throttle: trim throttle setting [0-1]
	:param epsilon: step to take for perturbation in velocity, w
	:return: dTdDeltaW: partial derivative [N-s/m]
	"""
	vAero = VehicleAerodynamicsModel.VehicleAerodynamicsModel()
	Va = math.hypot(u, v, w)
	Fx, Mx = vAero.CalculatePropForces(Va, Throttle)
	Va = math.hypot(u, v, w + epsilon)
	Fxplus, Mxplus = vAero.CalculatePropForces(Va, Throttle)
	dTdDeltaW = (Fxplus - Fx) / epsilon
	return dTdDeltaW