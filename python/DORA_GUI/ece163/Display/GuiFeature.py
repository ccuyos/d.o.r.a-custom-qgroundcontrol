'''
* File: GuiFeature.py
* Author: Carl Vincent Cuyos
* Brief: Library for adding GUI features to ECE163 simulator
* Created on June 2, 2023
'''

import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets
import math
import traceback
import os
import datetime
from ..Constants import GUIConstants as pyGUI

def createOuterLayout():
    return QtWidgets.QHBoxLayout()
def setOuterLayout(widgetPtr, outerLayout):
    widgetPtr.setLayout(outerLayout)
def createInnerLayout():
    return QtWidgets.QVBoxLayout()
def addInnerLayout(outerLayout, innerLayout, spacing):
    outerLayout.addLayout(innerLayout)
    outerLayout.addItem(spacing)
def createLabel(layout, label):
    layout.addWidget(QtWidgets.QLabel(label))


def createConsole(consoleLabel, consoleText, innerLayout):
    console = QtWidgets.QLabel(consoleLabel)
    innerLayout.addWidget(console)
    text = QtWidgets.QPlainTextEdit(consoleText)
    text.setReadOnly(True)
    innerLayout.addWidget(text)
    return text
def createSection(name, paramDict, formLayout):
    pass
def createForm(innerLayout):
    pass
def createBoxColumn(outerLayout):
    pass
def createBoxRow(outerLayout):
    pass
def createTextInput(text, value, formLayout, textDict):
    pass
def createButton(name, function, innerLayout):
    button = QtWidgets.QPushButton(name)
    button.clicked.connect(function)
    innerLayout.addWidget(button)
    return button
def createSpacing(size = pyGUI.customSpace, length = 0, width = 0):
    match size:
        case pyGUI.smallSpace:
            length, width = pyGUI.smallSize, pyGUI.smallSize
        case pyGUI.mediumSpace:
            length, width = pyGUI.mediumSize, pyGUI.mediumSize
        case pyGUI.largeSpace:
            length, width = pyGUI.largeSize, pyGUI.largeSize
        case pyGUI.extremeSpace:
            length, width = pyGUI.extremeSize, pyGUI.extremeSize
        case pyGUI.customSpace:
            length, width = length, width
    spaceSize = QtWidgets.QSpacerItem(length, width, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
    return spaceSize
def createStatus(innerLayout, msg = "Status: NaN"):
    statusText = QtWidgets.QLabel(msg)
    innerLayout.addWidget(statusText)
    return statusText
def generateTimeStamp():
    return datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")


