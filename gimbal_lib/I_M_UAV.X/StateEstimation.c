/* 
 * File:   StateEstimation.c
 * Author: Margaret Silva
 * Brief: Contains all functions needed to estimate the current state of the 
 *      aircraft
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

#include "StateEstimation.h"
#include <math.h>
#include "AirframeConstants.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/*******************************************************************************
 * PRIVATE TYPEDEFS                                                            *
 ******************************************************************************/
struct estimation_biases {
    float b_Va; // airspeed bias 
    float Va_gps; // airspeed estimated by gps
    float Va_acc; // accumulated estimated airspeed from accelerometers
    float b_chi; // course bias
    float b_h; // altitude bias
    float b_p; // p bias
    float b_q; // q bias
    float b_r; // r bias
    float b_pn; // north position bias
    float b_pe; // east position bias
};

struct filter_gains {
    float Kp_Va; // Kp airspeed
    float Ki_Va; // Ki airspeed
    float Kp_chi; // Kp chi
    float Ki_chi; // Ki chi
    float Kp_acc; // Kp attitude from accels
    float Ki_acc; // Ki attitude from accels
    float Kp_mag; // Kp attitude from mag
    float Ki_mag; // Ki attitude from mag
    float Kp_pos; // Kp position
    float Ki_pos; // Ki position
};

struct stored_sensors {
    float gps_sog; // previous gps reading - speed over ground
    float gps_cog; // previous gps reading - course over ground
    float R_hat[3][3]; // previous estimated DCM matrix 
};

/*******************************************************************************
 * PRIVATE #DEFINES                                                            *
 ******************************************************************************/
#define DEFAULT_BIAS_STATE {.b_Va = 0,.Va_gps = 0,.Va_acc = 0,.b_chi = 0,.b_h = 0,\
    .b_p = 0, .b_q = 0, .b_r = 0, .b_pn = 0, .b_pe = 0}

#define ALT_MOVING_AVG_LEN 50
#define ALT_MOVING_AVG_LEN_F 50.0
#define GPS_MOVING_AVG_LEN 50
#define GPS_MOVING_AVG_LEN_F 50.0

// toggle flags
#define USE_SCALED_ALTITUDE_INPUTS

// macros
// formula source: https://keisan.casio.com/exec/system/1224585971
#define PRESSURE_TO_ALITITUDE(p, p0, T) (153.846 * (pow((p0/p), 0.190223) - 1) * (T + 273.15))

/*******************************************************************************
 * STATIC VARIABLES                                                            *
 ******************************************************************************/
static struct vehicle_state current_state = {
    .pn = 0.0,
    .pe = 0.0,
    .pd = 0.0,
    .u = 0.0,
    .v = 0.0,
    .w = 0.0,
    .yaw = 0.0,
    .pitch = 0.0,
    .roll = 0.0,
    .p = 0.0,
    .q = 0.0,
    .r = 0.0,
    .chi = 0.0,
    .alpha = 0.0,
    .beta = 0.0,
    .Va = 0.0,
    .R =
    {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    }
};

static struct vehicle_state true_state = {
    .pn = 0.0,
    .pe = 0.0,
    .pd = 0.0,
    .u = 0.0,
    .v = 0.0,
    .w = 0.0,
    .yaw = 0.0,
    .pitch = 0.0,
    .roll = 0.0,
    .p = 0.0,
    .q = 0.0,
    .r = 0.0,
    .chi = 0.0,
    .alpha = 0.0,
    .beta = 0.0,
    .Va = 0.0,
    .R =
    {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    }
};

static struct vehicle_state debug_state = {
    .pn = 0.0,
    .pe = 0.0,
    .pd = 0.0,
    .u = 0.0,
    .v = 0.0,
    .w = 0.0,
    .yaw = 0.0,
    .pitch = 0.0,
    .roll = 0.0,
    .p = 0.0,
    .q = 0.0,
    .r = 0.0,
    .chi = 0.0,
    .alpha = 0.0,
    .beta = 0.0,
    .Va = 0.0,
    .R =
    {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    }
};

static struct estimation_biases current_biases = {
    .b_Va = 0.0,
    .Va_gps = 0.0,
    .Va_acc = 0.0,
    .b_chi = 0.0,
    .b_h = 0.0,
    .b_p = 0.0,
    .b_q = 0.0,
    .b_r = 0.0,
    .b_pn = 0.0,
    .b_pe = 0.0
};

static struct filter_gains gains = {
    .Kp_Va = 0.11,
    .Ki_Va = 0.11,
    .Kp_chi = 0.5,
    .Ki_chi = 0.05,
    .Kp_acc = 5.0,
    .Ki_acc = 0.5,
    .Kp_mag = 15.0,
    .Ki_mag = 1.0,
    .Kp_pos = 0.8,
    .Ki_pos = 0.08
};

static struct stored_sensors prev_sensors = {
    .gps_sog = 0.0,
    .gps_cog = 0.0,
    .R_hat =
    {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    }
};

// moving average variables
// altitude from barometer
static float altitude_data[ALT_MOVING_AVG_LEN] = {0};
static int baro_pos = 0;
static double alt_sum = 0.0;
// airspeed from gps
static float gps_data[GPS_MOVING_AVG_LEN] = {0};
static int gps_pos = 0;
static double gps_sum = 0.0;

/*******************************************************************************
 * PRIVATE FUNCTION PROTOTYPES                                                 *
 ******************************************************************************/
/**
 * @Function attitudeEstimation()
 * @param IMU_data - data from the imu, including acc, gyro, and mag
 * @param dT - time between measurements, used for integration
 * @return none
 * @brief  using the current sensor vals, estimates current attitude
 * @author Margaret Silva */
void attitudeEstimation(struct IMU_out IMU_data, float dT);

/**
 * @Function altitudeEstimation()
 * @param Barometer data - data from baro [Pa]
 * @param dT - time between measurements, used for integration
 * @return none
 * @brief  using sensor data, estimates altitude
 * @author Margaret Silva */
void altitudeEstimation(float baro_pressure, float baro_temp, float dT);

/**
 * @Function courseEstimation()
 * @param GPS_data - data from GPS
 * @param dT - time between measurements, used for integration
 * @return none
 * @brief  estimates course using gps and estimates roll, yaw, and Va
 * @author Margaret Silva */
void courseEstimation(struct GPS_data GPS_data, float dT, uint8_t data_status_flag);

/**
 * @Function positionEstimation()
 * @param GPS_data - data from GPS
 * @param dT - time between measurements, used for integration
 * @return none
 * @brief estimates north and east position of UAV using GPS and estimated course
 * @author Margaret Silva */
void positionEstimation(struct GPS_data GPS_data, float dT, uint8_t data_status_flag);

/**
 * @Function airspeedEstimation()
 * @param IMU_data - data from IMU
 *        GPS_data - data from GPS
 *        dT - time between measurements, used for integration
 * @return none
 * @brief  estimates airpseed using imu, gps, and estimated DCM matrix
 * @author Margaret Silva */
void airspeedEstimation(struct IMU_out IMU_data, struct GPS_data GPS_data, float dT);

/**
 * @Function R_exp()
 * @param w[3] - vector of rotation rates
 *        dT - integration time constants
 *        exp_mat - exponential matrix
 * @return none
 * @brief  helper function which calculates the matrix exponential exp(-[wx]dT)
 * @author Margaret Silva */
void R_exp(float w[3], float dT, float exp_mat[3][3]);

/**
 * @Function dcm2Euler()
 * @param float dcm[3][3] - dcm matrix 
 * @return none
 * @brief  helper function which extracts euler angles from dcm matrix and 
 *         stores them in the current state struct
 * @author Margaret Silva */
void dcm2Euler(float dcm[3][3]);

// source https://gist.github.com/bmccormack/d12f4bf0c96423d03f82

static float movingAvg(float *ptrArrNumbers, double *ptrSum, int pos, float len, float nextNum) {
    //Subtract the oldest number from the prev sum, add the new number
    *ptrSum = *ptrSum - ptrArrNumbers[pos] + nextNum;
    //Assign the nextNum to the position in the array
    ptrArrNumbers[pos] = nextNum;
    //return the average
    return *ptrSum / len;
}

/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATIONS                                             *
 ******************************************************************************/

/**
 * @Function initStateEstimation()
 * @param none
 * @return none
 * @brief  initializes state estimation
 * @author Margaret Silva */
void initStateEstimation(void) {
    // define variables that are used to help reset arrays
    int i;
    int j;
    float I[3][3] = {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    };
    // zero out current state and biases
    current_state = (struct vehicle_state) DEFAULT_VEHICLE_STATE;
    current_biases = (struct estimation_biases) DEFAULT_BIAS_STATE;
    // zero out previous sensors
    prev_sensors.gps_cog = 0.0;
    prev_sensors.gps_sog = 0.0;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            prev_sensors.R_hat[i][j] = I[i][j];
        }
    }
    // reset moving average filter
    for (i = 0; i < ALT_MOVING_AVG_LEN; i++) {
        altitude_data[i] = 0.0;
    }
    baro_pos = 0;
    alt_sum = 0.0;
    for (i = 0; i < GPS_MOVING_AVG_LEN; i++) {
        gps_data[i] = 0.0;
    }
    gps_pos = 0;
    gps_sum = 0.0;
}

/**
 * @Function setStateEstimationGains()
 * @param state_est_gains - array of state estimation filter gains
 *     array order: Kp_Va, Ki_Va, Kp_chi, Ki_chi, Kp_acc, Ki_acc, Kp_mag, Ki_mag, Kp_pos, Ki_pos
 *     note: the array has extra blank space to keep it the same length as the 
 *           mavlink packet array length
 * @return none
 * @brief  sets gains for state estimation algorithms
 * @author Margaret Silva */
void setStateEstimationGains(float state_est_gains[16]){
    gains.Kp_Va = state_est_gains[0];
    gains.Ki_Va = state_est_gains[1];
    gains.Kp_chi = state_est_gains[2];
    gains.Ki_chi = state_est_gains[3];
    gains.Kp_acc = state_est_gains[4];
    gains.Ki_acc = state_est_gains[5];
    gains.Kp_mag = state_est_gains[6];
    gains.Ki_mag = state_est_gains[7];
    gains.Kp_pos = state_est_gains[8];
    gains.Ki_pos = state_est_gains[9];
}

/**
 * @Function updateStateEstimation()
 * @param IMU output, GPS output, Barometer output
 * @param sensor_flag - indicates which sensors have updated
 * @return vehicle_state - the estimated vehicle state
 * @brief  updates state estimation given the new sensor input
 * @author Margaret Silva */
struct vehicle_state updateStateEstimation(struct IMU_out IMU_data, struct GPS_data GPS_data,
        float baro_pressure, float baro_temp, uint8_t sensor_flag) {
    if (sensor_flag & IMU_AVAILABLE) {
        attitudeEstimation(IMU_data, IMU_DT);
//        airspeedEstimation(IMU_data, GPS_data, GPS_DT);
    }
    if (sensor_flag & BARO_AVAILABLE) {
        altitudeEstimation(baro_pressure, baro_temp, BARO_DT);
    }
    courseEstimation(GPS_data, GPS_DT, sensor_flag);
    positionEstimation(GPS_data, GPS_DT, sensor_flag);
    return current_state;
}

/**
 * @Function getCurrentState()
 * @param none
 * @return current estimated vehicle state
 * @brief  returns the current state estimation
 * @author Margaret Silva */
struct vehicle_state getCurrentState(void) {
    return current_state;
}

/**
 * @Function getTrueState()
 * @param none
 * @return current true vehicle state
 * @brief  returns the true vehicle state
 * @author Margaret Silva */
struct vehicle_state getTrueState(void) {
    return true_state;
}

/**
 * @Function getDebugState()
 * @param none
 * @return current vehicle state that's a mix of true and estimated states
 * @brief  returns the current state estimation
 * @author Margaret Silva */
struct vehicle_state getDebugState(void) {
    debug_state.pd = current_state.pd;
    debug_state.yaw = current_state.yaw;
    debug_state.pitch = current_state.pitch;
    debug_state.roll = current_state.roll;
    debug_state.p = current_state.p;
    debug_state.q = current_state.q;
    debug_state.r = current_state.r;
    debug_state.chi = current_state.chi;
    debug_state.Va = current_state.Va;

    return debug_state;
}

/**
 * @Function setCurrentState()
 * @param state_vals - an array containing the 16 state values in the same order as the struct
 * @return none
 * @brief  sets the stored vehicle state to the values passed in as the array arg
 * @author Margaret Silva */
void setCurrentState(float state_vals[16]) {
    current_state.pn = state_vals[0];
    current_state.pe = state_vals[1];
    current_state.pd = state_vals[2];
    current_state.u = state_vals[3];
    current_state.v = state_vals[4];
    current_state.w = state_vals[5];
    current_state.yaw = state_vals[6];
    current_state.pitch = state_vals[7];
    current_state.roll = state_vals[8];
    current_state.p = state_vals[9];
    current_state.q = state_vals[10];
    current_state.r = state_vals[11];
    current_state.chi = state_vals[12];
    current_state.alpha = state_vals[13];
    current_state.beta = state_vals[14];
    current_state.Va = state_vals[15];
}

/**
 * @Function setTrueState()
 * @param state_vals - an array containing the 16 state values in the same order as the struct
 * @return none
 * @brief  sets the stored true vehicle state to the values passed in as the array arg
 * @author Margaret Silva */
void setTrueState(float state_vals[16]) {
    true_state.pn = state_vals[0];
    true_state.pe = state_vals[1];
    true_state.pd = state_vals[2];
    true_state.u = state_vals[3];
    true_state.v = state_vals[4];
    true_state.w = state_vals[5];
    true_state.yaw = state_vals[6];
    true_state.pitch = state_vals[7];
    true_state.roll = state_vals[8];
    true_state.p = state_vals[9];
    true_state.q = state_vals[10];
    true_state.r = state_vals[11];
    true_state.chi = state_vals[12];
    true_state.alpha = state_vals[13];
    true_state.beta = state_vals[14];
    true_state.Va = state_vals[15];
}

/**
 * @Function getCurrentStateArray(state_vals[16)
 * @param current estimated vehicle state as an array of the 16 state values
 * @return none
 * @brief  returns the current state estimation in a way that can be sent to the sim
 * @note: only returns state information that's actually estimated. The rest of the 
 *      packet is intended to store actuator command information
 * @author Margaret Silva */
void getCurrentStateArray(float state_vals[16]) {
    state_vals[0] = 0.0;
    state_vals[1] = 0.0;
    state_vals[2] = 0.0;
    state_vals[3] = 0.0;
    state_vals[4] = current_state.pd;
    state_vals[5] = current_state.yaw;
    state_vals[6] = current_state.pitch;
    state_vals[7] = current_state.roll;
    state_vals[8] = current_state.p;
    state_vals[9] = current_state.q;
    state_vals[10] = current_state.r;
    state_vals[11] = current_state.chi;
    state_vals[12] = current_state.Va;
    state_vals[13] = 0.0;
    state_vals[14] = 0.0;
    state_vals[15] = 0.0;
}

/**
 * @Function getDebugStateArray(state_vals[16)
 * @param current debu vehicle state as an array of the 16 state values
 * @return none
 * @brief  returns the debug state estimation in a way that can be sent to the sim
 * @note: only returns state information that's actually estimated. The rest of the 
 *      packet is intended to store actuator command information
 * @author Margaret Silva */
void getDebugStateArray(float state_vals[16]) {
    state_vals[0] = 0.0;
    state_vals[1] = 0.0;
    state_vals[2] = 0.0;
    state_vals[3] = 0.0;
    state_vals[4] = debug_state.pd;
    state_vals[5] = debug_state.yaw;
    state_vals[6] = debug_state.pitch;
    state_vals[7] = debug_state.roll;
    state_vals[8] = debug_state.p;
    state_vals[9] = debug_state.q;
    state_vals[10] = debug_state.r;
    state_vals[11] = debug_state.chi;
    state_vals[12] = debug_state.Va;
    state_vals[13] = 0.0;
    state_vals[14] = 0.0;
    state_vals[15] = 0.0;
}

/*******************************************************************************
 * PRIVATE FUNCTION IMPLEMENTATIONS                                            *
 ******************************************************************************/

/**
 * @Function attitudeEstimation()
 * @param IMU_data - data from the imu, including acc, gyro, and mag
 * @param dT - time between measurements, used for integration
 * @return none
 * @brief  using the current sensor vals, estimates current attitude
 * @author Margaret Silva */
void attitudeEstimation(struct IMU_out IMU_data, float dT) {
    // init all vars used
    int i;
    float b_hat[3] = {current_biases.b_p, current_biases.b_q, current_biases.b_r};
    // outputs
    float b_plus[3]; // updated biases for gyros
    float gyros_biased[3]; // biased gyro values, used for euler angle estimation
    float R_hat_plus[3][3]; // new dcm matrix
    // intermediate values
    float norm = 0.0; // magnitude of a vector
    float temp[3];
    float temp_mat[3][3];
    float a[3]; // gyro feedback term
    float b[3]; // gyro feedback term
    float c[3]; // bias term
    float d[3]; // bias term
    float gyro_feedback[3]; // feedback term using biased gyro values used to find Rplus
    float b_dot[3]; // change in bias
    float acc_mag; // magnitude of accelerometer 
    // errors from accels and mags
    float w_meas_a[3];
    float w_meas_m[3];
    // sensor data vectors
    float gyros[3] = {IMU_data.gyro.x, IMU_data.gyro.y, IMU_data.gyro.z};
    float acc_b[3] = {IMU_data.acc.x, IMU_data.acc.y, IMU_data.acc.z};
    float mag_b[3] = {IMU_data.mag.x, IMU_data.mag.y, IMU_data.mag.z};
    // intertial frame sensor readings
    float acc_i[3] = {0, 0, -g0};
    float mag_i[3] = MAGFIELD_VEC; // get this for specific area
    // identity matrix
    float I[3][3] = {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    };

    // normalize the following vectors
    norm = lin_alg_v_norm(acc_b); // body accelerometer
    acc_mag = norm;
    if (fabs(norm) > 0.0001) {
        for (i = 0; i < 3; i++) {
            acc_b[i] = acc_b[i] / norm;
        }
    }
    norm = lin_alg_v_norm(mag_b); // body magnetometer
    if (fabs(norm) > 0.0001) {
        for (i = 0; i < 3; i++) {
            mag_b[i] = mag_b[i] / norm;
        }
    }
    norm = lin_alg_v_norm(acc_i); // inertial accelerometer
    if (fabs(norm) > 0.0001) {
        for (i = 0; i < 3; i++) {
            acc_i[i] = acc_i[i] / norm;
        }
    }
    norm = lin_alg_v_norm(mag_i); // inertial magnetometer
    if (fabs(norm) > 0.0001) {
        for (i = 0; i < 3; i++) {
            mag_i[i] = mag_i[i] / norm;
        }
    }

    // bias gyros
    lin_alg_v_v_sub(gyros, b_hat, gyros_biased);

    // make estimate using accels and magnetometer
    lin_alg_m_v_mult(current_state.R, acc_i, temp); // transform from interial->body
    lin_alg_cross(acc_b, temp, w_meas_a); // get cross product to find error
    lin_alg_m_v_mult(current_state.R, mag_i, temp);
    lin_alg_cross(mag_b, temp, w_meas_m);

    // gyro feedback terms
    lin_alg_s_v_mult(gains.Kp_acc, w_meas_a, a); // from accelerometer
    lin_alg_s_v_mult(gains.Kp_mag, w_meas_m, b); // from magnetometer

    // bias terms
    lin_alg_s_v_mult(-gains.Ki_acc, w_meas_a, c); // from accelerometer
    lin_alg_s_v_mult(-gains.Ki_mag, w_meas_m, d); // from magnetometer

    // ensure that accelerometer readings aren't thrown off by high acceleration
    if ((acc_mag < (1.1 * g0)) && (acc_mag > (0.9 * g0))) {
        lin_alg_v_v_add(a, b, temp);
        lin_alg_v_v_add(gyros_biased, temp, gyro_feedback); // gyro_feedback = gyros_biased + a + b
        lin_alg_v_v_add(c, d, b_dot); // b_dot = c + d
    } else {
        lin_alg_v_v_add(gyros_biased, b, gyro_feedback); // gyro_feedback = gyros_biased + b
        lin_alg_set_v(d[0], d[1], d[2], b_dot); // b_dot = d
    }

    // calculate new values for R 
    R_exp(gyro_feedback, dT, temp_mat);
    lin_alg_m_m_mult(temp_mat, current_state.R, R_hat_plus); // R_plus = Rexp(gyro_feedback) * R_hat
    // calculate new values for bias
    lin_alg_s_v_mult(dT, b_dot, temp);
    lin_alg_v_v_add(b_hat, temp, b_plus); // b_plus = b_hat + dT*b_dot

    // add outputs to current state and biases
    current_state.p = gyros_biased[0];
    current_state.q = gyros_biased[1];
    current_state.r = gyros_biased[2];
    current_biases.b_p = b_plus[0];
    current_biases.b_q = b_plus[1];
    current_biases.b_r = b_plus[2];
    lin_alg_m_m_mult(I, R_hat_plus, current_state.R);
    // record euler angles from dcm
    dcm2Euler(current_state.R);
}

/**
 * @Function altitudeEstimation()
 * @param Barometer data - data from baro [Pa]
 * @param dT - time between measurements, used for integration
 * @return none
 * @brief  using sensor data, estimates altitude
 * @author Margaret Silva */
void altitudeEstimation(float baro_pressure, float baro_temp, float dT) {
#ifdef USE_SCALED_ALTITUDE_INPUTS
    float new_alt_est = -baro_pressure; // baro pressure is already scaled
#else 
    float new_alt_est;
    new_alt_est = 100.0*((baro_pressure - GROUND_PRESSURE) / (rho * g0));
    // prevent divide by zero error
    //    if (fabs(baro_pressure) > 0.0001) {
    //        new_alt_est = PRESSURE_TO_ALITITUDE(baro_pressure, GROUND_PRESSURE, baro_temp);
    //    } else{
    //        new_alt_est = 0.0;
    //    }
#endif
    float altitude;

    // take moving average of data
    altitude = movingAvg(altitude_data, &alt_sum, baro_pos, ALT_MOVING_AVG_LEN_F, new_alt_est);
    baro_pos++;
    if (baro_pos >= ALT_MOVING_AVG_LEN) {
        baro_pos = 0;
    }
    current_state.pd = -altitude; 
}

/**
 * @Function courseEstimation()
 * @param GPS_data - data from GPS
 * @param dT - time between measurements, used for integration
 * @return none
 * @brief  estimates course using gps and estimates roll, yaw, and Va
 * @author Margaret Silva */
void courseEstimation(struct GPS_data GPS_data, float dT, uint8_t data_status_flag) {
    float chi_dot = 0; // incremental change in course
    float chi_hat_gps = GPS_data.cog;

    // estimate chi dot from other estimated quantities
    if (fabs(GPS_data.spd) < 0.0001) { // prevent divide by 0 errors
        chi_dot = 0;
    } else {
        chi_dot = (g0 / GPS_data.spd) * tan(current_state.roll) * cos(current_state.chi - current_state.yaw);
    }

    // ignore gps estimates before gps had updated for the first time
    if ((data_status_flag & GPS_FIRST_DATA_RECEIVED) == 0) {
        chi_dot = 0;
        chi_hat_gps = 0;
        current_state.chi = 0;
    }

    // check if gps has updated
    if (data_status_flag & GPS_AVAILABLE) {
        current_biases.b_chi += dT * (chi_hat_gps - current_state.chi);
        current_state.chi = chi_hat_gps;
    }

    // add chi dot to estimate
    chi_dot += gains.Ki_chi * current_biases.b_chi + gains.Kp_chi * (chi_hat_gps - current_state.chi);
    current_state.chi += dT*chi_dot;

    // limit course in between [-2pi, 2pi]
    while (current_state.chi > 2.0 * M_PI) {
        current_state.chi -= 2.0 * M_PI;
    }
    while (current_state.chi < -2.0 * M_PI) {
        current_state.chi += 2.0 * M_PI;
    }
}

/**
 * @Function airspeedEstimation()
 * @param IMU_data - data from IMU
 *        GPS_data - data from GPS
 *        dT - time between measurements, used for integration
 * @return none
 * @brief  estimates airpseed using imu, gps, and estimated DCM matrix
 * @author Margaret Silva */
void airspeedEstimation(struct IMU_out IMU_data, struct GPS_data GPS_data, float dT) {
    int i;
    int j;
    float s_n = GPS_data.spd * cos(GPS_data.cog) - prev_sensors.gps_sog * cos(prev_sensors.gps_cog);
    float s_e = GPS_data.spd * sin(GPS_data.cog) - prev_sensors.gps_sog * sin(prev_sensors.gps_cog);
    float num_mag = sqrt(pow(s_n, 2) + pow(s_e, 2)); // magitude of numerator
    float den_mag;
    float vec1[3] = {1, 0, 0};
    float vec1_out[3];
    float vec2[3] = {1, 0, 0};
    float vec2_out[3];
    float R_T1[3][3]; // transposed matrix
    float R_T2[3][3];

    // estimate airspeed using gps readings
    lin_alg_m_transpose(current_state.R, R_T2); // get body -> inertial matrix
    lin_alg_m_transpose(prev_sensors.R_hat, R_T1);
    lin_alg_m_v_mult(R_T2, vec2, vec2_out);
    lin_alg_m_v_mult(R_T1, vec1, vec1_out);
    lin_alg_v_v_sub(vec2_out, vec1_out, vec1); // v = R2^T * v2 - R1^T * v1
    den_mag = lin_alg_v_norm(vec1); // use magnitude of vector as denominator

    // if the magnitudes are large enough, update the airspeed estimate from gps
    if ((fabs(den_mag) > 0.0001) & (fabs(num_mag) > 0.0001)) {
        current_biases.Va_gps = num_mag / den_mag;
        // record previous sensors
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                prev_sensors.R_hat[i][j] = current_state.R[i][j];
            }
        }
        prev_sensors.gps_cog = GPS_data.cog;
        prev_sensors.gps_sog = GPS_data.spd;

        // moving average filter on gps estimated airspeed
        current_biases.Va_gps = movingAvg(gps_data, &gps_sum, gps_pos, GPS_MOVING_AVG_LEN_F, current_biases.Va_gps);
        gps_pos++;
        if (gps_pos >= GPS_MOVING_AVG_LEN) {
            gps_pos = 0;
        }
        // calculate bias term from gps
        current_biases.b_Va += dT * (-gains.Ki_Va * (current_biases.Va_gps - current_state.Va));
    }

    // accumulate Va estimation from acclerometers
    current_biases.Va_acc += dT * (IMU_data.acc.x - g0 * sin(current_state.pitch));

    // complementary filter (kinda) with accelerometer (high pass) and gps (low pass)
    current_state.Va = current_biases.Va_acc - current_biases.b_Va + gains.Kp_Va * (current_biases.Va_gps - current_state.Va);
}

/**
 * @Function positionEstimation()
 * @param GPS_data - data from GPS
 * @param dT - time between measurements, used for integration
 * @return none
 * @brief estimates north and east position of UAV using GPS and estimated course
 * @author Margaret Silva */
void positionEstimation(struct GPS_data GPS_data, float dT, uint8_t data_status_flag){
    // define variables
    float pn_gps = GPS_data.lat; // convert between degrees and meters
    float pe_gps = GPS_data.lon; // convert between degrees and meters
    float pn_dot = GPS_data.spd*cos(current_state.chi); // est change in pn
    float pe_dot = GPS_data.spd*sin(current_state.chi); // est change in pe
    
    // ignore gps estimates before gps had updated for the first time
    if ((data_status_flag & GPS_FIRST_DATA_RECEIVED) == 0) {
        pn_dot = 0;
        pe_dot = 0;
        current_state.pn = INITIAL_PN;
        current_state.chi = INITIAL_PE;
    }
    
    // check if gps has updated
    if (data_status_flag & GPS_AVAILABLE) {
        // new estimate for pn
        current_biases.b_pn += dT*(pn_gps - current_state.pn);
        current_state.pn = pn_gps;
        // new estimate for pe
        current_biases.b_pe += dT*(pe_gps - current_state.pe);
        current_state.pe = pe_gps;
    }
    
    // add compensation from gps
    pn_dot += gains.Ki_pos*current_biases.b_pn + gains.Kp_pos*(pn_gps - current_state.pn);
    current_state.pn += dT*pn_dot;
    
    pe_dot += gains.Ki_pos*current_biases.b_pe + gains.Kp_pos*(pe_gps - current_state.pe);
    current_state.pe += dT*pe_dot;
}

/**
 * @Function R_exp()
 * @param w[3] - vector of rotation rates
 *        dT - integration time constants
 *        exp_mat - exponential matrix
 * @return none
 * @brief  helper function which calculates the matrix exponential exp(-[wx]dT)
 * @author Margaret Silva */
void R_exp(float w[3], float dT, float exp_mat[3][3]) {
    // define variables
    float wx[3][3] = {
        {0.0, -w[2], w[1]},
        {w[2], 0.0, -w[0]},
        {-w[1], w[0], 0.0}
    };
    float I[3][3] = {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    };
    float w_mag = lin_alg_v_norm(w);
    float matA[3][3];
    float matB[3][3];
    float temp[3][3];
    float a;
    float b;

    // calculate coefficients, use an approximation if m_mag is small
    if (w_mag < 0.2) {
        a = dT - ((pow(dT, 3) * pow(w_mag, 2)) / 6.0) + ((pow(dT, 5) * pow(w_mag, 4)) / 120.0);
        b = (pow(dT, 2) / 2.0) - ((pow(dT, 4) * pow(w_mag, 2)) / 24.0) + ((pow(dT, 6) * pow(w_mag, 4)) / 720.0);
    } else {
        a = (sin(w_mag * dT) / w_mag);
        b = (1.0 - cos(w_mag * dT)) / pow(w_mag, 2);
    }

    // multiply [wx] by the coefficients
    lin_alg_s_m_mult(a, wx, matA);
    lin_alg_m_m_mult(wx, wx, temp);
    lin_alg_s_m_mult(b, temp, matB);

    // combine matrices (I - A*[wx]) + B*[wx]**2
    lin_alg_m_m_sub(I, matA, temp);
    lin_alg_m_m_add(temp, matB, exp_mat);
}

/**
 * @Function dcm2Euler()
 * @param float dcm[3][3] - dcm matrix 
 * @return none
 * @brief  helper function which extracts euler angles from dcm matrix and 
 *         stores them in the current state struct
 * @author Margaret Silva */
void dcm2Euler(float dcm[3][3]) {
    if (dcm[0][2] < -1.0) {
        dcm[0][2] = -1.0;
    } else if (dcm[0][2] > 1.0) {
        dcm[0][2] = 1.0;
    }

    current_state.pitch = -1.0 * asin(dcm[0][2]);

    current_state.roll = atan2(dcm[1][2], dcm[2][2]);

    current_state.yaw = atan2(dcm[0][1], dcm[0][0]);
}


//#define STATE_ESTIMATION_FN_DEBUG

#ifdef STATE_ESTIMATION_FN_DEBUG
#include "SerialM32.h"
#include "Board.h"

int main() {
    int i;
    float exp_mat[3][3];
    float w[3] = {1, 2, 3};
    struct IMU_out imu_data;
    Board_init();
    Serial_init();

    printf("\r\n\nState Estimation Debug Code %s, %s \r\n", __DATE__, __TIME__);

    initStateEstimation();

    printf("\n Altitude Estimation Testing\n");
    for (i = 0; i < 10; i++) {
        altitudeEstimation(100000, 0.01);
        printf("cur est: %f \n", current_state.pd);
    }
    printf("\nAlt est -> exp: %f, re: %f \n", -106.5, current_state.pd);


    for (i = 0; i < 10; i++) {
        altitudeEstimation(0, 0.01);
        printf("cur est: %f \n", current_state.pd);
    }
    printf("\nAlt est -> exp: %f, re: %f \n", -8144.4, current_state.pd);

    printf("\nRexp Function Testing\n");
    printf("in: [1, 2 , 3]\n out:");
    R_exp(w, 0.01, exp_mat);
    lin_alg_m_print(exp_mat);
    w[0] = .01;
    w[1] = -10;
    w[2] = 33.547;
    printf("\nin: [1, 2 , 3]\n out:");
    R_exp(w, 0.01, exp_mat);
    lin_alg_m_print(exp_mat);


    printf("\n\nAttitude Estimation Testing\n");
    //    imu_data.gyro.x = 0.0;
    //    imu_data.gyro.y = 0.0;
    //    imu_data.gyro.z = 0.0;
    //
    //    imu_data.acc.x = 0.0;
    //    imu_data.acc.y = 0.0;
    //    imu_data.acc.z = 0.0;
    //
    //    imu_data.mag.x = 0.0;
    //    imu_data.mag.y = 0.0;
    //    imu_data.mag.z = 0.0;
    //
    //    attitudeEstimation(imu_data, 0.01);
    //    printf("\nbiases: p:%f, q:%f, r:%f\n", current_biases.b_p, current_biases.b_q, current_biases.b_r);
    //    printf("rates: p:%f, q:%f, r:%f\n", current_state.p, current_state.q, current_state.r);
    //    printf("angles: y:%f, p:%f, r:%f\n", current_state.yaw, current_state.pitch, current_state.roll);
    //    printf("dcm matrix: \n");
    //    lin_alg_m_print(current_state.R);

    imu_data.gyro.x = 0.01616848980807178;
    imu_data.gyro.y = 0.04961927283134717;
    imu_data.gyro.z = -0.031045578438633446;

    imu_data.acc.x = 0.0560310306601586;
    imu_data.acc.y = -0.27370076248339115;
    imu_data.acc.z = -10.007043373719073;

    imu_data.mag.x = 22746.034132473316;
    imu_data.mag.y = 5281.887001393938;
    imu_data.mag.z = 41386.75702149094;

    attitudeEstimation(imu_data, 0.01);
    printf("\nbiases: p:%f, q:%f, r:%f\n", current_biases.b_p, current_biases.b_q, current_biases.b_r);
    printf("rates: p:%f, q:%f, r:%f\n", current_state.p, current_state.q, current_state.r);
    printf("angles: y:%f, p:%f, r:%f\n", current_state.yaw, current_state.pitch, current_state.roll);

    imu_data.gyro.x = 0.021711413789178735;
    imu_data.gyro.y = 0.050763505688222854;
    imu_data.gyro.z = -0.030837076774831104;

    imu_data.acc.x = 0.25952188207724275;
    imu_data.acc.y = 0.058967892417263704;
    imu_data.acc.z = -9.883295068342836;

    imu_data.mag.x = 22768.279106589198;
    imu_data.mag.y = 5245.924564851648;
    imu_data.mag.z = 41463.6757876784;

    attitudeEstimation(imu_data, 0.01);
    printf("\nbiases: p:%f, q:%f, r:%f\n", current_biases.b_p, current_biases.b_q, current_biases.b_r);
    printf("rates: p:%f, q:%f, r:%f\n", current_state.p, current_state.q, current_state.r);
    printf("angles: y:%f, p:%f, r:%f\n", current_state.yaw, current_state.pitch, current_state.roll);

    imu_data.gyro.x = 0.020565376301704703;
    imu_data.gyro.y = 0.04521764205097616;
    imu_data.gyro.z = -0.028709604500071952;

    imu_data.acc.x = -0.3716499755492544;
    imu_data.acc.y = 0.09429415546331646;
    imu_data.acc.z = -9.439795911446897;

    imu_data.mag.x = 22752.684126170214;
    imu_data.mag.y = 5279.389891806871;
    imu_data.mag.z = 41397.64414581243;

    attitudeEstimation(imu_data, 0.01);
    printf("\nbiases: p:%f, q:%f, r:%f\n", current_biases.b_p, current_biases.b_q, current_biases.b_r);
    printf("rates: p:%f, q:%f, r:%f\n", current_state.p, current_state.q, current_state.r);
    printf("angles: y:%f, p:%f, r:%f\n", current_state.yaw, current_state.pitch, current_state.roll);

    while (1);
}

#endif