'''
    File: testChapter8.py
    Author: Margaret Silva
    Notes: Contains tests for functions within the vehicle estimation file
'''

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Controls.VehicleEstimator as Estimator
import ece163.Controls.VehicleClosedLoopControl as VCLC
import ece163.Containers.Controls as Controls
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits import mplot3d
import numpy as np

"""math.isclose doesn't work well for comparing things near 0 unless we 
use an absolute tolerance, so we make our own isclose:"""
isclose = lambda  a,b : math.isclose(a, b, abs_tol= 1e-12)

def compareVectors(a, b):
	"""A quick tool to compare two vectors"""
	el_close = [isclose(a[i][0], b[i][0]) for i in range(3)]
	return all(el_close)

#of course, you should test your testing tools too:
assert(compareVectors([[0], [0], [-1]],[[1e-13], [0], [-1+1e-9]]))
assert(not compareVectors([[0], [0], [-1]],[[1e-11], [0], [-1]]))
assert(not compareVectors([[1e8], [0], [-1]],[[1e8+1], [0], [-1]]))


failed = []
passed = []
def evaluateTest(test_name, boolean):
	"""evaluateTest prints the output of a test and adds it to one of two 
	global lists, passed and failed, which can be printed later"""
	if boolean:
		print(f"   passed {test_name}")
		passed.append(test_name)
	else:
		print(f"   failed {test_name}")
		failed.append(test_name)
	return boolean


#%% Test attitude state estimation using graphs

# n_steps = 25000
n_steps = 10000
# init vehicle controller
test_controls = VCLC.VehicleClosedLoopControl()
# control_gains = Controls.controlGains(3.0561, 0.001, 0.043177, 1.48226, 1.9, 5.0968, 2.5484, -13.1825, 
# 				      -0.7931, 0.07743, 0.030975, 1.493, 1.1466, -0.5206, -0.1)
control_gains = Controls.controlGains(3.0561, 0.001, 0.043177, .5, .05, .5*5.0968, .5*2.5484, -13.1825, 
				      -0.7931, 0.07743, 0.030975, 1.493, 1.1466, -0.5206, -0.1)
test_controls.setControlGains(controlGains=control_gains)
test_dynamics = test_controls.getVehicleAerodynamicsModel()
# init estimation
test_estimation = Estimator.VehicleEstimator(VPC.dT, test_dynamics)
# test_estimation.setFilterGains(2.5, 2.5, .25, .25)
test_estimation.setFilterGains()
# set up all data arrays 
time_arr = [VPC.dT*i for i in range(n_steps)]
actual_p = [0 for i in range(n_steps)]
actual_q = [0 for i in range(n_steps)]
actual_r = [0 for i in range(n_steps)]
p_diff = [0 for i in range(n_steps)]
q_diff = [0 for i in range(n_steps)]
r_diff = [0 for i in range(n_steps)]
est_yaw = [0 for i in range(n_steps)]
est_pitch = [0 for i in range(n_steps)]
est_roll = [0 for i in range(n_steps)]
yaw_diff = [0 for i in range(n_steps)]
actual_yaw = [0 for i in range(n_steps)]
pitch_diff = [0 for i in range(n_steps)]
actual_pitch = [0 for i in range(n_steps)]
roll_diff = [0 for i in range(n_steps)]
actual_roll = [0 for i in range(n_steps)]
Va_diff = [0 for i in range(n_steps)]
Va_true = [0 for i in range(n_steps)]
Va_est = [0 for i in range(n_steps)]
Va_cmd = [0 for i in range(n_steps)]
alt_diff = [0 for i in range(n_steps)]
alt_est = [0 for i in range(n_steps)]
alt_true = [0 for i in range(n_steps)]
alt_cmd = [0 for i in range(n_steps)]
course_diff = [0 for i in range(n_steps)]
course_true = [0 for i in range(n_steps)]
course_est = [0 for i in range(n_steps)]
course_cmd = [0 for i in range(n_steps)]
pn_est = [0 for i in range(n_steps)]
pn_true = [0 for i in range(n_steps)]
pe_est = [0 for i in range(n_steps)]
pe_true = [0 for i in range(n_steps)]
waypoint_swap_n = []
waypoint_swap_e = []
waypoint_swap_d = []

# Create waypoint array 
# W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[100], [0], [-100]], [[0], [100], [-100]], [[200], [200], [-100]], [[1000], [1000], [-150]]]
# W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[3*100], [0], [-100]], [[0], [3*100], [-100]], [[3*100], [3*100], [-100]], [[0], [0], [-100]]]
# W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[500], [1000], [VPC.InitialDownPosition]], [[-200], [10], [-80]], [[200], [100], [-50]]]
# W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[300], [VPC.InitialEastPosition], [-95]], [[600], [VPC.InitialEastPosition], [-90]], [[900], [VPC.InitialEastPosition], [-85]]]
# W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[3*100], [0], [-100]], [[0], [3*100], [-100]], [[3*100], [3*100], [-100]], [[0], [0], [-100]], [[300], [VPC.InitialEastPosition], [-95]], [[600], [VPC.InitialEastPosition], [-90]], [[900], [VPC.InitialEastPosition], [-85]], [[500], [1000], [VPC.InitialDownPosition]], [[-200], [10], [-80]], [[200], [100], [-50]]]
# W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[200], [0], [-100]], [[0], [200], [-100]], [[200], [200], [-100]], [[0], [0], [-100]], [[500], [0], [-100]], [[0], [100], [-100]], [[700], [700], [-100]], [[0], [0], [-100]]]
# W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[500], [0], [-100]], [[500], [500], [-100]], [[0], [500], [-100]], [[0], [0], [-100]], [[500], [0], [-100]], [[500], [500], [-100]], [[0], [500], [-100]], [[0], [0], [-100]]]
# W = W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[500], [0], [-100]], [[500], [500], [-100]], [[250], [700], [-100]], [[50], [900], [-100]], [[0], [500], [-100]], [[0], [0], [-100]], [[500], [0], [-100]], [[500], [500], [-100]], [[0], [500], [-100]], [[0], [0], [-100]]]

# butterfly pattern n = 15000
# W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[500], [0], [-100]], [[0], [500], [-100]], [[500], [500], [-100]], [[0], [0], [-100]], [[0], [0], [-100]]]

# snake pattern n = 18000
# W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[500], [500], [-100]], [[500], [1000], [-100]], [[0], [1000], [-100]], [[0], [500], [-100]], [[-500], [500], [-100]], [[-500], [1000], [-100]], [[-500], [1000], [-100]]]

# square pattern n = 21000
# W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[0], [200], [-100]], [[0], [1200], [-100]], [[-1000], [1200], [-100]], [[-1000], [200], [-100]], [[0], [200], [-100]], [[0], [200], [-100]]]

# zig zag n = 10000
W = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[0], [200], [-100]], [[400], [0], [-100]], [[800], [200], [-100]], [[1200], [0], [-100]], [[1600], [200], [-100]], [[1600], [200], [-100]]]

test_controls.setWaypointArray(W)


# best gains so far: Kp_att_a=5, Kp_att_h=15, Kp_alt=.8, Kp_spd=.11, Kp_course=.5, Ki_att_a=.5, Ki_att_h=1, Ki_spd=.11
# test_estimation.setFilterGains(Kp_att_a=5, Kp_att_h=15, Kp_alt=.8, Kp_spd=.11, Kp_course=.5, Ki_att_a=.5, Ki_att_h=1, Ki_spd=.11) 
test_estimation.setFilterGains(Kp_att_a=1, Kp_att_m=0, Kp_alt=.8, Kp_spd=.11, Kp_course=0.9, Ki_att_a=.1, Ki_att_m=0, Ki_spd=.11) 

# run simulation of state estimation
for i in range(n_steps):
	test_estimation.update()
	actual_p[i] = test_estimation.getVehicleState().p 
	actual_q[i] = test_estimation.getVehicleState().q
	actual_r[i] = test_estimation.getVehicleState().r
	p_diff[i] = test_estimation.getVehicleState().p - test_estimation.getStateEstimate().p
	q_diff[i] = test_estimation.getVehicleState().q - test_estimation.getStateEstimate().q
	r_diff[i] = test_estimation.getVehicleState().r - test_estimation.getStateEstimate().r
	est_yaw[i] = test_estimation.getStateEstimate().yaw
	est_pitch[i] = test_estimation.getStateEstimate().pitch
	est_roll[i] = test_estimation.getStateEstimate().roll
	actual_yaw[i] = test_estimation.getVehicleState().yaw 
	actual_pitch[i] = test_estimation.getVehicleState().pitch
	actual_roll[i] = test_estimation.getVehicleState().roll
	yaw_diff[i] = test_estimation.getVehicleState().yaw - test_estimation.getStateEstimate().yaw
	pitch_diff[i] = test_estimation.getVehicleState().pitch - test_estimation.getStateEstimate().pitch
	roll_diff[i] = (test_estimation.getVehicleState().roll - test_estimation.getStateEstimate().roll) #% math.pi
	Va_diff[i] = test_estimation.getVehicleState().Va - test_estimation.getStateEstimate().Va
	Va_true[i] = test_estimation.getVehicleState().Va
	Va_est[i] = test_estimation.getStateEstimate().Va
	alt_diff[i] = test_estimation.getVehicleState().pd - test_estimation.getStateEstimate().pd
	alt_est[i] = -test_estimation.getStateEstimate().pd
	alt_true[i] = -test_estimation.getVehicleState().pd
	course_diff[i] = math.degrees(test_estimation.getVehicleState().chi - test_estimation.getStateEstimate().chi)
	course_est[i] = math.degrees(test_estimation.getStateEstimate().chi)
	while course_est[i] > 360:
		course_est[i] -= 360
	while course_est[i] < -360:
		course_est[i] += 360
	course_true[i] = math.degrees(test_estimation.getVehicleState().chi)
	pn_est[i] = test_estimation.getStateEstimate().pn
	pn_true[i] = test_estimation.getVehicleState().pn
	pe_est[i] = test_estimation.getStateEstimate().pe
	pe_true[i] = test_estimation.getVehicleState().pe
	course_cmd[i], alt_cmd[i], wp_swap = test_controls.UpdateWithWaypoints()
	while course_cmd[i] > 360:
		course_cmd[i] -= 360
	while course_cmd[i] < -360:
		course_cmd[i] += 360
	if wp_swap > 0:
		waypoint_swap_n.append(test_estimation.getVehicleState().pn)
		waypoint_swap_e.append(test_estimation.getVehicleState().pe)
		waypoint_swap_d.append(test_estimation.getVehicleState().pd)
	# ref_controls = Controls.referenceCommands(VPC.InitialYawAngle, -VPC.InitialDownPosition, VPC.InitialSpeed)
	# if (i >= 500) and (i < 10000):
	# 	ref_controls = Controls.referenceCommands(VPC.InitialYawAngle + 0.5236, -VPC.InitialDownPosition + 50, VPC.InitialSpeed - 10)
	# elif (i>=10000) and (i < 15000):
	# 	ref_controls = Controls.referenceCommands(VPC.InitialYawAngle - 0.5236, -VPC.InitialDownPosition - 50, VPC.InitialSpeed + 15)
    # Va_cmd[i] = ref_controls.commandedAirspeed
	# alt_cmd[i] = -ref_controls.commandedAltitude
	# course_cmd[i] = math.degrees(ref_controls.commandedCourse)
    # test_controls.Update(ref_controls)
    

# display gains used
print("gains:", test_estimation.getFilterGains())

# graph simulation results
num_graphs = 7

plt.figure(1)
plt.subplot(math.ceil(num_graphs/2), 3, 1)
plt.plot(time_arr, pn_est, label="est")
plt.plot(time_arr, pn_true, label="true")
plt.title("North position comparison")
plt.ylabel("position (m)")
plt.legend()

plt.subplot(math.ceil(num_graphs/2), 3, 2)
plt.plot(time_arr, pe_est, label="est")
plt.plot(time_arr, pe_true, label="true")
plt.title("East position comparison")
plt.ylabel("position (m)")
plt.legend()

alt_cmd = [-alt_cmd[i] for i in range(len(alt_cmd))]
plt.subplot(math.ceil(num_graphs/2), 3, 3)
plt.plot(time_arr, alt_est, label="est")
plt.plot(time_arr, alt_true, label="true")
plt.plot(time_arr, alt_cmd, label="cmd")
plt.title("Altitude comparison")
plt.ylabel("altitude (m)")
plt.legend()

pn_diff = [pn_true[i] - pn_est[i] for i in range(len(pn_true))]
plt.subplot(math.ceil(num_graphs/2), 3, 4)
plt.plot(time_arr, pn_diff)
plt.title("North position error")
plt.ylabel("position (m)")

pe_diff = [pe_true[i] - pe_est[i] for i in range(len(pn_true))]
plt.subplot(math.ceil(num_graphs/2), 3, 5)
plt.plot(time_arr, pe_diff)
plt.title("East position error")
plt.ylabel("position (m)")
plt.xlabel("time (seconds)")

plt.subplot(math.ceil(num_graphs/2), 3, 6)
plt.plot(time_arr, alt_diff)
plt.title("Altitude error")
plt.ylabel("altitude (m)")
plt.xlabel("time (seconds)")

plt.subplot(math.ceil(num_graphs/2), 3, 7)
plt.plot(time_arr, course_est, label="est")
plt.plot(time_arr, course_true, label="true")
plt.plot(time_arr, course_cmd, label="cmd")
plt.title("Course comparison")
plt.ylabel("course (degrees)")
plt.xlabel("time (seconds)")
plt.legend()

plt.subplots_adjust(top=0.88,
bottom=0.0,
left=0.125,
right=0.9,
hspace=0.5,
wspace=0.2)

plt.figure(2)
waypoints_n = [W[i][0][0] for i in range(len(W))]
waypoints_e = [W[i][1][0] for i in range(len(W))]
waypoints_d = [W[i][2][0] for i in range(len(W))]
way_col = [i +1 for i in range(len(waypoints_e))]

c_map_arr = np.linspace(0, 1, len(pe_true))
# plt.scatter( pe_true, pn_true, c=c_map_arr, cmap=plt.get_cmap('Blues'), s=1, label="path")
plt.plot( pe_true, pn_true, label="path")

norm = mpl.colors.Normalize(vmin=min(way_col), vmax=max(way_col))
cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.Blues)
cmap.set_array([])

plt.scatter(waypoints_e, waypoints_n, c="orange", s = 40, label="waypoints")
plt.scatter(waypoint_swap_e, waypoint_swap_n, c="hotpink", s= 10, label="transition points")
plt.title("Position of aircraft")
plt.ylabel("north position (m)")
plt.xlabel("east position (m)")
plt.legend()

plt.figure(3)
waypoint_swap_d = [-waypoint_swap_d[i] for i in range(len(waypoint_swap_d))]
waypoints_d = [-waypoints_d[i] for i in range(len(waypoints_d))]
c_map_arr = np.linspace(0, 1, len(pe_true))
norm = mpl.colors.Normalize(vmin=min(c_map_arr), vmax=max(c_map_arr))
cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.Blues)
ax = plt.axes(projection ='3d')
# ax.scatter(pe_true, pn_true, alt_true, cmap=plt.get_cmap('Blues'), c=c_map_arr, s=1,label="path")
ax.plot(pe_true, pn_true, alt_true, label="path")
# ax.scatter(waypoints_e, waypoints_n, waypoints_d, c=way_col, cmap=plt.get_cmap('Wistia'), s= 50, label="waypoints")
ax.scatter(waypoints_e, waypoints_n, waypoints_d, c="orange", s= 50, label="waypoints")
ax.scatter(waypoint_swap_e, waypoint_swap_n, waypoint_swap_d, c="hotpink", s= 20, label="transition points")
plt.ylabel("north position (m)")
plt.xlabel("east position (m)")
ax.set_zlabel("altitude (m)")
plt.legend()

plt.figure(4)

plt.subplot(2, 2, 1)
plt.plot(time_arr, alt_est, label="est")
plt.plot(time_arr, alt_true, label="true")
plt.plot(time_arr, alt_cmd, label="cmd")
plt.title("Altitude comparison")
plt.ylabel("altitude (m)")
plt.legend()

plt.subplot(2, 2, 2)
plt.plot(time_arr, course_est, label="est")
plt.plot(time_arr, course_true, label="true")
plt.plot(time_arr, course_cmd, label="cmd")
plt.title("Course comparison")
plt.ylabel("course (degrees)")
# plt.xlabel("time (seconds)")
plt.legend()

plt.subplot(2, 2, 3)
alt_err = [alt_true[i] - alt_cmd[i] for i in range(n_steps)]
plt.plot(time_arr, alt_err)
plt.title("Altitude error")
plt.ylabel("altitude error (m)")
plt.xlabel("time (seconds)")
# plt.legend()

plt.subplot(2, 2, 4)
course_err = [course_true[i] - course_cmd[i] for i in range(n_steps)]
plt.plot(time_arr, course_err)
plt.title("Course error")
plt.ylabel("course error (degrees)")
plt.xlabel("time (seconds)")
# plt.legend()


plt.show()