/* 
 * File:   PublishMavlinkMsg.h
 * Author: Margaret Silva
 * 
 * This file contains all the mavlink publish functions used in the main file
 *
 * Created on May 26, 2023, 2:42 PM
 */

#ifndef PUBLISHMAVLINKMSG_H
#define	PUBLISHMAVLINKMSG_H


/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/
#include "StabilityController.h"
#include "Board.h"
#include "Radio_serial.h"
#include "common/mavlink.h"
#include "SerialM32.h"

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
// use this to switch between sending data over serial/over radio
#define GET_CHAR() get_char()
#define PUT_CHAR(c) put_char(c)
#define DATA_AVAILABLE() data_available()

// buffer defines
#define BUFFER_SIZE 1024

/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

/**
 * @Function publish_controller_output(float output, char controller_type[10])
 * @param output - float value of actuator commands
 * @param controller_type - name of controller output (ten chars max)
 * @brief invokes mavlink helper to send controller output over radio
 * @author aaron hunter
 * @modified for use in DORA system
 */
void publish_controller_output(float output, char controller_type[10]);

/**
 * @Function publish_actuator_cmds(struct controller_outputs actuators)
 * @param actuators - controller output struct containing commands to be sent to 
 *      all control surfaces on UAV
 * @brief invokes mavlink helper to send actuator commands over radio
 * @author Margaret Silva
 */
void publish_actuator_cmds(struct controller_outputs actuators);

/**
 * @Function publish_status_text(char text[50])
 * @param text - 50 chars of text as a status message
 * @brief invokes mavlink helper to send out status text
 * @author aaron hunter
 * @modified for use in DORA system
 */
void publish_status_text(char text[50]);

/**
 * @Function publish_hil_act_controls(uint64_t flags, float outputs[16])
 * @param flags - flag that determines what info is transmitted
 *      outputs - data to be sent over mavlink
 * @brief invokes mavlink helper to generate hil act controls packet and sends out via the radio
 * @author aaron hunter
 * @modified for use in DORA system
 */
void publish_hil_act_controls(uint64_t flags, float outputs[16]);

/**
 * @Function publish_heartbeat(uint8_t status)
 * @param status - int value that specifies the type of heartbeat
 * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
 * @author aaron hunter
 * @modified for use in DORA system
 */
void publish_heartbeat(uint8_t status);

/**
 * @Function publish_euler_angles(float angles[3])
 * @param angles[3] - yaw, pitch, and roll angles received from sim
 * @brief invokes mavlink helper to send actuator commands over radio
 * @author Margaret Silva
 */
void publish_euler_angles(float angles[3]);


#endif	/* PUBLISHMAVLINKMSG_H */

