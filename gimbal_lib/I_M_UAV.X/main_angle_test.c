/* 
 * File:   main_angle_test.c
 * Author: ccuyos, maansilv
 * Brief: Main code for testing angles of gimbal
 * Created on May 27, 2023, 1:57 PM
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "BOARD.h"
#include "serial.h"
#include "timers.h"
#include "BNO055.h"
#include "IMU.h"
#include "AttitudeEstimation.h"
#include "PublishMavlinkMsg.h"
#include "RC_Servo.h"
#include <math.h>

/*******************************************************************************
 * #DEFINES                                                                    *
 ******************************************************************************/

//Credits to Max Dunne
#define DELAY(x)    {int wait; for (wait = 0; wait <= x; wait++) {asm("nop");}}
#define A_BIT       18300
#define A_LOT       183000

#define POWERPIN_LAT LATFbits.LATF1
#define POWERPIN_TRIS TRISFbits.TRISF1
#define PRINT_PERIOD 100
#define SENSOR_PERIOD 20
#define SENSOR_DT 0.02

// used to determine what data is being sent in an HIL actuator commands packet
#define HIL_SEND_GAINS_TO_SIM 1 // send received gains back to sim as ack
#define HIL_RECEIVE_STATE 2 // receive state data from sim
#define HIL_RECEIVE_COMMANDS 3 // receive commanded course and height
#define HIL_RECEIVE_SENSORS 4 // receive sensor data from sim
#define HIL_SEND_STATE_TO_SIM 5 // send estimated states to sim
#define HIL_ACK_COMMANDS 6 // acknowledge new commands
#define STATE_EST_RECEIVE_GAINS 7 // receive state estimation gains from sim
#define HIL_ACK_STATE_EST_GAINS 8 // acknowledge received state estimation gains
#define STABILITY_CONTROLLER_RECEIVE_GAINS 9 // receives stability controller gains from sim
#define HEARTBEAT_HIL_ACTUATOR_ERROR 255

#define ACTUATOR_PERIOD (1 * 1000)

// for head tracking mapping to reference commands
#define XC_DEFAULT 0.0
#define HC_DEFAULT 100.0

enum {
    ZERO_HEAD_TRACKING, CAPTURE_HEAD_TRACKING
};

/*******************************************************************************
 * VARIABLES                                                                   *
 ******************************************************************************/

/*Variables*/
struct IMU_out IMU_est;
struct FRT curr_time;
struct IMU IMU;
struct IMU_euler angleCL;
struct IMU_euler angleOL;

static float angles[3] = {0, 0, 0};

static int new_state_received = 0;

/*******************************************************************************
 * FUNCTIONS                                                                   *
 ******************************************************************************/

void TransferIMU(void) {
    //Transfer sensor values from sensor libraries to attitude estimation library

    /*Accelerometer*/
    IMU_est.acc.x = -IMU.acc_true.x;
    IMU_est.acc.y = -IMU.acc_true.y;
    IMU_est.acc.z = -IMU.acc_true.z;

    /*Gyroscope*/
    IMU_est.gyro.x = IMU.gyro_rad.x;
    IMU_est.gyro.y = IMU.gyro_rad.y;
    IMU_est.gyro.z = IMU.gyro_rad.z;

}

/*******************************************************************************
 * MAIN                                                                        *
 ******************************************************************************/

int main(void) {
    int cur_time = 0;
    int heartbeat_start = 0;
    int response_start = 0;
    int actuator_start_time = 0;
    unsigned int pwm_signal;
    int pwm_dir = 1;
    int angle = 0;
    int i;
    float set_angles[3] = {0, 0, 0};
    float max_angle;
    float min_angle;

    //Initialize required modules
    BOARD_Init();
    SERIAL_Init();
    TIMERS_Init();
    initEstimation();
    RCServo_Init(SERVO_PWM_1);
    RCServo_Init(SERVO_PWM_2);
    RCServo_Init(SERVO_PWM_3);
    // printf("I.M.U main compiled on %s %s. \n", __TIME__, __DATE__);
    POWERPIN_LAT = 0;
    POWERPIN_TRIS = 0;
    POWERPIN_LAT = 1;
    DELAY(100);
//    if (BNO055_Init() != TRUE) {
//        printf("IMU Initialization failed\n");
//        while (1);
//    }

//    setEstimationGains(KP_ACC, KI_ACC, KP_MAG, KI_MAG);
    heartbeat_start = TIMERS_GetMilliSeconds();
    response_start = TIMERS_GetMilliSeconds();
    actuator_start_time = TIMERS_GetMilliSeconds();

    pwm_signal = RC_SERVO_CENTER_PULSE;
    RCServo_SetPulseFromAngle(0, YAW_SERVO);
    RCServo_SetPulseFromAngle(0, PITCH_SERVO);
    RCServo_SetPulseFromAngle(0, ROLL_SERVO);

    max_angle = YAW_MAX_ANGLE;
    min_angle = YAW_MIN_ANGLE;

    while (1) {
        cur_time = TIMERS_GetMilliSeconds();

        if ((cur_time - actuator_start_time) > 20) {
            // gradually move towards desired angle
            for (i = 0; i < 3; i++) {
                if (angle > set_angles[i]) {
                    set_angles[i] += 0.25;
                } else if (angle < set_angles[i]) {
                    set_angles[i] -= 0.25;
                }
            }
            // move RC servos
            set_angles[1] = 0.0;
            set_angles[2] = 0.0;
            RCServo_SetPulseFromAngle(set_angles[0], YAW_SERVO);
            RCServo_SetPulseFromAngle(set_angles[1], PITCH_SERVO);
            RCServo_SetPulseFromAngle(set_angles[2], ROLL_SERVO);

            actuator_start_time = cur_time;
        }
        
        if ((cur_time - heartbeat_start) > ACTUATOR_PERIOD) {
            publish_heartbeat(11);
            heartbeat_start = cur_time;
            response_start = cur_time;

            if (pwm_dir == 1) {
                angle = angle + 5;
                if (angle > max_angle) {
                    pwm_dir = 0;
                }
            } else if (pwm_dir == 0) {
                angle = angle - 5;
                if (angle < min_angle) {
                    pwm_dir = 1;
                }
            }
        }

        if ((cur_time - response_start) > 100) {
            // human readable
            // printf("y: %f, p: %f, r: %f \n", set_angles[0], set_angles[1], set_angles[2]);
            
            // matlab readable
            publish_euler_angles_redux(set_angles);
            // printf("%0.2f, %0.2f, %0.2f;\n", set_angles[0], set_angles[1], set_angles[2]);

            response_start = cur_time;
        }        
    }

    while (1);
    BOARD_End();

    while (1);
}

