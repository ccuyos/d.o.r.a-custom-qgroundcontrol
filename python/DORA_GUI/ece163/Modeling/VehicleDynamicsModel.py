"""
This module is where all of the vehicle dynamics are computed for the simulation. It includes the kinematics of both the
translational and rotational dynamics. Included are both the derivative, and the integration functions, and the rotations
of forces to the body frame.
"""

import math
from ..Containers import States
from ..Utilities import MatrixMath
from ..Utilities import Rotations
from ..Constants import VehiclePhysicalConstants as VPC


class VehicleDynamicsModel():
	"""
	:ivar state: the current state of the vehicle, as an instance of States.vehicleState
	:ivar dot:  the current time-derivative of the state, as an instance of States.vehicleState
	:ivar dT:  the timestep that this object uses when Update()ing.
	"""
	
	def __init__(self, dT=VPC.dT):
		"""
		Initializes the class, and sets the time step (needed for Rexp and integration). Instantiates attributes for vehicle state, and time derivative of vehicle state.

		:param dT: defaults to VPC.dT
		:return: none
		"""
		self.state = States.vehicleState()
		self.dot = States.vehicleState()
		self.dT = dT
		return


	def getVehicleState(self):
		"""
		Getter method to read the vehicle state

		:return: state (from class vehicleState)
		"""
		return self.state


	def setVehicleState(self, state):
		"""
		Setter method to write the vehicle state

		:param state: state to be set ( an instance of Containers.States.vehicleState)
		:return: none
		"""
		self.state = state
		return

	def getVehicleDerivative(self):
		"""
		Getter method to read the vehicle state time derivative

		:return: dot   ( an instance of Containers.States.vehicleState)
		"""
		return self.dot


	def setVehicleDerivative(self, dot):
		"""
		Setter method to write the vehicle state time derivative

		:param dot: dot to be set (should be an instance of Containers.States.vehicleState)
		:return: none
		"""
		self.dot = dot
		return


	def reset(self):
		"""
		Reset the Vehicle state to initial conditions

		:return: none
		"""
		resetState = States.vehicleState()
		self.setVehicleState(resetState)
		self.setVehicleDerivative(resetState)
		return


	def Update(self, forcesMoments):
		"""
		Function that implements the integration such that the state is updated using the forces and moments passed in
		as the arguments (dT is internal from the member). State is updated in place (self.state is updated). Use
		getVehicleState to retrieve state. Time step is defined in VehiclePhyscialConstants.py

		:param forcesMoments: forces [N] and moments [N-m] defined in forcesMoments class
		
		:return: none
		"""
		self.dot = self.derivative(self.state, forcesMoments)
		self.state = self.IntegrateState(self.dT, self.state, self.dot )
		return


	def derivative(self, state, forcesMoments):
		"""
		Function to compute the time-derivative of the state given body frame forces and moments

		:param state: state to differentiate, as a States.vehicleState object
		:param forcesMoments: forces [N] and moments [N-m] as an Inputs.forcesMoments object
		
		:return: the current time derivative, in the form of a States.vehicleState object
		"""
		dot = States.vehicleState()
		uvw = [[state.u], [state.v], [state.w]]
		Pneddot = MatrixMath.multiply(MatrixMath.transpose(state.R),uvw)
		dot.pn = Pneddot[0][0]
		dot.pe = Pneddot[1][0]
		dot.pd = Pneddot[2][0]
		state.chi = math.atan2(dot.pe, dot.pn)

		Wx = MatrixMath.skew(state.p,state.q,state.r)
		minusWx = MatrixMath.scalarMultiply(-1.,Wx)
		dot.R = MatrixMath.multiply(minusWx,state.R)

		sinphi = math.sin(state.roll)
		cosphi = math.cos(state.roll)
		costheta = math.cos(state.pitch)
		tantheta = math.tan(state.pitch)
		euldot = [[1., sinphi * tantheta, cosphi * tantheta],
				  [0., cosphi, -1. * sinphi],
				  [0., sinphi / costheta, cosphi / costheta]]
		euldot = MatrixMath.multiply(euldot,[[state.p],[state.q],[state.r]])
		dot.roll = euldot[0][0]
		dot.pitch = euldot[1][0]
		dot.yaw = euldot[2][0]

		Vneddot = MatrixMath.multiply(minusWx,uvw)
		FextBm = MatrixMath.scalarMultiply(1./VPC.mass,[[forcesMoments.Fx],[forcesMoments.Fy],[forcesMoments.Fz]])
		Vneddot = MatrixMath.add(Vneddot,FextBm)

		dot.u = Vneddot[0][0]
		dot.v = Vneddot[1][0]
		dot.w = Vneddot[2][0]

		pqrdot = MatrixMath.multiply(VPC.Jbody,[[state.p],[state.q],[state.r]])
		pqrdot = MatrixMath.multiply(minusWx,pqrdot)
		pqrdot = MatrixMath.add(pqrdot,[[forcesMoments.Mx],[forcesMoments.My],[forcesMoments.Mz]])
		pqrdot = MatrixMath.multiply(VPC.JinvBody,pqrdot)

		dot.p = pqrdot[0][0]
		dot.q = pqrdot[1][0]
		dot.r = pqrdot[2][0]

		return dot

	def Rexp(self, dT, state, dot):
		"""
		Calculates the matrix exponential exp(-dT*[omega x]), which can be used in the closed form solution for the DCM integration from body-fixed rates. 
		
		See the document (ECE163_AttitudeCheatSheet.pdf) for details.

		:param dT: time step [sec]
		:param state: the vehicle state, in the form of a States.vehicleState object
		:param dot: the state derivative, in the form of a States.vehicleState object
		
		:return: Rexp: the matrix exponential to update the state
		"""
		eye = [[1.,0.,0.],[0.,1.,0.],[0.,0.,1.]]
		p = state.p + dot.p * dT/2.0
		q = state.q + dot.q * dT/2.0
		r = state.r + dot.r * dT/2.0
		wx = MatrixMath.skew(p,q,r)
		wx2 = MatrixMath.multiply(wx,wx)
		normW = math.hypot(p,q,r)
		if normW < 0.2: # needs a better number
			sincw = dT - (dT**3 * normW**2)/6.0 + (dT**5 * normW**4)/120.0
			oneminuscosw = (dT**2)/2.0 - (dT**4 * normW**2)/24.0 + (dT**6 * normW**4)/720.0
		else:
			sincw = math.sin(normW * dT)/normW
			oneminuscosw = (1 - math.cos(normW * dT))/(normW**2)
		Rexp = MatrixMath.add(eye,MatrixMath.scalarMultiply(-sincw,wx))
		Rexp = MatrixMath.add(Rexp,MatrixMath.scalarMultiply(oneminuscosw,wx2))
		return Rexp

	def ForwardEuler(self, dT, state, dot):
		"""
		Function to do the simple forwards integration of the state using the input derivative ("dot" - generated elsewhere by the derivative function). State is integrated
		using the update formula X_{k+1} = X_{k} + dX/dt * dT. The updated state is returned by the function. The state is 	held internally as an attribute of the class.

		:param dT: the timestep over which to forward integrate
		:param state: the initial state to integrate, as an instance of State.vehicleState 
		:param dot: the time-derivative of the state for performing integration, as an instance of State.vehicleState
		
		:return: new state, advanced by a timestep of dT (defined in States.vehicleState class)
		"""

		newState = States.vehicleState()
		oldState = state

		#we can do the single vars this way:
		for k in ["pn", "pe", "pd", "u", "v", "w", "p", "q", "r"]:
			newState.__dict__[k] = oldState.__dict__[k] + dT * dot.__dict__[k]

		#and then similar for R (Not needed in current version because we're using Rexp, it's included here for completeness)
		for i in range(3):
			for j in range(3):
				newState.R[i][j] = oldState.R[i][j] + dT * dot.R[i][j]

		return newState


	def IntegrateState(self, dT, state, dot):
		"""
		Updates the state given the derivative, and a time step. Attitude propagation is implemented as a DCM
		matrix exponential solution, all other state params are advanced via forward euler integration [x]k+1 = [x]k + xdot*dT. The integrated
		state is returned from the function. All derived variables in the state (e.g.: Va, alpha, beta, chi) should be copied
		from the input state to the returned state.

		chi for the newstate is atan2( pe_dot, pn_dot). This will be needed later in the course.

		
		:param dT: Time step [s]
		:param state: the initial state to integrate, as an instance of State.vehicleState 
		:param dot: the time-derivative of the state for performing integration, as an instance of State.vehicleState
		
		:return: new state, advanced by a timestep of dT, returned as an instance of the States.vehicleState class
		"""

		
		newState = self.ForwardEuler(dT, state, dot)

		Rexp = self.Rexp(dT, state, dot)  												# form the matrix exponential
		newState.R = MatrixMath.multiply(Rexp,state.R) 	                		# update rotation matrix
		newState.yaw, newState.pitch, newState.roll = Rotations.dcm2Euler(newState.R)	# extract Euler angles

		newState.alpha = state.alpha
		newState.beta = state.beta
		newState.Va = state.Va

		newState.chi = math.atan2(dot.pe, dot.pn)
		return newState
	def getVPC(self):
		return VPC
