"""Matrix Library Test Harness. Runs through various tests of the functions to determine that they are, in fact, working."""

import sys
sys.path.append('..')

import ece163.Utilities.MatrixMath as MatrixMath

print('hello')
A = [[0,1,3],[1,0,5],[3,4,-3]]
print('A =')
MatrixMath.matrixPrint(A)
A = MatrixMath.transpose(A)
print('A transpose = ')
MatrixMath.matrixPrint(A)
B = [[2, 1, 6],[1, 2, -3],[-1,2, 7]]
print('B = ')
MatrixMath.matrixPrint(B)
C = MatrixMath.multiply(A,B)
print('A * B = ')
MatrixMath.matrixPrint(C)
C = MatrixMath.add(A,B)
print('A + B = ')
MatrixMath.matrixPrint(C)
alpha = 1.5
C = MatrixMath.scalarMultiply(alpha,A)
print(str(alpha)+' * A =')
MatrixMath.matrixPrint(C)
B = MatrixMath.scalarDivide(alpha, C)
MatrixMath.matrixPrint(C)
R = MatrixMath.transpose([[1,2,3]])
print('R = ')
MatrixMath.matrixPrint(R)
print('dot product of R =')
C = MatrixMath.dotProduct(R,R)
MatrixMath.matrixPrint(C)
C = MatrixMath.skew(R[0][0],R[1][0],R[2][0])
print('Skew symmetric of R =')
MatrixMath.matrixPrint(C)
B = [[-1],[-3],[-5]]
print('B = ')
MatrixMath.matrixPrint(B)
C = MatrixMath.crossProduct(R,B)
print('Cross Product of R and B =')
MatrixMath.matrixPrint(C)
print(MatrixMath.size(C))
A = [[0, 1, 3], [1, 0, 5], [3, 4, -3]]
Xo = [5,10,15]
print('A = ')
MatrixMath.matrixPrint(A)
print('Xo = ',Xo)
C = MatrixMath.offset(A,Xo[0],Xo[1],Xo[2])
print('A offset by Xo = ')
MatrixMath.matrixPrint(C)
Xn = [[5],[0],[5]]
Xobar = MatrixMath.vectorNorm(Xn)
print(Xobar)
Xnn = [[0.0],[0.0],[0.0]]
Xobar = MatrixMath.vectorNorm(Xnn)
print(Xobar)
MatrixMath.matrixPrint(C)
