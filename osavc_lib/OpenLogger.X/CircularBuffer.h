/* 
 * File:   CircularBuffer.h
 * Author: Margaret Silva
 * Brief: Used to create and handle circular buffers and their operations
 * Created on Jan 22, 2022, 1:22 am
 * Modified on Jan 22, 2022, 1:22 am
 */

#ifndef CIRCULARBUFFER_H // Header guard
#define	CIRCULARBUFFER_H //

/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/
#include <stdint.h>


/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
#define BUFFER_LEN 270      // long enough for two 128 byte payloads plus 7 bytes head/tail/etc
#define TRUE 0x01
#define FALSE 0x00

/*******************************************************************************
 * PUBLIC TYPEDEFS                                                             *
 ******************************************************************************/
struct CircularBuffer {
    unsigned char buffer[BUFFER_LEN]; // contains the data of the buffer
    int head; // the pointer to the head of the buffer
    int tail; // the pointer to the tail of the buffer    
};

/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

/**
 * @Function createBuffer()
 * @param none
 * @return a newly initialized circular buffer
 * @brief used to create a new empty circular buffer
 * @note 
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 1:30 am */
struct CircularBuffer createBuffer();


/**
 * @Function bufferEnqueue()
 * @param buffer, a pointer to a circular buffer struct
 *        item, an item to be added to the buffer
 * @return TRUE to indicate success, FALSE to indicate failure
 * @brief used to add a new item to a buffer
 * @note if the buffer is full, this function doesn't execute
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 1:48 am */
uint8_t bufferEnqueue(struct CircularBuffer * buffer, unsigned char item);


/**
 * @Function bufferDequeue()
 * @param buffer, a pointer to a circular buffer struct
 *        item, a pointer to which the dequeued item is written to
 * @return TRUE to indicate success, FALSE to indicate failure
 * @brief used to pull an item from the buffer
 * @note if the buffer is empty, this function doesn't execute
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 1:57 am */
uint8_t bufferDequeue(struct CircularBuffer * buffer, unsigned char * item);


/**
 * @Function isBufferEmpty()
 * @param buffer, a pointer to a circular buffer struct
 * @return TRUE or FALSE
 * @brief used to check if the indicated buffer is empty or not
 * @note 
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 2:00 am */
uint8_t isBufferEmpty(struct CircularBuffer * buffer);


/**
 * @Function isBufferFull()
 * @param buffer, a pointer to a circular buffer struct
 * @return TRUE or FALSE
 * @brief used to check if the indicated buffer is full or not
 * @note 
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 1:53 am */
uint8_t isBufferFull(struct CircularBuffer * buffer);


/**
 * @Function bufferLength()
 * @param buffer, a pointer to a circular buffer struct
 * @return the number of items currently in the buffer
 * @brief used to count the number of items in the given buffer
 * @note 
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.22 2:03 am */
int bufferLength(struct CircularBuffer * buffer);


/**
 * @Function bufferDelete()
 * @param buffer, a pointer to a circular buffer struct
 * @param num_bytes, the number of bytes to be deleted, starting at the tail of the buffer
 * @return TRUE or FALSE, FALSE indicating that an error has occurred
 * @brief used to quickly remove items from the end of the buffer
 * @note will fail if the number of bytes exceeds the current buffer length
 * @author Margaret Silva,
 * @modified Margaret Silva, 2022.1.30 7:23 am */
uint8_t bufferDelete(struct CircularBuffer * buffer, unsigned char num_bytes);

#endif	/* CIRCULARBUFFER_H */ // End of header guard

