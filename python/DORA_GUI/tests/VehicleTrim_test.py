"""
Test harness to check if the numerical trim function is working.
"""

import math
import sys

import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt

sys.path.append('..')

import ece163.Controls.VehicleTrim as VehicleTrim
import ece163.Containers.States as States
import ece163.Containers.Inputs as Inputs

def set_axes_equal(ax):
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])

trimControls = Inputs.controlInputs()
trimState = States.vehicleState()

vTrim = VehicleTrim.VehicleTrim()
Vastar = 25.0
Gammastar = math.radians(0.0)
Kappastar = 0.0

check = vTrim.computeTrim(Vastar, Kappastar, Gammastar)
if check:
	print("Optimization successful")
else:
	print("Model converged outside of valid inputs, change parameters and try again")

alpha = vTrim.VehicleTrimModel.vehicleDynamics.state.alpha
beta = vTrim.VehicleTrimModel.vehicleDynamics.state.beta
phi = vTrim.VehicleTrimModel.vehicleDynamics.state.roll

points = vTrim.GenerateIdealPath(Vastar, Kappastar, Gammastar, 200.0, 0.1)

print("Optimized values: alpha* = {}, beta* = {}, phi* = {}".format(math.degrees(alpha), math.degrees(beta),
	  math.degrees(phi)))
print("Controls:")
trimControls = vTrim.getTrimControls()
print(trimControls)

print("State:")
trimState = vTrim.getTrimState()
print(trimState)

mpl.rcParams['legend.fontsize'] = 10
fig = plt.figure()
ax = fig.gca(projection='3d')

x = [row[0] for row in points]
y = [row[1] for row in points]
z = [row[2] for row in points]


ax.plot(x, y, zs=min(z), zdir='z', label='Projection in (x,y)')
ax.plot(y, z, zs=min(x), zdir='x', label='Projection in (y,z)')
ax.plot(x, z, zs=min(y), zdir='y', label='Projection in (x,z)')
ax.plot(x, y, z, label='Trajectory')
ax.scatter(x[0], y[0], z[0], label='start')
ax.scatter(x[-1], y[-1], z[-1], label='end')

ax.set_xlabel('East')
ax.set_ylabel('North')
ax.set_zlabel('Up')
set_axes_equal(ax)
ax.legend()

plt.show()

vTrim.exportTrim()