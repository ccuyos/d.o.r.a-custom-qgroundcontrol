/* 
 * File:   AS5047D.h
 * Author: Aaron Hunter
 * Brief: Driver for the rotary encoders 
 * Created on Dec 3, 2020 3:46 pm
 * Modified on 
 */

#ifndef DPS_310_H // Header guard
#define	DPS_310_H //

/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/

#include <sys/types.h>
#include "dps310_registers.h"

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
#define NUM_ENCODERS 3

#define DPS310_READ_WAIT_FOR_REG_ATTEMPTS 3



/*******************************************************************************
 * PUBLIC TYPEDEFS                                                             *
 ******************************************************************************/
 //All the potential encoders in the system
typedef enum {
    IDLE = DPS310_MEAS_CFG_MEAS_CTRL_IDLE,
    PRESSURE_MEASUREMENT = DPS310_MEAS_CFG_MEAS_CTRL_PRS,
    TEMPERATURE_MEASUREMENT = DPS310_MEAS_CFG_MEAS_CTRL_TMP
} dps310_mode_t;

struct BARO_out {
    float calibrated_temp; 
    float calibrated_pressure;
};


struct baro_coeff {
    int16_t C0;
    int16_t C1;
    int32_t C00;
    int32_t C10;
    int32_t C01;
    int32_t C11;
    int32_t C20;
    int32_t C21;
    int32_t C30;
};





/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

/**
 * @Function Encoder_Init(void)
 * @param freq, frequency of SPI rate
 * @return SUCCESS or ERROR
 * @brief initializes hardware in appropriate mode along with the needed interrupts */
uint8_t Barometer_init(void);

/**
 * @Function Barometer_start_data_acq(void);
 * @return none
 * @param none
 * @brief this function starts the SPI data read
 * @author Aaron Hunter
 **/
void Barometer_start_data_acq(void);


/**
 * @Function Barometer_Read(unsigned int)
 *  @param unsigned int input, containing 14-bit address of reg to read from DPS310
 * @return associated MISO from last command send as int
 * @brief initializes hardware in appropriate mode along with the needed interrupts */
int Barometer_Read(int input);

unsigned int Barometer_write(unsigned int input);


uint8_t Barometer_get_scaled_data(struct BARO_out* BARO_data);

/*
 * @function Barometer_is_data_ready
 * @param void
 * @return returns the status of the barometer
 * @note: Use as "if (Barometer_is_data_ready)"
 */
uint8_t Barometer_is_data_ready(void);


float dps_read_temperature(void);
float dps_read_pressure(void);
void dps_reset(void);
int dps_checkEvents(uint8_t meas_desired);





float dps_read_trueTemperature(void);



#endif	/* AS5047D_H */ // End of header guard

