"""
SensorsModel.py implements the functions required to simulate actual sensor outputs from the vehicle state and dynamics.
The class includes a function for generating the pure noise-free sensors, and another for corrupting the measurements with
realistic noise (including biases, wideband measurements noise, and slowly time-varying drifts). Note that there is no modeling
of temperature drift, and the outputs of the sensors are in engineering units.
"""
import math
import random
from ece163.Modeling import VehicleAerodynamicsModel
from ece163.Utilities import MatrixMath
from ..Containers import Sensors
from ..Constants import VehiclePhysicalConstants as VPC
from ..Constants import VehicleSensorConstants as VSC
from ..Modeling import VehicleAerodynamicsModel

testingAbs_tol = 1e-6

class GaussMarkov():
	def __init__(self, dT=VPC.dT, tau=1e6, eta=0.0):
		"""
		def __init__(self, dT=VPC.dT, tau=1e6, eta=0.0):
		Function to initialize the GaussMarkov code that generates the exponentially correlated noise which is used for
		the slowly varying bias drift of the gyros as well as the GPS position. Implements the noise model characterized by
		a first order Gauss-Markov process: dv/dt = -1/tau v + w, where w is a white noise process with N(0,eta).

		:param dT: time step [s]
		:param tau: correlation time [s]
		:param eta: standard deviation of white noise process
		:return: none
		"""
		self.dT = dT
		self.tau = tau
		self.eta = eta
		self.v = 0.0
		return

	def reset(self):
		"""
		Wrapper function that resets the GaussMarkov model
		:return: none
		"""
		self.v = 0.0
		return

	def update(self, vnoise=None):
		"""
		def update(self, vnoise=None):
		Function that updates the Gauss-Markov process, and returns the updated value (as well as updating the internal
		value that holds until the next call of the function.

		:param vnoise: optional parameter to drive the function with a known value. If None, then use random.gauss(0,sigma)
		:return: v, new noise value (also updated internally)
		"""
		if vnoise is None:
			vnoise = random.gauss(0.0, self.eta)

		self.v = math.exp(-self.dT / self.tau) * self.v + vnoise
		return self.v


class GaussMarkovXYZ():
	def __init__(self, dT=VPC.dT, tauX=1e6, etaX=0.0, tauY=None, etaY=None, tauZ=None, etaZ=None):
		"""
		def __init__(self, dT=VPC.dT, tauX=1e6, etaX=0.0, tauY=None, etaY=None, tauZ=None, etaZ=None):
		Function to aggregate three Gauss-Markov models into a triplet that returns the X, Y, and Z axes of the
		time-varying drift; if (tau, eta) are None, then will default to the same values for each model.

		:param dT: time step [s]
		:param tau: correlation time [s]
		:param eta: standard deviation of white noise process
		:return: none
		"""
		self.gmX = GaussMarkov(dT, tauX, etaX)
		if None in [tauY, etaY]:
			self.gmY = GaussMarkov(dT, tauX, etaX)
		else:
			self.gmY = GaussMarkov(dT, tauY, etaY)
		if None in [tauZ, etaZ]:
			if None in [tauY, etaY]:
				self.gmZ = GaussMarkov(dT, tauX, etaX)
			else:
				self.gmZ = GaussMarkov(dT, tauY, etaY)
		else:
			self.gmZ = GaussMarkov(dT, tauZ, etaZ)
		return

	def reset(self):
		"""
		Wrapper function that resets the GaussMarkovXYZ models
		:return: none
		"""
		self.gmX.reset()
		self.gmY.reset()
		self.gmZ.reset()

	def update(self, vXnoise=None, vYnoise=None, vZnoise=None):
		"""
		def update(self, vXnoise=None, vYnoise=None, vZnoise=None):
		Function that updates the Gauss-Markov processes, and returns the updated values (as well as updating the internal
		values that holds until the next call of the function.

		:param vXnoise: optional parameter to drive the X function with a known value. If None, then use random.gauss(0,sigma)
		:param vYnoise: optional parameter to drive the Y function with a known value. If None, then use random.gauss(0,sigma)
		:param vZnoise: optional parameter to drive the Z function with a known value. If None, then use random.gauss(0,sigma)
		:return: vX, vY, vZ, new noise values for each axis (also updated internally)
		"""
		vX = self.gmX.update(vXnoise)
		vY = self.gmY.update(vYnoise)
		vZ = self.gmZ.update(vZnoise)
		return vX, vY, vZ


class SensorsModel():
	def __init__(self, aeroModel=VehicleAerodynamicsModel.VehicleAerodynamicsModel(), taugyro=VSC.gyro_tau, etagyro=VSC.gyro_eta,
				 tauGPS=VSC.GPS_tau, etaGPSHorizontal=VSC.GPS_etaHorizontal, etaGPSVertical=VSC.GPS_etaVertical, gpsUpdateHz=VSC.GPS_rate):
		"""
		def __init__(self, aeroModel=VehicleAerodynamicsModel.VehicleAerodynamicsModel(), taugyro=VSC.gyro_tau, etagyro=VSC.gyro_eta, tauGPS=VSC.GPS_tau, etaGPSHorizontal=VSC.GPS_etaHorizontal, etaGPSVertical=VSC.GPS_etaVertical, gpsUpdateHz=VSC.GPS_rate):
		Function to initialize the SensorsModel code. Will contain both the true sensors outputs and the noisy sensor outputs
		using the noise and biases found in the Constants.VehicleSensorConstants file. Biases for the sensors are set at
		instantiation and not updated further during the code run. All sensor outputs are in Engineering Units (it is assumed
		that the raw ADC counts to engineering unit scaling has already been done). Note that if the biases are set to None at
		instantiation, then they will be set to random values using uniform distribution with the appropriate bias scaling from
		the sensors constants. SensorsModel class keeps the Gauss-Markov models for gyro biases and GPS.

		:param aeroModel: handle to the VehicleAerodynamicsModel class
		:param taugyro: Gauss-Markov time constant for gyros [s]
		:param etagyro: Gauss-Markov process noise standard deviation [rad/s]
		:param tauGPS: Gauss-Markov time constant for GPS [s]
		:param etaGPSHorizontal: Gauss-Markov process noise standard deviation [m]
		:param etaGPSVertical: Gauss-Markov process noise standard deviation [m]
		:param gpsUpdateHz: Update rate for GPS measurements [Hz]
		:return: none
		"""
		vehicleDynamics = aeroModel.getVehicleDynamicsModel()
		self.dT = vehicleDynamics.dT
		self.sensorsTrue = Sensors.vehicleSensors()
		self.sensorsNoisy = Sensors.vehicleSensors()
		self.sensorBiases = self.initializeBiases()
		self.sensorSigmas = self.initializeSigmas()
		self.gyroGaussMarkov = GaussMarkovXYZ(self.dT, taugyro, etagyro)
		self.aeroModel = aeroModel
		self.gpsGaussMarkov = GaussMarkovXYZ(1.0 / gpsUpdateHz, tauGPS, etaGPSHorizontal, tauGPS, etaGPSHorizontal, tauGPS, etaGPSVertical)
		self.updateTicks = 0
		self.gpsTickUpdate = int(1.0/(self.dT * gpsUpdateHz))
		return


	def initializeBiases(self, gyroBias=VSC.gyro_bias , accelBias=VSC.accel_bias, magBias=VSC.mag_bias, baroBias=VSC.baro_bias,
						 pitotBias=VSC.pitot_bias):
		"""
		def initializeBiases(self, gyroBias=VSC.gyro_bias , accelBias=VSC.accel_bias, magBias=VSC.mag_bias, baroBias=VSC.baro_bias, pitotBias=VSC.pitot_bias):
		Function to generate the biases for each of the sensors. Biases are set with a uniform random number from -1 to 1 that is
		then multiplied by the sigma_bias. The biases for all sensors is returned as a Sensors.vehicleSensors class. Note that GPS
		is an unbiased sensor (though noisy), thus all the GPS biases are set to 0.0

		:param gyroBias: bias scaling for the gyros [rad/s]
		:param accelBias: bias scaling for the accelerometers [m/s^2]
		:param magBias: bias scaling for the magnetometers [nT]
		:param baroBias: bias scaling for the barometer [N/m^2]
		:param pitotBias: bias scaling for the pitot tube [N/m^2]
		:return: sensorBiases, class Sensors.vehicleSensors
		"""
		sensorBiases = Sensors.vehicleSensors()
		sensorBiases.gyro_x = gyroBias * random.uniform(-1.0, 1.0)
		sensorBiases.gyro_y = gyroBias * random.uniform(-1.0, 1.0)
		sensorBiases.gyro_z = gyroBias * random.uniform(-1.0, 1.0)
		sensorBiases.accel_x = accelBias * random.uniform(-1.0, 1.0)
		sensorBiases.accel_y = accelBias * random.uniform(-1.0, 1.0)
		sensorBiases.accel_z = accelBias * random.uniform(-1.0, 1.0)
		sensorBiases.mag_x = magBias * random.uniform(-1.0, 1.0)
		sensorBiases.mag_y = magBias * random.uniform(-1.0, 1.0)
		sensorBiases.mag_z = magBias * random.uniform(-1.0, 1.0)
		sensorBiases.baro = baroBias * random.uniform(-1.0, 1.0)
		sensorBiases.pitot = pitotBias * random.uniform(-1.0, 1.0)
		sensorBiases.gps_n = 0.0
		sensorBiases.gps_e = 0.0
		sensorBiases.gps_alt = 0.0
		sensorBiases.gps_sog = 0.0
		sensorBiases.gps_cog = 0.0
		return sensorBiases


	def initializeSigmas(self, gyroSigma=VSC.gyro_sigma, accelSigma=VSC.accel_sigma, magSigma=VSC.mag_sigma,
						   baroSigma=VSC.baro_sigma, pitotSigma=VSC.pitot_sigma, gpsSigmaHorizontal=VSC.GPS_sigmaHorizontal,
						   gpsSigmaVertical=VSC.GPS_sigmaVertical, gpsSigmaSOG=VSC.GPS_sigmaSOG, gpsSigmaCOG=VSC.GPS_sigmaCOG):
		"""
		def initializeSigmas(self, gyroSigma=VSC.gyro_sigma, accelSigma=VSC.accel_sigma, magSigma=VSC.mag_sigma, baroSigma=VSC.baro_sigma, pitotSigma=VSC.pitot_sigma, gpsSigmaHorizontal=VSC.GPS_sigmaHorizontal, gpsSigmaVertical=VSC.GPS_sigmaVertical, gpsSigmaSOG=VSC.GPS_sigmaSOG, gpsSigmaCOG=VSC.GPS_sigmaCOG):
		Function to gather all of the white noise standard deviations into a single vehicleSensor class object. These will be
		used as the input to generating the white noise added to each sensor when generating the noisy sensor data.

		:param gyroSigma: gyro white noise [rad/s]
		:param accelSigma: accelerometer white noise [m/s^2]
		:param magSigma: magnetometer white noise [nT]
		:param baroSigma: barometer white noise [N/m]
		:param pitotSigma: airspeed white noise [N/m]
		:param gpsSigmaHorizontal: GPS horizontal white noise [m]
		:param gpsSigmaVertical: GPS vertical white noise [m]
		:param gpsSigmaSOG: GPS Speed over ground white noise [m/s]
		:param gpsSigmaCOG: GPS Course over ground white noise, nominal [rad]
		:return: sensorSigmas, class Sensors.vehicleSensors
		"""
		sensorSigmas = Sensors.vehicleSensors()
		sensorSigmas.gyro_x = gyroSigma
		sensorSigmas.gyro_y = gyroSigma
		sensorSigmas.gyro_z = gyroSigma
		sensorSigmas.accel_x = accelSigma
		sensorSigmas.accel_y = accelSigma
		sensorSigmas.accel_z = accelSigma
		sensorSigmas.mag_x = magSigma
		sensorSigmas.mag_y = magSigma
		sensorSigmas.mag_z = magSigma
		sensorSigmas.baro = baroSigma
		sensorSigmas.pitot = pitotSigma
		sensorSigmas.gps_n = gpsSigmaHorizontal
		sensorSigmas.gps_e = gpsSigmaHorizontal
		sensorSigmas.gps_alt = gpsSigmaVertical
		sensorSigmas.gps_sog = gpsSigmaSOG
		sensorSigmas.gps_cog = gpsSigmaCOG
		return sensorSigmas


	def updateGPSTrue(self, state, dot):
		"""
		def updateGPSTrue(self, state, dot):
		Function to update the GPS sensor state (this will be called to update the GPS data from the state and the derivative) at
		the required rate. Note that GPS reports back altitude as + above mean sea level.

		:param state: class States.vehicleState, currect vehicle state
		:param dot: class States.vehicleState, current state derivative
		:return: gps_n, [North - m], gps_e [East - m], gps_alt [Altitude - m], gps_SOG [Speed over ground, m/s], gps_COG [Course over ground, rad]
		"""
		gps_n = state.pn
		gps_e = state.pe
		gps_alt = -state.pd
		gps_sog = math.hypot(dot.pe, dot.pn)
		gps_cog = math.atan2(dot.pe, dot.pn)
		return gps_n, gps_e, gps_alt, gps_sog, gps_cog


	def updateAccelsTrue(self, state, dot):
		"""
		def updateAccelsTrue(self, state, dot):
		Function to update the accelerometer sensor. Will be called within the updateSensors functions.

		:param state: class States.vehicleState, current vehicle state
		:param dot: class States.vehicleState, current state derivative
		:return: accel_x, accel_y, accel_z, body frame specific force [m/s^2]
		"""
		g = [[0.0], [0.0], [VPC.g0]]
		gBody = MatrixMath.multiply(state.R, g)

		uvw = [[state.u], [state.v], [state.w]]
		Wx = MatrixMath.skew(state.p, state.q, state.r)
		uvwdot = [[dot.u], [dot.v], [dot.w]]
		WxV = MatrixMath.multiply(Wx, uvw)
		accels = MatrixMath.add(uvwdot, WxV)
		accels = MatrixMath.subtract(accels,gBody)

		accel_x = accels[0][0]
		accel_y = accels[1][0]
		accel_z = accels[2][0]
		return accel_x, accel_y, accel_z


	def updateMagsTrue(self, state):
		"""
		def updateMagsTrue(self, state):
		Function to update the magnetometer sensor. Will be called within the updateSensors functions.

		:param state: class States.vehicleState, current vehicle state
		:return: mag_x, mag_y, mag_z, body frame magnetic field [nT]
		"""
		mags = MatrixMath.multiply(state.R, VSC.magfield)
		mag_x = mags[0][0]
		mag_y = mags[1][0]
		mag_z = mags[2][0]
		return mag_x, mag_y, mag_z


	def updateGyrosTrue(self, state):
		"""
		def updateGyrosTrue(self, state):
		Function to update the rate gyro sensor. Will be called within the updateSensors functions.
		:param state: class States.vehicleState, current vehicle state
		:return: gyro_x, gyro_y, gyro_z, body frame rotation rates [rad/s]
		"""
		return state.p, state.q, state.r


	def updatePressureSensorsTrue(self, state):
		"""
		def updatePressureSensorsTrue(self, state):
		Function to update the pressure sensors onboard the aircraft. Will be called within the updateSensors functions.
		The two pressure sensors are static pressure (barometer) and dynamic pressure (pitot tube). The barometric pressure
		is references off of the ground static pressure in VehicleSensorConstants at Pground.

		:param state: class States.vehicleState, current vehicle state
		:return: baro, pitot in [N/m^2]
		"""
		baro = VSC.Pground + VPC.rho * VPC.g0 * state.pd
		pitot = 0.5 * VPC.rho * state.Va ** 2
		return baro, pitot


	def updateSensorsTrue(self, prevTrueSensors, state, dot):
		"""
		def updateSensorsTrue(self, prevTrueSensors, state, dot):
		Function to generate the true sensors given the current state and state derivative. Sensor suite is 3-axis accelerometer,
		3-axis rate gyros, 3-axis magnetometers, a barometric altimeter, a pitot airspeed, and GPS with an update rate specified
		in the VehicleSensorConstants file. For the GPS	update, the previous value is returned until a new update occurs. Previous
		value is contained within prevTrueSensors.

		:param prevTrueSensors:  class Sensors.vehicleSensors(), previous true sensor readings (no noise)
		:param state: class States.vehicleState, current vehicle state
		:param dot: class States.vehicleState, current state derivative
		:return: true sensor outputs (no noise, no biases), Sensors.vehicleSensors class
		"""
		sensorsTrue = Sensors.vehicleSensors()
		sensorsTrue.gyro_x, sensorsTrue.gyro_y, sensorsTrue.gyro_z = self.updateGyrosTrue(state)
		sensorsTrue.accel_x, sensorsTrue.accel_y, sensorsTrue.accel_z = self.updateAccelsTrue(state, dot)
		sensorsTrue.mag_x, sensorsTrue.mag_y, sensorsTrue.mag_z = self.updateMagsTrue(state)
		sensorsTrue.baro, sensorsTrue.pitot = self.updatePressureSensorsTrue(state)
		# check if the GPS is ready to update
		if (self.updateTicks % self.gpsTickUpdate == 0):
			# gps_n, gps_e, gps_alt, gps_sog, gps_cog
			sensorsTrue.gps_n, sensorsTrue.gps_e, sensorsTrue.gps_alt, \
			sensorsTrue.gps_sog, sensorsTrue.gps_cog = self.updateGPSTrue(state, dot)
		else:
			sensorsTrue.gps_n = prevTrueSensors.gps_n
			sensorsTrue.gps_e = prevTrueSensors.gps_e
			sensorsTrue.gps_alt = prevTrueSensors.gps_alt
			sensorsTrue.gps_sog = prevTrueSensors.gps_sog
			sensorsTrue.gps_cog = prevTrueSensors.gps_cog
		return sensorsTrue


	def updateSensorsNoisy(self, trueSensors=Sensors.vehicleSensors(), noisySensors=Sensors.vehicleSensors(),
						   sensorBiases=Sensors.vehicleSensors(), sensorSigmas=Sensors.vehicleSensors()):
		"""
		updateSensorsNoisy(self, trueSensors=Sensors.vehicleSensors(), noisySensors=Sensors.vehicleSensors(), sensorBiases=Sensors.vehicleSensors(), sensorSigmas=Sensors.vehicleSensors()):
		Function to generate the noisy sensor data given the true sensor readings, the biases, and the sigmas for the white
		noise on each sensor. The gauss markov models for the gyro biases and GPS positions are updated here. The GPS COG
		white noise must be scaled by the ratio of VPC.initialSpeed / actual ground speed. GPS is only updated if the correct
		number of ticks have gone by to indicate that a new GPS measurement should be generated. The GPS COG must be limited
		to within +/- PI. If no GPS update has occurred, then the values for the GPS sensors should be copied from the
		noisySensors input to the output.

		:param trueSensors: Sensors.vehicleSensors class, true values (no noise)
		:param noisySensors: Sensors.vehicleSensors class, previous noisy sensor values
		:param sensorBiases: Sensors.vehicleSensors class, fixed biases for each sensor
		:param sensorSigmas: Sensors.vehicleSensors class, standard deviations of white noise on each sensor
		:return: noisy sensor data, Sensors.vehicleSensors class.
		"""
		sensorsNoisy = Sensors.vehicleSensors()
		gbx, gby, gbz = self.gyroGaussMarkov.update()	# gyro bias GM update
		sensorsNoisy.gyro_x = trueSensors.gyro_x + sensorBiases.gyro_x + gbx + random.gauss(0.0, sensorSigmas.gyro_x)
		sensorsNoisy.gyro_y = trueSensors.gyro_y + sensorBiases.gyro_y + gby + random.gauss(0.0, sensorSigmas.gyro_y)
		sensorsNoisy.gyro_z = trueSensors.gyro_z + sensorBiases.gyro_z + gbz + random.gauss(0.0, sensorSigmas.gyro_z)
		sensorsNoisy.accel_x = trueSensors.accel_x + sensorBiases.accel_x + random.gauss(0.0, sensorSigmas.accel_x)
		sensorsNoisy.accel_y = trueSensors.accel_y + sensorBiases.accel_y + random.gauss(0.0, sensorSigmas.accel_y)
		sensorsNoisy.accel_z = trueSensors.accel_z + sensorBiases.accel_z + random.gauss(0.0, sensorSigmas.accel_z)
		sensorsNoisy.mag_x = trueSensors.mag_x + sensorBiases.mag_x + random.gauss(0.0, sensorSigmas.mag_x)
		sensorsNoisy.mag_y = trueSensors.mag_y + sensorBiases.mag_y + random.gauss(0.0, sensorSigmas.mag_y)
		sensorsNoisy.mag_z = trueSensors.mag_z + sensorBiases.mag_z + random.gauss(0.0, sensorSigmas.mag_z)
		sensorsNoisy.baro = trueSensors.baro + sensorBiases.baro + random.gauss(0.0, sensorSigmas.baro)
		sensorsNoisy.pitot = trueSensors.pitot + sensorBiases.pitot + random.gauss(0.0, sensorSigmas.pitot)
		# check if GPS needs updating
		if (self.updateTicks % self.gpsTickUpdate == 0):
			gpsBn, gpsBe, gpsBalt = self.gpsGaussMarkov.update()	# GPS gauss markov update
			sensorsNoisy.gps_n = trueSensors.gps_n + gpsBn + random.gauss(0.0, sensorSigmas.gps_n)
			sensorsNoisy.gps_e = trueSensors.gps_e + gpsBe + random.gauss(0.0, sensorSigmas.gps_e)
			sensorsNoisy.gps_alt = trueSensors.gps_alt + gpsBalt + random.gauss(0.0, sensorSigmas.gps_alt)
			sensorsNoisy.gps_sog = trueSensors.gps_sog + random.gauss(0.0, sensorSigmas.gps_sog)
			if math.isclose(sensorsNoisy.gps_sog, 0.0):
				sensorsNoisy.gps_cog = trueSensors.gps_cog + random.gauss(0.0, sensorSigmas.gps_cog * 100.0)
			else:
				sensorsNoisy.gps_cog = trueSensors.gps_cog + random.gauss(0.0, sensorSigmas.gps_cog * VPC.InitialSpeed / trueSensors.gps_sog)
			sensorsNoisy.gps_cog = math.fmod(sensorsNoisy.gps_cog, math.pi)	# limit to +/-PI
		else:
			sensorsNoisy.gps_n = noisySensors.gps_n
			sensorsNoisy.gps_e = noisySensors.gps_e
			sensorsNoisy.gps_alt = noisySensors.gps_alt
			sensorsNoisy.gps_sog = noisySensors.gps_sog
			sensorsNoisy.gps_cog = noisySensors.gps_cog
		return sensorsNoisy


	def update(self):
		"""
		Wrapper function to update the Sensors (both true and noisy) using the state and dot held within the self.AeroModel.
		Note that we are passing in a handle to the class for this so we don't have to explicitly use getters from the
		VehicleAerodynamics model. Sensors states are updated internally within self.

		:return: none
		"""
		self.sensorsTrue = self.updateSensorsTrue(self.sensorsTrue, self.aeroModel.getVehicleState(), self.aeroModel.getVehicleDynamicsModel().getVehicleDerivative())
		self.sensorsNoisy = self.updateSensorsNoisy(self.sensorsTrue, self.sensorsNoisy, self.sensorBiases, self.sensorSigmas)
		self.updateTicks += 1
		return


	def getSensorsTrue(self):
		"""
		Wrapper function to return the true sensor values
		
		:return: true sensor values, Sensors.vehicleSensors class
		"""
		return self.sensorsTrue


	def getSensorsNoisy(self):
		"""
		Wrapper function to return the noisy sensor values

		:return: noisy sensor data, Sensors.vehicleSensors class
		"""
		return self.sensorsNoisy


	def reset(self):
		"""
		Function to reset the module to run again. Should reset the Gauss-Markov models, re-initialize the sensor biases,
		and reset the sensors true and noisy to pristine conditions

		:return: none
		"""
		self.sensorsTrue = Sensors.vehicleSensors()
		self.sensorsNoisy = Sensors.vehicleSensors()
		self.sensorBiases = self.initializeBiases()
		self.sensorSigmas = self.initializeSigmas()
		self.gpsGaussMarkov.reset()
		self.gyroGaussMarkov.reset()
		self.updateTicks = 0
		return
	def getVPC(self):
		return VPC

	


