/* ************************************************************************** */
/** State Estimation Library

  @File Name
    StateEstimation.h

  @Summary
    This library contains all the functions used to have the autopilot estimate
    the current state of the aircraft

  @Description
    Provides state estimation functionality
 
 * Created on February 8, 2023, 1:52 PM
 */
/* ************************************************************************** */

#ifndef _STATE_ESTIMATION_H
#define	_STATE_ESTIMATION_H

/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/
#include "ICM_20948.h"
#include "NEO_M8N.h"
#include "Lin_alg_float.h"

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
#define DEFAULT_VEHICLE_STATE {.pn = 0.0,.pe = 0.0,.pd = 0.0,.u = 0.0,.v = 0.0, \
    .w = 0.0,.yaw = 0.0,.pitch =0.0,.roll = 0.0,.p = 0.0,.q = 0.0,.r = 0.0, \
            .chi = 0.0,.alpha = 0.0,.beta = 0.0,.Va = 0.0, \
            .R = {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}}}

/*******************************************************************************
 * PUBLIC TYPEDEFS                                                             *
 ******************************************************************************/

struct vehicle_state {
    float pn; // position north [m]
    float pe; // position east [m]
    float pd; // position down [m]
    float u; // speed in body x direction [m/s]
    float v; // speed in body y direction [m/s]
    float w; // speed in body z direction [m/s]
    float yaw; // yaw angle [rad]
    float pitch; // pitch angle [rad]
    float roll; // roll angle [rad]
    float p; // body roll rate about body-x axis [rad/s]
    float q; // body pitch rate about body-y axis [rad/s]
    float r; // body yaw rate about body-z axis [rad/s]
    float chi; // course angle
    float alpha; // angle of attack [rad]
    float beta; // sideslip angle [rad]
    float Va; // airspeed [rad]
    float R[3][3]; // DCM matrix
};

/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

/**
 * @Function initStateEstimation()
 * @param none
 * @return none
 * @brief  initializes state estimation
 * @author Margaret Silva */
void initStateEstimation(void);


/**
 * @Function setStateEstimationGains()
 * @param state_est_gains - array of state estimation filter gains
 *     array order: Kp_Va, Ki_Va, Kp_chi, Ki_chi, Kp_acc, Ki_acc, Kp_mag, Ki_mag
 *     note: the array has extra blank space to keep it the same length as the 
 *           mavlink packet array length
 * @return none
 * @brief  sets gains for state estimation algorithms
 * @author Margaret Silva */
void setStateEstimationGains(float state_est_gains[16]);


/**
 * @Function updateStateEstimation()
 * @param IMU output, GPS output, Barometer output
 * @param sensor_flag - indicates which sensors have updated
 * @return vehicle_state - the estimated vehicle state
 * @brief  updates state estimation given the new sensor input
 * @author Margaret Silva */
struct vehicle_state updateStateEstimation(struct IMU_out IMU_data, struct GPS_data GPS_data,
        float baro_pressure, float baro_temp, uint8_t sensor_flag);


/**
 * @Function getCurrentState()
 * @param none
 * @return current estimated vehicle state
 * @brief  returns the current state estimation
 * @author Margaret Silva */
struct vehicle_state getCurrentState(void);


/**
 * @Function getTrueState()
 * @param none
 * @return current true vehicle state
 * @brief  returns the true vehicle state
 * @author Margaret Silva */
struct vehicle_state getTrueState(void);


/**
 * @Function getDebugState()
 * @param none
 * @return current vehicle state that's a mix of true and estimated states
 * @brief  returns the current state estimation
 * @author Margaret Silva */
struct vehicle_state getDebugState(void);


/**
 * @Function setCurrentState()
 * @param state_vals - an array containing the 16 state values in the same order as the struct
 * @return none
 * @brief  sets the stored vehicle state to the values passed in as the array arg
 * @author Margaret Silva */
void setCurrentState(float state_vals[16]);


/**
 * @Function setTrueState()
 * @param state_vals - an array containing the 16 state values in the same order as the struct
 * @return none
 * @brief  sets the stored true vehicle state to the values passed in as the array arg
 * @author Margaret Silva */
void setTrueState(float state_vals[16]);


/**
 * @Function getCurrentStateArray(state_vals[16)
 * @param current estimated vehicle state as an array of the 16 state values
 * @return none
 * @brief  returns the current state estimation in a way that can be sent to the sim
 * @note: only returns state information that's actually estimated. The rest of the 
 *      packet is intended to store actuator command information
 * @author Margaret Silva */
void getCurrentStateArray(float state_vals[16]);


/**
 * @Function getDebugStateArray(state_vals[16)
 * @param current debu vehicle state as an array of the 16 state values
 * @return none
 * @brief  returns the debug state estimation in a way that can be sent to the sim
 * @note: only returns state information that's actually estimated. The rest of the 
 *      packet is intended to store actuator command information
 * @author Margaret Silva */
void getDebugStateArray(float state_vals[16]);

#endif	/* _STATE_ESTIMATION_H */

