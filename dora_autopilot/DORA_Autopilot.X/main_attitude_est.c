/* 
 * File:   main_demo.c
 * Author: Margaret Silva
 * Brief: main file for demonstrating data transfer functionality 
 *
 * Created on April 22, 2023, 2:13 PM
 */


/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

// standard libraries
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <string.h>
#include <sys/attribs.h>  //for ISR definitions
#include "xc.h"
#include "p32mx795f512l.h"

// DORA libraries
#include "AirframeConstants.h"
#include "StabilityController.h"
#include "StateEstimation.h"
#include "WaypointFollowing.h"
#include "PD.h"
#include "PI.h"
#include "PublishMavlinkMsg.h"
#include "RC_Control.h"

// OSAVC libraries
#include "SerialM32.h"
#include "Board.h"
#include "System_timer.h"
#include "Radio_serial.h"
#include "common/mavlink.h"
#include "NEO_M8N.h"
#include "RC_RX.h"
#include "RC_servo.h"
#include "ICM_20948.h"
#include "ICM_20948_registers.h"
#include "AS5047D.h"
#include "PID.h"

/*******************************************************************************
 * #DEFINES                                                                    *
 ******************************************************************************/
#define HEARTBEAT_PERIOD 1000 //1 sec interval for hearbeat update
#define SENDING_PERIOD 20 // 20 ms between 
#define ENCODER_PERIOD 10 //re-initiate encoder at 100 Hz
#define CONTROL_FREQUENCY (1000/CONTROL_PERIOD) //frequency in Hz
#define GPS_PERIOD 100 //10 Hz update rate
#define RAW 1
#define SCALED 2

// timing macros
#define SENSOR_PERIOD 10

//Sensor Macros
#define INTERFACE_MODE IMU_SPI_MODE
#define RAW 1
#define SCALED 2

// used to determine what data is being sent in an HIL actuator commands packet
#define HIL_SEND_GAINS_TO_SIM 1 // send received gains back to sim as ack
#define HIL_RECEIVE_STATE 2 // receive state data from sim
#define HIL_RECEIVE_COMMANDS 3 // receive commanded course and height
#define HIL_RECEIVE_SENSORS 4 // receive sensor data from sim
#define HIL_SEND_STATE_TO_SIM 5 // send estimated states to sim
#define HIL_ACK_COMMANDS 6 // acknowledge new commands
#define STATE_EST_RECEIVE_GAINS 7 // receive state estimation gains from sim
#define HIL_ACK_STATE_EST_GAINS 8 // acknowledge received state estimation gains
#define STABILITY_CONTROLLER_RECEIVE_GAINS 9 // receives stability controller gains from sim
#define HEARTBEAT_HIL_ACTUATOR_ERROR 255

// used to control printing speed of messages
#define CONSOLE_OUTPUT_FREQUENCY 50

// flags used to show which mode system is in
#define MANUAL_STATUS_FLAG 7
#define AUTO_STATUS_FLAG 5

/*******************************************************************************
 * VARIABLES                                                                   *
 ******************************************************************************/

// gains array from simulator
static struct controller_gains simulator_gains = DEFAULT_CONTROLLER_GAINS;

// controller commands
static float yaw_c = 0;
static float pitch_c = 0;
static float roll_c = 0;

// new state flag, used to command controller to only update when new state data is received
static uint8_t new_cmd_received = FALSE;

enum mav_output_type {
    USB,
    RADIO
};

// sensor data structs
static struct IMU_out IMU_data;
static float Baro_data;
static struct GPS_data GPS_out;
static uint8_t ready_sensors_flag = 0;
static uint8_t autonomous_status_flag = MANUAL_STATUS_FLAG;
static uint8_t first_gps_flag = 0;
static uint8_t imuReady = FALSE;

/*IMU data*/
int IMU_err = 0;
struct IMU_out IMU_data_raw = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
struct IMU_out IMU_data_scaled = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
struct IMU_out IMU_raw = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //container for raw IMU data
struct IMU_out IMU_scaled = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //container for scaled IMU data
static struct IMU_out IMU_data; //container for IMU data from the HIL simulator

// debug variables
static int gps_received_check = 0;


/*******************************************************************************
 * FUNCTION PROTOTYPES                                                         *
 ******************************************************************************/

/**
 * @function checkHILEvents(void)
 * @param none
 * @returns none
 * @brief looks for messages sent over the radio serial port to OSAVC
 * @author Margaret Silva
 */
void checkHILEvents(void);

///**
// * @function check_IMU_events(void)
// * @param none
// * @brief detects when IMU SPI transaction completes and then publishes data over Mavlink
// * @author Aaron Hunter
// */
//void check_IMU_events(void);
//
//void CalibrateSensors(void);


/*******************************************************************************
 * FUNCTIONS                                                                   *
 ******************************************************************************/

/**
 * @function checkHILEvents(void)
 * @param none
 * @returns none
 * @brief looks for messages sent over the radio serial port to OSAVC
 * @author Margaret Silva
 */
void checkHILEvents(void) {
    uint8_t channel = MAVLINK_COMM_0;
    uint8_t msg_byte;
    mavlink_message_t msg_rx;
    mavlink_status_t msg_rx_status;
    mavlink_hil_actuator_controls_t hil_sim_state;
    mavlink_statustext_t status_text;

    if (DATA_AVAILABLE()) {
        msg_byte = GET_CHAR();
        if (mavlink_parse_char(channel, msg_byte, &msg_rx, &msg_rx_status)) {
            switch (msg_rx.msgid) {
                case MAVLINK_MSG_ID_HIL_ACTUATOR_CONTROLS:
                    // used to pass in state data from simulator to controller
                    mavlink_msg_hil_actuator_controls_decode(&msg_rx, &hil_sim_state);
                    switch (hil_sim_state.flags) {
                        case HIL_RECEIVE_STATE:
                            setTrueState(hil_sim_state.controls);
                            yaw_c = radians(hil_sim_state.controls[0]);
                            pitch_c = radians(hil_sim_state.controls[1]);
                            roll_c = radians(hil_sim_state.controls[2]);
                            hil_sim_state.controls[6] = radians(hil_sim_state.controls[6]);
                            hil_sim_state.controls[7] = radians(hil_sim_state.controls[7]);
                            hil_sim_state.controls[8] = radians(hil_sim_state.controls[8]);
                            setTrueState(hil_sim_state.controls);
                            new_cmd_received = TRUE;
                            // publish_hil_act_controls(HIL_ACK_COMMANDS, hil_sim_state.controls);
                            break;
                        case HIL_RECEIVE_COMMANDS:
                            // receives data from IMU resulting angles
                            yaw_c = hil_sim_state.controls[0];
                            pitch_c = hil_sim_state.controls[1];
                            roll_c = hil_sim_state.controls[2];
                            // new_cmd_received = TRUE;
                            // publish_hil_act_controls(HIL_ACK_COMMANDS, hil_sim_state.controls);
                            break;
                        case STABILITY_CONTROLLER_RECEIVE_GAINS: // controller gains
                            initStabilityController();
                            initStateEstimation();
                            first_gps_flag = 0;
                            setStabilityControllerGains(hil_sim_state.controls);
                            publish_hil_act_controls(HIL_SEND_GAINS_TO_SIM, hil_sim_state.controls);
                            break;
                        case STATE_EST_RECEIVE_GAINS: // state estimation gains
                            setStateEstimationGains(hil_sim_state.controls);
                            initStabilityController();
                            initStateEstimation();
                            first_gps_flag = 0;
                            publish_hil_act_controls(HIL_ACK_STATE_EST_GAINS, hil_sim_state.controls);
                            break;
                        default:
                            publish_heartbeat(HEARTBEAT_HIL_ACTUATOR_ERROR);
                            break;
                    }
                    break;
                case MAVLINK_MSG_ID_STATUSTEXT:
                    // debug message
                    mavlink_msg_statustext_decode(&msg_rx, &status_text);
                    if (strcmp(status_text.text, "I AM SIM") == 0) {
                        publish_status_text("I AM AUTOPILOT");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

/**
 * @function check_IMU_events(void)
 * @param none
 * @brief detects when IMU SPI transaction completes and then publishes data over Mavlink
 * @author Aaron Hunter
 */
void check_IMU_events(void) {
    if (IMU_is_data_ready() == TRUE) {
        imuReady = TRUE;
        IMU_get_raw_data(&IMU_data_raw);
        IMU_get_norm_data(&IMU_scaled);

        IMU_scaled.acc.x = -((IMU_data_raw.acc.x - BIAS_ACCX) * SCALE_ACCX) * ms2;
        IMU_scaled.acc.y = -((IMU_data_raw.acc.y - BIAS_ACCY) * SCALE_ACCY) * ms2;
        IMU_scaled.acc.z = -((IMU_data_raw.acc.z - BIAS_ACCZ) * SCALE_ACCZ) * ms2;

        //Calibrating gyros
        float angularX = (IMU_data_raw.gyro.x / CONV_GYRO) - OFFSET_GYROX;
        float angularY = (IMU_data_raw.gyro.y / CONV_GYRO) - OFFSET_GYROY;
        float angularZ = (IMU_data_raw.gyro.z / CONV_GYRO) - OFFSET_GYROZ;
        IMU_scaled.gyro.x = radians(angularX);
        IMU_scaled.gyro.y = -radians(angularY);
        IMU_scaled.gyro.z = -radians(angularZ);
        //        IMU_scaled.gyro.x += angularX * dT_GYRO - DRIFT_GYROX;
        //        IMU_scaled.gyro.y += angularY * dT_GYRO - DRIFT_GYROY;
        //        IMU_scaled.gyro.z += angularZ * dT_GYRO - DRIFT_GYROZ;


        //Calibrating magnetometer
        IMU_scaled.mag.x = IMU_scaled.mag.x * 22.6338;
        IMU_scaled.mag.y = IMU_scaled.mag.y * 5.1879;
        IMU_scaled.mag.z = IMU_scaled.mag.z * 41.2284;

    }
}

/*******************************************************************************
 * MAIN                                                                        *
 ******************************************************************************/

int main() {
    // system values
    uint32_t cur_time = 0;
    uint32_t warmup_time = 250; //time in ms to allow subsystems to stabilize (IMU))
    uint32_t print_start_time = 0;
    uint32_t heartbeat_start_time = 0;
    uint32_t log_start_time = 0;
    float angles[3];

    // initialize board, serial, and radio
    Board_init(); //board configuration
    Serial_init(); //start debug terminal (USB)
    Sys_timer_init(); //start the system timer
    /*small delay to get all the subsystems time to get online*/
    while (cur_time < warmup_time) {
        cur_time = Sys_timer_get_msec();
    }

    IMU_err = IMU_init(INTERFACE_MODE);

    //Initialize communication and actuators
    Radio_serial_init(); //start the radios
    RCRX_init(); //initialize the radio control system
    RC_channels_init(); //set channels to midpoint of RC system
    RC_servo_init(RC_SERVO_TYPE, AIL_PWM); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, ELE_PWM); // start the servo subsystem
    RC_servo_init(ESC_UNIDIRECTIONAL_TYPE, THR_PWM); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, RUD_PWM); // start the servo subsystem

    printf("\r\n\nDemo application %s, %s \r\n", __DATE__, __TIME__);

    initStabilityController();
    initStateEstimation();

    while (1) {
        cur_time = Sys_timer_get_msec();
        check_IMU_events(); //Check sensors

        if (cur_time - log_start_time >= SENSOR_PERIOD) { //every 20 ms
            IMU_start_data_acq();
            // update state estimation
            if (imuReady) {
                updateStateEstimation(IMU_scaled, GPS_out, 0, 0, IMU_AVAILABLE);
                log_start_time = cur_time;
                imuReady = FALSE;
            }
        }

        // publish heartbeat
        if (cur_time - heartbeat_start_time >= HEARTBEAT_PERIOD) {
            heartbeat_start_time = cur_time; //reset the timer
            // publish_heartbeat(autonomous_status_flag);
            publish_heartbeat(0);
        }

        if (cur_time - print_start_time >= 100) { //every 20 ms
            // human readable
            // printf("est:  y: %0.2f, p: %0.2f, r: %0.2f\n", degrees(getCurrentState().yaw),
            //      degrees(getCurrentState().pitch), degrees(getCurrentState().roll));

            // matlab readable
            // printf("%0.2f,%0.2f, %0.2f;\n", degrees(getCurrentState().yaw),
            //      degrees(getCurrentState().pitch), degrees(getCurrentState().roll));
            angles[0] = degrees(getCurrentState().yaw);
            angles[1] = degrees(getCurrentState().pitch);
            angles[2] = degrees(getCurrentState().roll);
            publish_euler_angles(angles);

            // sensor data
            // printf("gyro: x: %0.2f, y: %0.2f, z: %0.2f\n",
            //      IMU_scaled.gyro.x, IMU_scaled.gyro.y, IMU_scaled.gyro.z);
            // printf("accel: x: %0.2f, y: %0.2f, z: %0.2f\n",
            //      IMU_scaled.acc.x, IMU_scaled.acc.y, IMU_scaled.acc.z);
            print_start_time = cur_time;
        }
    }
}
