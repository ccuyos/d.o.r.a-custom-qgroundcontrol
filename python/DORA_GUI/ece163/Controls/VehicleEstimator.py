"""
File that implements the Estimation schemes required to perform closed loop control using the
sensor outputs, rather than access directly to the vehicle state. We implement the attitude estimation
using the complementary filter (Mahoney form) outlined in the estimation cheat sheet. The course over
ground estimation will likewise be a complementary filter fusing heading rate (\dot{\psi}) and the GPS
Course-over-Ground (COG). For Vehicle speed over ground, we will need to implement an estimator for the
wind based on course turns to predict the wind, using the Palmeiere forumation.

This file will create an estimated state which is then fed into the VehicleClosedLoop controls using our
same successive loop closure autopilot structure.
"""

import math
import sys

sys.path.append('..')

import ece163.Containers.Inputs as Inputs
import ece163.Containers.Controls as Controls
import ece163.Containers.Sensors as Sensors

import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Modeling.VehicleAerodynamicsModel as VehicleAerodynamicsModule
import ece163.Sensors.SensorsModel as SensorsModel

class VehicleEstimator():
    def __init__(self, dT = VPC.dT):
        """
        Class that contains everything needed to do the Estimation for recreating the vehicle state from
        the sensor readings.

        [TODO: This needs to be very much fleshed out]
        """
        return
    
    def reset(self):
        """
        Resets the estimator class so that the internal states are returned to their native contents
        and the estimator can run anew.
        """
        return
    
    def updateComplementaryAttitudeFilter(self, sensorData = Sensors.vehicleSensors()):
        """
        Computes the complementary filter for attitude estimation, returns the estimated rates [p,q,r],
        the estimated DCM (R), and the gyro biases [bx, by, bz]. The Euler angles are extracted from the
        estimated DCM.
        """
        return

    
    def update(self):
        """
        Creates the estimate of the state, updates it.
        """
        return
        