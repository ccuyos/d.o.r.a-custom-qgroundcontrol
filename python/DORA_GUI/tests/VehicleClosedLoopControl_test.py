"""
Test file to check that the closed loop control updated correctly
"""

import math
import sys
sys.path.append('..')
import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt

import ece163.Controls.VehicleTrim as VehicleTrim
import ece163.Containers.States as States
import ece163.Containers.Inputs as Inputs
import ece163.Containers.Controls as Controls
import ece163.Controls.VehiclePerturbationModels as VehiclePerturbationModels
import ece163.Controls.VehicleClosedLoopControl as VehicleClosedLoopControl
import ece163.Controls.VehicleControlGains as VehicleControlGains
import ece163.Utilities.MatrixMath as MatrixMath

vClp = VehicleClosedLoopControl.VehicleClosedLoopControl()
vGains = VehicleControlGains.VehicleControlGains()
vPb = VehiclePerturbationModels.VehiclePerturbation()

trimControls = Inputs.controlInputs()
trimState = States.vehicleState()

vTrim = VehicleTrim.VehicleTrim()
Vastar = 25.0
Gammastar = math.radians(0.0)
Kappastar = 0.0

check = vTrim.computeTrim(Vastar, Kappastar, Gammastar)
if check:
	print("Optimization successful")
else:
	print("Model converged outside of valid inputs, change parameters and try again")

print("Controls:")
trimControls = vTrim.getTrimControls()
print(trimControls)

print("State:")
trimState = vTrim.getTrimState()
print(trimState)

vPb.setTrimStateandInputs(trimState, trimControls)

vPb.CreateTransferFunction()
print(vPb.transferFunction)

vGains.setLinearizedModel(vPb.getTransferFunction())

tuningParams = Controls.controlTuning()
vControlGains = VehicleControlGains.VehicleControlGains()

tuningParams.Wn_roll = 20
tuningParams.Zeta_roll = 1.0 / math.sqrt(2)

tuningParams.Wn_course = tuningParams.Wn_roll / 20.0
tuningParams.Zeta_course = 1.0

tuningParams.Wn_sideslip = tuningParams.Wn_roll / 2.0
tuningParams.Zeta_sideslip = 0.707

tuningParams.Wn_pitch = 24.0
tuningParams.Zeta_pitch = 0.707

tuningParams.Wn_altitude = tuningParams.Wn_pitch / 30.0
tuningParams.Zeta_altitude = 1.0

tuningParams.Wn_SpeedfromThrottle = 3.0
tuningParams.Zeta_SpeedfromThrottle = 2

tuningParams.Wn_SpeedfromElevator = tuningParams.Zeta_SpeedfromThrottle
tuningParams.Zeta_SpeedfromElevator = tuningParams.Zeta_SpeedfromThrottle

vGains.computeGains(tuningParams)
print(vGains.controlGains)

vClp.setTrimInputs(trimControls)
vClp.setControlGains(vGains.getControlGains())
vClp.setVehicleState(trimState)

print(vClp.rollFromCourse)
Hz = 100
time = 0

refCommands = Controls.referenceCommands()
refCommands.commandedCourse = 0	# meters
commandOffset = -20
refCommands.commandedAirspeed = Vastar
refCommands.commandedAltitude = 100.0
datalist = [[time, refCommands.commandedAltitude, refCommands.commandedAirspeed, math.degrees(refCommands.commandedCourse),
	  math.degrees(refCommands.commandedPitch), math.degrees(refCommands.commandedRoll), -vClp.vehicleAerodynamics.vehicleDynamics.state.pd,
	  vClp.vehicleAerodynamics.vehicleDynamics.state.Va, math.degrees(vClp.vehicleAerodynamics.vehicleDynamics.state.chi),
	  math.degrees(vClp.vehicleAerodynamics.vehicleDynamics.state.pitch), math.degrees(vClp.vehicleAerodynamics.vehicleDynamics.state.roll)]]

for x in range(0, 100 * Hz):
	time += vClp.dT
	vClp.Update(refCommands)
	d = [time, refCommands.commandedAltitude, refCommands.commandedAirspeed, math.degrees(refCommands.commandedCourse),
	  math.degrees(refCommands.commandedPitch), math.degrees(refCommands.commandedRoll), -vClp.vehicleAerodynamics.vehicleDynamics.state.pd,
	  vClp.vehicleAerodynamics.vehicleDynamics.state.Va, math.degrees(vClp.vehicleAerodynamics.vehicleDynamics.state.chi),
	  math.degrees(vClp.vehicleAerodynamics.vehicleDynamics.state.pitch), math.degrees(vClp.vehicleAerodynamics.vehicleDynamics.state.roll)]

	if (x%1000 == 0):
		refCommands.commandedCourse += commandOffset
		commandOffset *= -1

	# print(" ".join('{: 7.3f}'.format(t) for t in d))
	datalist.append(d)

t = [row[0] for row in datalist]
Hc = [row[1] for row in datalist]
Vc = [row[2] for row in datalist]
ChiC = [row[3] for row in datalist]
TheC = [row[4] for row in datalist]
PhiC = [row[5] for row in datalist]
H = [row[6] for row in datalist]
Va = [row[7] for row in datalist]
Chi = [row[8] for row in datalist]
The = [row[9] for row in datalist]
Phi = [row[10] for row in datalist]

mpl.rcParams['legend.fontsize'] = 10
fig = plt.figure(1)
plt.plot(t,Hc,t,H)

fig = plt.figure(2)
plt.plot(t,Vc,t,Va)

fig = plt.figure(3)
plt.plot(t,ChiC,t,Chi)

fig = plt.figure(4)
plt.plot(t,TheC,t,The)

fig = plt.figure(5)
plt.plot(t,PhiC,t,Phi)
plt.show()
