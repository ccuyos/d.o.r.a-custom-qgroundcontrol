'''
/*
 * File:   mavlinkConsoleWidget.py
 * Author: Carl Vincent Cuyos
 * Brief:  widget used for HIL testing using MAVLINK protocol
 * project
 * Created on May 1, 2023
 *
'''
import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets
import sys
from . import vehicleDisplay
from ..Containers import States
import math
import traceback
import os
import datetime


mavlinkThreadRate = 20

## DORA Addition ##
import ece163.Constants.MAVLINKConstants as mvc
import ece163.Containers.Inputs as Inputs
from pymavlink import mavutil
from enum import Enum
import time
import pickle

class EstimatedGains():
	def __init__(self):
		self.estDict = mvc.estDict




class mavlinkConsoleTab(QtWidgets.QWidget):
	def __init__(self, parent=None):
		super().__init__(parent)

		overallClosure = QtWidgets.QHBoxLayout()
		self.setLayout(overallClosure)


		### FOR SPACING ####
		spacerItem_small = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
		spacerItem_medium = QtWidgets.QSpacerItem(50, 50, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
		spacerItem_large = QtWidgets.QSpacerItem(100, 100, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
		spacerItem_extreme = QtWidgets.QSpacerItem(500, 500, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
		##### FOR CONSOLES #####
		self.usedLayout = QtWidgets.QVBoxLayout()

		#self.setLayout(self.usedLayout)
		self.mytime = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")

		# Adds one widget to the layout
		self.generalConsole = QtWidgets.QLabel("General Console")
		#self.generalConsole.setGeometry(QtCore.QRect(20, 340, 1, 1))
		self.usedLayout.addWidget(self.generalConsole)
		self.mavlinkConsole = QtWidgets.QPlainTextEdit("MAVLINK Console Compiled at {}\n".format(self.mytime))
		self.mavlinkConsole.setReadOnly(True)

		self.usedLayout.addWidget(self.mavlinkConsole)

		# Add a console for displaying incoming messages
		self.usedLayout.addWidget(QtWidgets.QLabel("MAVLINK Messages"))
		self.consoleRX = QtWidgets.QPlainTextEdit("MAVLINK Message Console Compiled at {}\n".format(self.mytime))
		self.consoleRX.setReadOnly(True)
		self.usedLayout.addWidget(self.consoleRX)

		# Add a console for display waypoints stored
		self.waypointText = QtWidgets.QLabel("Mission Path Console")
		self.usedLayout.addWidget(self.waypointText)
		self.waypointConsole = QtWidgets.QPlainTextEdit("0,0, -100")
		self.waypointConsole.setReadOnly(True)

		self.usedLayout.addWidget(self.waypointConsole)


		overallClosure.addLayout(self.usedLayout)

		## Add spacing
		overallClosure.addItem(spacerItem_small)

		##### FOR BUTTONS
		# Create another vertical box
		self.usedLayout2 = QtWidgets.QVBoxLayout()
		self.usedLayout2.addWidget(QtWidgets.QLabel("MAVLINK Buttons"))
		# Adding debug button
		self.debugmavlinkButton = QtWidgets.QPushButton("MAVLINK Debug")
		self.debugmavlinkButton.clicked.connect(self.runDebug)
		self.usedLayout2.addWidget(self.debugmavlinkButton)
		# Adding gains button
		self.mavlinkGainsButton = QtWidgets.QPushButton("MAVLINK Send Gains")
		self.mavlinkGainsButton.clicked.connect(self.runSendGains)
		self.usedLayout2.addWidget(self.mavlinkGainsButton)
		# Adding est gains button
		self.mavlinkEstGainsButton = QtWidgets.QPushButton("MAVLINK Send Est. Gains")
		self.mavlinkEstGainsButton.clicked.connect(self.runSendEstGains)
		self.usedLayout2.addWidget(self.mavlinkEstGainsButton)
		# Adding save est gains button
		self.mavlinkEstGainsSave = QtWidgets.QPushButton("MAVLINK Save Est. Gains")
		self.mavlinkEstGainsSave.clicked.connect(self.saveEstGains)
		self.usedLayout2.addWidget(self.mavlinkEstGainsSave)
		# Adding add waypoint button
		self.waypointButton = QtWidgets.QPushButton("MAVLINK Send Waypoint")
		self.waypointButton.clicked.connect(self.runSendWaypoint)

		self.usedLayout2.addWidget(self.waypointButton)

		# Adding status display for buttons widget
		self.buttonsStatusText = QtWidgets.QLabel("Status: NaN")
		self.usedLayout2.addWidget(self.buttonsStatusText)

		# Adding status display for state of plane
		self.autopilotStatusText = QtWidgets.QLabel("Heartbeat: N/A")
		self.usedLayout2.addWidget(self.autopilotStatusText)

		# Adding heartbeat display for state of pic
		self.gimbalStatus = QtWidgets.QLabel("Heartbeat#2: N/A")
		self.usedLayout2.addWidget(self.gimbalStatus)

		# Adding status display for yaw, pitch, roll
		## Add spacing
		self.usedLayout2.addStretch()
		overallClosure.addLayout(self.usedLayout2)
		overallClosure.addItem(spacerItem_small)

		#### FOR EDITORS
		self.usedLayoutEditor = QtWidgets.QFormLayout()
		# Add title
		estGainSection = QtWidgets.QLabel('State Estimation Gains')
		estGainSection.setAlignment((QtCore.Qt.AlignLeft))
		self.usedLayoutEditor.addRow(estGainSection)

		## Create state estimation gains inputs
		self.estGains = EstimatedGains()

		try:
			with open(os.path.join(sys.path[0], mvc.defaultEstGainsFileName), 'rb') as f:
				self.estGains = pickle.load(f)
		except (FileNotFoundError, EOFError):
			pass


		self.estGainsDict = dict()
		for i in mvc.estGainsNames:
			self.createTextInput(i, self.estGains.estDict[i], self.usedLayoutEditor, overallClosure, self.estGainsDict)

		## Add title for waypoints
		waypointSection = QtWidgets.QLabel('Waypoint Input')
		waypointSection.setAlignment((QtCore.Qt.AlignLeft))
		self.usedLayoutEditor.addRow(waypointSection)

		## Create waypoint dict
		self.waypointDict = dict()
		for i in mvc.waypointNames:
			self.createTextInput(i, mvc.waypointDict[i], self.usedLayoutEditor, overallClosure, self.waypointDict)



		## Add spacing
		overallClosure.addItem(spacerItem_small)

		# For further information
		if mvc.GUI_mode == mvc.advHILMode:
			### For Actuator Controls
			self.usedLayout3 = QtWidgets.QVBoxLayout()

			### For Estimated States
			self.usedLayout4 = QtWidgets.QVBoxLayout()

			### For Sensor Display
			overallClosure.addItem(spacerItem_small)
			self.usedLayout5 = QtWidgets.QVBoxLayout()
			self.usedLayout5.addWidget(QtWidgets.QLabel("Sensor Diagnostics"))
			self.acc_text = QtWidgets.QLabel(mvc.accText.format(0,0,0))
			self.gyro_text = QtWidgets.QLabel(mvc.gyroText.format(0,0,0))
			self.mag_text = QtWidgets.QLabel(mvc.magText.format(0, 0, 0))
			self.baro_text = QtWidgets.QLabel(mvc.baroText.format(0))
			self.gps_text = QtWidgets.QLabel(mvc.gpsText.format(0,0,0,0,0))
			self.usedLayout5.addWidget(self.acc_text)
			self.usedLayout5.addWidget(self.gyro_text)
			self.usedLayout5.addWidget(self.mag_text)
			self.usedLayout5.addWidget(self.baro_text)
			self.usedLayout5.addWidget(self.gps_text)
			self.usedLayout5.addStretch()
			overallClosure.addLayout(self.usedLayout5)
		overallClosure.addStretch()

		### END FORMATTING #####

		## MAVLINK communication variables ##
		self.master = 0 # OSAVC master
		self.master2 = 0 # Gimbal master
		self.master3 = 0 # CoolTerm master
		self.heartbeatStatus = mvc.HIL_HEARTBEAT_DEFAULT
		self.simCount = 0
		self.currentHeartbeat = 0
		self.prevHeartbeat = 0

		## MAVLINK Pointers from other widgets
		self.gainsPt = 0
		self.refPt = 0
		self.simPt = 0
		self.dispPt = vehicleDisplay.vehicleDisplay()
		self.curGains = [0 for i in range(0, 16)]

		## Commanded variables ##
		self.commandedCourse = 0
		self.commandedAltitude = 0
		self.commandedAirspeed = 0

		## GPS variables ##
		self.gps_lat = 0
		self.gps_lon = 0
		self.gps_altitude = 0
		self.gps_speed = 0
		self.gps_course = 0

		## Control surface variables##
		self.thr = 0
		self.ele = 0
		self.ail = 0
		self.rud = 0

		## Reference commands variables
		self.commandedYaw = 0
		self.commandedPitch = 0
		self.commandedRoll = 0
		self.actualYaw = 0
		self.actualPitch = 0
		self.actualRoll = 0
		self.estYaw = 0
		self.estPitch = 0
		self.estRoll = 0
		self.imuFile = open("Yaw_t1.txt", "w")


		## Autopilot variables ##
		self.newControls = [0 for i in range(0, 16)]
		self.newStates = [0 for i in range(0, 16)]
		self.estStates = [0 for i in range(0,16)] #
		self.noisy_sensor = [0 for i in range(0, 16)]
		self.actual_sensor = [0 for i in range(0,16)]
		self.sensor_label = ['acc_x',
							 'acc_y',
							 'acc_z',
							 'gyro_x',
							'gyro_y',
							 'gyro_z',
							'mag_x',
							 'mag_y',
							 'mag_z',
							'baro',
							 'lat',
							 'lon',
							 'alt',
							 'sog',
							 'cog', 'NaN']
		self.mav_msg = mvc.mavlinkDebugMsg

		## Flags
		self.readyFlag = False
		self.commandedFlag = False
		self.checkHeartbeat = False
		self.checkGPS = False
		self.ackGPS = False
		self.boolGPS = 0

		# Period
		self.statePeriod = 0
		self.refPeriod = 0

		self.gpsList = [0, 0, 0, 0, 0] # GPS time stamps
		self.gpsRXList = [0, 0, 0, 0, 0] # GPS RX stamps
		self.gpsTXList = [0,0,0,0,0] # GPS TX stamps

		# Waypoint variables
		self.waypointList = list()

		return

	def mavlink_init(self):
		"""
		:Description: used for initializing the MAVLINK connection between autopilot and simulator.
		The user may also use this for initializing required threads and/or other connections
		to the ground station
		:param none
		:return: none
		"""

		# Check if mode is autopilot mode
		if mvc.HIL_mode == mvc.autopilotMode:
			# Initialize connection for plane and gimbal
			self.mavlinkConsole.appendPlainText("ECE163 Simulator: Autopilot Mode")
			self.master = mavutil.mavlink_connection(mvc.com, baud=mvc.baudrate)
			self.master.wait_heartbeat(timeout=2)

			self.master2 = mavutil.mavlink_connection(mvc.com2, baud=mvc.baudrate)
			self.master2.wait_heartbeat(timeout=2)

			## Initialize MAVLINK threads
			self.RxThread = QtCore.QTimer()
			self.TxThread = QtCore.QTimer()
			self.RxThread.setInterval(mavlinkThreadRate)
			self.TxThread.setInterval(mavlinkThreadRate)
			self.RxThread.timeout.connect(self.runMAVLINK_RX)
			self.TxThread.timeout.connect(self.runMAVLINK_TX)
			# Begin MAVLINK protocol
		elif mvc.HIL_mode == mvc.simMode:
			self.mavlinkConsole.appendPlainText("ECE163 Simulator: Simulation Mode")
	def mavlink_start(self):
		"""
		:Description: used for starting the MAVLINK protocol which requires enabling protocol threads
		:param none
		:return: none
		"""
		# Check if mode is autopilot
		if mvc.HIL_mode == mvc.autopilotMode:
			# Print out the target system found by the first connection
			self.mav_msg = 'target_system {}, target component {}'.format \
											 (self.master.target_system, self.master.target_component)

			# Begin threads
			self.RxThread.start()
			if mvc.HIL_command_mode == mvc.ece167_mode:
				self.TxThread.start()
			# Send dummy state for autopilot
			self.master.mav.hil_actuator_controls_send(
				time_usec=0,
				controls=self.newStates,
				mode=32,
				flags=mvc.HIL_STATECOMS
			)

			# Send dummy sensor for autopilot
			self.master.mav.hil_actuator_controls_send(
				time_usec = 0, controls = self.noisy_sensor, mode = 32, flags = mvc.HIL_SENSOR
			)
			# Use mavprint function for showing establish connection
			self.mavprint(self.mav_msg)
		else:
			pass
	def mavlink_end(self):
		"""
		:Description: used for ending the MAVLINK protocol which requires disabling protocol threads
		:param none
		:return: none
		"""
		if mvc.HIL_mode == mvc.autopilotMode:
			self.RxThread.stop()
			self.imuFile.close()
	def mavprint(self, mavlinkText, com = mvc.msg_default, type = mvc.type_default):
		"""
		:Description: used for printing MAVLINK packets to the consoles.
		:param: mavlinkText: string data to be displayed from the MAVLINK packet
				com: enum of console to display MAVLINK packet. Defaults to general console.
				type: enum for message type. Can be used for debugging purposes
		:return: none
		"""
		if (com == mvc.msg_default):
			self.mavlinkConsole.appendPlainText(mavlinkText)
		elif (com == mvc.msg_tx):
			self.consoleTX.appendPlainText(mavlinkText)
		elif (com == mvc.msg_rx):
			self.consoleRX.appendPlainText(mavlinkText)

	def printPacket(self, packet, com = mvc.msg_default, msg = 'NaN message available', label = ['Param#{}'.format(i) for i in range (0, 16)]):
		"""
		:Description: used for printing an entire MAVLINK packet array. Typically used for message frames in lists.
		:param: packet: list containing values to be printed from MAVLINK packet
				msg: string indicating message packet's origin (i.e. from HEARTBEAT)
				label: list containing names associated with the values to be printed from MAVLINK packet
		:return: none
		"""
		self.mavprint(msg, com)
		for i in range(len(packet)):
			self.mav_msg = "{}:{}".format(label[i], packet[i])
			self.mavprint(self.mav_msg, com)

	def runMAVLINK_RX(self):
		"""
		:Description: main function used for running the receive protocol for the MAVLINK communication.
					  Checks for received message using .recv_match() and handles message processing through type or by
					  given flags by the developer.
		:param none
		:note: See MAVLINKConstants.py for adjusting flag enums and packet frames.
		:return: none
		"""
		if mvc.HIL_mode == mvc.autopilotMode:
			self.mav_msg = "{0:.2f},{1:.2f},{2:.2f},{3:.2f},{4:.2f},{5:.2f}\n".format(self.actualYaw, self.actualPitch,
																					  self.actualRoll, self.estYaw,
																					  self.estPitch, self.estRoll)
			self.imuFile.write(self.mav_msg)
			# Get received message from master
			msg = self.master.recv_match()
			msg2 = self.master2.recv_match()
			# Check if message is empty
			if not msg and msg2:
				return
			# Check if there is communication between the autopilot and the simulator
			self.simCount += 1
			if self.simCount > 100:
				self.autopilotStatusText.setText("Heartbeat: Inactive")
			elif self.simCount <= 1:
				self.autopilotStatusText.setText("Heartbeat: Active")
			# Parse the message based on the type
			try:
				if msg.get_type() == mvc.HEARTBEAT:
					self.simCount = 0
					self.master.mav.heartbeat_send(type = mavutil.mavlink.MAV_TYPE_FIXED_WING,
												   autopilot = mavutil.mavlink.MAV_AUTOPILOT_GENERIC,
												   base_mode = mavutil.mavlink.MAV_MODE_FLAG_HIL_ENABLED,
												   custom_mode = 0,
												   system_status = mavutil.mavlink.MAV_STATE_ACTIVE,
												   mavlink_version = 2)

				elif msg.get_type() == mvc.HIL_CONTROLS:
					if msg.mode == mvc.HIL_CONTROLS_EULER_PLANE:
						self.estYaw = msg.roll_ailerons
						self.estPitch = msg.pitch_elevator
						self.estRoll = msg.yaw_rudder

				elif msg.get_type() == mvc.HIL_FLOAT_PACKET:
					if msg.flags == mvc.HIL_ACKGAINS:
						self.mav_msg = "Received Gains"
						self.printPacket(msg.controls, mvc.msg_rx, self.mav_msg)
						self.mavprint(self.mav_msg, mvc.msg_rx)

					elif msg.flags == mvc.HIL_STATE_EST:
						self.mav_msg = "Estimated States"
						self.estStates = msg.controls
						self.ail = self.estStates[mvc.cmd_ail]
						self.ele = self.estStates[mvc.cmd_ele]
						self.rud = self.estStates[mvc.cmd_rud]
						self.thr = self.estStates[mvc.cmd_thr]
						self.ackGPS = self.estStates[mvc.ack_gps]
						self.boolGPS = self.estStates[mvc.ack_gpsBool]
						self.readyFlag = True
						#self.printPacket(msg.controls, mvc.msg_rx, self.mav_msg)
					elif msg.flags == mvc.HIL_SENSOR:
						self.noisy_sensor = msg.controls
						self.readyFlag = True
					elif msg.flags == mvc.HIL_ACKCOMS:
						self.mav_msg = "Received Comms"
						#self.printPacket(msg.controls, mvc.msg_rx, self.mav_msg)
					elif msg.flags == mvc.HIL_ACKESTGAINS:
						self.mav_msg = "Received Estimated Gains"
						self.printPacket(msg.controls, mvc.msg_rx, self.mav_msg)
					elif msg.flags == mvc.HIL_ACKWAYPOINTS:
						self.mav_msg = "Received Waypoints"
						self.mavprint(self.mav_msg, mvc.msg_rx)

					elif msg.flags == mvc.HIL_ERROR:
						self.mav_msg = "Error acknowledgement from autopilot"
						self.mavprint(self.mav_msg, mvc.msg_rx)

				elif msg.get_type() == mvc.HIL_DEBUG:
					if msg.text == mvc.mavlinkDebugMsgAck:
						self.mavprint("Systems alive", mvc.msg_rx)

			except:
				pass

			self.currentHeartbeat += 1
			if self.currentHeartbeat > 100:
				self.gimbalStatus.setText("Heartbeat#2: Inactive")
			elif self.currentHeartbeat <= 1:
				self.gimbalStatus.setText("Heartbeat#2: Active")

			try:
				if msg2.get_type() == mvc.HIL_CONTROLS:
					if msg2.mode == mvc.HIL_CONTROLS_REFCOMS:
						self.refCourse = msg2.roll_ailerons
						self.refAltitude = msg2.pitch_elevator
					elif msg2.mode == mvc.HIL_CONTROLS_129COMS:
						self.mav_msg = "Received Ack from #2"
						self.mavprint(self.mav_msg, mvc.msg_rx)
					elif msg2.mode == mvc.HIL_CONTROLS_EULER_GIMBAL:
						self.actualYaw = msg2.roll_ailerons
						self.actualPitch = msg2.pitch_elevator
						self.actualRoll = msg2.yaw_rudder

				elif msg2.get_type() == mvc.HEARTBEAT:
					self.currentHeartbeat = 0
			except:
				pass
		elif mvc.HIL_mode == mvc.simMode:
			pass

	def runMAVLINK_TX(self):
		"""
		:Description: main function used for running the transmit protocol for the MAVLINK communication.
					  Transmits all stored values from simulation update. If developer encounters serial overflow,
					  make sure to implement some form of period check to control MAVLINK packet rate.
	    :param none
	    :note: If dealing with multiple connections, make sure to thoroughly
	    	   confirm if connection requires the transmitted message
		:return: none
		"""
		# Send new states to the autopilot. Check if 10 sim periods has passed before sending updated sim states
		if mvc.HIL_mode == mvc.autopilotMode:
			self.statePeriod += 1
			if self.statePeriod > 15:
				self.statePeriod = 0
				self.master2.mav.hil_actuator_controls_send(
					time_usec=0, controls=self.newStates, mode=32, flags=mvc.HIL_STATECOMS
				)
			#Send new sensor values to autopilot
			self.master.mav.hil_actuator_controls_send(
				time_usec = 0, controls = self.noisy_sensor, mode = 32, flags = mvc.HIL_SENSOR
			)
			# Send new reference commands to the autopilot
			self.refPeriod +=1
			if self.commandedFlag and self.refPeriod > 1:
				self.refPeriod = 0
				self.master.mav.hil_actuator_controls_send(
						time_usec = 0,
						controls = [self.commandedCourse, self.commandedAltitude, self.commandedAirspeed] + [0 for i in range(0,13)],
						mode = 1,
						flags = mvc.HIL_REFCOMS
				)
			# Reset ready flag for the simulation
			self.readyFlag = False

		if mvc.GUI_mode == mvc.advHILMode:
			self.runSensorUpdate()




	def runDebug(self):
		"""
		:Description: main function used for responding to a debug input from user.
					  Allows the user to perform a quick check on the link between ground station
					  and autopilot.
		:param none
		:note: See MAVLINKConstants.py to adjust debug message used for communication
		:return: none
		"""
		if mvc.HIL_mode == mvc.simMode:
			return
		self.master.mav.statustext_send(0, mvc.mavlinkDebugMsg.encode(), 0)
	def runSendGains(self):
		"""
		:Description: main function used for responding
		:param none
		:note: See MAVLINKConstants.py to adjust debug message used for communication
		:return: none
		"""
		if mvc.HIL_mode == mvc.simMode:
			self.buttonsStatusText.setText("Gains Applied")
			self.gainsPt.applyGains()
			return
		curGains = self.gainsPt.buildCurrentGains()
		gainsLib = {'p_roll': curGains.kp_roll,
						 'd_roll': curGains.kd_roll,
						 'i_roll': curGains.ki_roll,
						 'p_side': curGains.kp_sideslip,
						 'i_side': curGains.ki_sideslip,
						 'p_course': curGains.kp_course,
						 'i_course': curGains.ki_course,
						 'p_pitch': curGains.kp_pitch,
						 'd_pitch': curGains.kd_pitch,
						 'p_alt': curGains.kp_altitude,
						 'i_alt': curGains.ki_altitude,
						 'p_tSpeed': curGains.kp_SpeedfromThrottle,
						 'i_tSpeed': curGains.ki_SpeedfromThrottle,
						 'p_eSpeed': curGains.kp_SpeedfromElevator,
						 'i_eSpeed': curGains.ki_SpeedfromElevator
						 }
		gainsName = list(gainsLib.keys())
		gainsData = list(gainsLib.values()) + list([0])

		self.master.mav.hil_actuator_controls_send(
			time_usec=0, controls=gainsData, mode=0, flags=mvc.HIL_CONTGAINS
		)

		self.buttonsStatusText.setText('Status: Gains Sent Complete')

	def runSensorUpdate(self):
		"""
		:Description: sets the updated sensors in the diagnostics section of the MAVLINK widget
		:param none
		:return: none
		"""
		# Update accelerometer
		self.acc_text.setText(mvc.accText.format(self.noisy_sensor[0], self.noisy_sensor[1], self.noisy_sensor[2]))
		# Update gyroscope
		self.gyro_text.setText(mvc.gyroText.format(self.noisy_sensor[3], self.noisy_sensor[4], self.noisy_sensor[5]))
		#Update magnetometer
		self.mag_text.setText(mvc.magText.format(self.noisy_sensor[6], self.noisy_sensor[7], self.noisy_sensor[8]))
		#Update barometer
		self.baro_text.setText(mvc.baroText.format(self.noisy_sensor[9]))
		#Update gps
		self.gps_text.setText(mvc.gpsText.format(self.noisy_sensor[10], self.noisy_sensor[11], self.noisy_sensor[12], self.noisy_sensor[13], self.noisy_sensor[14]))



	def resetmavlinkConsole(self):
		"""
		:Description: function used for resetting MAVLINK widget's features
		:param none
		:return: none
		"""
		# self.mavlink_end()
		# # Clear all consoles
		# self.mavlinkConsole.clear()
		# self.consoleRX.clear()
		# self.waypointConsole.clear()
		# # Close all masters
		# self.master.close()
		# self.master2.close() # Crashes sim

		# Reinitialize all mavlink protocols
		#self.mavlink_init()
		self.mytime = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
		# self.mavlinkConsole.appendPlainText("MAVLINK Console Recompiled at {}".format(self.mytime))
		# self.mav_msg = 'target_system {}, target component {}'.format(self.master.target_system,
		# 															  self.master.target_component)
		self.mavprint(self.mav_msg)

	## Creating widget helper functions
	def createTextInput(self, text, value, innerLayout, outerLayout, textDict):
		"""
		:Description: function for creating text box for parameter adjustment
		:param: text - string representation of the parameter name
				value - float of the initial value given to the parameter
				innerLayout - QtWidget instance for defining the inner layout of the widget
				outerLayout - QtWidget instance for defining the outer layout of the widget
				textDict - used for storing the parameter instances for compiling user input.
		:return: none
		"""
		lineEdit =  QtWidgets.QLineEdit()
		doubleValid = QtGui.QDoubleValidator()
		lineEdit.setText(str(value))
		lineEdit.setValidator(doubleValid)
		innerLayout.addRow(text, lineEdit)
		outerLayout.addLayout(innerLayout)
		textDict[text] = lineEdit

	def runSendEstGains(self):
		"""
		:Description: function used for sending estimated gains via MAVLINK protocol with the flag HIL_ESTGAINS
		:param: none
		:return: none
		"""
		if mvc.HIL_mode == mvc.simMode:
			return
		# Fetch Qline values from dictionary
		estList = list(self.estGainsDict.values())
		reformatList = list()
		# Add float converted values of estimated gains to list
		for i in range(len(estList)):
			reformatList.append(float(estList[i].text()))
		reformatList = reformatList + [0.0 for i in range(0,8)]
		# Publishes estimation gains to the autopilot
		self.master.mav.hil_actuator_controls_send(
			time_usec=0, controls=reformatList, mode=0, flags=mvc.HIL_ESTGAINS
		)
		self.buttonsStatusText.setText('Status: Estimated Gains Sent Complete')
	def buildEstGains(self):
		"""
		:Description: function used for building estimated gains from user input.
		:param: none
		:return: EstimatedGains() object with estimated gains input from widget
		"""
		self.templateEst = EstimatedGains()
		for name in mvc.estGainsNames:
			self.templateEst.estDict[name] = float(self.estGainsDict[name].text())
		return self.templateEst


	def saveEstGains(self):
		"""
		:Description: function used for saving estimated gains from user input.
		:param: none
		:note: Use protocol = pickle.HIGHEST_PROTOCOL to ensure that dictionary variables are saved
		:return: none
		"""
		self.toSaveEst = self.buildEstGains()
		with open(os.path.join(sys.path[0], mvc.defaultEstGainsFileName), 'wb') as f:
			pickle.dump(self.toSaveEst, f, protocol = pickle.HIGHEST_PROTOCOL)
		self.buttonsStatusText.setText('Status: Estimated Gains Saved')

	def getTime(self):
		"""
		:Description: function used for saving estimated gains from user input.
		:param: none
		:note: Use protocol = pickle.HIGHEST_PROTOCOL to ensure that dictionary variables are saved
		:return: none
		"""
		mytime = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
		return mytime

	def runSendWaypoint(self):
		"""
		:Description: function used for sending waypoints to the autopilot using HIL_WAYPOINTS flag
		:param: none
		:note: current function establishes a cross path waypoint but can be configured for different paths
		:return: none
		"""
		# Organize waypoints to create coords instance from user input
		wayList = list(self.waypointDict.values())
		wayPn = float(wayList[0].text())
		wayPe = float(wayList[1].text())
		wayPd = float(wayList[2].text())
		coords = wayPn, wayPe, wayPd
		self.waypointList.append(coords)

		# Generate cross path based on the waypoint provided by the user
		pathList = list()
		crossPath = self.dispPt.createSquare(coords)
		for i in range(len(crossPath)):
			for j in range(len(crossPath[i])):
				pathList.append(crossPath[i][j])
		pathList += [0 for i in range(4)]

		# Publish the waypoint to the autopilot using HIL_WAYPOINTS flag
		if mvc.HIL_mode == mvc.autopilotMode:
			self.master.mav.hil_actuator_controls_send(
				time_usec=0, controls=pathList, mode=0, flags=mvc.HIL_WAYPOINTS
			)

		# Set the waypoints stored so far:
		for i in crossPath:
			wayCoords = list(i)
			wayText = "{}, {}, {}".format(wayCoords[0], wayCoords[1], wayCoords[2])
			self.waypointConsole.appendPlainText(wayText)
		# Update status text to let users know about waypoints sent
		self.buttonsStatusText.setText("Status: Sent Waypoint\n{0:.3f}\n{1:.3f}\n{2:.3f}".format(wayPn, wayPe, wayPd))


	# Pointer functions for getting references from gains, control response, and simulation tabs
	def setGainsWidget(self, pointer):
		self.gainsPt = pointer


	def setRefWidget(self, pointer):
		self.refPt = pointer


	def setSimulation(self, pointer):
		self.simPt = pointer


	def setDisplayWidget(self, pointer):
		self.dispPt = pointer






