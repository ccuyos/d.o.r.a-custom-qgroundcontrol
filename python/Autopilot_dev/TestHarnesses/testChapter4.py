'''
    This is the test harness for the functions implemented in Lab 2
    Specifically, this tests the Vehicle Aerodynamics Model funtions, along with the wind model
    functions and related aerodynamics functions
    This test harness uses the statistics python library for its statistical tests

    Author: Margaret Silva
'''

#%% Initialization of test harness and helpers:

import math
import statistics

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Utilities.MatrixMath as mm
import ece163.Utilities.Rotations as Rotations
import ece163.Modeling.VehicleDynamicsModel as VDM
import ece163.Modeling.VehicleAerodynamicsModel as VAM
import ece163.Modeling.WindModel as WM
import ece163.Containers.Inputs as Inputs
import ece163.Containers.States as States
from ece163.Constants import VehiclePhysicalConstants as VPC

"""math.isclose doesn't work well for comparing things near 0 unless we 
use an absolute tolerance, so we make our own isclose:"""
isclose = lambda  a,b : math.isclose(a, b, abs_tol= 1e-12)

def compareVectors(a, b):
	"""A quick tool to compare two vectors"""
	el_close = [isclose(a[i][0], b[i][0]) for i in range(3)]
	return all(el_close)

#of course, you should test your testing tools too:
assert(compareVectors([[0], [0], [-1]],[[1e-13], [0], [-1+1e-9]]))
assert(not compareVectors([[0], [0], [-1]],[[1e-11], [0], [-1]]))
assert(not compareVectors([[1e8], [0], [-1]],[[1e8+1], [0], [-1]]))



failed = []
passed = []
def evaluateTest(test_name, boolean):
	"""evaluateTest prints the output of a test and adds it to one of two 
	global lists, passed and failed, which can be printed later"""
	if boolean:
		print(f"   passed {test_name}")
		passed.append(test_name)
	else:
		print(f"   failed {test_name}")
		failed.append(test_name)
	return boolean

def compareMatrices(A, B):
	"""Compare two matrices"""
	for i in range(len(A)):
		for j in range(len(A[0])):
			if not math.isclose(A[i][j], B[i][j], abs_tol= 1e-12):
				return False
	return True

def compareTuples(a, b):
    """Compare two tuples"""
    if len(a) != len(b):
        return False
    for i in range(len(a)):
        if not math.isclose(a[i], b[i], abs_tol= 1e-12):
            return False
    return True


#%% Basic Functionality testing

print("Testing basic class functions")

cur_test = "set/getVehicleState() test"
testVAM = VAM.VehicleAerodynamicsModel()
testState = States.vehicleState(p=3, q=2, u=1, pd=-.4)
testVAM.setVehicleState(testState)
test_result = testVAM.getVehicleState()
expected_result = testState
if not evaluateTest(cur_test, test_result==expected_result ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)



#%% gravityForces() testing

print("Testing gravityForces()")

cur_test = "gravityForces() rotationless test"
testVAM = VAM.VehicleAerodynamicsModel()
testState = States.vehicleState()
test_result = testVAM.gravityForces(testState)
expected_result = Inputs.forcesMoments(Fx=0, Fy=0, Fz=VPC.mass*VPC.g0)
if not evaluateTest(cur_test, test_result==expected_result ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)

cur_test = "gravityForces() upside down test"
testVAM = VAM.VehicleAerodynamicsModel()
testState = States.vehicleState(roll = math.radians(180))
test_result = testVAM.gravityForces(testState)
expected_result = Inputs.forcesMoments(Fx=0, Fy=0, Fz=-VPC.mass*VPC.g0)
if not evaluateTest(cur_test, test_result==expected_result ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)

cur_test = "gravityForces() complex test"
testVAM = VAM.VehicleAerodynamicsModel()
testState = States.vehicleState(roll = math.pi/4, pitch=(-5*math.pi)/7, yaw=-math.pi/3)
test_result = testVAM.gravityForces(testState)
rot = Rotations.euler2DCM(-math.pi/3, (-5*math.pi)/7, math.pi/4)
expected_result = mm.multiply(rot, [[0], [0], [VPC.mass*VPC.g0]])
expected_result = Inputs.forcesMoments(Fx=expected_result[0][0], Fy=expected_result[1][0], Fz=expected_result[2][0])
if not evaluateTest(cur_test, test_result==expected_result ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)




#%% CalculateCoeff_alpha() testing

print("Testing CalculateCoeff_alpha()")

cur_test = "CalculateCoeff_alpha() zero alpha test"
testVAM = VAM.VehicleAerodynamicsModel()
testalpha = 0
test_result = testVAM.CalculateCoeff_alpha(testalpha)
expected_result = (VPC.CL0, VPC.CDp + (VPC.CL0**2)/(math.pi*VPC.e*VPC.AR), VPC.CM0)
if not evaluateTest(cur_test, compareTuples(test_result, expected_result) ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)

cur_test = "CalculateCoeff_alpha() large alpha test"
testVAM = VAM.VehicleAerodynamicsModel()
testalpha = 90
test_result = testVAM.CalculateCoeff_alpha(testalpha)
expected_result = (2*math.sin(testalpha)*math.cos(testalpha), 2*math.sin(testalpha)**2, VPC.CM0 + VPC.CMalpha*testalpha)
if not evaluateTest(cur_test, compareTuples(test_result, expected_result) ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)

cur_test = "CalculateCoeff_alpha() stall alpha test"
testVAM = VAM.VehicleAerodynamicsModel()
testalpha = VPC.alpha0
sig = testVAM.sigma(testalpha, VPC.alpha0, VPC.M)
test_result = testVAM.CalculateCoeff_alpha(testalpha)
expected_result = (
            (1-sig)*(VPC.CL0 + VPC.CLalpha*testalpha)  
                + sig*2*math.sin(testalpha)*math.cos(testalpha), 
            (1-sig)*(VPC.CDp + (VPC.CL0 + VPC.CLalpha*testalpha)**2/(math.pi*VPC.e*VPC.AR)) 
                + sig*2*math.sin(testalpha)**2, 
            VPC.CM0 + VPC.CMalpha*testalpha)
if not evaluateTest(cur_test, compareTuples(test_result, expected_result) ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)



#%% aeroForces() testing

print("Testing aeroForces()")

cur_test = "aeroForces() zero test"
testVAM = VAM.VehicleAerodynamicsModel()
testState = States.vehicleState()
test_result = testVAM.aeroForces(testState)
expected_result = Inputs.forcesMoments()
if not evaluateTest(cur_test, test_result==expected_result ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)

cur_test = "aeroForces() only Va test"
testVAM = VAM.VehicleAerodynamicsModel()
testState = States.vehicleState()
testState.Va = 10
test_result = testVAM.aeroForces(testState)
expected_result = Inputs.forcesMoments()
expected_result.Fx = .5*VPC.rho*(testState.Va**2)*VPC.S*(-VPC.CDp - ((VPC.CL0)**2/(math.pi * VPC.e * VPC.AR)))
expected_result.Fy = .5*VPC.rho*(testState.Va**2)*VPC.S*(VPC.CY0)
expected_result.Fz = .5*VPC.rho*(testState.Va**2)*VPC.S*(-VPC.CL0)
expected_result.Mx = .5*VPC.rho*(testState.Va**2)*VPC.S*(VPC.b*VPC.Cl0)
expected_result.My = .5*VPC.rho*(testState.Va**2)*VPC.S*(VPC.c*VPC.CM0)
expected_result.Mz = .5*VPC.rho*(testState.Va**2)*VPC.S*(VPC.b*VPC.Cn0)
if not evaluateTest(cur_test, test_result==expected_result ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)

cur_test = "aeroForces() complex test"
testVAM = VAM.VehicleAerodynamicsModel()
testState = States.vehicleState()
testState.Va = 10
testState.alpha = 15
testState.beta = 2
testState.p = 3
testState.r = 1
test_result = testVAM.aeroForces(testState)
expected_result = Inputs.forcesMoments()
CL, CD, CM = testVAM.CalculateCoeff_alpha(testState.alpha)
CY, Cl, Cn = testVAM.CalculateCoeff_beta(testState.beta)
Cx = -CD*math.cos(testState.alpha) + CL*math.sin(testState.alpha)
Cxq = -VPC.CDq*math.cos(testState.alpha) + VPC.CLq*math.sin(testState.alpha)
Cz = -CD*math.sin(testState.alpha) - CL*math.cos(testState.alpha)
Czq = -VPC.CDq*math.sin(testState.alpha) - VPC.CLq*math.cos(testState.alpha)
expected_result.Fx = .5*VPC.rho*(testState.Va**2)*VPC.S*(Cx + Cxq*(VPC.c/(2*testState.Va))*testState.q)
expected_result.Fy = .5*VPC.rho*(testState.Va**2)*VPC.S*(CY + VPC.CYp*(VPC.b/(2*testState.Va))*testState.p + VPC.CYr*(VPC.b/(2*testState.Va))*testState.r)
expected_result.Fz = .5*VPC.rho*(testState.Va**2)*VPC.S*(Cz + Czq*(VPC.c/(2*testState.Va))*testState.q)
expected_result.Mx = .5*VPC.rho*(testState.Va**2)*VPC.S*VPC.b*(Cl + VPC.Clp*(VPC.b/(2*testState.Va))*testState.p + VPC.Clr*(VPC.b/(2*testState.Va))*testState.r)
expected_result.My = .5*VPC.rho*(testState.Va**2)*VPC.S*VPC.c*(CM + VPC.CMq*(VPC.c/(2*testState.Va))*testState.q)
expected_result.Mz = .5*VPC.rho*(testState.Va**2)*VPC.S*VPC.b*(Cn + VPC.Cnp*(VPC.b/(2*testState.Va))*testState.p + VPC.Cnr*(VPC.b/(2*testState.Va))*testState.r)
if not evaluateTest(cur_test, test_result==expected_result ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)




#%% CalculatePropForces() testing

print("Testing CalculatePropForces()")

cur_test = "CalculatePropForces() zero test"
testVAM = VAM.VehicleAerodynamicsModel()
test_result = testVAM.CalculatePropForces(0, 0)
expected_result = [0, 0]        # value should be close to zero, but not exactly zero
if not evaluateTest(cur_test, math.isclose(test_result[0], expected_result[0], abs_tol=0.001) and math.isclose(test_result[1],expected_result[1], abs_tol=.001) ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)

cur_test = "CalculatePropForces() Va only test"
testVAM = VAM.VehicleAerodynamicsModel()
test_result = testVAM.CalculatePropForces(10, 0)
expected_result = [-3.5580964225, 0.2754911622682]       
if not evaluateTest(cur_test, isclose(test_result[0], expected_result[0]) and isclose(test_result[1],expected_result[1]) ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)

cur_test = "CalculatePropForces() Throttle only test"
testVAM = VAM.VehicleAerodynamicsModel()
test_result = testVAM.CalculatePropForces(0, .5)
expected_result = [21.817680754, -0.6194943564]       
if not evaluateTest(cur_test, math.isclose(test_result[0], expected_result[0], abs_tol=0.001) and math.isclose(test_result[1],expected_result[1], abs_tol=.001) ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)

cur_test = "CalculatePropForces() complex test"
testVAM = VAM.VehicleAerodynamicsModel()
test_result = testVAM.CalculatePropForces(-5, -.3)
expected_result = [5.929963444, -0.239601543667]       
if not evaluateTest(cur_test, math.isclose(test_result[0], expected_result[0], abs_tol=0.001) and math.isclose(test_result[1],expected_result[1], abs_tol=.001) ):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)




#%% controlForces() testing

print("Testing controlForces()")

cur_test = "controlForces() zero test"
testVAM = VAM.VehicleAerodynamicsModel()
test_controls = Inputs.controlInputs(0, 0, 0, 0)
test_result = testVAM.controlForces(testVAM.vehicleDynamics.state, test_controls)
expected_prop = testVAM.CalculatePropForces(0, 0)
expected_result = Inputs.forcesMoments(Fx = expected_prop[0], Mx=expected_prop[1])
if not evaluateTest(cur_test, test_result==expected_result):
    print("test failed")
    print("expected:", expected_result)
    print("returned:", test_result)

cur_test = "controlForces() aileron test 1"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = .5, q=.5, r=.5, u=5)
test_controls = Inputs.controlInputs(Aileron=1)
test_controls2 = Inputs.controlInputs(Aileron=-1)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, t1.Fy > t2.Fy):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() aileron test 2"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = 50, q=-.5, r=.5, u=5, v = 33)
test_controls = Inputs.controlInputs(Aileron=1, Throttle=1)
test_controls2 = Inputs.controlInputs(Aileron=.9, Throttle=1)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, t1.Fy > t2.Fy):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() aileron test 3"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = .3, q=8, r=.5, u=5, v = 33)
test_controls = Inputs.controlInputs(Aileron=1, Rudder=.3, Elevator=.1, Throttle=.8)
test_controls2 = Inputs.controlInputs(Aileron=.5, Rudder=.3, Elevator=.1, Throttle=.8)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, t1.Fy > t2.Fy):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() elevator test 1"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = .5, q=.5, r=.5, u=5)
test_controls = Inputs.controlInputs(Elevator=1)
test_controls2 = Inputs.controlInputs(Elevator=-1)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, (t1.My < t2.My)):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() elevator test 2"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = 15, q=.35, r=-.5, u=5, v=.0189)
test_controls = Inputs.controlInputs(Elevator=.3)
test_controls2 = Inputs.controlInputs(Elevator=-1)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, (t1.My < t2.My)):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() elevator test 3"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = 15, q=.35, r=-.5, u=5, v=.0189)
test_controls = Inputs.controlInputs(Elevator=.3, Aileron=.2, Rudder=-.8, Throttle=.1)
test_controls2 = Inputs.controlInputs(Elevator=-.1, Aileron=.2, Rudder=-.8, Throttle=.1)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, (t1.My < t2.My)):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() rudder test 1"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = 15, q=.35, r=-.5, u=5, v=.0189)
test_controls = Inputs.controlInputs(Rudder=-.8,Elevator=.3, Aileron=.2, Throttle=.1)
test_controls2 = Inputs.controlInputs(Rudder=.8, Elevator=-.1, Aileron=.2, Throttle=.1)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, (t1.Mx < t2.Mx)):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() rudder test 2"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = 54, q=-3, r=-.5, u=5, v=.0189)
test_controls = Inputs.controlInputs(Rudder=-.8,Elevator=.3, Aileron=.2, Throttle=.1)
test_controls2 = Inputs.controlInputs(Rudder=.8, Elevator=-.1, Aileron=.2, Throttle=.1)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, (t1.Mz > t2.Mz)):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() rudder test 3"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = 15, q=-.35, r=-.5, u=5, w=4)
test_controls = Inputs.controlInputs(Rudder=-.8,Elevator=.3, Aileron=.2, Throttle=.1)
test_controls2 = Inputs.controlInputs(Rudder=.8, Elevator=-.1, Aileron=.2, Throttle=.1)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, (t1.Mx < t2.Mx)):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() throttle test 1"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = 15, q=.35, r=-.5, u=5, v=.0189)
test_controls = Inputs.controlInputs(Throttle=.1)
test_controls2 = Inputs.controlInputs(Throttle=.5)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, (t1.Fx < t2.Fx)):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() throttle test 2"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = 15, q=.35, r=-.5, u=5, v=.0189)
test_controls = Inputs.controlInputs(Rudder=.8,Elevator=.3, Aileron=.2, Throttle=.9)
test_controls2 = Inputs.controlInputs(Rudder=.8, Elevator=-.1, Aileron=.2, Throttle=.1)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, (t1.Fx > t2.Fx)):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)

cur_test = "controlForces() throttle test 3"
testVAM = VAM.VehicleAerodynamicsModel()
test_state = States.vehicleState(p = 15, q=.35, r=-.5, u=5, v=.0189)
test_controls = Inputs.controlInputs(Rudder=.8,Elevator=.3, Aileron=.2, Throttle=.1)
test_controls2 = Inputs.controlInputs(Rudder=.8, Elevator=.3, Aileron=.2, Throttle=.5)
t1 = testVAM.controlForces(test_state, test_controls)
t2 = testVAM.controlForces(test_state, test_controls2)
if not evaluateTest(cur_test, (t1.Fx < t2.Fx)):
    print("test failed")
    print("expected:", t1)
    print("returned:", t2)




#%% Update() testing

print("Testing Update()")

cur_test = "Update() no control forces"

testVAM = VAM.VehicleAerodynamicsModel()
test_controls = Inputs.controlInputs()
for i in range(10):
    testVAM.Update(test_controls)
print(testVAM.getVehicleState())



#%% Wind Model Testing
print("Testing Wind Model")

cur_test = "Wind Model Init() test"
windModel = WM.WindModel()
if not evaluateTest(cur_test, windModel.xu[0][0]==0 ):
    print("test failed")
    print("expected:", 0)
    print("returned:", windModel.xu)


cur_test = "Wind Model createTransferFns() zero test"
windModel = WM.WindModel()
result = (windModel.getDrydenTransferFns())
expected = ([[1]], [[0]], [[1]], [[1, 0], [0, 1]], [[0], [0]], [[1, 1]], [[1, 0], [0, 1]], [[0], [0]], [[1, 1]])
if not evaluateTest(cur_test, all([compareMatrices(expected[i], result[i]) for i in range(len(result))]) ):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)


cur_test = "Wind Model createTransferFns() u test"
windModel = WM.WindModel(Va = 30, drydenParamters=Inputs.drydenParameters(10, 300.5, .1, .3, .8, 3))
result = (windModel.getDrydenTransferFns())
result = result[0:3]
expected = ([[0.9704455335485082]], [[0.009851488817163948]], [[0.4145929793656026]])
if not evaluateTest(cur_test, all([compareMatrices(expected[i], result[i]) for i in range(len(result))]) ):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)

cur_test = "Wind Model createTransferFns() v test"
windModel = WM.WindModel(Va = .1, drydenParamters=Inputs.drydenParameters(500, .025, 78, 15, .333, .97))
result = (windModel.getDrydenTransferFns())
result = result[3:6]
expected = ([[0.9223578615862302, -0.15372631026437172], [0.009607894391523233, 0.9992210167184161]], [[0.009607894391523233], [4.868645509899036e-05]], [[0.6508185458546895, 1.5030010505712232]])
if not evaluateTest(cur_test, all([compareMatrices(expected[i], result[i]) for i in range(len(result))]) ):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)

cur_test = "Wind Model createTransferFns() w test"
windModel = WM.WindModel(Va = .1, drydenParamters=Inputs.drydenParameters(420, 666, 7, .24, 50, .0001))
result = (windModel.getDrydenTransferFns())
result = result[6:9]
expected = ([[0.9997143163245872, -2.040524802164773e-06], [0.009998571530607387, 0.9999999897968902]], [[0.009998571530607387], [4.999523802724605e-05]], [[1.1679834016380371e-05, 9.633364733496223e-08]])
if not evaluateTest(cur_test, all([compareMatrices(expected[i], result[i]) for i in range(len(result))]) ):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)

cur_test = "Wind Model Update() zeros test"
windModel = WM.WindModel()
windModel.Update(1, 1, 1)
result = windModel.getWind()
expected = States.windState()
if not evaluateTest(cur_test, result == expected):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)

cur_test = "Wind Model Update() no gusts test"
windModel = WM.WindModel(drydenParamters=VPC.DrydenNoGusts)
test = States.windState(Wd=50, We=-3)
windModel.setWind(test)
windModel.Update()
result = windModel.getWind()
expected = States.windState(Wd=50, We=-3)
if not evaluateTest(cur_test, result == expected):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)

cur_test = "Wind Model Update() yes gusts test"
windModel = WM.WindModel(drydenParamters=VPC.DrydenLowAltitudeModerate)
test = States.windState(Wd=50, We=-3)
windModel.setWind(test)
windModel.Update(1, 1, 1)
result = windModel.getWind()
expected = States.windState(Wd=50, We=-3, Wu=0.005976673386526738, Wv=0.00731796688484073, Ww=0.009639504249637413)
if not evaluateTest(cur_test, result == expected):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)

cur_test = "Wind Model Update() statistical test - standard deviation"
windModel = WM.WindModel(drydenParamters=VPC.DrydenLowAltitudeModerate)
windModel.setWind(test)
data = []
for i in range(1000):
    windModel.reset()
    windModel.Update()
    data.append(windModel.getWind().Wv)
result = statistics.stdev(data)
expected = 0.007339626
if not evaluateTest(cur_test, math.isclose(result, expected, abs_tol=10**-3)):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)

cur_test = "Wind Model Update() statistical test - average"
result = statistics.mean(data)
expected = 0
if not evaluateTest(cur_test, math.isclose(result, expected, abs_tol=10**-3)):
    print("test failed")
    print("expected:", expected)
    print("returned:", result)


#%% Print results:

total = len(passed) + len(failed)
print(f"\n---\nPassed {len(passed)}/{total} tests")
[print("   " + test) for test in passed]

if failed:
	print(f"Failed {len(failed)}/{total} tests:")
	[print("   " + test) for test in failed]