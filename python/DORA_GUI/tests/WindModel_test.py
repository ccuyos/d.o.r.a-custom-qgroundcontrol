"""
Test harness for wind gust model code. Will generate a sequence of gusts from random white noise inputs.
"""

import random
import csv
import sys

sys.path.append('..')

import ece163.Modeling.WindModel as WindModel
import ece163.Constants.VehiclePhysicalConstants as VPC

dT = VPC.dT
Hz = int(1/dT)
time = 0.0

Va = 25.

wnd = WindModel.WindModel(dT, Va)
wnd.reset()
wnd.CreateDydenTransferFns(dT, VPC.InitialSpeed, VPC.DrydenLowAltitudeLight)

print("Starting Iteration\n\n")
print("Time [s]\tu_x\tGust_u [m/s]\tGust_v [m/s]\tGust_w [m/s]")
uu = 0.0

dataList = [[time, uu, wnd.Wind.Wu, wnd.Wind.Wv, wnd.Wind.Ww]]
for x in range(0, 100 * Hz):
	time += dT
	uu = random.gauss(0, 1)
	wnd.Update(uu, uu, uu)
	d = [time, uu, wnd.Wind.Wu, wnd.Wind.Wv, wnd.Wind.Ww]
	print(" ".join('{: 7.9f}'.format(t) for t in d))
	dataList.append(d)

with open('data.csv', 'w', newline='') as f:
	writer = csv.writer(f)
	writer.writerows(dataList)
