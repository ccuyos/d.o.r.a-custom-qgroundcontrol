/* 
 * File:   IMU.c
 * Author: Carl Vincent Cuyos
 * Brief: Contains code for Lab3 of ECE167
 * Created on April 30, 2023, 1:10pm
 */
#include <sys/attribs.h>
#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "BOARD.h"
#include "serial.h"
#include "timers.h"
#include "AD.h"
#include "BNO055.h"
#include "IMU.h"
#include "MatrixMath.h"


//Period for services
#define PRINT_PERIOD 20 //Print every 10 ms
#define FILTER_PERIOD 20 //Filter every 1 ms
#define CHECK_PERIOD 10 //Check results every 1 seconds
#define HR_TO_SEC 3600
#define SEC_TO_MILI 1000
#define dT 0.02


//Matrix related variables
#define DIM 3
#define X 0 
#define Y 1
#define Z 2



/*Variables*/
struct FRT time;
struct IMU imu;

/*Matrix calibration values*/
static float A_mag[DIM][DIM] = {
    {1, 0, 0},
    { 0, 1, 0},
    {0, 0, 1}
};
static float b_mag[DIM][DIM] = {
    {-10.4933,0,0}, 
    {-22.8693,0,0}, 
    {37.0395,0,0}
};



/**
 * @function FIFO_Enqueue 
 * @param data, the data to be enqueued to the array 
 * @brief used for enqueue data to an array
 * @return none
 * @author: ccuyos
 */
void FIFO_Enqueue(IMU_samples* sample);

/**
 * @function: generateEmptyFIFO
 * @param none
 * @brief used for generating an empty array 
 * @return none
 * @author: ccuyos
 */
void generateEmptyFIFO(void);

/**
 *@function: printFIFO 
 *@param fifo to be printed
 *@brief used for printing the current array
 *@return none
 *@author: ccuyos
 */

void printFIFO(float* fifo_array);


/**
 * @function MovingAverage
 * @param none
 * @brief used for doing a moving average filter for sensor data
 * @return none
 * @author ccuyos
 */
void MovingAverage(void);

/**
 * @function CalibrateIMU
 * @param none
 * @brief used for calibrating the IMU
 * @return none
 * @author ccuyos
 */
void CalibrateIMU(void);
/*Prototype functions*/

/* 
 * @function serviceReady
 * @param tick, number of ticks currently for respective service
 *        period, period for the respective service
 * @brief Returns SUCCESS if ready for servicing
 */
uint8_t serviceReady(unsigned int* tick, int period){
    //Check if period has been reached
    if (TIMERS_GetMilliSeconds() - *tick >= period){
        //Overwrite tick variable with current ms reading
        *tick = TIMERS_GetMilliSeconds();
        return SUCCESS;
    }
    return 0;
}

/**
 * @function FIFO_Enqueue 
 * @param data, the data to be enqueued to the array 
 * @brief used for enqueue data to an array
 * @return none
 * @author: ccuyos
 */
void FIFO_Enqueue(IMU_samples* sample){
    //Calculate sum
    sample->get_sum.x = sample->get_sum.x - sample->x[sample->pointerX] + sample->get_data.x;
    sample->get_sum.y = sample->get_sum.y - sample->y[sample->pointerY] + sample->get_data.y;
    sample->get_sum.z = sample->get_sum.z - sample->z[sample->pointerZ] + sample->get_data.z;
    
    //Add data to the array
    sample->x[sample->pointerX] = sample->get_data.x;
    sample->y[sample->pointerY] = sample->get_data.y;
    sample->z[sample->pointerZ] = sample->get_data.z;
    //Move the pointer forward
    sample->pointerX++;
    sample->pointerY++;
    sample->pointerZ++;
    //Check for index out of range
    sample->pointerX =  sample->pointerX%SAMPLING_SIZE;
    sample->pointerY =  sample->pointerY%SAMPLING_SIZE;
    sample->pointerZ =  sample->pointerZ%SAMPLING_SIZE;
}

/**
 * @function MovingAverage
 * @param none
 * @brief used for doing a moving average filter for sensor data
 * @return none
 * @author ccuyos
 */
void MovingAverage(void){
    //Enqueue data to the data array
    
    //Accelerometer
    imu.acc_true.x = imu.acc_raw.get_sum.x/SAMPLING_SIZE;
    imu.acc_true.y = imu.acc_raw.get_sum.y/SAMPLING_SIZE;
    imu.acc_true.z = imu.acc_raw.get_sum.z/SAMPLING_SIZE;
   
    
    //Magnetometer
    imu.mag_true.x = imu.mag_raw.get_sum.x/SAMPLING_SIZE;
    imu.mag_true.y = imu.mag_raw.get_sum.y/SAMPLING_SIZE;
    imu.mag_true.z = imu.mag_raw.get_sum.z/SAMPLING_SIZE;
}


/**
 * @function CalibrateIMU
 * @param none
 * @brief used for calibrating the imu values based on the desired inputs
 *        state estimation
 * @return none
 * @author ccuyos
 */
void CalibrateIMU(void){
    //Accelerometer - remove bias and scale down (w/ gravity conversion)
    imu.acc_true.x = (imu.acc_true.x - ACC_X_BIAS) * ACC_X_SCALE/G;
    imu.acc_true.y = (imu.acc_true.y - ACC_Y_BIAS) * ACC_Y_SCALE/G;
    imu.acc_true.z = (imu.acc_true.z - ACC_Z_BIAS) * ACC_Z_SCALE/G; 
    
    
    //Magnetometer;
    float out1[DIM][DIM] = {{},{},{}};
    float out2[DIM][DIM] = {{},{},{}};
    float mag_store[DIM][DIM]= {{imu.mag_true.x * 0.15, 0, 0}, 
                                {imu.mag_true.y * 0.15,0,0}, 
                                {imu.mag_true.z * 0.15,0,0}};
    MatrixMultiply(A_mag, mag_store, out1);    //Atilde * mag_vector
    MatrixAdd(out1, b_mag, out2);             //Adding Btilde
    
    //Add scaling to the magnetometer to 1 G
    out2[X][0] = out2[X][0] * MAG_X_SCALE * X_INTENSITY;
    out2[Y][0] = out2[Y][0] * MAG_Y_SCALE * Y_INTENSITY;
    out2[Z][0] = out2[Z][0] * MAG_Z_SCALE * Z_INTENSITY;
    //Add calibrated value back to true variable
    imu.mag_true.x = out2[X][0];
    imu.mag_true.y = out2[Y][0];
    imu.mag_true.z = out2[Z][0];
    

    //Gyroscope
    //Convert gyroscope to degrees
    imu.gyro_true.x = (imu.gyro_out.x / CONV_GYRO) - GYRO_X_BIAS; //currently in deg/sec
    imu.gyro_deg.x += imu.gyro_true.x*dT - (GYRO_X_DRIFT);
    imu.gyro_rad.x = deg2rad(imu.gyro_true.x);
    
    imu.gyro_true.y = (imu.gyro_out.y/CONV_GYRO) - GYRO_Y_BIAS; //currently in deg/sec
    imu.gyro_deg.y += (imu.gyro_true.y*dT - (GYRO_Y_DRIFT));
    imu.gyro_rad.y = deg2rad(imu.gyro_true.y);
    
    imu.gyro_true.z = (imu.gyro_out.z/CONV_GYRO) - GYRO_Z_BIAS; //currently in deg/sec
    imu.gyro_deg.z += (imu.gyro_true.z*dT - (GYRO_Z_DRIFT));
    imu.gyro_rad.z = deg2rad(imu.gyro_true.z);
   
}

/**
 * @function GetIMU()
 * @param none
 * @brief wrapper function for fetching imu library related variables
 * @return none
 * @author ccuyos
 */
struct IMU GetIMU(void){
    return imu;
}

/**
 * @function GetTime()
 * @param none
 * @brief wrapper function for fetching time related variables
 * @return none
 * @author ccuyos
 */
struct FRT GetTime(void){
    return time;
}

/**
 * @function ProcessIMU()
 * @param none
 * @brief used for fetching raw IMU values from the BNO555
 *        and applying a moving average filter for smoothing
 * @return none
 * @author ccuyos
 */
void ProcessIMU(void){
     //Fetch Accelerometer Data
    imu.acc_raw.get_data.x  = BNO055_ReadAccelX();
    imu.acc_raw.get_data.y  = BNO055_ReadAccelY();
    imu.acc_raw.get_data.z = BNO055_ReadAccelZ();

    //Fetch Magnetometer Data
    imu.mag_raw.get_data.x = BNO055_ReadMagX();
    imu.mag_raw.get_data.y = BNO055_ReadMagY();
    imu.mag_raw.get_data.z = BNO055_ReadMagZ();

    //Fetch Gyroscope Data

    imu.gyro_out.x = BNO055_ReadGyroX();
    imu.gyro_out.y = BNO055_ReadGyroY();
    imu.gyro_out.z = BNO055_ReadGyroZ();

    //Enqueue accelerometer and magnetometer
    FIFO_Enqueue(&imu.acc_raw);
    FIFO_Enqueue(&imu.mag_raw);

    //Apply moving average filter then calibrate IMU
    MovingAverage();
    CalibrateIMU();
}



#ifdef IMU_TESTING
#define POWERPIN_LAT LATFbits.LATF1
#define POWERPIN_TRIS TRISFbits.TRISF1

/*Edit to configure which sensor to print in the IMU*/
#define ACC_PRINT
//#define MAG_PRINT
//#define GYRO_PRINT
int main(void){
    //Initialize required modules
    BOARD_Init();
    SERIAL_Init();
    TIMERS_Init(); 
    printf("IMU.c compiled on %s %s.\n", __TIME__, __DATE__);
    //Initialize BNO055
    POWERPIN_LAT = 0;
    POWERPIN_TRIS = 0;
    POWERPIN_LAT = 1;
    DelayMicros(100000); //Add delay to let sensor initialize
    //Initialize BNO055
    if (BNO055_Init() != TRUE){
        printf("IMU Initialization failed\n");
        while(1);
    }
 
    
    while (1){
        //Check if print service is ready
        if (serviceReady(&time.printTick, PRINT_PERIOD)){
            
            
#ifdef ACC_PRINT
            /*Print ACC data*/
            printf("%d, %0.6f, %0.6f, %0.6f\n", time.printTick, imu.acc_true.x, imu.acc_true.y, imu.acc_true.z);
#endif   
            
#ifdef MAG_PRINT
             /*Print MAG data*/
            printf("%0.6f, %0.6f, %0.6f\n", imu.mag_true.x, imu.mag_true.y, imu.mag_true.z);
#endif
            
#ifdef GYRO_PRINT
          /*Print GYRO data*/
            printf("%0.6f, %0.6f, %0.6f\n", imu.gyro_deg.x, imu.gyro_deg.y, imu.gyro_deg.z);
#endif             
            
             
        }
        //Check if filter service is ready
        if (serviceReady(&time.filterTick, FILTER_PERIOD)){
                
            //Fetch Accelerometer Data
            imu.acc_raw.get_data.x  = BNO055_ReadAccelX();
            imu.acc_raw.get_data.y  = BNO055_ReadAccelY();
            imu.acc_raw.get_data.z = BNO055_ReadAccelZ();

            //Fetch Magnetometer Data
            imu.mag_raw.get_data.x = BNO055_ReadMagX();
            imu.mag_raw.get_data.y = BNO055_ReadMagY();
            imu.mag_raw.get_data.z = BNO055_ReadMagZ();

            //Fetch Gyroscope Data

            imu.gyro_out.x = BNO055_ReadGyroX();
            imu.gyro_out.y = BNO055_ReadGyroY();
            imu.gyro_out.z = BNO055_ReadGyroZ();

            //Enqueue accelerometer and magnetometer
            FIFO_Enqueue(&imu.acc_raw);
            FIFO_Enqueue(&imu.mag_raw);
            
            //Apply moving average filter then calibrate IMU
            MovingAverage();
            CalibrateIMU();
            
            
        } 
    }
    
    BOARD_End();
    while(1);
}

#endif