"""Vehicle Dynamics Model Library Test Harness. Runs through various tests of the functions to determine that they are, in fact, working."""

import math
import csv
import sys
sys.path.append('..')

import ece163.Modeling.VehicleDynamicsModel as VehicleDynamicsModel
import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Utilities.MatrixMath as MatrixMath
import ece163.Containers.Inputs as Inputs

v = VehicleDynamicsModel.VehicleDynamicsModel()
fm = Inputs.forcesMoments()

fm.Fx = 10
fm.Fy = -5  # 10
fm.Fz = -2  # 10
fm.Mx = 0.1  # 0.1
fm.My = -0.05  # 0.1
fm.Mz = 0.025  # 0.1

time = 0.

v.state.p = math.radians(0.)
v.state.q = math.radians(0.)
v.state.r = math.radians(0.)

Hz = 50
v.dT = 1./Hz
finalTime = 45

print("Begin Integration with Fixed Forces and Moments, dT = ", v.dT)
print("Euler Angles (deg) --> roll: {: 7.3f} pitch: {: 7.3f} yaw: {: 7.3f}".format(math.degrees(v.state.roll),
																					   math.degrees(v.state.pitch),
																					   math.degrees(v.state.yaw)))
print("Initial DCM:")
MatrixMath.matrixPrint(v.state.R)
print("Moment of Inertia Matrix: J [Kg m^4]")
MatrixMath.matrixPrint(VPC.Jbody)
print("Moment of Inertia Inverse: Jinv")
MatrixMath.matrixPrint(VPC.JinvBody)
print("Starting Iteration\n\n")
print("Time [s]\tN [m]\tE [m]\tAlt [m]\tu [m/s]\tv [m/s]\tw [m/s]\troll [deg]\tpitch [deg]\tyaw [deg]\tp [deg/s]\tq [deg/s]\tr [deg/s]")

dataList = [[time, v.state.pn, v.state.pe, -v.state.pd, v.state.u, v.state.v, v.state.w, math.degrees(v.state.roll), math.degrees(v.state.pitch),
			 math.degrees(v.state.yaw), math.degrees(v.state.p), math.degrees(v.state.q), math.degrees(v.state.r)]]


for x in range(0, finalTime*Hz):
	time += v.dT
	# fm.Mz = M*math.sin(0.1*math.pi*time)
	v.Update(fm)
	d = [time, v.state.pn, v.state.pe, -v.state.pd, v.state.u, v.state.v, v.state.w, math.degrees(v.state.roll), math.degrees(v.state.pitch),
		 math.degrees(v.state.yaw), math.degrees(v.state.p), math.degrees(v.state.q), math.degrees(v.state.r)]
	print(" ".join('{: 7.3f}'.format(t) for t in d))
	dataList.append(d)

print(v.state)
print(v.dot)


with open('data.csv', 'w', newline='') as f:
	writer = csv.writer(f)
	writer.writerows(dataList)

# v = VehicleDynamicsModel.VehicleDynamicsModel()
# Fx = 0	# in units of acceleration [m/s^2]
# Fy = 0
# Fz = 0
#
# RR = v.state.R
#
# v.state.p = math.radians(-5.)
# v.state.q = math.radians(0.)
# v.state.r = math.radians(0.)
#
# dT = 0.1
#
# print("Begin Iteration of Matrix Exponential Test, dT = ",dT)
# print("Body-Fixed Rates [deg/s] -- p: {: 7.3f} q: {: 7.3f} r: {: 7.3f}".format(math.degrees(v.state.p),
# 																	   math.degrees(v.state.q),
# 																	   math.degrees(v.state.r)))
# print(v.state.R)
# print("\n\n")
# newYaw, newPitch, newRoll = Rotations.dcm2Euler(v.state.R)
# print("Euler Angles (deg) --> yaw: {: 7.3f} pitch: {: 7.3f} roll: {: 7.3f}".format(math.degrees(newYaw),
# 																			 math.degrees(newPitch),
# 																			 math.degrees(newRoll)))
# for x in range(0,100):
# 	RRexp = v.Rexp(dT)
# 	RRdot = MatrixMath.skew(v.state.p,v.state.q,v.state.r)
# 	RRdot = MatrixMath.multiply(MatrixMath.scalarMultiply(-1.0,RRdot),RR)
# 	RR = MatrixMath.add(RR,MatrixMath.scalarMultiply(dT,RRdot))
# 	reOrtho = MatrixMath.subtract([[1.,0.,0.],[0.,1.,0.],[0.,0.,1.]],MatrixMath.multiply(RR,MatrixMath.transpose(RR)))
# 	reOrtho = MatrixMath.multiply(reOrtho,RR)
# 	RR = MatrixMath.add(RR,MatrixMath.scalarMultiply(0.5,reOrtho))
#
# 	v.state.R = MatrixMath.multiply(RRexp,v.state.R)
# 	newYaw, newPitch, newRoll = Rotations.dcm2Euler(v.state.R)
# 	Yaw, Pitch, Roll = Rotations.dcm2Euler(RR)
#
# 	sinphi = math.sin(v.state.roll)
# 	cosphi = math.cos(v.state.roll)
# 	costheta = math.cos(v.state.pitch)
# 	tantheta = math.tan(v.state.pitch)
# 	euldot = [[1., sinphi * tantheta, cosphi * tantheta],
# 			  [0., cosphi, -1. * sinphi],
# 			  [0., sinphi / costheta, cosphi / costheta]]
# 	euldot = MatrixMath.multiply(euldot, [[v.state.p], [v.state.q], [v.state.r]])
# 	eulupdate = MatrixMath.add([[v.state.roll],[v.state.pitch],[v.state.yaw]],MatrixMath.scalarMultiply(dT,euldot))
# 	v.state.roll = eulupdate[0][0]
# 	v.state.pitch = eulupdate[1][0]
# 	v.state.yaw = eulupdate[2][0]
# 	print("Yaw: {: 7.3f} | {: 7.3f} | {: 7.3f} ; pitch: {: 7.3f} | {: 7.3f} | {: 7.3f} ; roll: {: 7.3f} | {: 7.3f} | {: 7.3f}".format(math.degrees(newYaw),
# 				math.degrees(Yaw),math.degrees(v.state.yaw),math.degrees(newPitch),math.degrees(Pitch),math.degrees(v.state.pitch),math.degrees(newRoll),
# 				math.degrees(Roll),math.degrees(v.state.roll)))
#
