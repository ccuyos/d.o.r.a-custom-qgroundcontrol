"""All the vehicle constants that define the various constants that are used in the simulation and control
   model. These are physical constants, mathematical constants, and the rest."""

import math
from ece163.Utilities import MatrixMath
from ece163.Containers import Inputs
import ece163.Constants.MAVLINKConstants as mvc
import pickle
import sys
import os

## Helper functions ###
def loadVPC(dataName, constants):
	"""
	Function: loadVPC(self, dataName, constants)
	Description: loads a section pickle file into a dictionary constant
	Params: dataName - file name of the VPC section (i.e. mass, lift, drag)
			constants - associated dictionary of VPC section
	Return: the loaded dictionary constant if file found, else constants
	Author: Carl Vincent Cuyos
	"""
	fileName = "VPC_{}.pickle"
	try:
		with open(os.path.join(sys.path[0], fileName.format(dataName)), 'rb') as f:
			return pickle.load(f)
	except (FileNotFoundError, EOFError):
		return constants
def reloadSection(paramNames, paramConst):
	for i in range(len(paramNames)):
		idx = paramNames[i]
		exec("{} = {}".format(idx, paramConst[idx]))
def updateVPC():
	for i in range(len(varLib)):
		exec("{0}Dict = loadVPC({0}File, {0}Dict)".format(varLib[i]))
		exec("{0}LoadConst = {0}Dict".format(varLib[i]))
		exec("{0}LoadName = {0}Names".format(varLib[i]))
		exec("reloadSection({0}LoadName, {0}LoadConst)".format(varLib[i]))



if mvc.airframeMode == mvc.defaultAirframe:
	LINEARMAX = 250
	ROTATEMAX = 180

	dT = 1/100	# Time step for simulation
	RstarMax = 2500	# maximum radius before considered a straight line

	# parameters for Aerosonde UAV
	InitialSpeed = 25.0	# [m/s]
	InitialNorthPosition = 0.0	# displacement to north [m]
	InitialEastPosition = 0.0	# displacement to east [m]
	InitialDownPosition = -100.0	# [m], negative is above ground
	InitialYawAngle = math.radians(0.0)	# initial heading angle [rad]

	mass = 11  # [kg]
	rho = 1.2682  # [kg / m^3]
	g0 = 9.81  # gravity, [m/s^2]
	b = 2.8956  # wing-span [m]
	c = 0.18994  # wing chord [m]
	S = 0.55  # wing area [m^2]
	e = 0.9  # Oswald's Efficiency Factor []

	AR = b ** 2 / S

	M = 50.  # barrier function coefficient for angle of attack
	alpha0 = math.radians(27.)  # angle at which stall occurs [deg]

	Jxx = 0.8244  # [kg m^2]
	Jyy = 1.135  # [kg m^2]
	Jzz = 1.759  # [kg m^2]
	Jxz = 0.1204  # [kg m^2]

	Jbody = [[Jxx, 0., -Jxz], [0., Jyy, 0.], [-Jxz, 0., Jzz]]
	Jdet = (Jxx * Jzz - Jxz ** 2)
	JinvBody = MatrixMath.scalarMultiply(1. / Jdet, [[Jzz, 0., Jxz], [0., Jdet / Jyy, 0.], [Jxz, 0., Jxx]])

	Gamma1 = (Jxx - Jyy + Jzz)* JinvBody[0][2]
	Gamma2 = (Jzz * (Jzz - Jyy) + Jxz ** 2) / Jdet
	Gamma7 = (Jxx * (Jxx - Jyy) + Jxz ** 2) / Jdet

	# Aerodynamic Partial Derivatives for Forces

	# Lift
	CL0 = 0.23  # zero angle of attack lift coefficient
	CLalpha = math.pi * AR / (1 + math.sqrt(1 + (AR / 2.) ** 2))
	CLalpha = 5.61  # given in book
	CLq = 7.95  # needs to be normalized by c/2*Va
	CLdeltaE = 0.13  # lift due to elevator deflection

	# Drag
	CDp = 0.06  # minimum drag, derived from terminal velocity of 74m/s (148 knots)
	CDalpha = 0.03  # drag slope
	CD0 = 0.043  # intercept of linarized drag slope
	CDq = 0  # drag wrt pitch rate
	CDdeltaE = 0.0135  # drag due to elevator deflection

	# Pitching Moment
	CM0 = 0.0135  # intercept of pitching moment
	CMalpha = -2.74  # pitching moment slope
	CMq = -38.21  # pitching moment wrt q
	CMdeltaE = -0.99  # pitching moment from elevator

	# Sideforce
	CY0 = 0.
	CYbeta = -0.98
	CYp = 0.
	CYr = 0.
	CYdeltaA = 0.075
	CYdeltaR = 0.19

	# Rolling Moment
	Cl0 = 0.
	Clbeta = -0.13
	Clp = -0.51
	Clr = 0.25
	CldeltaA = 0.17
	CldeltaR = 0.0024

	# Yawing Moment
	Cn0 = 0.
	Cnbeta = 0.073
	Cnp = 0.069
	Cnr = -0.095
	CndeltaA = -0.011
	CndeltaR = -0.069

	# Propeller Thrust
	Sprop = 0.2027 # propellor area [m^2]
	kmotor = 80.  # motor constant
	kTp = 0.  # motor torque constant
	kOmega = 0.  # motor speed constant
	Cprop = 1.0  # propeller thrust coefficient

	# Alternate Propellor Model
	D_prop = 20. * (0.0254)  # prop diameter in m
	#
	# # Motor parameters
	KV = 145.  # from datasheet RPM/V
	KQ = (1. / KV) * 60. / (2. * math.pi)  # KQ in N-m/A, V-s/rad
	R_motor = 0.042  # ohms
	i0 = 1.5  # no-load (zero-torque) current (A)

	# Inputs
	ncells = 12.
	V_max = 3.7 * ncells  # max voltage for specified number of battery cells

	# Coeffiecients from prop_data fit (from lecture slide)
	C_Q2 = -0.01664
	C_Q1 = 0.004970
	C_Q0 = 0.005230
	C_T2 = -0.1079
	C_T1 = -0.06044
	C_T0 = 0.09357

	# roll rate and yaw rate derived parameters
	Cp0 = JinvBody[0][0] * Cl0 + JinvBody[0][2] * Cn0
	Cpbeta = JinvBody[0][0] * Clbeta + JinvBody[0][2] * Cnbeta
	Cpp = JinvBody[0][0] * Clp + JinvBody[0][2] * Cnp
	Cpr = JinvBody[0][0] * Clr + JinvBody[0][2] * Cnr
	CpdeltaA = JinvBody[0][0] * CldeltaA + JinvBody[0][2] * CndeltaA
	CpdeltaR = JinvBody[0][0] * CldeltaR + JinvBody[0][2] * CndeltaR
	Cr0 = JinvBody[0][2] * Cl0 + JinvBody[2][2] * Cn0
	Crbeta = JinvBody[0][2] * Clbeta + JinvBody[2][2] * Cnbeta
	Crp = JinvBody[0][2] * Clp + JinvBody[2][2] * Cnp
	Crr = JinvBody[0][2] * Clr + JinvBody[2][2] * Cnr
	CrdeltaA = JinvBody[0][2] * CldeltaA + JinvBody[2][2] * CndeltaA
	CrdeltaR = JinvBody[0][2] * CldeltaR + JinvBody[2][2] * CndeltaR

	# rudder and aileron trim matrix
	CprdeltaARinv = MatrixMath.scalarMultiply(1.0 / (CpdeltaA * CrdeltaR - CpdeltaR * CrdeltaA), [[CrdeltaR, -CpdeltaR],
																										[-CrdeltaA, CpdeltaA]])

	# Dyden Wind Gust Model Coefficients
	DrydenLowAltitudeLight = Inputs.drydenParameters(200.0, 200.0, 50.0, 1.06, 1.06, 0.7)
	DrydenLowAltitudeModerate = Inputs.drydenParameters(200.0, 200.0, 50.0, 2.12, 2.12, 1.4)
	DrydenHighAltitudeLight = Inputs.drydenParameters(533.0, 533.0, 533.0, 1.5, 1.5, 1.5)
	DrydenHighAltitudeModerate = Inputs.drydenParameters(533.0, 533.0, 533.0, 3.0, 3.0, 3.0)
	DrydenNoGusts = Inputs.drydenParameters(200.0, 200.0, 50.0, 0.0, 0.0, 0.0)
	DrydenNoWind = Inputs.drydenParameters(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

	SteadyWinds = [('No Wind',(0.0, 0.0, 0.0)),('Light Wind',(3.0, -5.0, 0.0)),
				   ('Moderate Wind',(-12.0, 3.5, 0.0)), ('Strong Wind',(-16.0, -16.0, 0.0))]
	GustWinds = [('No Gusts', DrydenNoGusts),('Light Low Altitude', DrydenLowAltitudeLight), ('Moderate Low Altitude', DrydenLowAltitudeModerate),
				 ('Light High Altitude', DrydenHighAltitudeLight), ('Moderate High Altitude', DrydenHighAltitudeModerate)]

	#Min Max Control Inputs for checking limits
	minControls = Inputs.controlInputs(0.0, -math.radians(25.0), -math.radians(25.0), -math.radians(25.0))
	maxControls = Inputs.controlInputs(1.0, math.radians(25.0), math.radians(25.0), math.radians(25.0))

	#Aircraft maneuver limits
	bankAngleLimit = 60.0	# [deg] This is aggressive, 30-45 degrees is more usual
	courseAngleLimit = 45.0	# [deg] how much course change before saturating integrator
	pitchAngleLimit = 30.0	# [deg] This is aggressive, 15-20 degrees is more usual
	altitudeHoldZone = 30.0	# [m] the saturation zone when to switch modes on altitude hold

	massNames = ["InitialSpeed", 'mass', 'b', 'c', 'S', "alpha0", 'Jxx', 'Jyy', 'Jzz', 'Jxz']
	massValues = [InitialSpeed, mass, b, c, S, alpha0, Jxx, Jyy, Jzz, Jxz]
	liftNames = ['CL0', 'CLalpha', 'CLq', 'CLdeltaE']
	liftValues = [CL0, CLalpha, CLq, CLdeltaE]
	dragNames = ['CD0', 'CDalpha', 'CDp', 'CDq', 'CDdeltaE']
	dragValues = [CD0, CDalpha, CDp, CDq, CDdeltaE]
	pitchNames = ['CM0', 'CMalpha', 'CMq', 'CMdeltaE']
	pitchValues = [CM0, CMalpha, CMq, CMdeltaE]
	sideNames = ['CY0', 'CYbeta', 'CYp', 'CYr', 'CYdeltaA', 'CYdeltaR']
	sideValues = [CY0, CYbeta, CYp, CYr, CYdeltaA, CYdeltaR]
	rollNames = ['Cl0', 'Clbeta', 'Clp', 'Clr', 'CldeltaA', 'CldeltaR']
	rollValues = [Cl0, Clbeta, Clp, Clr, CldeltaA, CldeltaR]
	yawNames = ['Cn0', 'Cnbeta', 'Cnp', 'Cnr', 'CndeltaA', 'CndeltaR']
	yawValues = [Cn0, Cnbeta, Cnp, Cnr, CndeltaA, CndeltaR]
	motorNames = ['kmotor', 'kTp', 'kOmega', 'KV', 'KQ', 'R_motor', 'i0', 'ncells', 'V_max']
	motorValues = [kmotor, kTp, kOmega, KV, KQ, R_motor, i0, ncells, V_max]
	propNames = ['Sprop', 'Cprop', 'D_prop', 'C_Q0', 'C_T0']
	propValues = [Sprop, Cprop, D_prop, C_Q0, C_T0]
	limitNames = ["bankAngleLimit", "courseAngleLimit", "pitchAngleLimit", "altitudeHoldZone"]
	limitValues = [bankAngleLimit, courseAngleLimit, pitchAngleLimit, altitudeHoldZone]

	defaultFile = "VPC_{}.pickle"
	varLib = ['mass', 'lift', 'drag', 'pitch', 'side', 'roll', 'yaw', 'motor', 'prop', 'limit']

	massFile = varLib[0]
	liftFile = varLib[1]
	dragFile = varLib[2]
	pitchFile = varLib[3]
	sideFile = varLib[4]
	rollFile = varLib[5]
	yawFile = varLib[6]
	motorFile = varLib[7]
	propFile = varLib[8]
	limitFile = varLib[9]

	# Create dictionaries for each VPC
	massDict = dict(zip(massNames, massValues))
	liftDict = dict(zip(liftNames, liftValues))
	dragDict = dict(zip(dragNames, dragValues))
	pitchDict = dict(zip(pitchNames, pitchValues))
	sideDict = dict(zip(sideNames, sideValues))
	rollDict = dict(zip(rollNames, rollValues))
	yawDict = dict(zip(yawNames, yawValues))
	motorDict = dict(zip(motorNames, motorValues))
	propDict = dict(zip(propNames, propValues))
	limitDict = dict(zip(limitNames, limitValues))

elif mvc.airframeMode == mvc.explorerAirframe:
	LINEARMAX = 250
	ROTATEMAX = 180

	dT = 1 / 100  # Time step for simulation
	RstarMax = 2500  # maximum radius before considered a straight line

	# parameters for Aerosonde UAV
	InitialSpeed = 15.0  # [m/s]
	InitialNorthPosition = 0.0  # displacement to north [m]
	InitialEastPosition = 0.0  # displacement to east [m]
	InitialDownPosition = -100.0  # [m], negative is above ground
	InitialYawAngle = math.radians(0.0)  # initial heading angle [rad]

	mass = 0.948  # [kg] 0.948
	rho = 1.2682  # [kg / m^3]
	g0 = 9.81  # gravity, [m/s^2]
	b = 1.5  # wing-span [m]
	c = 0.160  # wing chord [m]
	S = 0.24  # wing area [m^2]
	e = 0.9  # Oswald's Efficiency Factor []

	AR = b ** 2 / S

	M = 50.  # barrier function coefficient for angle of attack
	stallAngle = 11
	alpha0 = math.radians(stallAngle)  # angle at which stall occurs [deg]

	Jxx = 0.28286  # [kg m^2] ???
	Jyy = 0.178614  # [kg m^2] ???
	Jzz = 0.365827  # [kg m^2] ???
	Jxz = 0.073396535  # [kg m^2] ???

	Jbody = [[Jxx, 0., -Jxz], [0., Jyy, 0.], [-Jxz, 0., Jzz]]
	Jdet = (Jxx * Jzz - Jxz ** 2)
	JinvBody = MatrixMath.scalarMultiply(1. / Jdet, [[Jzz, 0., Jxz], [0., Jdet / Jyy, 0.], [Jxz, 0., Jxx]])

	Gamma1 = (Jxx - Jyy + Jzz) * JinvBody[0][2]
	Gamma2 = (Jzz * (Jzz - Jyy) + Jxz ** 2) / Jdet
	Gamma7 = (Jxx * (Jxx - Jyy) + Jxz ** 2) / Jdet

	# Aerodynamic Partial Derivatives for Forces

	# Lift
	CL0 = 0.36294  # zero angle of attack lift coefficient
	CLalpha = math.pi * AR / (1 + math.sqrt(1 + (AR / 2.) ** 2))
	CLalpha = 5.161536  # given in book
	CLq = 7.659471  # needs to be normalized by c/2*Va
	CLdeltaE = 0.1044  # ??? lift due to elevator deflection

	# Drag
	CDp = 0.0  # ???? minimum drag
	CDalpha = 0.0  # ???? drag slope
	CD0 = 0.0  # ???? intercept of linarized drag slope
	CDq = 0  # ???? drag wrt pitch rate
	CDdeltaE = 0.0  # drag due to elevator deflection

	# Pitching Moment
	CM0 = 0.0  # intercept of pitching moment
	CMalpha = -0.655797  # ??? pitching moment slope
	CMq = -21.365163  # ??? pitching moment wrt q
	CMdeltaE = -0.19226  # ??? pitching moment from elevator

	# ??????? Sideforce
	CY0 = 0.
	CYbeta = -0.156  # ????
	CYp = 0.0
	CYr = 0.0
	CYdeltaA = 0.000210  # ????
	CYdeltaR = -0.001715

	# Rolling Moment
	Cl0 = 0.
	Clbeta = -0.025271  # ????
	Clp = -0.560230
	Clr = 0.095392
	CldeltaA = 0.209039
	CldeltaR = 0.000036

	# Yawing Moment
	Cn0 = 0.
	Cnbeta = 0.052131
	Cnp = -0.029271  # ????
	Cnr = -0.042798
	CndeltaA = -0.003558
	CndeltaR = -0.000645

	# Propeller Thrust
	Sprop = 0.00861  # propeller area [m^2]
	kmotor = 80.  # motor constant
	kTp = 0.  # motor torque constant
	kOmega = 0.  # motor speed constant
	Cprop = 1.3962  # propeller thrust coefficient

	# Alternate Propellor Model
	D_prop = 0.2286  # prop diameter in m
	#
	# # Motor parameters
	KV = 1120.  # from datasheet RPM/V
	KQ = (1. / KV) * 60. / (2. * math.pi)  # KQ in N-m/A, V-s/rad
	R_motor = 0.068  # ohms
	i0 = 1.2  # no-load (zero-torque) current (A)

	# Inputs
	ncells = 3.
	V_max = 12.6  # max voltage for specified number of battery cells

	# Coeffiecients from prop_data fit (from lecture slide)
	# C_Q2 = -0.01664
	# C_Q1 = 0.004970
	C_Q0 = 0.005230
	# C_T2 = -0.1079
	# C_T1 = -0.06044
	C_T0 = 0.09357

	# roll rate and yaw rate derived parameters
	Cp0 = JinvBody[0][0] * Cl0 + JinvBody[0][2] * Cn0
	Cpbeta = JinvBody[0][0] * Clbeta + JinvBody[0][2] * Cnbeta
	Cpp = JinvBody[0][0] * Clp + JinvBody[0][2] * Cnp
	Cpr = JinvBody[0][0] * Clr + JinvBody[0][2] * Cnr
	CpdeltaA = JinvBody[0][0] * CldeltaA + JinvBody[0][2] * CndeltaA
	CpdeltaR = JinvBody[0][0] * CldeltaR + JinvBody[0][2] * CndeltaR
	Cr0 = JinvBody[0][2] * Cl0 + JinvBody[2][2] * Cn0
	Crbeta = JinvBody[0][2] * Clbeta + JinvBody[2][2] * Cnbeta
	Crp = JinvBody[0][2] * Clp + JinvBody[2][2] * Cnp
	Crr = JinvBody[0][2] * Clr + JinvBody[2][2] * Cnr
	CrdeltaA = JinvBody[0][2] * CldeltaA + JinvBody[2][2] * CndeltaA
	CrdeltaR = JinvBody[0][2] * CldeltaR + JinvBody[2][2] * CndeltaR

	# rudder and aileron trim matrix
	CprdeltaARinv = MatrixMath.scalarMultiply(1.0 / (CpdeltaA * CrdeltaR - CpdeltaR * CrdeltaA), [[CrdeltaR, -CpdeltaR],
																								  [-CrdeltaA,
																								   CpdeltaA]])

	# Dyden Wind Gust Model Coefficients
	DrydenLowAltitudeLight = Inputs.drydenParameters(200.0, 200.0, 50.0, 1.06, 1.06, 0.7)
	DrydenLowAltitudeModerate = Inputs.drydenParameters(200.0, 200.0, 50.0, 2.12, 2.12, 1.4)
	DrydenHighAltitudeLight = Inputs.drydenParameters(533.0, 533.0, 533.0, 1.5, 1.5, 1.5)
	DrydenHighAltitudeModerate = Inputs.drydenParameters(533.0, 533.0, 533.0, 3.0, 3.0, 3.0)
	DrydenNoGusts = Inputs.drydenParameters(200.0, 200.0, 50.0, 0.0, 0.0, 0.0)
	DrydenNoWind = Inputs.drydenParameters(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

	SteadyWinds = [('No Wind', (0.0, 0.0, 0.0)), ('Light Wind', (3.0, -5.0, 0.0)),
				   ('Moderate Wind', (-12.0, 3.5, 0.0)), ('Strong Wind', (-16.0, -16.0, 0.0))]
	GustWinds = [('No Gusts', DrydenNoGusts), ('Light Low Altitude', DrydenLowAltitudeLight),
				 ('Moderate Low Altitude', DrydenLowAltitudeModerate),
				 ('Light High Altitude', DrydenHighAltitudeLight),
				 ('Moderate High Altitude', DrydenHighAltitudeModerate)]

	# Min Max Control Inputs for checking limits
	minControls = Inputs.controlInputs(0.0, -math.radians(25.0), -math.radians(25.0), -math.radians(25.0))
	maxControls = Inputs.controlInputs(1.0, math.radians(25.0), math.radians(25.0), math.radians(25.0))

	# Aircraft maneuver limits
	bankAngleLimit = 30.0  # [deg] This is aggressive, 30-45 degrees is more usual
	courseAngleLimit = 45.0  # [deg] how much course change before saturating integrator
	pitchAngleLimit = 15.0  # [deg] This is aggressive, 15-20 degrees is more usual
	altitudeHoldZone = 30.0  # [m] the saturation zone when to switch modes on altitude hold
	nominalThrottle = 0.5  # nominal throttle required for straight and level flight

	massNames = ["InitialSpeed", 'mass', 'b', 'c', 'S', "alpha0", 'Jxx', 'Jyy', 'Jzz', 'Jxz']
	massValues = [InitialSpeed, mass, b, c, S, alpha0, Jxx, Jyy, Jzz, Jxz]
	liftNames = ['CL0', 'CLalpha', 'CLq', 'CLdeltaE']
	liftValues = [CL0, CLalpha, CLq, CLdeltaE]
	dragNames = ['CD0', 'CDalpha', 'CDp', 'CDq', 'CDdeltaE']
	dragValues = [CD0, CDalpha, CDp, CDq, CDdeltaE]
	pitchNames = ['CM0', 'CMalpha', 'CMq', 'CMdeltaE']
	pitchValues = [CM0, CMalpha, CMq, CMdeltaE]
	sideNames = ['CY0', 'CYbeta', 'CYp', 'CYr', 'CYdeltaA', 'CYdeltaR']
	sideValues = [CY0, CYbeta, CYp,CYr, CYdeltaA, CYdeltaR]
	rollNames = ['Cl0', 'Clbeta', 'Clp', 'Clr', 'CldeltaA', 'CldeltaR']
	rollValues = [Cl0, Clbeta, Clp, Clr, CldeltaA, CldeltaR]
	yawNames = ['Cn0', 'Cnbeta', 'Cnp', 'Cnr', 'CndeltaA', 'CndeltaR']
	yawValues = [Cn0, Cnbeta, Cnp, Cnr, CndeltaA, CndeltaR]
	motorNames = ['kmotor', 'kTp', 'kOmega', 'KV', 'KQ', 'R_motor', 'i0', 'ncells', 'V_max']
	motorValues = [kmotor, kTp, kOmega, KV, KQ, R_motor, i0, ncells, V_max]
	propNames = ['Sprop', 'Cprop', 'D_prop', 'C_Q0', 'C_T0']
	propValues = [Sprop, Cprop, D_prop, C_Q0, C_T0]
	limitNames = ["bankAngleLimit", "courseAngleLimit", "pitchAngleLimit", "altitudeHoldZone"]
	limitValues = [bankAngleLimit, courseAngleLimit, pitchAngleLimit, altitudeHoldZone]



	defaultFile = "VPC_{}.pickle"
	varLib = ['mass', 'lift', 'drag', 'pitch', 'side', 'roll', 'yaw', 'motor', 'prop', 'limit']

	massFile = varLib[0]
	liftFile = varLib[1]
	dragFile = varLib[2]
	pitchFile = varLib[3]
	sideFile = varLib[4]
	rollFile = varLib[5]
	yawFile = varLib[6]
	motorFile = varLib[7]
	propFile = varLib[8]
	limitFile = varLib[9]

	# Create dictionaries for each VPC
	massDict = dict(zip(massNames, massValues))
	liftDict = dict(zip(liftNames, liftValues))
	dragDict = dict(zip(dragNames, dragValues))
	pitchDict = dict(zip(pitchNames, pitchValues))
	sideDict = dict(zip(sideNames, sideValues))
	rollDict = dict(zip(rollNames, rollValues))
	yawDict = dict(zip(yawNames, yawValues))
	motorDict = dict(zip(motorNames, motorValues))
	propDict = dict(zip(propNames, propValues))
	limitDict = dict(zip(limitNames, limitValues))

	# Check for pickles and if so, then overwrite all VPC variables
	#massDict = loadVPC(massFile, massDict)
updateVPC()












