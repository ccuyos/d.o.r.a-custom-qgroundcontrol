/* 
 * File:   AirframeConstants.h
 * Author: Margaret Silva
 * Brief: This header file contains all constants used in the other files of 
 * this autopilot
 *
 * Created on February 14, 2023, 2:40 PM
 */

#ifndef AIRFRAMECONSTANTS_H
#define	AIRFRAMECONSTANTS_H

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
// HIL toggles
#define HIL_SRL
//#define HIL_STATE_TESTING
//#define USING_163_SIMULATED_PLANE // swaps between using values dervived from real airframe and simulated airframe
#define USE_AUTONOMOUS_CONTROLS // control actuators using outputs of stability controller
        
//integration constant
#define SCALE_TIME 1
#define DT_ms (SCALE_TIME*10) // [ms]
#define DT_s (SCALE_TIME*0.01) // [s]
#define CONTROL_PERIOD (SCALE_TIME*10) // Period for control loop in msec #10 * scale time

// Sensor measurement periods
#define IMU_SCALE_TIME 1
#define IMU_MEAS_PERIOD (IMU_SCALE_TIME*10)
#define IMU_DT (IMU_SCALE_TIME*0.01)

#define GPS_SCALE_TIME 1
#define GPS_MEAS_PERIOD (GPS_SCALE_TIME*1000)
#define GPS_DT (GPS_SCALE_TIME*0.01)

#define BARO_SCALE_TIME 1
#define BARO_MEAS_PERIOD (BARO_SCALE_TIME*10)
#define BARO_DT (BARO_SCALE_TIME*0.01)

// Sensor availability flags
#define IMU_AVAILABLE 0b0001//(1 << 0)
#define GPS_AVAILABLE 0b0010//(1 << 1)
#define BARO_AVAILABLE 0b0100//(1 << 2)
#define GPS_FIRST_DATA_RECEIVED 0b1000//(1 << 3) // at least 1 gps packet has been received

// safety constraints
#define IDEAL_AIRSPEED 15.0
#define IDEAL_ALTITUDE 30.0

// altitude hold threshold
#define ALTITUDE_HOLD_THRESHOLD 30.0 // [m]

// define actuator limits (angles are in radians)
#ifdef USING_163_SIMULATED_PLANE
#define MAX_AILERON_ANGLE 0.43633 
#define MIN_AILERON_ANGLE -0.43633 
#define MAX_RUDDER_ANGLE 0.43633 
#define MIN_RUDDER_ANGLE -0.43633 
#define MAX_ELEVATOR_ANGLE 0.43633 
#define MIN_ELEVATOR_ANGLE -0.43633 
#else // actual airframe characteristics
#define MAX_AILERON_ANGLE 0.349  
#define MIN_AILERON_ANGLE -0.349  
#define MAX_RUDDER_ANGLE 0.436  
#define MIN_RUDDER_ANGLE -.384 
#define MAX_ELEVATOR_ANGLE 0.349  
#define MIN_ELEVATOR_ANGLE -0.367  
#endif
#define MAX_THROTTLE 1.0
#define MIN_THROTTLE 0.0

// define turn and pitch limits
#ifdef USING_163_SIMULATED_PLANE
#define MAX_ROLL_ANGLE 1.04719755    
#define MIN_ROLL_ANGLE -1.04719755
#define MAX_PITCH_ANGLE 0.52359878
#define MIN_PITCH_ANGLE -0.52359878
#define COURSE_ANGLE_LIMIT 0.7853981634
#else // actual airframe characteristics
#define MAX_ROLL_ANGLE 0.5236    
#define MIN_ROLL_ANGLE -0.5236 
#define MAX_PITCH_ANGLE 0.2618
#define MIN_PITCH_ANGLE -0.2618
#define COURSE_ANGLE_LIMIT 0.7854
#endif

// define angle to pwm outputs for servos
#define AILERON_SLOPE -23.6 // pwm/deg
#define AILERON_INTERCEPT 1498 // pwm
#define RUDDER_SLOPE -18.7 // pwm/deg
#define RUDDER_INTERCEPT 1516 // pwm
#define ELEVATOR_SLOPE -21.5 // pwm/deg
#define ELEVATOR_INTERCEPT 1473 // pwm

// define trim values - calculated by simulator
#ifdef USING_163_SIMULATED_PLANE    
#define TRIM_CONTROLS_THROTTLE 0.8082
#define TRIM_CONTROLS_ELEVATOR -0.1235
#define TRIM_CONTROLS_AILERON 0.007474
#define TRIM_CONTROLS_RUDDER -0.001232
#else // actual airframe characteristics
#define TRIM_CONTROLS_THROTTLE 0.5
#define TRIM_CONTROLS_ELEVATOR 0.001079
#define TRIM_CONTROLS_AILERON 0.0001308
#define TRIM_CONTROLS_RUDDER -0.0000257
#endif

// general physical constants
#define g0 9.81
#define rho 1.2682
#define MAGFIELD_VEC {22750.0, 5286.8, 41426.3} // update this for specific area

// starting location
#define INITIAL_PN 0
#define INITIAL_PE 0
#define INITIAL_PD -100

// sensor constants
#define GROUND_PRESSURE 987.75 // 101325.0 // update this with true ground pressure 

#endif	/* AIRFRAMECONSTANTS_H */

