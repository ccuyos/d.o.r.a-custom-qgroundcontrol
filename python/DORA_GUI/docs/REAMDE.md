# ECE163 Autogenerated Documentation

For the ECE163 class, we use python sphinx to autogenerate the code documentation and push it both to the website and to create the LaTEX version of the code documentation. This allows the code to be updated and the subsequent documentation to be generated easily and to accomodate refactoring and other required changes.

## Setup -- Python Sphinx

The tool we use is Sphinx (see: [Sphinx-Doc.org](https://www.sphinx-doc.org/en/master/index.html)) to generate the documentation. To install sphinx, you need to use pip:

```python -m pip install -U sphinx```

The -U flag upgrades the specified package to its latest stable version. Note that the scripts might be placed in a weird folder and you will need to add that folder to the path in order to get it to work.

## Using the RST files

Sphinx uses RST files to designate which files to document and how. These files are in the repo (see for example: [index.rst](./index.rst)). Mode details on these can be found in the documentation, and in the structure of the development/docs folder.

We have also provided a quick way to autogenerate the rst file using the [GenerateTemplate.py](../../Utilities/SphinxTemplateGeneration/GenerateTemplate.py) file. See the help on that function.

## Generating the documentation

Once the setup is completed, then the documentation can be generated using the included make files. These must be run from Powershell or the command prompt under windows.

Navigate to the development/docs folder and run:

```make html``` to make the website version of the documentation. It will be found in _build/html. Once it has been created, use an FTP program to upload it to the classes website under the docs folder.

```make latex``` to create the latex version (which can be made into a PDF and uploaded to the website). For reasons that are not entirely clear, the file created is ECE163_codedocumentation rather than ECE163_CodeDocumentation. This must be changed by hand before uploading to the CANVAS website.