"""
Test file to check that the computation of gains works
"""

import math
import sys

sys.path.append('..')
import ece163.Controls.VehicleTrim as VehicleTrim
import ece163.Controls.VehicleControlGains as VehicleControlGains
import ece163.Controls.VehiclePerturbationModels as VehiclePerturbationModels
import ece163.Containers.States as States
import ece163.Containers.Inputs as Inputs
import ece163.Containers.Controls as Controls
import ece163.Containers.Linearized as Linearized

vTrim = VehicleTrim.VehicleTrim()
trimControls = Inputs.controlInputs()
trimState = States.vehicleState()
controlGains = Controls.controlGains()
tuningParams = Controls.controlTuning()
XferFn = Linearized.transferFunctions()

vTrim = VehicleTrim.VehicleTrim()
Vastar = 25.0
Gammastar = math.radians(2.0)
Kappastar = 0.0

check = vTrim.computeTrim(Vastar, Kappastar, Gammastar)
if check:
	print("Optimization successful")
else:
	print("Model converged outside of valid inputs, change parameters and try again")

print("Controls:")
trimControls = vTrim.getTrimControls()
print(trimControls)

print("State:")
trimState = vTrim.getTrimState()
print(trimState)

XferFn = VehiclePerturbationModels.CreateTransferFunction(trimState, trimControls)
print(XferFn)

tP = Controls.controlTuning()


tuningParams.Wn_roll = 20
tuningParams.Zeta_roll = 1.0 / math.sqrt(2)

tuningParams.Wn_course = tuningParams.Wn_roll / 20.0
tuningParams.Zeta_course = 1.0

tuningParams.Wn_sideslip = tuningParams.Wn_roll / 2.0
tuningParams.Zeta_sideslip = 0.707

tuningParams.Wn_pitch = 24.0
tuningParams.Zeta_pitch = 0.707

tuningParams.Wn_altitude = tuningParams.Wn_pitch / 30.0
tuningParams.Zeta_altitude = 1.0

tuningParams.Wn_SpeedfromThrottle = 3.0
tuningParams.Zeta_SpeedfromThrottle = 2

tuningParams.Wn_SpeedfromElevator = tuningParams.Wn_SpeedfromThrottle
tuningParams.Zeta_SpeedfromElevator = tuningParams.Zeta_SpeedfromThrottle

controlGains = VehicleControlGains.computeGains(tuningParams, XferFn)
print(controlGains)
tP = VehicleControlGains.computeTuningParameters(controlGains, XferFn)
print("injected: ")
print(tuningParams)
print("recovered :")
print(tP)





