'''
    Author: Margaret Silva
    This file contains the functions used to model the vehicle aerodynamics
'''

import math
from ..Containers import States
from ..Containers import Inputs
from ..Modeling import VehicleDynamicsModel as VDM
from ..Modeling import WindModel 
from ..Utilities import MatrixMath
from ..Utilities import Rotations
from ..Constants import VehiclePhysicalConstants as VPC

class VehicleAerodynamicsModel():

    def __init__(self, initialSpeed=VPC.InitialSpeed, initialHeight=VPC.InitialDownPosition):
        ''' Initializes current instance of vehicle aerodynamics model '''
        self.vehicleDynamics = VDM.VehicleDynamicsModel()
        self.vehicleDynamics.state.u = initialSpeed 
        self.vehicleDynamics.state.pd = initialHeight
        self.initialSpeed = initialSpeed
        self.initialHeight = initialHeight
        self.windModel = WindModel.WindModel()

    def reset(self):
        ''' Returns vehicle aerodynamics model to inital state '''
        self.vehicleDynamics = VDM.VehicleDynamicsModel()
        self.vehicleDynamics.state.u = self.initialSpeed 
        self.vehicleDynamics.state.pd = self.initialHeight
        self.windModel = WindModel.WindModel()

    def getVehicleState(self):
        ''' Returns the current vehicle state of the aerodynamics model '''
        return self.vehicleDynamics.getVehicleState()

    def setVehicleState(self, newDynamics):
        ''' Set the stored vehicle state to the specified argument '''
        self.vehicleDynamics.setVehicleState(newDynamics)

    def getVehicleDynamicsModel(self):
        ''' Returns the vehicle dynamics model '''
        return self.vehicleDynamics
    
    def getWindModel(self):
        ''' Returns the wind model '''
        return self.windModel
    
    def setWindModel(self, newModel):
        ''' Sets the current wind model to the one passed as argument '''
        self.windModel = newModel

    def Update(self, controls):
        ''' Updates the current state of the vehicle using the aerodynamic forces '''
        self.windModel.Update()
        forces = self.updateForces(self.vehicleDynamics.state, controls, self.windModel.getWind())
        self.vehicleDynamics.Update(forces)
    

    def CalculateAirspeed(self, state, windState):
        ''' 
            Calculates airspeed from wind state and current velocity 
            returns Va [m/s], alpha [rad], beta [rad]
        '''
        # lamba function to make a number within the range -1 < n < 1
        rc = lambda n: -1.0 if n < -1.0 else (1.0 if n > 1.0 else n)

        # extract steady state and gust vectors
        steady = [[windState.Wn], [windState.We], [windState.Wd]]
        gust = [[windState.Wu], [windState.Wv], [windState.Ww]]
        # extract vehicle velocity vector
        vehicle = [[state.u], 
                   [state.v], 
                   [state.w]]

        # calculate course and pitch wind angles
        Ws = math.hypot(windState.Wn, windState.We, windState.Wd)
        if math.isclose(Ws, 0):
            xw = 0
            yw = 0
        else:
            xw = math.atan2(windState.We, windState.Wn)
            # make sure input to arcsin is within range
            yw = -1.0*math.asin(rc(windState.Wd/Ws))

        # calculate rotation matrices
        Rxw = Rotations.euler2DCM(xw, 0, 0)
        Ryw = Rotations.euler2DCM(0, yw, 0)
        Ryxw = MatrixMath.multiply(Ryw, Rxw)

        # rotate wind vectors into the body frame
        gust_i = MatrixMath.multiply(MatrixMath.transpose(Ryxw), gust)
        wind_i = MatrixMath.add(steady, gust_i)
        wind_b = MatrixMath.multiply(state.R, wind_i)
        Va_vec = MatrixMath.subtract(vehicle, wind_b)

        # calculate returned params
        Va = math.hypot(Va_vec[0][0], Va_vec[1][0], Va_vec[2][0])
        if math.isclose(Va, 0):
            return 0, 0, 0
        alpha = math.atan2(Va_vec[2][0], Va_vec[0][0])
        beta = math.asin(rc(Va_vec[1][0]/Va))

        return Va, alpha, beta
    

    def gravityForces(self, state):
        ''' Projects gravity forces into the body frame '''
        gI = [[0.0], [0.0], [VPC.mass*VPC.g0]]   # force due to gravity in the inertial frame
        Fg = MatrixMath.multiply(state.R, gI)   # rotate from intertial to body frame

        # turn resulting vector into a forcesMoments object
        force_Fg = Inputs.forcesMoments(Fx=Fg[0][0], Fy=Fg[1][0], Fz=Fg[2][0])
        return force_Fg


    def sigma (self, a , a0 , M):
        ''' Helper function to calculate the value of the sigma function '''
        # prevent overflow errors
        try:
            math.exp(- M * (a - a0 ))
        except OverflowError:
            return 1.0    # if the value overflows, the value of sigma has saturated to 1
        try:
            math.exp(M * ( a+ a0 ))
        except OverflowError:
            return 1.0

        # assuming no overflow errors
        num = (1.0 + math.exp (- M *(a - a0 )) + math.exp (M *( a+ a0 )))
        den = (1.0 + math.exp (- M *(a - a0 ))) *(1.0 + math.exp (M *( a+ a0 ) ))
        return num / den


    def CalculateCoeff_alpha(self, alpha):
        ''' 
            Calculates coefficients of lift and drag 
            returns CL, CD, CM
        '''
        CL_attached = VPC.CL0 + VPC.CLalpha * alpha
        CD_attached = VPC.CDp + ((CL_attached**2)/(math.pi * VPC.e * VPC.AR))
        CL_sep = 2.0*math.sin(alpha)*math.cos(alpha)
        CD_sep = 2.0*(math.sin(alpha)**2)
        sig = self.sigma(alpha, VPC.alpha0, VPC.M)

        CL = (1.0 - sig)*CL_attached + sig*CL_sep
        CD = (1.0 - sig)*CD_attached + sig*CD_sep
        CM = VPC.CM0 + VPC.CMalpha*alpha

        return CL, CD, CM


    def CalculateCoeff_beta(self, beta):
        ''' 
            Helper function to calculate coefficients that are a function of beta 
            returns CY, Cl, Cn
        '''
        CY = VPC.CY0 + VPC.CYbeta*beta
        Cl = VPC.Cl0 + VPC.Clbeta*beta
        Cn = VPC.Cn0 + VPC.Cnbeta*beta

        return CY, Cl, Cn


    def aeroForces(self, state):
        ''' Calculates aerodynamic forces acting on plane without taking actuator controls into account '''
        # alpha rotation matrix
        R_alpha = Rotations.euler2DCM(yaw=0.0, pitch=state.alpha, roll=0.0)
        # calculate coefficients
        CL, CD, CM = self.CalculateCoeff_alpha(state.alpha)
        CY, Cl, Cn = self.CalculateCoeff_beta(state.beta)

        # prevent divide by zero error
        if math.isclose(state.Va, 0.0):
            c_2Va = 1.0
            b_2Va = 1.0
        else:
            c_2Va = 0.5*VPC.c*state.q / state.Va
            b_2Va = 0.5 * VPC.b / state.Va

        # calculate forces
        F_lift = 0.5*VPC.rho*(state.Va**2)*VPC.S*(CL + VPC.CLq*c_2Va)
        F_drag = 0.5*VPC.rho*(state.Va**2)*VPC.S*(CD + VPC.CDq*c_2Va)
        F_y = 0.5*VPC.rho*(state.Va**2)*VPC.S*(CY + VPC.CYp*b_2Va*state.p + VPC.CYr*b_2Va*state.r)

        # calculate moments
        m = 0.5*VPC.rho*(state.Va**2)*VPC.S*VPC.c*(CM + VPC.CMq*c_2Va)
        l = 0.5*VPC.rho*(state.Va**2)*VPC.S*VPC.b*(Cl + VPC.Clp*b_2Va*state.p + VPC.Clr*b_2Va*state.r)
        n = 0.5*VPC.rho*(state.Va**2)*VPC.S*VPC.b*(Cn + VPC.Cnp*b_2Va*state.p + VPC.Cnr*b_2Va*state.r)

        # convert forces into the correct frame (body frame)
        forces = MatrixMath.multiply(R_alpha, [[-F_drag], [F_y], [-F_lift]])

        # package calculated values into the forcesMoments object
        force_aero = Inputs.forcesMoments()
        force_aero.Fx = forces[0][0]
        force_aero.Fy = forces[1][0]
        force_aero.Fz = forces[2][0]
        force_aero.Mx = l
        force_aero.My = m
        force_aero.Mz = n

        return force_aero


    def controlForces(self, state, controls):
        ''' Calculates aerodynamic forces from the control surface deflections '''
        # get force and moment from propellor
        Fx_prop, Mx_prop = self.CalculatePropForces(state.Va, controls.Throttle)

        # alpha rotation matrix
        R_alpha = Rotations.euler2DCM(yaw=0.0, pitch=state.alpha, roll=0.0)

        # calculate forces from controls
        F_lift = 0.5*VPC.rho*(state.Va**2)*VPC.S*(VPC.CLdeltaE*controls.Elevator)
        F_drag = 0.5*VPC.rho*(state.Va**2)*VPC.S*(VPC.CDdeltaE*controls.Elevator)
        F_y = 0.5*VPC.rho*(state.Va**2)*VPC.S*(VPC.CYdeltaA*controls.Aileron + VPC.CYdeltaR*controls.Rudder)

        # calculate moments from controls
        l = 0.5*VPC.rho*(state.Va**2)*VPC.S*VPC.b*(VPC.CldeltaA*controls.Aileron + VPC.CldeltaR*controls.Rudder)
        m = 0.5*VPC.rho*(state.Va**2)*VPC.S*VPC.c*(VPC.CMdeltaE*controls.Elevator)
        n = 0.5*VPC.rho*(state.Va**2)*VPC.S*VPC.b*(VPC.CndeltaA*controls.Aileron + VPC.CndeltaR*controls.Rudder)

        # convert forces into the correct frame (body frame)
        forces = MatrixMath.multiply(R_alpha, [[-F_drag], [F_y], [-F_lift]])

        # package calculated values into the forcesMoments object
        force_control = Inputs.forcesMoments()
        force_control.Fx = forces[0][0] + Fx_prop   # include propeller force
        force_control.Fy = forces[1][0]
        force_control.Fz = forces[2][0]
        force_control.Mx = l + Mx_prop  # include propeller force
        force_control.My = m
        force_control.Mz = n

        return force_control


    def CalculatePropForces(self, Va, Throttle):
        ''' 
            Caculates the propeller forces and torques on the aircraft
            returns Fx_prop, Mx_prop
        '''
        # calculate propeller speed omega in rad/sec
        Vin = VPC.V_max*Throttle
        a = (VPC.rho*(VPC.D_prop**5)*VPC.C_Q0)/(4.0*(math.pi**2))
        b = ((VPC.rho*(VPC.D_prop**4)*Va*VPC.C_Q1)/(2.0*math.pi)) + ((VPC.KQ ** 2)/VPC.R_motor)
        c = VPC.rho*(VPC.D_prop**3)*(Va**2)*VPC.C_Q2 - VPC.KQ*(Vin/VPC.R_motor) + VPC.KQ*VPC.i0

        # prevent issues with propellor model - avoid imaginary omega values
        try:
            omega = (-b + math.sqrt((b**2) - 4.0*a*c))/(2.0*a)
        except:
            omega = 100.0

        # calculate advance ratio
        J = (2.0*math.pi*Va)/(omega*VPC.D_prop)

        # use advance ratio to calculate CQ and CT
        CT = VPC.C_T0 + VPC.C_T1*J + VPC.C_T2*(J**2)
        CQ = VPC.C_Q0 + VPC.C_Q1*J + VPC.C_Q2*(J**2)

        # calculate propeller force and torque from CT and CQ
        Fx = (VPC.rho*(omega**2)*(VPC.D_prop**4)*CT)/(4.0*(math.pi**2))
        Mx = (-VPC.rho*(omega**2)*(VPC.D_prop**5)*CQ)/(4.0*(math.pi**2))

        return Fx, Mx


    def updateForces(self, state, controls, wind=None):
        ''' Updates all aerodynamic, propulsive, and gravity forces and moments '''

        if wind == None:
            # calculate Va, alpha, and beta from current state
            Va = math.hypot(state.u, state.v, state.w)
            alpha = math.atan2(state.w, state.u)         # angle of attack
            # prevent divide by zero error
            try:
                beta = math.asin(state.v/Va)
            except ZeroDivisionError:
                beta = 0.0
        else:
            Va, alpha, beta = self.CalculateAirspeed(state, wind)

        # update state variable
        state.Va = Va
        state.alpha = alpha
        state.beta = beta

        # calculate all forces acting on the plane
        F_gravity = self.gravityForces(state)
        F_aero = self.aeroForces(state)
        F_control = self.controlForces(state, controls)

        # combine forces
        F_total = Inputs.forcesMoments()
        F_total.Fx = F_gravity.Fx + F_aero.Fx + F_control.Fx
        F_total.Fy = F_gravity.Fy + F_aero.Fy + F_control.Fy
        F_total.Fz = F_gravity.Fz + F_aero.Fz + F_control.Fz
        # combine the moments
        F_total.Mx = F_gravity.Mx + F_aero.Mx + F_control.Mx
        F_total.My = F_gravity.My + F_aero.My + F_control.My
        F_total.Mz = F_gravity.Mz + F_aero.Mz + F_control.Mz

        return F_total