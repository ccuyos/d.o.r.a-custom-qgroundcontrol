"""Rotations Library Test Harness. Runs through various tests of the functions to determine that they are, in fact, working."""

import math
import sys
sys.path.append('..')

import ece163.Utilities.Rotations as Rotations
import ece163.Utilities.MatrixMath as MatrixMath


yaw = -135
pitch = 54
roll = -15
print("Euler Angles --> yaw: {}\tpitch: {}\troll: {}".format(yaw, pitch, roll))
R = Rotations.euler2DCM(math.radians(yaw), math.radians(pitch), math.radians(roll))
print("DCM = ")
MatrixMath.matrixPrint(R)
newYaw, newPitch, newRoll = Rotations.dcm2Euler(R)
print("Recovered Euler Angles (deg) --> yaw: {}\tpitch: {}\troll: {}".format(math.degrees(newYaw),math.degrees(newPitch),math.degrees(newRoll)))
Q = Rotations.euler2Quaternion(math.radians(yaw), math.radians(pitch), math.radians(roll))
print('Unit quaternion = ')
MatrixMath.matrixPrint(Q)
print("Check on length of Q = ",str(MatrixMath.dotProduct(Q,Q)))
qYaw, qPitch, qRoll = Rotations.quaternion2Euler(Q)
print("Euler angles recovered from Q --> yaw: {}\tpitch: {}\troll: {}".format(math.degrees(qYaw),math.degrees(qPitch),math.degrees(qRoll)))
print("For the DCM from quaternion: ")
Rq = Rotations.quaternion2DCM(Q)
MatrixMath.matrixPrint(Rq)
print("Error between quaternion and Euler DCM = ")
MatrixMath.matrixPrint(MatrixMath.add(R,MatrixMath.scalarMultiply(-1.0,Rq)))
print("Checking the ned2enu conversion...")
Renu = MatrixMath.matrixPrint(Rotations.ned2enu(Rq))