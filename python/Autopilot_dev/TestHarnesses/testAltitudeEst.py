'''
    File: testStateEstOnFlightData.py
    Author: Margaret Silva
    Notes: Tests state estimation algorithms on test flight data
'''

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Controls.VehicleEstimator as Estimator
import ece163.Controls.VehicleClosedLoopControl as VCLC
import ece163.Containers.Controls as Controls
import ece163.Containers.Sensors as Sensors
import matplotlib.pyplot as plt
import csv
import pandas as pd

"""math.isclose doesn't work well for comparing things near 0 unless we 
use an absolute tolerance, so we make our own isclose:"""
isclose = lambda  a,b : math.isclose(a, b, abs_tol= 1e-12)

def compareVectors(a, b):
	"""A quick tool to compare two vectors"""
	el_close = [isclose(a[i][0], b[i][0]) for i in range(3)]
	return all(el_close)

#of course, you should test your testing tools too:
assert(compareVectors([[0], [0], [-1]],[[1e-13], [0], [-1+1e-9]]))
assert(not compareVectors([[0], [0], [-1]],[[1e-11], [0], [-1]]))
assert(not compareVectors([[1e8], [0], [-1]],[[1e8+1], [0], [-1]]))


failed = []
passed = []
def evaluateTest(test_name, boolean):
	"""evaluateTest prints the output of a test and adds it to one of two 
	global lists, passed and failed, which can be printed later"""
	if boolean:
		print(f"   passed {test_name}")
		passed.append(test_name)
	else:
		print(f"   failed {test_name}")
		failed.append(test_name)
	return boolean

#%% Begin Testing

#%% Write data from .txt to .csv file
# f = open('test_flight_5_21_2023.TXT', 'r')
# f_lines = (line.split(',') for line in f if line)

# f_csv = open('test_flight5_21_2023v2.csv', 'w', newline='')
# writer = csv.writer(f_csv)
# writer.writerows(f_lines)

#%% Parse data from .txt file

time_idx = 1
accX_idx = 2
accY_idx = 3
accZ_idx = 4
gyroX_idx = 5
gyroY_idx = 6
gyroZ_idx = 7
magX_idx = 8
magY_idx = 9
magZ_idx = 10
gpsLat_idx = 13
gpsLon_idx = 14
gpsSpd_idx = 15
gpsCog_idx = 16
temperature_idx = 17
pressure_idx = 18

DRIFT_GYROX = -0.004070617273368
DRIFT_GYROY = 2.369648499684174*(10**(-4))
DRIFT_GYROZ = 0.001435278922658
dT_GYRO = 0.02

dT = 20/1000

# Read columns from data set
# f_csv = open('test_flight5_21_2023.csv', 'r')
# full_df = pd.read_csv("baro_staircase_test.txt")
# full_df.to_csv('baro_staircase_test.csv')

full_df = pd.read_csv("baro_staircase_test.csv")

baro_data = full_df.iloc[:, 1]

n_steps = len(baro_data)

sensors_list = [Sensors.vehicleSensors(baro=100*float(baro_data[i])) for i in range(n_steps)]

est_alt = [0 for i in range(n_steps)]

test_controls = VCLC.VehicleClosedLoopControl()
control_gains = Controls.controlGains(3.0561, 0.001, 0.043177, .5, .05, 5.0968, 2.5484, -13.1825, 
				      -0.7931, 0.07743, 0.030975, 1.493, 1.1466, -0.5206, -0.1)
test_controls.setControlGains(controlGains=control_gains)
test_dynamics = test_controls.getVehicleAerodynamicsModel()
test_estimation = Estimator.VehicleEstimator(dT, test_dynamics)
test_estimation.setFilterGains(Kp_att_a=.2, Kp_att_m=0.0, Kp_alt=.8, Kp_spd=.11, Kp_course=5, 
			       Ki_att_a=.02, Ki_att_m=0.0, Ki_spd=.11) 

for i in range(n_steps):
	test_estimation.update(sensors_list[i])
	est_alt[i] = -test_estimation.getStateEstimate().pd

#%% graph results
time_arr = [dT*i for i in range(n_steps)]

est_alt_min = min(est_alt)

est_alt = [est_alt[i] - est_alt_min for i in range(n_steps)]

true_alt = [9.7536 for i in range(n_steps)]

true_alt_half = [4.8768 for i in range(n_steps)]

true_alt_qu = [2.4384 for i in range(n_steps)]

true_alt_3qu = [7.3152 for i in range(n_steps)]

t_flight_start = math.floor(550/dT)

print(max(est_alt) - min(est_alt))

print(9.7536 - max(est_alt) + min(est_alt))

plt.plot(time_arr, est_alt, label="est alt")
plt.plot(time_arr, true_alt_qu, label="alt of 1st flight")
plt.plot(time_arr, true_alt_half, label="alt of 2nd flight")
plt.plot(time_arr, true_alt_3qu, label="alt of 3rd flight")
plt.plot(time_arr, true_alt, label="alt of 4th flight")

plt.title("Stair Climbing Altitude Estimation Test")
plt.ylabel("height above ground (m)")
plt.xlabel("time (seconds)")
plt.legend()

plt.show()
