/* 
 * File:   PI.h
 * Author: Margaret Silva
 * Brief: Interface to PD controller module
 * Uses code from Aaron Hunter's code from the OSAVC PID library to create a 
 * PD controller module
 *
 * Created on February 14, 2023, 2:03 PM
 */

#ifndef PD_H
#define	PD_H

/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/
// standard libraries
#include <stdint.h>

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/

/*******************************************************************************
 * PUBLIC TYPEDEFS                                                             *
 ******************************************************************************/
typedef struct PD_controller {
    float dt; //loop update time (sec)
    float kp; // proportional gain
    float kd; // derivative gain
    float u_max; // output upper bound
    float u_min; //output lower bound
    float trim; // trim input
    float u_calc; // calculated output
    float u; // output returned (may be different due to actuator limits)
    float c0; // pre-computed constants
    float c1; 
    float c2; 
    float error[3]; // explicit array of last three error values
} PD_controller;

/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

/**
 * @Function PD_init(*pd);
 * @param *pd, pointer to PD_controller type
 * @brief initializes the PD_controller struct
 * @note initializes error array
 * @author Aaron Huter,
 * @modified */
void PD_init(PD_controller *pd);



/**
 * @Function PD_update(PD_controller *pd, float reference, float measurement)
 * @param *pd, pointer to PD_controller type
 * @param, reference, the current process setpoint
 * @param measurmeent, the current process measurement
 * @param dot, the derivative of the current process measurement
 * @brief implements a standard parallel PD
 * @note derivative filtering is not implemented
 * @author Margaret Silva,
 * @modified  2/24/23*/
void PD_update(PD_controller *pd, float reference, float measurement, float dot);


#endif	/* PD_H */

