from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtOpenGL import *
import sys
import PyQt5
import pyqtgraph.opengl
from ..Modeling import VehicleGeometry
import numpy

# this is deprecated code no longer in use
sys.exit(0)

class mainInterface(QMainWindow):
	updateVehiclePositionSignal = pyqtSignal(list)  # signal for redrawing the vehicle

	def __init__(self, parent = None):
		super().__init__(parent)

		self.mainWidget = QWidget()
		self.mainLayout = QVBoxLayout()
		self.setCentralWidget(self.mainWidget)
		self.mainWidget.setLayout(self.mainLayout)

		# we will always have the openGLBase Widget
		self.openGLWindow = pyqtgraph.opengl.GLViewWidget()
		self.mainLayout.addWidget(self.openGLWindow)

		# a random grid, will likely be changed afterwards
		self.openGLWindow.setGeometry(0, 0, 1000, 1000)
		self.grid = pyqtgraph.opengl.GLGridItem()
		self.grid.scale(20, 20, 20)
		self.openGLWindow.addItem(self.grid)

		# and an arbitrary camera starting position
		self.openGLWindow.setCameraPosition(distance=200)

		# a copy of the vehicle, we assume we will always want a vehicle
		self.vehicleDrawInstance = VehicleGeometry.drawVehicle()

		# we need to grab the vertices for the vehicle each update
		newVertices = numpy.array(self.vehicleDrawInstance.getNewPoints(0, 0, 0, 0, 0, 0))

		# faces and colors only need to be done once
		newFaces = numpy.array(self.vehicleDrawInstance.faces)
		newColors = numpy.array(self.vehicleDrawInstance.colors)

		# we convert the vertices to meshdata which allows us not to have to translate points every time
		self.vehicleMeshData = pyqtgraph.opengl.MeshData(vertexes=newVertices, faces=newFaces, faceColors=newColors)

		# and we create the meshItem, we do not smooth to make the triangles be clean colors
		self.openGLVehicle = pyqtgraph.opengl.GLMeshItem(meshdata=self.vehicleMeshData, drawEdges=True, smooth=False, computeNormals=False)

		# always add the vehicle to the display
		self.openGLWindow.addItem(self.openGLVehicle)

		# and add an axis

		self.Axis = pyqtgraph.opengl.GLAxisItem(glOptions='additive')
		self.Axis.setSize(200,200,200)
		self.openGLWindow.addItem(self.Axis)

		# make it a usable size, the openGL window has a tendency to disappear unless I give it a size
		self.resize(1280, 720)

		# we connect the signal to the update vehicle position, this should be thread safe
		self.updateVehiclePositionSignal.connect(self.drawNewVehiclePosition)

		return


	def updateVehiclePosition(self, newPosition):
		"""this function signals that the vehicle needs to be redrawn, it does not draw it itself"""
		self.updateVehiclePositionSignal.emit(newPosition)
		return

	def drawNewVehiclePosition(self, newPosition):
		"""this is the response to the update position signal, it should never be called directly"""

		# we simply create a new set of vertices
		newVertices=numpy.array(self.vehicleDrawInstance.getNewPoints(*newPosition))
		self.vehicleMeshData.setVertexes(newVertices)  # update our mesh with them
		self.openGLVehicle.setMeshData(meshdata=self.vehicleMeshData, smooth=False, computeNormals=False)  # and setMeshData automatically invokes a redraw
		return




if __name__ == "__main__":

	sys._excepthook = sys.excepthook

	def my_exception_hook(exctype, value, tracevalue):
		# Print the error and traceback
		import traceback
		with open("LastCrash.txt", 'w') as f:
			# f.write(repr(exctype))
			# f.write('\n')
			# f.write(repr(value))
			# f.write('\n')
			traceback.print_exception(exctype, value, tracevalue, file=f)
			# traceback.print_tb(tracevalue, file=f)
		print(exctype, value, tracevalue)
		# Call the normal Exception hook after
		sys._excepthook(exctype, value, tracevalue)
		sys.exit(0)

	# Set the exception hook to our wrapping function
	sys.excepthook = my_exception_hook



	qtApp = QApplication(sys.argv)
	ourWindow = mainInterface()
	ourWindow.show()
	qtApp.exec()