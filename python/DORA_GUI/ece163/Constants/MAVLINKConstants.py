
'''
Title: MAVLINKConstants.py
Date: 3/01/2023
Description: Lists all the MAVLINK constants and enums used for GroundControl
'''
## Configure usage of MAVLINK
mavlinkNone = 0
mavlinkQGC = 1
mavlinkAir = 2
mavlinkMode = 2
### MAVLINK DEFINITIONS ###
MAV_ONCE = 0 # Only print once
MAV_CONST = 1 # Print constantly
mavlinkDebugMsg = 'I AM SIM'
mavlinkDebugMsgAck = 'I AM AUTOPILOT'
## COMS and BAUDRATE
com = "COM12" # COM7 for OSAVC 1.2, COM12 for OSAVC 1.3
com2 = "COM8"
serialbaudrate = 115200
radiobaudrate = 57600
baudrate = serialbaudrate

simPeriod = 20
heartbeatFreq = 1000/simPeriod # sim runs at 20 ms, need to check every 1000 ms (1000/20) (but runUpdate() twice)
debugFreq = 50
rtFreq = 1 # real time frequency (50 hz)
sampleMAF = 25 # 100 samples for moving average filter

## MAVLINK MESSAGE TYPES (as text)
type_default = "DEFAULT"
HEARTBEAT = 'HEARTBEAT'
HIL_CONTROLS  = 'HIL_CONTROLS'
HIL_FLOAT_PACKET = 'HIL_ACTUATOR_CONTROLS'
HIL_DEBUG  = 'STATUSTEXT'


## MAVLINK PACKET TYPES for HIL_ACTUATOR_CONTROLS (RX)
HIL_ACKGAINS = 1 # For gain acknowledgement
HIL_ACKCOMS = 6 # For ref comms acknowledgement
HIL_ACKESTGAINS = 8 # For state estimator gains acknowledgement
HIL_STATE_EST = 5 # For receiving estimated states
HIL_ACKWAYPOINTS = 11 # For waypoints acknowledgement
HIL_ERROR = 255 # For error packets

## MAVLINK PACKET TYPES for HIL_ACTUATOR_CONTROLS (TX)
HIL_STATECOMS = 2 #For transmit states
HIL_REFCOMS = 3 # For transmit reference commands
HIL_SENSOR = 4 #For all noisy sensors
HIL_ESTGAINS = 7 # For transmitting estimated gains
HIL_CONTGAINS = 9 # For transmitting stability controller gains
HIL_WAYPOINTS = 10 # For transmitting waypoints

# HEARTBEAT STATUS
HIL_HEARTBEAT_DEFAULT = 0
HIL_HEARTBEAT_MANUAL = 7
HIL_HEARTBEAT_AUTO = 5
HIL_HEARTBEAT_SENSOR = 6

# HIL_CONTROLS
HIL_CONTROLS_REFCOMS = 1
HIL_CONTROLS_129COMS = 2
HIL_CONTROLS_EULER_GIMBAL = 3
HIL_CONTROLS_EULER_PLANE = 4

## HIL: Flight Mode ##
simMode = 0
autopilotMode = 1
HIL_mode = simMode



## HIL: State Mode ##
sim_state = 0
est_state  = 1
HIL_state_mode = -1

## HIL: Commanded Mode
ece129_mode = 0
ece167_mode = 1
HIL_command_mode = ece129_mode
## HIL: GUI mode - increases verbosity level of GUI features##
defaultMode = 0
simpleHILMode = 1
advHILMode = 2
GUI_mode = advHILMode

## HIL: Airframe mode ##

defaultAirframe = 0
explorerAirframe = 1
airframeMode = explorerAirframe

## AUTOPILOT PACKET LENGTHS ##
commandLength = 16
true_statesLength = 4
est_statesLength = 4
sensor_imuLength = 9

## MAVLINK Plotting variables
mavlinkPointer = 0 # Place holder for MAVLINK
plotWindow = 500
defaultPlot = 0
scrollPlot = 1
plotMode = scrollPlot

## MAVLINK Message
msg_default = 0
msg_tx = 1
msg_rx = 2


## ESTIMATED STATES INDEX
est_pn = 0
est_pe = 0
est_pd = 4
est_u = 0
est_v = 0
est_w = 0
est_yaw = 5
est_pitch = 6
est_roll = 7
est_p = 8
est_q = 9
est_r = 10
est_chi = 11
est_alpha = 0
est_beta = 0
est_Va = 12

# ACTUATOR COMMAND INDEX
cmd_ail = 0
cmd_ele = 1
cmd_rud = 2
cmd_thr = 3

# Acknowledge flags
ack_gps = 13
ack_gpsBool = 14

# Sensor message formatting
accText = "Acc:\nx: {0:.6f}\ny: {1:.6f}\nz: {2:.6f}\n"
gyroText = "Gyro:\nx: {0:.6f}\ny: {1:.6f}\nz: {1:.6f}\n"
magText = "Mag:\nx: {0:.6f}\ny: {1:.6f}\nz: {2:.6f}\n"
baroText = "Baro:\nalt: {0:.6f}\n"
gpsText = "GPS:\nlat: {0:.6f}\nlon: {1:.6f}\nalt: {2:.6f}\nsog:  {3:.6f}\ncog:  {4:.6f}"


# Text input lists
# Link: https://doc.qt.io/qtforpython-5/PySide2/QtGui/QColor.html#predefined-colors
defaultColors = ["red", "orange", "yellow", "green", "blue", "indigo", "violet"]

waypointDict = {
    "N": 0,
    "E": 0,
    "D": -100,
    "Flag": 0,
}
waypointNames = list(waypointDict.keys())
waypointVals = list(waypointDict.values())


estDict = {
    'Kp_Va': 0.11,
    'Ki_Va': 0.11,
    'Kp_chi': 0.5,
    'Ki_chi': 0.05,
    'Kp_acc': 5.0,
    'Ki_acc': 0.5,
    'Kp_mag': 15.0,
    'Ki_mag': 1.0
}
estGainsNames = list(estDict.keys())
defaultestGains = list(estDict.values())
# estGainsNames = ['Kp_Va', 'Ki_Va', 'Kp_chi', 'Ki_chi', 'Kp_acc', 'Ki_acc', 'Kp_mag', 'Ki_mag']
# defaultestGains = [0.11, 0.11, 0.5, 0.05, 5.0, 0.5, 15.0, 1.0]
# Saving configurations
defaultEstGainsFileName = "VehicleEstGains_Data.pickle"

#### SELF NOTES ####

### EXAMPLE OF HBOX ###
# topBoxEnclosure = QtWidgets.QHBoxLayout()
# self.usedLayout.addLayout(topBoxEnclosure)
# sampleBox = QtWidgets.QVBoxLayout()
# self.sampleLabel = QtWidgets.QLabel("Sample Box")
# sampleBox.addWidget(self.sampleLabel)
# topBoxEnclosure.addLayout(sampleBox)