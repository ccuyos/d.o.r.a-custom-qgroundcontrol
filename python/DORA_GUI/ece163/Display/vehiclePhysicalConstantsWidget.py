'''
/*
 * File:   vehiclePhysicalConstantsWidget.py
 * Author: Carl Vincent Cuyos
 * Co-Author: Khushi Shah
 * Brief:  widget used for modifying VPC variables for changing airframe characteristics
 * project
 * Created on May 19, 2023
 *
'''

import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets
from ..Constants import VehiclePhysicalConstants as vpc
from ..Constants import GUIConstants as pyGUI
import sys
import os
import datetime

import math
import threading
import pickle
import matplotlib.pyplot as plt
import matplotlib
import time

class VPC_Constants():
	def __init__(self):

		# Mass Properties
		self.massConstants = vpc.massDict
		# Lift Coefficients
		self.liftConstants = vpc.liftDict
		# Drag Coefficients
		self.dragConstants = vpc.dragDict
		# Pitch Coefficients
		self.pitchConstants = vpc.pitchDict
		# Sideslip coefficients
		self.sideConstants = vpc.sideDict
		# Roll coefficients
		self.rollConstants = vpc.rollDict
		# Yaw coefficients
		self.yawConstants = vpc.yawDict
		# Motor coefficients
		self.motorConstants = vpc.motorDict
		# Propeller constants
		self.propConstants = vpc.propDict
		# Limit constants
		self.limitConstants = vpc.limitDict


class vehiclePhysicalConstantsWidget(QtWidgets.QWidget):
	testFinishedSignal = QtCore.pyqtSignal(bool)
	def __init__(self, parent = None):
		super().__init__(parent)
		self.parentInstance = parent

		self.usedLayout = QtWidgets.QVBoxLayout()

		self.setLayout(self.usedLayout)
		self.overallLayout = self.createBoxRow(self.usedLayout)

		# Load all VPC constants
		self.plane = self.loadAllVPC()


		# Add First form
		outer_Box1 = self.createBoxColumn(self.overallLayout)
		box1Form = self.createForm(outer_Box1)
		#  # Add mass properties
		self.massTextDict = self.createSection("Mass Properties", self.plane.massConstants, box1Form)
		self.motorTextDict = self.createSection("Motor Coefficients", self.plane.motorConstants, box1Form)
		self.propTextDict = self.createSection("Propeller Constants", self.plane.propConstants, box1Form)
		outer_Box1.addStretch()

		# Add second form
		outer_Box2 = self.createBoxColumn(self.overallLayout)
		box2Form = self.createForm(outer_Box2)
		self.rollTextDict = self.createSection("Roll Coefficients", self.plane.rollConstants, box2Form)
		self.pitchTextDict = self.createSection("Pitch Coefficients", self.plane.pitchConstants, box2Form)
		self.yawTextDict = self.createSection("Yaw Coefficients", self.plane.yawConstants, box2Form)
		outer_Box2.addStretch()

		# Add third form
		outer_Box3 = self.createBoxColumn(self.overallLayout)
		box3Form = self.createForm(outer_Box3)

		self.liftTextDict = self.createSection("Lift Coefficients", self.plane.liftConstants, box3Form)
		self.dragTextDict = self.createSection("Drag Coefficients", self.plane.dragConstants, box3Form)
		self.sideTextDict = self.createSection("SideSlip Coefficients", self.plane.sideConstants, box3Form)
		outer_Box3.addStretch()

		# Add fourth form
		outer_Box4 = self.createBoxColumn(self.overallLayout)
		box4Form = self.createForm(outer_Box4)

		self.limitTextDict = self.createSection("Aircraft Maneuver Limits", self.plane.limitConstants, box4Form)
		outer_Box4.addStretch()

		self.createSpacing(self.overallLayout, pyGUI.mediumSpace)
		# Add a buttons column
		outer_Box5 = self.createBoxColumn(self.overallLayout)

		applyVPC_button = self.createButton("Apply VPC Constants", self.runApplyVPC, outer_Box5)
		saveVPC_button = self.createButton("Save VPC Constants", self.saveAllVPC, outer_Box5)
		hardResetVPC_button = self.createButton("Refresh VPC", self.resetVPC, outer_Box5)
		self.buttonStatus = self.createStatus(outer_Box5)
		outer_Box5.addStretch()
		self.overallLayout.addStretch()


	def buildVPC(self, paramNames, paramConstants, paramTextDict):
		"""
		Function: buildVPC(self, paramNames, paramConstants, paramTextDict)
		Description: Builds each VPC section by overwriting the pre-existing values in the VPC object
		Params: paramNames - names of the paremeters given a VPC section
				paramConstants - constants of the parameters given a VPC section
				paramTextDict - dictionary containing QtWidgets.QLineEdit() for fetching text values
		Return: None
		Author: Carl Vincent Cuyos
		"""
		for name in paramNames:
			paramConstants[name] = float(paramTextDict[name].text())

	def buildCurrentVPC(self):
		"""
		Function: buildCurrentVPC(self)
		Description: Builds each VPC section by overwriting the pre-existing values in the VPC object
		Params: None
		Return: planeObject - VPC_Constants() object that contains the updated vpc constants from the fill-in VPC widget
		Author: Carl Vincent Cuyos
		"""
		planeObject = VPC_Constants()
		for i in range(len(vpc.varLib)):
			exec("self.buildVPC(vpc.{0}Names, planeObject.{0}Constants, self.{0}TextDict)".format(vpc.varLib[i]))
		return planeObject
	def runApplyVPC(self):
		"""
		Function: runApplyVPC(self)
		Description: Applies VPC Widget Constants to the VehiclePhysicalConstants.py file
		Params: None
		Return: planeObject - VPC_Constants() object that contains the updated vpc constants from the fill-in VPC widget
		Author: Carl Vincent Cuyos
		"""
		self.plane = self.buildCurrentVPC()
		self.buttonStatus.setText("Applied VPC Constants")
		return
	def loadAllVPC(self):
		"""
		Function: loadAllVPC(self)
		Description: Loads all the VPC values from the pickle files
		Params: None
		Return: loadVal - object that contains the loaded values from the pickle files
		Author: Carl Vincent Cuyos
		"""
		loadVal = VPC_Constants()
		for i in range(len(vpc.varLib)):
			exec("loadVal.{0}Constants = self.loadVPC(vpc.{0}File, loadVal.{0}Constants)".format(vpc.varLib[i]))
		self.reloadVPCFile(loadVal)
		return loadVal

	def saveAllVPC(self):
		"""
		Function: saveAllVPC(self)
		Description: save all the VPC values to the pickle files
		Params: None
		Return: None
		Author: Carl Vincent Cuyos
		"""
		vpcAll = self.buildCurrentVPC()
		for i in range(len(vpc.varLib)):
			exec("self.saveVPC(vpc.{0}File, self.plane.{0}Constants)".format(vpc.varLib[i]))
		self.buttonStatus.setText("Saved VPC Constants")
	def saveVPC(self, dataName, constants):
		"""
		Function: saveVPC(self, dataName, constants)
		Description: dumps the dictionary constant into a pickle file
		Params: dataName - file name of the VPC section (i.e. mass, lift, drag)
				constants - associated dictionary of VPC section
		Return: None
		Author: Carl Vincent Cuyos
		"""
		fileName = vpc.defaultFile
		with open(fileName.format(dataName), 'wb') as handle:
			pickle.dump(constants, handle, protocol=pickle.HIGHEST_PROTOCOL)
	def loadVPC(self, dataName, constants):
		"""
		Function: loadVPC(self, dataName, constants)
		Description: loads a section pickle file into a dictionary constant
		Params: dataName - file name of the VPC section (i.e. mass, lift, drag)
				constants - associated dictionary of VPC section
		Return: the loaded dictionary constant if file found, else constants
		Author: Carl Vincent Cuyos
		"""
		fileName = vpc.defaultFile
		try:
			with open(os.path.join(sys.path[0], fileName.format(dataName)), 'rb') as f:
				return pickle.load(f)
		except (FileNotFoundError, EOFError):
			return constants

	def reloadSection(self, paramNames, paramConst):
		"""
		Function: reloadSection(paramNames, paramConst)
		Description: reloads the section data found in VehiclePhysicalConstants.py
		Params: paramNames - names containing the VPC variables as strings
				paramConst - associated dictionary constant for the section
		Return: None
		Author: Carl Vincent Cuyos
		"""
		for i in range(len(paramNames)):
			idx = paramNames[i]
			exec("vpc.{} = {}".format(idx, paramConst[idx]))
			#exec("print(vpc.{})".format(idx))

	def reloadVPCFile(self, toLoad):
		"""
		Function: reloadVPCFile(toLoad)
		Description: reloads all data found in VehiclePhysicalConstants.py
		Params: toLoad - VPC_Constants object class that needs to be reloaded
		Return: None
		Author: Carl Vincent Cuyos
		"""
		for i in range(len(vpc.varLib)):
			exec("{0}LoadConst = toLoad.{0}Constants".format(vpc.varLib[i]))
			exec("{0}LoadName = vpc.{0}Names".format(vpc.varLib[i]))
			exec("self.reloadSection({0}LoadName, {0}LoadConst)".format(vpc.varLib[i]))

	def resetVPC(self):
		self.buttonStatus.setText("Reloaded VPC Constants")

	## GUI Helper Functions ## TO-DO Move to dedicate .py file

	def createSection(self, name, paramDict,formLayout):
		sectionDict = dict()
		paramNames = list(paramDict.keys())
		paramValues = list(paramDict.values())
		section = QtWidgets.QLabel(name)
		section.setAlignment(QtCore.Qt.AlignLeft)
		formLayout.addWidget(section)
		for i in range(len(paramDict)):
			self.createTextInput(paramNames[i], paramValues[i], formLayout, sectionDict)
		return sectionDict

	def createForm(self, innerLayout):
		formLayout = QtWidgets.QFormLayout()

		innerLayout.addLayout(formLayout)
		return formLayout

	def createBoxColumn(self, outerLayout):
		columnBox = QtWidgets.QVBoxLayout()
		outerLayout.addLayout(columnBox)
		return columnBox

	def createBoxRow(self, outerLayout):
		rowBox = QtWidgets.QHBoxLayout()
		outerLayout.addLayout(rowBox)
		return rowBox
	def createTextInput(self, text, value, formLayout, textDict):
		lineEdit = QtWidgets.QLineEdit()
		doubleValid = QtGui.QDoubleValidator()
		lineEdit.setText(str(value))
		lineEdit.setValidator(doubleValid)
		formLayout.addRow(text, lineEdit)
		textDict[text] = lineEdit

	def createButton(self, name, function, innerLayout):
		button = QtWidgets.QPushButton(name)
		button.clicked.connect(function)
		innerLayout.addWidget(button)
		return button

	def createSpacing(self, layout, size = pyGUI.customSpace, length = 0, width = 0):
		if size == pyGUI.smallSpace:
			length, width = pyGUI.smallSize, pyGUI.smallSize
		elif size == pyGUI.mediumSpace:
			length, width = pyGUI.mediumSize,pyGUI.mediumSize
		elif size == pyGUI.largeSpace:
			length, width = pyGUI.largeSize, pyGUI.largeSize
		elif size == pyGUI.extremeSpace:
			length, width = pyGUI.extremeSize, pyGUI.extremeSize
		else:
			length, width = length, width
		spaceSize = QtWidgets.QSpacerItem(length, width, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
		layout.addItem(spaceSize)
	def createStatus(self, outerLayout, msg = "Status: NaN"):
		status = QtWidgets.QLabel(msg)
		outerLayout.addWidget(status)
		return status

