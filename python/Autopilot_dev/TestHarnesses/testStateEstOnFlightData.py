'''
    File: testStateEstOnFlightData.py
    Author: Margaret Silva
    Notes: Tests state estimation algorithms on test flight data
'''

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Controls.VehicleEstimator as Estimator
import ece163.Controls.VehicleClosedLoopControl as VCLC
import ece163.Containers.Controls as Controls
import ece163.Containers.Sensors as Sensors
import matplotlib.pyplot as plt
import csv
import pandas as pd

"""math.isclose doesn't work well for comparing things near 0 unless we 
use an absolute tolerance, so we make our own isclose:"""
isclose = lambda  a,b : math.isclose(a, b, abs_tol= 1e-12)

def compareVectors(a, b):
	"""A quick tool to compare two vectors"""
	el_close = [isclose(a[i][0], b[i][0]) for i in range(3)]
	return all(el_close)

#of course, you should test your testing tools too:
assert(compareVectors([[0], [0], [-1]],[[1e-13], [0], [-1+1e-9]]))
assert(not compareVectors([[0], [0], [-1]],[[1e-11], [0], [-1]]))
assert(not compareVectors([[1e8], [0], [-1]],[[1e8+1], [0], [-1]]))


failed = []
passed = []
def evaluateTest(test_name, boolean):
	"""evaluateTest prints the output of a test and adds it to one of two 
	global lists, passed and failed, which can be printed later"""
	if boolean:
		print(f"   passed {test_name}")
		passed.append(test_name)
	else:
		print(f"   failed {test_name}")
		failed.append(test_name)
	return boolean

#%% Begin Testing

#%% Write data from .txt to .csv file
# f = open('test_flight_5_21_2023.TXT', 'r')
# f_lines = (line.split(',') for line in f if line)

# f_csv = open('test_flight5_21_2023v2.csv', 'w', newline='')
# writer = csv.writer(f_csv)
# writer.writerows(f_lines)

#%% Parse data from .txt file

time_idx = 1
accX_idx = 2
accY_idx = 3
accZ_idx = 4
gyroX_idx = 5
gyroY_idx = 6
gyroZ_idx = 7
magX_idx = 8
magY_idx = 9
magZ_idx = 10
gpsLat_idx = 13
gpsLon_idx = 14
gpsSpd_idx = 15
gpsCog_idx = 16
temperature_idx = 17
pressure_idx = 18

DRIFT_GYROX = -0.004070617273368
DRIFT_GYROY = 2.369648499684174*(10**(-4))
DRIFT_GYROZ = 0.001435278922658
dT_GYRO = 0.02

dT = 16/1000

# Read columns from data set
# f_csv = open('test_flight5_21_2023.csv', 'r')
# full_df = pd.read_csv("test_flight_5_21_2023.TXT")
# full_df.to_csv('test_flight5_21_2023v2.csv')

full_df = pd.read_csv("test_flight5_21_2023v2.csv")
# time = full_df.iloc[:, time_idx] 

accX = full_df.iloc[:, accX_idx]
accY = full_df.iloc[:, accY_idx]
accZ = full_df.iloc[:, accZ_idx]

gyroX_acc = full_df.iloc[:, gyroX_idx]
gyroX = [0 for i in range(len(accX))]
gyroY_acc = full_df.iloc[:, gyroY_idx]
gyroY = [0 for i in range(len(accX))]
gyroZ_acc = full_df.iloc[:, gyroZ_idx]
gyroZ = [0 for i in range(len(accX))]

magX = full_df.iloc[:, magX_idx]
magY = full_df.iloc[:, magY_idx]
magZ = full_df.iloc[:, magZ_idx]

baro_from_csv = full_df.iloc[:, pressure_idx]
baro = [0 for i in range(len(accX))]

gps_cog_meas = full_df.iloc[:, gpsCog_idx]
gps_cog = [0 for i in range(len(accX))]
gps_sog_meas = full_df.iloc[:, gpsSpd_idx]
gps_sog = [0 for i in range(len(accX))]
gps_alt = [False for i in range(len(accX))]

n_steps = len(accX)

# correct gyro values to get change and not accumulation
for i in range(n_steps):
	if i > 0:
		gyroX[i] = math.radians((gyroX_acc[i] - gyroX_acc[i - 1] + DRIFT_GYROX) / dT_GYRO)
		gyroY[i] = math.radians((gyroY_acc[i] - gyroY_acc[i - 1] + DRIFT_GYROY) / dT_GYRO)
		gyroZ[i] = math.radians((gyroZ_acc[i] - gyroZ_acc[i - 1] + DRIFT_GYROZ) / dT_GYRO)

# zero order holds for any missing baro data
for i in range(n_steps):
	if math.isnan(baro_from_csv[i]):
		baro[i] = baro[i - 1]
	else:
		baro[i] = float(baro_from_csv[i])

# clean up gps data into parsable format
for i in range(n_steps):
	if i > 0:
		if math.isnan(gps_cog_meas[i]):
			# implement zero order hold on data
			gps_cog[i] = gps_cog[i - 1]
			gps_sog[i] = gps_sog[i - 1]
			gps_alt[i] = False
		else:
			# mark that new data has been received
			gps_cog[i] = math.radians(float(gps_cog_meas[i]))
			gps_sog[i] = (0.514444)*float(gps_sog_meas[i]) # convert knots to meters/s
			if (gps_sog[i] < 5):
				gps_cog[i] = 0
			gps_alt[i] = True	

sensors_list = [Sensors.vehicleSensors(gyro_x=float(gyroX[i]), gyro_y=float(gyroY[i]), gyro_z=float(gyroZ[i]), 
				       accel_x=-float(accX[i]), accel_y=-float(accY[i]), accel_z=-float(accZ[i]),
					   mag_x=(10**3)*float(magX[i]), mag_y=(10**3)*float(magY[i]), mag_z=(10**3)*float(magZ[i]),
					   baro=100*float(baro[i]), 
					   gps_alt=gps_alt[i], gps_cog=gps_cog[i], gps_sog=gps_sog[i]) 
					   for i in range(len(accX))]

est_yaw = [0 for i in range(len(accX))]
est_pitch = [0 for i in range(len(accX))]
est_roll = [0 for i in range(len(accX))]
est_alt = [0 for i in range(len(accX))]
est_course = [0 for i in range(len(accX))]

test_controls = VCLC.VehicleClosedLoopControl()
control_gains = Controls.controlGains(3.0561, 0.001, 0.043177, .5, .05, 5.0968, 2.5484, -13.1825, 
				      -0.7931, 0.07743, 0.030975, 1.493, 1.1466, -0.5206, -0.1)
test_controls.setControlGains(controlGains=control_gains)
test_dynamics = test_controls.getVehicleAerodynamicsModel()
test_estimation = Estimator.VehicleEstimator(dT, test_dynamics)
test_estimation.setFilterGains(Kp_att_a=.2, Kp_att_m=0.0, Kp_alt=.8, Kp_spd=.11, Kp_course=5, 
			       Ki_att_a=.02, Ki_att_m=0.0, Ki_spd=.11) 

for i in range(n_steps):
	test_estimation.update(sensors_list[i])
	est_yaw[i] = test_estimation.getStateEstimate().yaw
	est_pitch[i] = test_estimation.getStateEstimate().pitch
	est_roll[i] = test_estimation.getStateEstimate().roll
	est_alt[i] = -test_estimation.getStateEstimate().pd
	est_course[i] = test_estimation.getStateEstimate().chi

#%% graph results
time_arr = [dT*i for i in range(n_steps)]

t_flight_start = math.floor(550/dT)

plt.figure(1)
est_roll = [math.degrees(est_roll[i]) for i in range(n_steps)]
plt.subplot(3, 1, 1)
plt.plot(time_arr[t_flight_start:], est_roll[t_flight_start:], label="est")
plt.title("Roll")
plt.ylabel("angle (degrees)")
# plt.xlabel("time (seconds)")
plt.legend()

est_pitch = [math.degrees(est_pitch[i]) for i in range(n_steps)]
plt.subplot(3, 1, 2)
plt.plot(time_arr[t_flight_start:], est_pitch[t_flight_start:], label="est")
plt.title("Pitch")
plt.ylabel("angle (degrees)")
# plt.xlabel("time (seconds)")
plt.legend()

est_yaw = [math.degrees(est_yaw[i]) for i in range(n_steps)]
plt.subplot(3, 1, 3)
plt.plot(time_arr[t_flight_start:], est_yaw[t_flight_start:], label="est")
plt.title("Yaw")
plt.ylabel("angle (degrees)")
plt.xlabel("time (seconds)")
plt.legend()

plt.figure(2)

plt.subplot(3, 1, 1)
plt.plot(time_arr[t_flight_start:], est_alt[t_flight_start:], label="est")
plt.title("Altitude")
plt.ylabel("height above ground (m)")
# plt.xlabel("time (seconds)")
plt.legend()

plt.subplot(3, 1, 2)
est_course = [math.degrees(est_course[i]) % 360 for i in range(n_steps)]
plt.plot(time_arr[t_flight_start:], est_course[t_flight_start:], label="est")
plt.title("Course")
plt.ylabel("angle (degrees)")
plt.xlabel("time (seconds)")
plt.legend()

plt.figure(3)

plt.subplot(3, 1, 1)
gps_cog = [math.degrees(gps_cog[i]) for i in range(n_steps)]
plt.plot(time_arr[t_flight_start:], gps_cog[t_flight_start:], label="data")
plt.title("gps cog")
plt.ylabel("angle (degrees)")
# plt.xlabel("time (seconds)")
plt.legend()

plt.subplot(3, 1, 2)
plt.plot(time_arr[t_flight_start:], gps_sog[t_flight_start:], label="data")
plt.title("gps sog")
plt.ylabel("speed (m/s)")
plt.xlabel("time (seconds)")
plt.legend()

plt.figure(4)
plt.plot(time_arr, est_alt, label="est")
plt.title("Altitude")
plt.ylabel("height above ground (m)")
# plt.xlabel("time (seconds)")
plt.legend()

plt.show()

