"""
Will calculate the total forces and moments (aerodynamic, thrust, gravity, and control surface deflection) on the vehicle
using the vehicle state and wind.
"""

import math
import csv
import sys

sys.path.append('..')

import ece163.Modeling.VehicleAerodynamicsModel as VehicleAerodynamicsModel
import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Containers.States as vehicleState
import ece163.Containers.Inputs as Inputs

fm = VehicleAerodynamicsModel.VehicleAerodynamicsModel()  # forces and moments
controls = Inputs.controlInputs()
myState = vehicleState.vehicleState()

fm.reset()

Hz = 100
fm.vehicleDynamics.dT = 1 / Hz
time = 0.

Va = 25.

fm.setWindModel(12.0, -5.2, 0.0, VPC.DrydenLowAltitudeLight)

myState.u = Va
myState.w = -5.0
fm.setVehicleState(myState)

controls.Throttle = 0
controls.Elevator = math.radians(5.)
controls.Aileron = math.radians(0.)
fm.vehicleDynamics.state.pd = -100

fm.CalculateAirspeed(fm.vehicleDynamics.state, fm.windModel.Wind)
print("Starting Iteration\n\n")
print(
	"Time [s]\tN [m]\tE [m]\tAlt [m]\tu [m/s]\tv [m/s]\tw [m/s]\troll [deg]\tpitch [deg]\tyaw [deg]\tp [deg/s]\tq [deg/s]\tr [deg/s]")

dataList = [[time, fm.vehicleDynamics.state.pn, fm.vehicleDynamics.state.pe, -fm.vehicleDynamics.state.pd,
			 fm.vehicleDynamics.state.u, fm.vehicleDynamics.state.v, fm.vehicleDynamics.state.w,
			 math.degrees(fm.vehicleDynamics.state.roll), math.degrees(fm.vehicleDynamics.state.pitch),
			 math.degrees(fm.vehicleDynamics.state.yaw), math.degrees(fm.vehicleDynamics.state.p),
			 math.degrees(fm.vehicleDynamics.state.q), math.degrees(fm.vehicleDynamics.state.r),
			 fm.vehicleDynamics.state.Va, math.degrees(fm.vehicleDynamics.state.alpha),
			 math.degrees(fm.vehicleDynamics.state.beta), math.degrees(fm.vehicleDynamics.state.chi)]]

for x in range(0, 100 * Hz):
	time += fm.vehicleDynamics.dT
	fm.Update(controls)
	d = [time, fm.vehicleDynamics.state.pn, fm.vehicleDynamics.state.pe, -fm.vehicleDynamics.state.pd,
			 fm.vehicleDynamics.state.u, fm.vehicleDynamics.state.v, fm.vehicleDynamics.state.w,
			 math.degrees(fm.vehicleDynamics.state.roll), math.degrees(fm.vehicleDynamics.state.pitch),
			 math.degrees(fm.vehicleDynamics.state.yaw), math.degrees(fm.vehicleDynamics.state.p),
			 math.degrees(fm.vehicleDynamics.state.q), math.degrees(fm.vehicleDynamics.state.r),
			 fm.vehicleDynamics.state.Va, math.degrees(fm.vehicleDynamics.state.alpha),
			 math.degrees(fm.vehicleDynamics.state.beta), math.degrees(fm.vehicleDynamics.state.chi)]
	print(" ".join('{: 7.3f}'.format(t) for t in d))
	dataList.append(d)

with open('data.csv', 'w', newline='') as f:
	writer = csv.writer(f)
	writer.writerows(dataList)
