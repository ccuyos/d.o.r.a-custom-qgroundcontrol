/* ************************************************************************** */
/** Autopilot Feedback Controller Library

  @File Name
    AutopilotController.h

  @Summary
    This library contains all the functions used to determine the actuator outputs
    needed for the aircraft to achieve the input course and pitch angles

  @Description
    This library is where users implement their controller. Internal functions can
    be altered, but the outputs of the actuator return functions should remain
    unchanged
 
 * Created on February 8, 2023, 1:52 PM
 */
/* ************************************************************************** */

#ifndef _STABILITY_CONTROLLER_H
#define	_STABILITY_CONTROLLER_H

/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/
#include "StateEstimation.h"

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
#define DEFAULT_CONTROLLER_GAINS {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
#define DEFAULT_CONTROLLER_OUTPUTS {0, 0, 0, 0}

/*******************************************************************************
 * PUBLIC TYPEDEFS                                                             *
 ******************************************************************************/
struct controller_outputs{
    float d_aileron; // aileron deflection [rad]
    float d_elevator; // elevator deflection [rad]
    float d_rudder; // rudder deflection [rad]
    float d_throttle; // throttle power [on a scale from 0 to 1]
};


struct controller_gains{
    // roll
    float kp_roll;
    float kd_roll;
    float ki_roll;
    // rudder (sideslip or yaw damping)
    float kp_rudder;
    float ki_rudder;
    // course 
    float kp_course;
    float ki_course;
    // pitch 
    float kp_pitch;
    float kd_pitch;
    // altitude
    float kp_altitude;
    float ki_altitude;
    // speed
    float kp_speedFromThrottle;
    float ki_speedFromThrottle;
    float kp_speedFromElevator;
    float ki_speedFromElevator;
};


/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

/**
 * @Function initStabilityController()
 * @param none
 * @return none
 * @brief initializes stability controller and resets any accumulated values
 * Resets all PI/PD/PID controllers and accumulated values
 * @author Margaret Silva */
void initStabilityController(void);


/**
 * @Function updateStabilityController()
 * @param current_state, the aircraft's current state
 *        X_c, the aircraft's commanded course angle [rad]
 *        h_c, the aircraft's commanded altitude [m]
 *        Va_c, the aircraft's commanded airspeed [m/s]
 * @return controller outputs calculated by controller
 * @brief updates the controller with the current vehicle state and commanded
 *        course and altitude
 * @note the implementation for this controller uses successive loop closure
 *  with decoupled lateral and longitudinal dynamics 
 * @author Margaret Silva */
struct controller_outputs updateStabilityController(struct vehicle_state current_state, 
        float X_c, float h_c, float Va_c);


/**
 * @Function setStabilityControllerGains(controller_gains gains)
 * @param gains - controller gains for both lateral and longitudinal controllers
 *  @note: by default, this array is arranged in the order
 *      0: Kp roll, 
 *      1: Kd roll, 
 *      2: Ki roll, 
 *      3: rudder Kp, 
 *      4: rudder Ki, 
 *      5: course Kp, 
 *      6: course Ki,
 *      7: pitch Kp,
 *      8: pitch Kd,
 *      9: altitude Kp, 
 *      10: altitude Ki,
 *      11: airspeed from throttle Kp,
 *      12: airspeed from throttle Ki, 
 *      13: airspeed from pitch Kp,
 *      14: airspeed from pitch Ki
 * @return none
 * @brief updates the each PI/PD/PID controller within StabilityController.c 
 *  with the new set of gains passed in by the gains array
 * @author Margaret Silva */
void setStabilityControllerGains(float gains[16]);


/**
 * @Function getStabilityControllerGains()
 * @param none
 * @return current controller gains as a controller_gains struct
 * @brief returns the current controller gains
 * @author Margaret Silva */
struct controller_gains getStabilityControllerGains(void);

#endif	/* _STABILITY_CONTROLLER_H */

