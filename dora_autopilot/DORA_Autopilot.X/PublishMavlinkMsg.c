/* 
 * File:   PublishMavlinkMsg.c
 * Author: Margaret Silva
 * 
 * This file contains all the mavlink publish functions used in the main file
 *
 * Created on May 26, 2023, 2:42 PM
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/
#include "PublishMavlinkMsg.h"

/*******************************************************************************
 * STATIC VARIABLES                                                            *
 ******************************************************************************/
static mavlink_system_t mavlink_system = {
    1, // System ID (1-255)
    MAV_COMP_ID_AUTOPILOT1 // Component ID (a MAV_COMPONENT value)
};

#define SEND_EULER_ANGLES_FROM_DORA 4
/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATIONS                                             *
 ******************************************************************************/

/**
 * @Function publish_controller_output(float output, char controller_type[10])
 * @param output - float value of actuator commands
 * @param controller_type - name of controller output (ten chars max)
 * @brief invokes mavlink helper to send controller output over radio
 * @author aaron hunter
 * @modified for use in DORA system
 */
void publish_controller_output(float output, char controller_type[10]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_named_value_float_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            0,
            controller_type,
            output);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_actuator_cmds(struct controller_outputs actuators)
 * @param actuators - controller output struct containing commands to be sent to 
 *      all control surfaces on UAV
 * @brief invokes mavlink helper to send actuator commands over radio
 * @author Margaret Silva
 */
void publish_actuator_cmds(struct controller_outputs actuators) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_hil_controls_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            0, // time (unused)
            actuators.d_aileron,
            actuators.d_elevator,
            actuators.d_rudder,
            actuators.d_throttle,
            0.0, 0.0, 0.0, 0.0, // unused aux commands
            0,
            0
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_status_text(char text[50])
 * @param text - 50 chars of text as a status message
 * @brief invokes mavlink helper to send out status text
 * @author aaron hunter
 * @modified for use in DORA system
 */
void publish_status_text(char text[50]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t severity = 5;
    uint16_t id = 0;
    uint8_t chunk_seq = 0;
    mavlink_msg_statustext_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            severity,
            text,
            id,
            chunk_seq
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}
/**
 * @Function publish_hil_act_controls(uint64_t flags, float outputs[16])
 * @param flags - flag that determines what info is transmitted
 *      outputs - data to be sent over mavlink
 * @brief invokes mavlink helper to generate hil act controls packet and sends out via the radio
 * @author aaron hunter
 * @modified for use in DORA system
 */
void publish_hil_act_controls(uint64_t flags, float outputs[16]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_hil_actuator_controls_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            0,
            outputs,
            1,
            flags);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_heartbeat(uint8_t status)
 * @param status - int value that specifies the type of heartbeat
 * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
 * @author aaron hunter
 * @modified for use in DORA system
 */
void publish_heartbeat(uint8_t status) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t mode = MAV_MODE_FLAG_MANUAL_INPUT_ENABLED | MAV_MODE_FLAG_SAFETY_ARMED;
    uint32_t custom = 0;
    mavlink_msg_heartbeat_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC,
            mode,
            custom,
            status);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_euler_angles(float angles[3])
 * @param angles[3] - yaw, pitch, and roll angles received from sim
 * @brief invokes mavlink helper to send actuator commands over radio
 * @author Margaret Silva
 */
void publish_euler_angles(float angles[3]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_hil_controls_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            0, // time (unused)
            angles[0],
            angles[1],
            angles[2],
            0.0,
            0.0, 0.0, 0.0, 0.0, // unused aux commands
            SEND_EULER_ANGLES_FROM_DORA,
            0
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}