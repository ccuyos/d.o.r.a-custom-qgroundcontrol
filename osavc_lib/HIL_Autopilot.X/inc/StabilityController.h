/* ************************************************************************** */
/** Autopilot Feedback Controller Library

  @File Name
    AutopilotController.h

  @Summary
    This library contains all the functions used to determine the actuator outputs
    needed for the aircraft to achieve the input course and pitch angles

  @Description
    This library is where users implement their controller. Internal functions can
    be altered, but the outputs of the actuator return functions should remain
    unchanged
 
 * Created on February 8, 2023, 1:52 PM
 */
/* ************************************************************************** */

#ifndef _STABILITY_CONTROLLER_H
#define	_STABILITY_CONTROLLER_H

/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/
#include "StateEstimation.h"

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
#define DEFAULT_CONTROLLER_GAINS {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
#define DEFAULT_CONTROLLER_OUTPUTS {0, 0, 0, 0}

/*******************************************************************************
 * PUBLIC TYPEDEFS                                                             *
 ******************************************************************************/
struct controller_outputs{
    float d_aileron;
    float d_elevator;
    float d_rudder;
    float d_throttle;
};


struct controller_gains{
    // roll
    float kp_roll;
    float kd_roll;
    float ki_roll;
    // rudder (sideslip or yaw damping)
    float kp_rudder;
    float ki_rudder;
    // course 
    float kp_course;
    float ki_course;
    // pitch 
    float kp_pitch;
    float kd_pitch;
    // altitude
    float kp_altitude;
    float ki_altitude;
    // speed
    float kp_speedFromThrottle;
    float ki_speedFromThrottle;
    float kp_speedFromElevator;
    float ki_speedFromElevator;
};


/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

/**
 * @Function initController()
 * @param none
 * @return none
 * @brief initializes controller to have the values saved on the SD card
 * @author Margaret Silva */
void initController(void);


/**
 * @Function updateController()
 * @param current_state, the aircraft's current state
 *        X_c, the aircraft's commanded course angle
 *        h_c, the aircraft's commanded altitude
 *        Va_c, the aircraft's commanded airspeed
 * @return controller outputs calculated by controller
 * @brief updates the controller with the current state estimation and commanded
 *          course and altitude
 * @author Margaret Silva */
struct controller_outputs updateController(struct vehicle_state current_state, 
        float X_c, float h_c, float Va_c);


/**
 * @Function setControllerGains(controller_gains gains)
 * @param gains - controller gains for both lateral and longitudinal controllers
 * @return none
 * @brief updates the controller with the a new set of gains. This function only works for controllers
 *          that match with the default 163 controller structure
 * @author Margaret Silva */
void setControllerGains(float gains[16]);


/**
 * @Function getControllerGains()
 * @param none
 * @return current controller gains
 * @brief returns the current controller gains. This only works with the default
 *      controller set up as is
 * @author Margaret Silva */
struct controller_gains getControllerGains(void);

#endif	/* _STABILITY_CONTROLLER_H */

