# D.O.R.A Autopilot

## Team Members

Carl Cuyos (ccuyos@ucsc.edu), Brendan Kirk (brkirk@ucsc.edu), Kevin Mok (kemok@ucsc.edu), Khushi Shah (khkshah@ucsc.edu), 
Margaret (Gretl) Silva (maansilv@ucsc.edu)

## Quick Start Guide

To run autopilot controller on OSAVC: Within the folder "dora_autopilot", compile the project "DORA_Autopilot.X". To run 
waypoint following, compile with "main_waypoint_following.c" as the main. To run the normal stability controller that 
receives reference commands, compile with "main_state_est_testing.c" as the main. To test attitude estimation with the 
ICM-20948, compile with the main file "main_attitude_est.c".

To run the gimbal controller: Within the folder "gimbal-lib", compile the project "I_M_UAV.X", using "main_capstone.c" as 
the main file. If you are using a pic with the typical I/O shield, pin 3 corresponds to the yaw servo in the gimbal, pin 5
corresponds to the pitch servo in the gimbal, and pin 6 corresponds to the roll servo in the gimbal. 

To run the simulator: Go to the folder "python/ECE163 HIL Simulator" and run the file DORA_HIL.py to launch
the GUI. 

## Detailed User Guide In Progress


## Special Thanks

To Gabriel Elkaim, for mentoring us through our capstone project

To Aaron Hunter, for the use of his OSAVC library

To Young Wang, for the use of his Beluga airframe design