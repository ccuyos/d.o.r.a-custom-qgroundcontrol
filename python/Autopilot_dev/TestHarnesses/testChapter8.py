'''
    File: testChapter8.py
    Author: Margaret Silva
    Notes: Contains tests for functions within the vehicle estimation file
'''

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Controls.VehicleEstimator as Estimator
import ece163.Controls.VehicleClosedLoopControl as VCLC
import ece163.Containers.Controls as Controls
import matplotlib.pyplot as plt

"""math.isclose doesn't work well for comparing things near 0 unless we 
use an absolute tolerance, so we make our own isclose:"""
isclose = lambda  a,b : math.isclose(a, b, abs_tol= 1e-12)

def compareVectors(a, b):
	"""A quick tool to compare two vectors"""
	el_close = [isclose(a[i][0], b[i][0]) for i in range(3)]
	return all(el_close)

#of course, you should test your testing tools too:
assert(compareVectors([[0], [0], [-1]],[[1e-13], [0], [-1+1e-9]]))
assert(not compareVectors([[0], [0], [-1]],[[1e-11], [0], [-1]]))
assert(not compareVectors([[1e8], [0], [-1]],[[1e8+1], [0], [-1]]))


failed = []
passed = []
def evaluateTest(test_name, boolean):
	"""evaluateTest prints the output of a test and adds it to one of two 
	global lists, passed and failed, which can be printed later"""
	if boolean:
		print(f"   passed {test_name}")
		passed.append(test_name)
	else:
		print(f"   failed {test_name}")
		failed.append(test_name)
	return boolean


#%% Test attitude state estimation using graphs

n_steps = 25000
# init vehicle controller
test_controls = VCLC.VehicleClosedLoopControl()
# control_gains = Controls.controlGains(3.0561, 0.001, 0.043177, 1.48226, 1.9, 5.0968, 2.5484, -13.1825, 
# 				      -0.7931, 0.07743, 0.030975, 1.493, 1.1466, -0.5206, -0.1)
control_gains = Controls.controlGains(3.0561, 0.001, 0.043177, .5, .05, 5.0968, 2.5484, -13.1825, 
				      -0.7931, 0.07743, 0.030975, 1.493, 1.1466, -0.5206, -0.1)
test_controls.setControlGains(controlGains=control_gains)
test_dynamics = test_controls.getVehicleAerodynamicsModel()
# init estimation
test_estimation = Estimator.VehicleEstimator(VPC.dT, test_dynamics)
test_estimation.setFilterGains(2.5, 2.5, .25, .25)
# set up all data arrays 
time_arr = [VPC.dT*i for i in range(n_steps)]
actual_p = [0 for i in range(n_steps)]
actual_q = [0 for i in range(n_steps)]
actual_r = [0 for i in range(n_steps)]
p_diff = [0 for i in range(n_steps)]
q_diff = [0 for i in range(n_steps)]
r_diff = [0 for i in range(n_steps)]
est_yaw = [0 for i in range(n_steps)]
est_pitch = [0 for i in range(n_steps)]
est_roll = [0 for i in range(n_steps)]
yaw_diff = [0 for i in range(n_steps)]
actual_yaw = [0 for i in range(n_steps)]
pitch_diff = [0 for i in range(n_steps)]
actual_pitch = [0 for i in range(n_steps)]
roll_diff = [0 for i in range(n_steps)]
actual_roll = [0 for i in range(n_steps)]
Va_diff = [0 for i in range(n_steps)]
Va_true = [0 for i in range(n_steps)]
Va_est = [0 for i in range(n_steps)]
Va_cmd = [0 for i in range(n_steps)]
alt_diff = [0 for i in range(n_steps)]
alt_est = [0 for i in range(n_steps)]
alt_true = [0 for i in range(n_steps)]
alt_cmd = [0 for i in range(n_steps)]
course_diff = [0 for i in range(n_steps)]
course_true = [0 for i in range(n_steps)]
course_est = [0 for i in range(n_steps)]
course_cmd = [0 for i in range(n_steps)]
pn_est = [0 for i in range(n_steps)]
pn_true = [0 for i in range(n_steps)]
pe_est = [0 for i in range(n_steps)]
pe_true = [0 for i in range(n_steps)]

# best gains so far: Kp_att_a=5, Kp_att_h=15, Kp_alt=.8, Kp_spd=.11, Kp_course=.5, Ki_att_a=.5, Ki_att_h=1, Ki_spd=.11
test_estimation.setFilterGains(Kp_att_a=5, Kp_att_m=15, Kp_alt=.8, Kp_spd=.11, Kp_course=.5, Ki_att_a=.5, Ki_att_m=1, Ki_spd=.11) 
# test_estimation.setFilterGains(Kp_att_a=1, Kp_att_m=0, Kp_alt=.8, Kp_spd=.11, Kp_course=0.9, Ki_att_a=.1, Ki_att_m=0, Ki_spd=.11) 

# run simulation of state estimation
for i in range(n_steps):
	# update simulation
	test_estimation.update()

	# record data to graph later
	actual_p[i] = test_estimation.getVehicleState().p 
	actual_q[i] = test_estimation.getVehicleState().q
	actual_r[i] = test_estimation.getVehicleState().r
	p_diff[i] = test_estimation.getVehicleState().p - test_estimation.getStateEstimate().p
	q_diff[i] = test_estimation.getVehicleState().q - test_estimation.getStateEstimate().q
	r_diff[i] = test_estimation.getVehicleState().r - test_estimation.getStateEstimate().r
	est_yaw[i] = test_estimation.getStateEstimate().yaw
	est_pitch[i] = test_estimation.getStateEstimate().pitch
	est_roll[i] = test_estimation.getStateEstimate().roll
	actual_yaw[i] = test_estimation.getVehicleState().yaw 
	actual_pitch[i] = test_estimation.getVehicleState().pitch
	actual_roll[i] = test_estimation.getVehicleState().roll
	yaw_diff[i] = test_estimation.getVehicleState().yaw - test_estimation.getStateEstimate().yaw
	pitch_diff[i] = test_estimation.getVehicleState().pitch - test_estimation.getStateEstimate().pitch
	roll_diff[i] = (test_estimation.getVehicleState().roll - test_estimation.getStateEstimate().roll) #% math.pi
	Va_diff[i] = test_estimation.getVehicleState().Va - test_estimation.getStateEstimate().Va
	Va_true[i] = test_estimation.getVehicleState().Va
	Va_est[i] = test_estimation.getStateEstimate().Va
	alt_diff[i] = test_estimation.getVehicleState().pd - test_estimation.getStateEstimate().pd
	alt_est[i] = test_estimation.getStateEstimate().pd
	alt_true[i] = test_estimation.getVehicleState().pd
	course_diff[i] = math.degrees(test_estimation.getVehicleState().chi - test_estimation.getStateEstimate().chi)
	course_est[i] = math.degrees(test_estimation.getStateEstimate().chi)
	course_true[i] = math.degrees(test_estimation.getVehicleState().chi)
	pn_est[i] = test_estimation.getStateEstimate().pn
	pn_true[i] = test_estimation.getVehicleState().pn
	pe_est[i] = test_estimation.getStateEstimate().pe
	pe_true[i] = test_estimation.getVehicleState().pe
	
	# give new set of reference commands
	ref_controls = Controls.referenceCommands(VPC.InitialYawAngle, -VPC.InitialDownPosition, VPC.InitialSpeed)
	if (i >= 500) and (i < 10000):
		ref_controls = Controls.referenceCommands(VPC.InitialYawAngle + 0.5236, -VPC.InitialDownPosition + 50, VPC.InitialSpeed - 10)
	elif (i>=10000) and (i < 15000):
		ref_controls = Controls.referenceCommands(VPC.InitialYawAngle - 0.5236, -VPC.InitialDownPosition - 50, VPC.InitialSpeed + 15)
	Va_cmd[i] = ref_controls.commandedAirspeed
	alt_cmd[i] = -ref_controls.commandedAltitude
	course_cmd[i] = math.degrees(ref_controls.commandedCourse)
	test_controls.Update(ref_controls)

# display gains used
print("gains:", test_estimation.getFilterGains())

# graph simulation results
num_graphs = 18

p_diff = [math.degrees(p_diff[i]) for i in range(n_steps)]
plt.subplot(math.ceil(num_graphs/2), 3, 1)
plt.plot(time_arr, p_diff)
plt.title("Roll rate error")
plt.ylabel("angular rate (degrees/sec)")
# plt.xlabel("time (seconds)")

q_diff = [math.degrees(q_diff[i]) for i in range(n_steps)]
plt.subplot(math.ceil(num_graphs/2), 3, 2)
plt.plot(time_arr, q_diff)
plt.title("Pitch rate error")
plt.ylabel("angular rate (degrees/sec)")
# plt.xlabel("time (seconds)")

r_diff = [math.degrees(r_diff[i]) for i in range(n_steps)]
plt.subplot(math.ceil(num_graphs/2), 3, 3)
plt.plot(time_arr, r_diff)
plt.title("Yaw rate error")
plt.ylabel("angular rate (degrees/sec)")
# plt.xlabel("time (seconds)")

roll_diff = [math.degrees(roll_diff[i]) for i in range(n_steps)]
plt.subplot(math.ceil(num_graphs/2), 3, 4)
plt.plot(time_arr, roll_diff)
plt.title("Roll error")
plt.ylabel("angle (degrees)")
# plt.xlabel("time (seconds)")

pitch_diff = [math.degrees(pitch_diff[i]) for i in range(n_steps)]
plt.subplot(math.ceil(num_graphs/2), 3, 5)
plt.plot(time_arr, pitch_diff)
plt.title("Pitch error")
plt.ylabel("angle (degrees)")
# plt.xlabel("time (seconds)")

yaw_diff = [math.degrees(yaw_diff[i]) for i in range(n_steps)]
plt.subplot(math.ceil(num_graphs/2), 3, 6)
plt.plot(time_arr, yaw_diff)
plt.title("Yaw error")
plt.ylabel("angle (degrees)")
# plt.xlabel("time (seconds)")

est_roll = [math.degrees(est_roll[i]) for i in range(n_steps)]
actual_roll = [math.degrees(actual_roll[i]) for i in range(n_steps)]
plt.subplot(math.ceil(num_graphs/2), 3, 7)
plt.plot(time_arr, est_roll, label="est")
plt.plot(time_arr, actual_roll, label="true")
plt.title("Roll comparison")
plt.ylabel("angle (degrees)")
# plt.xlabel("time (seconds)")
plt.legend()

est_pitch = [math.degrees(est_pitch[i]) for i in range(n_steps)]
actual_pitch = [math.degrees(actual_pitch[i]) for i in range(n_steps)]
plt.subplot(math.ceil(num_graphs/2), 3, 8)
plt.plot(time_arr, est_pitch, label="est")
plt.plot(time_arr, actual_pitch, label="true")
plt.title("Pitch comparison")
plt.ylabel("angle (degrees)")
# plt.xlabel("time (seconds)")
plt.legend()

est_yaw = [math.degrees(est_yaw[i]) for i in range(n_steps)]
actual_yaw = [math.degrees(actual_yaw[i]) for i in range(n_steps)]
plt.subplot(math.ceil(num_graphs/2), 3, 9)
plt.plot(time_arr, est_yaw, label="est")
plt.plot(time_arr, actual_yaw, label="true")
plt.title("Yaw comparison")
plt.ylabel("angle (degrees)")
# plt.xlabel("time (seconds)")
plt.legend()

plt.subplot(math.ceil(num_graphs/2), 3, 10)
# plt.plot(time_arr, Va_diff)
# plt.title("Airspeed error")
# plt.ylabel("speed (m/s)")
# # plt.xlabel("time (seconds)")
plt.plot(time_arr, pn_est, label="est")
plt.plot(time_arr, pn_true, label="true")
plt.title("North position comparison")
plt.ylabel("position (m)")
plt.legend()

plt.subplot(math.ceil(num_graphs/2), 3, 11)
plt.plot(time_arr, alt_diff)
plt.title("Altitude error")
plt.ylabel("altitude (m)")
# plt.xlabel("time (seconds)")

plt.subplot(math.ceil(num_graphs/2), 3, 12)
plt.plot(time_arr, course_diff)
plt.title("Course error")
plt.ylabel("angle (degrees)")
# plt.xlabel("time (seconds)")

plt.subplot(math.ceil(num_graphs/2), 3, 13)
# plt.plot(time_arr, Va_est, label="est")
# plt.plot(time_arr, Va_true, label="true")
# plt.plot(time_arr, Va_cmd, label="cmd")
# plt.title("Airspeed comparison")
# plt.ylabel("speed (m/s)")
# # plt.xlabel("time (seconds)")
# plt.legend()
plt.plot(time_arr, pe_est, label="est")
plt.plot(time_arr, pe_true, label="true")
plt.title("East position comparison")
plt.ylabel("position (m)")
plt.legend()

plt.subplot(math.ceil(num_graphs/2), 3, 14)
plt.plot(time_arr, alt_est, label="est")
plt.plot(time_arr, alt_true, label="true")
plt.plot(time_arr, alt_cmd, label="cmd")
plt.title("Altitude comparison")
plt.ylabel("altitude (m)")
# plt.xlabel("time (seconds)")
plt.legend()

plt.subplot(math.ceil(num_graphs/2), 3, 15)
plt.plot(time_arr, course_est, label="est")
plt.plot(time_arr, course_true, label="true")
plt.plot(time_arr, course_cmd, label="cmd")
plt.title("Course comparison")
plt.ylabel("angle (degrees)")
plt.xlabel("time (seconds)")
plt.legend()

pn_diff = [pn_true[i] - pn_est[i] for i in range(len(pn_true))]
plt.subplot(math.ceil(num_graphs/2), 3, 16)
plt.plot(time_arr, pn_diff)
plt.title("North position error")
plt.ylabel("position (m)")
plt.xlabel("time (seconds)")

pe_diff = [pe_true[i] - pe_est[i] for i in range(len(pn_true))]
plt.subplot(math.ceil(num_graphs/2), 3, 17)
plt.plot(time_arr, pe_diff)
plt.title("East position error")
plt.ylabel("position (m)")
plt.xlabel("time (seconds)")

plt.subplot(math.ceil(num_graphs/2), 3, 18)
plt.plot(time_arr, Va_est, label="est")
plt.plot(time_arr, Va_true, label="true")
plt.plot(time_arr, Va_cmd, label="cmd")
plt.title("Airspeed comparison")
plt.ylabel("speed (m/s)")
plt.xlabel("time (seconds)")
plt.legend()

plt.subplots_adjust(top=0.88,
bottom=0.0,
left=0.125,
right=0.9,
hspace=0.5,
wspace=0.2)

plt.show()