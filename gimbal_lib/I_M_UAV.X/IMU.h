/*
   File:   IMU.h
   Author: ccuyos
   Brief: header file for IMU
   Created on May 27, 2023, 1:44 AM
 */

#ifndef IMU_H
#define IMU_H

#include <math.h>
/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
#define SAMPLING_SIZE 10
#define G 9.80
//Accelerometer
#define ACC_X_SCALE 0.0097
#define ACC_Y_SCALE 0.0103
#define ACC_Z_SCALE 0.01
#define ACC_X_BIAS 0.7740
#define ACC_Y_BIAS 0.3655
#define ACC_Z_BIAS 1.7285

//Magnetometer
#define CONV_MAG 0.15
#define MAG_X_SCALE 0.0113
#define MAG_Y_SCALE 0.0125
#define MAG_Z_SCALE 0.0127
#define X_INTENSITY 22645.8
#define Y_INTENSITY 5191.7
#define Z_INTENSITY 41236.1


#define NT_TO_G 100000 

//Gyroscope
#define CONV_GYRO 131.068 //
#define GYRO_X_BIAS -0.0816
#define GYRO_Y_BIAS -0.0945 //-0.0967
#define GYRO_Z_BIAS -0.0054
#define GYRO_X_DRIFT -1.082126758874988e-05
#define GYRO_Y_DRIFT 0.001109666480928
#define GYRO_Z_DRIFT -4.294238025995358e-04



/*Helper macros*/
#define PI 3.14159265
#define rad2deg(r) (r*180/M_PI)
#define deg2rad(d) (d* M_PI/180)
//Credits to Max Dunne
#define DELAY(x)    {int wait; for (wait = 0; wait <= x; wait++) {asm("nop");}}
#define A_BIT       18300
#define A_LOT       183000
/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/
//IMU structs
typedef struct{
    float x; 
    float y;
    float z;
} IMU_axis;

typedef struct{
    int x;
    int y;
    int z;
}IMU_raw;
typedef struct {
    IMU_axis get_data;
    IMU_axis get_sum;
    float x[SAMPLING_SIZE];
    float y[SAMPLING_SIZE];
    float z[SAMPLING_SIZE];
    int pointerX;
    int pointerY;
    int pointerZ;
} IMU_samples;


struct IMU{
    IMU_samples acc_raw;
    IMU_samples mag_raw;
    IMU_raw gyro_out;
    
    IMU_axis acc_true;
    IMU_axis mag_true;
    IMU_axis gyro_true;
    IMU_axis gyro_deg; 
    IMU_axis gyro_rad;
};

//FRT struct for storing time
struct FRT{
    uint32_t us;
    uint32_t ms;
    uint32_t printTick;
    uint32_t filterTick;
    uint32_t checkTick;
};

/**
 * 
 * @function serviceReady
 * @param tick, number of ticks currently for respective service
 *        period, period for the respective service
 * @brief Returns SUCCESS if ready for servicing
 * @author: ccuyos
 */
uint8_t serviceReady(unsigned int* tick, int period);

/**
 * @function GetIMU()
 * @param none
 * @brief wrapper function for fetching imu library related variables
 * @return none
 * @author ccuyos
 */
struct IMU GetIMU(void);

/**
 * @function GetTime()
 * @param none
 * @brief wrapper function for fetching time related variables
 * @return none
 * @author ccuyos
 */
struct FRT GetTime(void);

/**
 * @function ProcessIMU()
 * @param none
 * @brief used for fetching raw IMU values from the BNO555
 *        and applying a moving average filter for smoothing
 * @return none
 * @author ccuyos
 */
void ProcessIMU(void);


#endif // imu_h
