'''
    Author: Margaret Silva

    Code related to homework 4
'''


import sys
sys.path.append("..") #python is horrible, no?

from ece163 . Utilities import MatrixMath as mm
from ece163.Constants import VehiclePhysicalConstants as VPC
import math
import matplotlib.pyplot as plt

va = 10
a_1 = - ((VPC.rho*(va**2)*VPC.S*VPC.b)/(2*VPC.Jzz))*VPC.Cnr*(VPC.b/(va*2))
a_2 = ((VPC.rho*(va**2)*VPC.S*VPC.b)/(2*VPC.Jzz))*VPC.Cnbeta
b_2 = a_2
w_n =  math.sqrt(a_2)
z = a_1/(2*w_n)
DC = 1/(1 - 5*(VPC.CndeltaR/VPC.Cnbeta))
print(a_1, a_2, b_2)
print(VPC.CndeltaR/VPC.Cnbeta)


A = [[ - a_1 , - a_2 ] ,[1 , 0]]
B = [[1] , [0]]
C = [[0 , b_2 ]]

dT = 0.0001
T_tot = 16
n_steps = int ( T_tot / dT )

t_data = [i* dT for i in range ( n_steps )]
yaw_data = [0 for i in range ( n_steps )]
rudder_data = [0 for i in range ( n_steps )]

# wind_data = [ (0 if t < 1 else 10* math . pi /180) for t in t_data ]
wind_data = [0 for i in range(n_steps)]

Kp = -5
Ki = -1.2
acccumlator = 0
error_dl = 0
acc_max = 100
acc_min = -100
ideal_resp = [0 for i in range(n_steps)]
wc = 3

x = [[0] ,[0]]
for i in range ( n_steps ):
    # record our data
    yaw_data [i ] = mm . multiply (C , x) [0][0]
    # find u(t):
    # u_no_ctrl = wind_data [i]   # no feedback controller
    ideal_resp[i] = math.sin(wc*dT*i)
    # ideal_resp[i] = 0
    error = ideal_resp[i] - yaw_data[i]
    acccumlator +=  (dT/2)*(error + error_dl)
    if acccumlator > acc_max:
        acccumlator = acc_max
    elif acccumlator < acc_min:
        acccumlator = acc_min
    rudder_data[i] = Kp*error + Ki*acccumlator
    u = wind_data[i] +(VPC.CndeltaR/VPC.Cnbeta)*(rudder_data[i])    # yes feedback controller
    error_dl = error
    # calculate derivative :
    x_dot = mm . add (
     mm . multiply (A ,x) ,
    mm . scalarMultiply (u ,B))
    # forward euler update :
    x = mm . add (x ,mm . scalarMultiply ( dT , x_dot ) )

plt . close (" all ")
fig , ax = plt . subplots ()
ax . plot ( t_data , wind_data , label = " wind angle")
# ax . plot ( t_data , ideal_resp , label = " ideal response")
ax . plot ( t_data , ideal_resp , label = " ideal response - wc="+str(wc))
ax . plot ( t_data , yaw_data , label = " yaw response - Kp="+str(round(Kp, 2))+", Ki="+str(round(Ki, 2)))
ax . plot ( t_data , rudder_data , label = " rudder deflection")
# ax . plot ( t_data , [0 for i in range ( n_steps )], label="desired yaw")
ax . set_xlabel (" time (s)")
ax . set_ylabel (" angle ( rad )")
ax . legend ()
plt.show()

# yaw_data_ctrl = yaw_data[:]

# yaw_data = [0 for i in range ( n_steps )]

# wind_data = [ (0 if t < 1 else 10* math . pi /180) for t in t_data ]

# x = [[0] ,[0]]
# for i in range ( n_steps ):
#     # record our data
#     yaw_data [i ] = mm . multiply (C , x) [0][0]
#     # find u(t):
#     u = wind_data [i]   # no feedback controller
#     # u = wind_data[i] + Kp*yaw_data[i]    # yes feedback controller
#     # calculate derivative :
#     x_dot = mm . add (
#      mm . multiply (A ,x) ,
#     mm . scalarMultiply (u ,B))
#     # forward euler update :
#     x = mm . add (x ,mm . scalarMultiply ( dT , x_dot ) )

# # plt . close (" all ")
# fig , ax = plt . subplots ()
# ax . plot ( t_data , wind_data , label = " wind angle ")
# ax . plot ( t_data , yaw_data_ctrl , label = " yaw response - Kp="+str(Kp)+", Ki="+str(Ki))
# ax . plot ( t_data , yaw_data , label = " yaw response - no control")
# ax . plot ( t_data , [0 for i in range ( n_steps )] )
# ax . set_xlabel (" time (s)")
# ax . set_ylabel (" angle ( rad )")
# ax . legend ()
# plt.show()



