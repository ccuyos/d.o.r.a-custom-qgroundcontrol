'''
    File: testChapter7.py
    Author: Margaret Silva
    Notes: Contains tests for functions within the sensors model file
'''

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Utilities.MatrixMath as mm
import ece163.Utilities.Rotations as Rotations
import ece163.Modeling.VehicleAerodynamicsModel as VAM
import ece163.Containers.States as States
import ece163.Containers.Sensors as sens
import ece163.Constants.VehiclePhysicalConstants as VPC
import matplotlib.pyplot as plt
import ece163.Sensors.SensorsModel as SensorsModel

"""math.isclose doesn't work well for comparing things near 0 unless we 
use an absolute tolerance, so we make our own isclose:"""
isclose = lambda  a,b : math.isclose(a, b, abs_tol= 1e-12)

def compareVectors(a, b):
	"""A quick tool to compare two vectors"""
	el_close = [isclose(a[i][0], b[i][0]) for i in range(3)]
	return all(el_close)

#of course, you should test your testing tools too:
assert(compareVectors([[0], [0], [-1]],[[1e-13], [0], [-1+1e-9]]))
assert(not compareVectors([[0], [0], [-1]],[[1e-11], [0], [-1]]))
assert(not compareVectors([[1e8], [0], [-1]],[[1e8+1], [0], [-1]]))



failed = []
passed = []
def evaluateTest(test_name, boolean):
	"""evaluateTest prints the output of a test and adds it to one of two 
	global lists, passed and failed, which can be printed later"""
	if boolean:
		print(f"   passed {test_name}")
		passed.append(test_name)
	else:
		print(f"   failed {test_name}")
		failed.append(test_name)
	return boolean



#%% Testing Gauss Markov

print("Testing Gauss Markov 1D class")

cur_test = "1D Gauss Markov test - zeros"
GM_test = SensorsModel.GaussMarkov()
result = GM_test.update(0)
expected = 0
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "1D Gauss Markov test - complex"
GM_test = SensorsModel.GaussMarkov(0.3, 500024, .76)
result = GM_test.update(.2)
expected = 0.2
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)


print("Testing Gauss Markov 3D class")

cur_test = "3D Gauss Markov set values test"
GM_test = SensorsModel.GaussMarkovXYZ(tauX = 42, etaX=33)
if not evaluateTest(cur_test, isclose(GM_test.Gx.tau,GM_test.Gy.tau) and isclose(GM_test.Gx.eta, GM_test.Gz.eta)):
	print("expected:", GM_test.Gx.tau, GM_test.Gx.eta)
	print("resulted:", GM_test.Gy.tau, GM_test.Gz.eta)



#%% Testing sensors model - update true values

print("Testing Sensor Model update true values")

cur_test = "SensorsModel test: update accels true"
sensors_test = SensorsModel.SensorsModel()
test_state = States.vehicleState(pe=5, q=12, w=4, r=-3, p=-5, v=7)
test_dot = States.vehicleState(pn=-4, yaw=4, pd=-5, v=22)
result = sensors_test.updateAccelsTrue(test_state, test_dot)
result = [[result[0]], [result[1]], [result[2]]]
expected = [[69.0], [42.0], [-44.81]]
if not evaluateTest(cur_test, compareVectors(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "SensorsModel test: update GPS true"
sensors_test = SensorsModel.SensorsModel()
test_state = States.vehicleState(pe=5, roll=4,q=12, w=.3, r=-3, p=-5, v=7)
test_dot = States.vehicleState(pn=-4, yaw=4, pd=-5, v=22, pe=-2)
result = sensors_test.updateGPSTrue(test_state, test_dot)
result = [[result[0]], [result[1]], [result[2]], [result[3]], [result[4]]]
expected = [[0.0], [5], [-0.0], [4.47213595499958], [-2.677945044588987]]
if not evaluateTest(cur_test, compareVectors(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "SensorsModel test: update gyros true"
sensors_test = SensorsModel.SensorsModel()
test_state = States.vehicleState(p=.2, q=-4, r=-2)
result = sensors_test.updateGyrosTrue(test_state)
result = [[result[0]], [result[1]], [result[2]]]
expected = [[.2], [-4], [-2]]
if not evaluateTest(cur_test, compareVectors(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "SensorsModel test: update mags true"
sensors_test = SensorsModel.SensorsModel()
test_state = States.vehicleState()
result = sensors_test.updateMagsTrue(test_state)
result = [[result[0]], [result[1]], [result[2]]]
expected = [[22750.0], [5286.8], [41426.3]]
if not evaluateTest(cur_test, compareVectors(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "SensorsModel test: update pressure sensors true"
sensors_test = SensorsModel.SensorsModel()
test_state = States.vehicleState(u=30, v=2, w=.3, pd=-40, p=.2, q=-4, r=-2)
result = sensors_test.updatePressureSensorsTrue(test_state)
result = [[result[0]], [result[1]], [0]]
expected = [[100827.35832], [573.283469], [0]]
if not evaluateTest(cur_test, compareVectors(result,expected)):
	print("expected:", expected)
	print("resulted:", result)


print("Testing Sensor Model update true sensors")

cur_test = "SensorsModel test: update true sensors - zeros test"
sensors_test = SensorsModel.SensorsModel()
test_state = States.vehicleState()
test_dot = States.vehicleState()
input = sens.vehicleSensors()
expected = sens.vehicleSensors(accel_z=-9.81, mag_x=22750.0, mag_y=5286.8, mag_z=41426.3, baro=101325.0)
result = sensors_test.updateSensorsTrue(input, test_state, test_dot)
if not evaluateTest(cur_test, result == expected):
	print("expected:", expected)
	print("resulted:", result)
	
cur_test = "SensorsModel test: update true sensors - basic gps"
sensors_test = SensorsModel.SensorsModel()
test_state = States.vehicleState()
test_dot = States.vehicleState()
input = sens.vehicleSensors(gps_sog=12, gps_cog=-12)
expected = sens.vehicleSensors(accel_z=-9.81, mag_x=22750.0, mag_y=5286.8, mag_z=41426.3, baro=101325.0)
result = sensors_test.updateSensorsTrue(input, test_state, test_dot)
if not evaluateTest(cur_test, result == expected):
	print("expected:", expected)
	print("resulted:", result)


print("Testing sensors model update noisy sensors (using graphs)")

n_steps = 5000
dT = VPC.dT
time_arr = [dT*i for i in range(n_steps)]
gyro_data = [0 for i in range(n_steps)]
acc_data = [0 for i in range(n_steps)]
mag_data = [0 for i in range(n_steps)]
gps_data = [0 for i in range(n_steps)]
gyro_data_e = [0 for i in range(n_steps)]
acc_data_e = [0 for i in range(n_steps)]
mag_data_e = [0 for i in range(n_steps)]
gps_data_e = [0 for i in range(n_steps)]
gyro_data_t = [0 for i in range(n_steps)]
acc_data_t = [0 for i in range(n_steps)]
mag_data_t = [0 for i in range(n_steps)]
gps_data_t = [0 for i in range(n_steps)]
test_model = VAM.VehicleAerodynamicsModel()
sensors_test = SensorsModel.SensorsModel(aeroModel=test_model)
for i in range(n_steps):
	sensors_test.update()
	gyro_data[i] = sensors_test.sensorsNoisy.gyro_x
	acc_data[i] = sensors_test.sensorsNoisy.accel_x
	mag_data[i] = sensors_test.sensorsNoisy.mag_x
	gps_data[i] = sensors_test.sensorsNoisy.gps_alt
	gyro_data_e[i] = sensors_test.sensorsNoisy.gyro_x - sensors_test.sensorsTrue.gyro_x
	acc_data_e[i] = sensors_test.sensorsNoisy.accel_x- sensors_test.sensorsTrue.accel_x
	mag_data_e[i] = sensors_test.sensorsNoisy.mag_x - sensors_test.sensorsTrue.mag_x
	gps_data_e[i] = sensors_test.sensorsNoisy.gps_alt - sensors_test.sensorsTrue.gps_alt
	gyro_data_t[i] = sensors_test.sensorsTrue.gyro_x
	acc_data_t[i] = sensors_test.sensorsTrue.accel_x
	mag_data_t[i] = sensors_test.sensorsTrue.mag_x
	gps_data_t[i] = sensors_test.sensorsTrue.gps_alt

plt.subplot(2, 2, 1)
plt.plot(time_arr, gyro_data)
plt.plot(time_arr, gyro_data_t)
plt.title("Gyro x axis")

plt.subplot(2, 2, 2)
plt.plot(time_arr, acc_data)
plt.plot(time_arr, acc_data_t)
plt.title("Accelerometer x axis")

plt.subplot(2, 2, 3)
plt.plot(time_arr, gps_data)
plt.plot(time_arr, gps_data_t)
plt.title("GPS alt")

plt.subplot(2, 2, 4)
plt.plot(time_arr, mag_data)
plt.plot(time_arr, mag_data_t)
plt.title("Magnetometer x axis")

plt.show()
	

plt.subplot(2, 2, 1)
plt.plot(time_arr, gyro_data_e)
plt.title("Gyro x axis - error")

plt.subplot(2, 2, 2)
plt.plot(time_arr, acc_data_e)
plt.title("Accelerometer x axis - error")

plt.subplot(2, 2, 3)
plt.plot(time_arr, gps_data_e)
plt.title("GPS alt - error")

plt.subplot(2, 2, 4)
plt.plot(time_arr, mag_data_e)
plt.title("Magnetometer x axis - error")

plt.show()

	


#%% Print results:

total = len(passed) + len(failed)
print(f"\n---\nPassed {len(passed)}/{total} tests")
[print("   " + test) for test in passed]

if failed:
	print(f"Failed {len(failed)}/{total} tests:")
	[print("   " + test) for test in failed]