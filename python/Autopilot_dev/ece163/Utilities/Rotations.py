"""
Author: Margaret Silva (maansilv@ucsc.edu)
This file contains helper function for rotations
"""

import math
from . import MatrixMath


def euler2DCM(yaw, pitch, roll):
    """
    converts euler angles into their corresponding direction cosine matrix using the [3-2-1] Euler set
    input: yaw, pitch, roll [rad]
    output: [3x3] dcm rotation matrix
    """
    
    # plug in the yaw pitch and roll angles into the dcm matrix formulas
    dcm = [[math.cos(pitch)*math.cos(yaw), 
            math.cos(pitch)*math.sin(yaw), 
            -math.sin(pitch)], 
        [math.sin(roll)*math.sin(pitch)*math.cos(yaw) - math.cos(roll)*math.sin(yaw), 
            math.sin(roll)*math.sin(pitch)*math.sin(yaw) + math.cos(roll)*math.cos(yaw), 
            math.sin(roll)*math.cos(pitch)], 
        [math.cos(roll)*math.sin(pitch)*math.cos(yaw) + math.sin(roll)*math.sin(yaw),
            math.cos(roll)*math.sin(pitch)*math.sin(yaw) - math.sin(roll)*math.cos(yaw),
            math.cos(roll)*math.cos(pitch)] ]

    return dcm



def dcm2Euler(dcm):
    """
    extracts the Euler angles from the rotation matrix in the form [yaw, pitch, roll] corresponding the [3-2-1] euler set
    input: [3x3] dcm rotation matrix
    output: yaw, pitch, roll [rad]
    """
    
    # range check: used to ensure numbers are within range
    rc = lambda n: -1.0 if n < -1.0 else (1.0 if n > 1.0 else n)

    # derive euler angles from the dcm
    pitch = -1.0*math.asin(rc(dcm[0][2]))

    roll = math.atan2(dcm[1][2], dcm[2][2])

    yaw = math.atan2(dcm[0][1], dcm[0][0])

    return [yaw, pitch, roll]


def ned2enu(points):
    """
    Changes coordinates from North-East-Down to East-North-Up
    input: [nx3] matrix of points in NED
    ouput: same set of [nx3] points in ENU
    """

    transform_mat = [[0, 1, 0], [1, 0, 0], [0, 0, -1]]
    transform_mat_inv = MatrixMath.transpose(transform_mat)

    return MatrixMath.multiply(points, transform_mat_inv)
    


