import math
import sys
sys.path.append('..')


import PyQt5.QtWidgets as QtWidgets

import ece163.Display.baseInterface as baseInterface
import ece163.Containers.States as vehicleState
import ece163.Display.GridVariablePlotter

class testInterface(baseInterface.baseInterface):
	def __init__(self, parent=None):
		self.vehicleState = vehicleState.vehicleState()
		self.t = 0
		super().__init__(parent)
		self.stateGrid = ece163.Display.GridVariablePlotter.GridVariablePlotter(2, 3, [['i']]*16)

		self.outPutTabs.addTab(self.stateGrid, "States")
		self.outPutTabs.setCurrentIndex(1)
		self.stateUpdateDefList.append(self.updateStatePlots)

		return

	def updateStatePlots(self, newState):
		# print('hi')
		stateList = list()
		for key in self.numericStatesDict.keys():
			stateList.append([getattr(newState, key)])
		# print(stateList)
		self.stateGrid.addNewAllData(stateList, [self.t]*6)
		return

	def getVehicleState(self):
		return self.vehicleState

	def runUpdate(self):
		self.t += .1
		self.vehicleState.pn += math.sin(self.t)
		self.vehicleState.pe += math.cos(self.t)
		self.vehicleState.pd += math.sin(self.t)

		return

	def resetSimulationActions(self):
		self.vehicleState = vehicleState.vehicleState()
		self.t = 0
		self.stateGrid.clearDataPointsAll()

sys._excepthook = sys.excepthook

def my_exception_hook(exctype, value, tracevalue):
	# Print the error and traceback
	import traceback
	with open("LastCrash.txt", 'w') as f:
		# f.write(repr(exctype))
		# f.write('\n')
		# f.write(repr(value))
		# f.write('\n')
		traceback.print_exception(exctype, value, tracevalue, file=f)
		# traceback.print_tb(tracevalue, file=f)
	print(exctype, value, tracevalue)
	# Call the normal Exception hook after
	sys._excepthook(exctype, value, tracevalue)
	sys.exit(0)

# Set the exception hook to our wrapping function
sys.excepthook = my_exception_hook



qtApp = QtWidgets.QApplication(sys.argv)
ourWindow = testInterface()
ourWindow.show()
qtApp.exec()