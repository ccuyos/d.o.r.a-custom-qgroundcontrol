/* 
 * File:   MatrixMath.h
 * Author: Margaret Silva
 * Note: This is an adapted version of the matrix math library I wrote in CSE 13E,
 *      with added functions to deal with vector operations
 *
 * Created on May 15, 2023, 2:41 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "MatrixMath.h"

//define helper functions
float MatrixDeterminant2by2(float M[2][2]);

/******************************************************************************
 * Matrix Display:
 *****************************************************************************/\

void MatrixPrint(float Array[3][3])
{
    int i, j;
    for (i = 0; i < DIM; i++) {
        for (j = 0; j < DIM; j++) {
            printf("%10.3f ", Array[i][j]);
        }
        printf("\n");
    }
}

/******************************************************************************
 * Matrix - Matrix Operations
 *****************************************************************************/

int MatrixEquals(float Array1[3][3], float Array2[3][3])
{
    //compare each element of the 2 arrays. If all elements are equal, return 1, else return 0
    int i, j;
    float compare;
    for (i = 0; i < DIM; i++) {
        for (j = 0; j < DIM; j++) {
            compare = Array1[i][j] - Array2[i][j];
            if (fabs(compare) > FP_DELTA) {
                return 0;
            }
        }
    }
    return 1;
}

void MatrixAdd(float M1[3][3], float M2[3][3], float result[3][3])
{
    int i, j;
    for (i = 0; i < DIM; i++) {
        for (j = 0; j < DIM; j++) {
            result[i][j] = M1[i][j] + M2[i][j];
        }
    }
}

/**
 * MatrixSub performs an element-wise matrix subtraction operation on two 3x3 matrices and 
 * "returns" the result by modifying the third argument.
 * @param: mat1, pointer to a summand 3x3 matrix
 * @param: mat2, pointer to a summand 3x3 matrix
 * @param: result, pointer to a 3x3 matrix that is modified to contain the subtractions of mat2 from mat1.
 * @return:  None
 * mat1 and mat2 are not modified by this function.  result is modified by this function.
 */
void MatrixSub(float mat1[3][3], float mat2[3][3], float result[3][3])
{
    int i, j;
    for (i = 0; i < DIM; i++) {
        for (j = 0; j < DIM; j++) {
            result[i][j] = mat1[i][j] - mat2[i][j];
        }
    }
}

void MatrixMultiply(float M1[3][3], float M2[3][3], float result[3][3])
{
    int i, j, k;
    //set result to 0 to avoid mistakes due to old values

    for (i = 0; i < DIM; i++) {
        //for each row of M1
        for (j = 0; j < DIM; j++) {
            //for each column of M2
            //clear result array
            result[i][j] = 0;
            for (k = 0; k < DIM; k++) {
                //iterate through each row and column
                result[i][j] += M1[i][k] * M2[k][j];
            }
        }
    }
}

/******************************************************************************
 * Vector - Vector Operations
 *****************************************************************************/

/**
 * VectorAdd performs an element-wise vector addition operation on two 3x1 vectors and 
 * "returns" the result by modifying the third argument.
 * @param: v1, pointer to a summand 3x1 vector
 * @param: v2, pointer to a summand 3x1 vector
 * @param: result, pointer to a 3x1 vector that is modified to contain the sum of v1 and v2.
 * @return:  None
 * v1 and v2 are not modified by this function.  result is modified by this function.
 */
void VectorAdd(float v1[3], float v2[3], float result[3])
{
    int i;
    for (i = 0; i < DIM; i++) {
        result[i] = v1[i] + v2[i];
    }
}

/**
 * VectorSub performs an element-wise vector subtraction operation on two 3x1 vectors and 
 * "returns" the result by modifying the third argument.
 * @param: v1, pointer to a 3x1 vector
 * @param: v2, pointer to a 3x1 vector
 * @param: result, pointer to a 3x1 vector that is modified to contain the subtraction of v2 from v1.
 * @return:  None
 * v1 and v2 are not modified by this function.  result is modified by this function.
 */
void VectorSub(float v1[3], float v2[3], float result[3])
{
    int i;
    for (i = 0; i < DIM; i++) {
        result[i] = v1[i] - v2[i];
    }
}

/**
 * VectorCross performs a cross product operation on two 3x1 vectors and 
 * "returns" the result by modifying the third argument.
 * @param: v1, pointer to a 3x1 vector
 * @param: v2, pointer to a 3x1 vector
 * @param: result, pointer to a 3x1 vector that is modified to contain the cross product of v1 and v2.
 * @return:  None
 * v1 and v2 are not modified by this function.  result is modified by this function.
 */
void VectorCross(float v1[3], float v2[3], float result[3])
{
    result[0] = v1[1] * v2[2] - v1[2] * v2[1];
    result[1] = v1[2] * v2[0] - v1[0] * v2[2];
    result[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

/******************************************************************************
 * Matrix - Vector Operations
 *****************************************************************************/

/**
 * MatrixVectorMultiply performs a matrix-vector multiplication operation on a 3x3 matrix
 * and a 3x1 vector and "returns" the result by modifying the third argument.
 * @param: mat, pointer to left factor 3x3 matrix
 * @param: v, pointer to right factor 3x1 vector
 * @param: result, pointer to matrix that is modified to contain the matrix product of mat and v.
 * @return: none
 * mat and v are not modified by this function.  result is modified by this function.
 */
void MatrixVectorMultiply(float mat[3][3], float v[3], float result[3])
{
    int i, j;
    for (i = 0; i < DIM; i++) {
        result[i] = 0;
        for (j = 0; j < DIM; j++) {
            result[i] += mat[i][j] * v[j];
        }
    }
}

/******************************************************************************
 * Matrix - Scalar Operations
 *****************************************************************************/

void MatrixScalarAdd(float x, float M[3][3], float result[3][3])
{
    int i, j;
    for (i = 0; i < DIM; i++) {
        for (j = 0; j < DIM; j++) {
            result[i][j] = M[i][j] + x;
        }
    }
}

void MatrixScalarMultiply(float x, float M[3][3], float result[3][3])
{
    int i, j;
    for (i = 0; i < DIM; i++) {
        for (j = 0; j < DIM; j++) {
            result[i][j] = M[i][j] * x;
        }
    }
}

/******************************************************************************
 * Vector - Scalar Operations
 *****************************************************************************/

/**
 * VectorScalarMultiply performs the multiplication of a vector and a scalar.
 * Each element of the matrix is multiplied x.
 * The result is "returned"by modifying the third argument.
 * @param: x, a scalar float
 * @param: vec, pointer to a 3x1 vector
 * @param: result, pointer to vector that is modified to contain vec * x.
 * @return: none
 * x and vec are not modified by this function.  result is modified by this function.
 */
void VectorScalarMultiply(float x, float v[3], float result[3])
{
    int i;
    for (i = 0; i < DIM; i++) {
        result[i] = x * v[i];
    }
}

/******************************************************************************
 * Unary Matrix Operations
 *****************************************************************************/

float MatrixTrace(float M[3][3])
{
    int i;
    float trace = 0;
    for (i = 0; i < DIM; i++) {
        trace += M[i][i];
    }
    return trace;
}

void MatrixTranspose(float M[3][3], float result[3][3])
{
    int i, j;
    for (i = 0; i < DIM; i++) {
        for (j = 0; j < DIM; j++) {
            result[j][i] = M[i][j];
        }
    }
}

void MatrixSubmatrix(int a, int b, float M[3][3], float result[2][2])
{
    int i, j;
    //use to iterate through the rows/columns of result
    int r_row = 0, r_col = 0;

    for (i = 0; i < DIM; i++) {
        //skip row a
        if (i == a) {
            continue;
        }
        for (j = 0; j < DIM; j++) {
            //skip column b
            if (j == b) {
                continue;
            }
            result[r_row][r_col] = M[i][j];
            r_col++;
        }
        r_row++;
        r_col = 0;
    }
}

float MatrixDeterminant(float M[3][3])
{
    int j;
    float det = 0;
    float sub[2][2];
    for (j = 0; j < DIM; j++) {
        MatrixSubmatrix(0, j, M, sub);
        det += (pow(-1, j)) * M[0][j] * MatrixDeterminant2by2(sub);
    }
    return det;
}

float MatrixDeterminant2by2(float M[2][2])
{
    float det;
    det = (M[0][0] * M[1][1]) - (M[0][1] * M[1][0]);
    return det;
}

void MatrixInverse(float M[3][3], float result[3][3])
{
    if (MatrixDeterminant(M) == 0) {
        //break if the determinant == 0, ie. there is no inverse
        return;
    }

    int i, j;
    float sub[2][2];
    float A[3][3], B[3][3];

    for (i = 0; i < DIM; i++) {
        for (j = 0; j < DIM; j++) {
            MatrixSubmatrix(i, j, M, sub);
            if (((i + j)&1) == 0) {
                //if i + j is even
                A[i][j] = MatrixDeterminant2by2(sub);
            } else {
                //if i + j is odd
                A[i][j] = (-1.0) * MatrixDeterminant2by2(sub);
            }
        }
    }
    float det_reciprocal = 1.0 / MatrixDeterminant(M);
    MatrixTranspose(A, B);
    MatrixScalarMultiply(det_reciprocal, B, result);
}

/******************************************************************************
 * Vector Unary Operations
 *****************************************************************************/

/**
 * VectorNorm returns the magnitude (norm) of the vector taken as argument
 * @param: v, pointer to 3x1 vector
 * @return: magnitude of vector
 * v is not modified by this function
 */
float VectorNorm(float v[3])
{
    return ((float) sqrt(pow(v[0], 2) + pow(v[1], 2) + pow(v[2], 2)));
}

/**
 * VectorSet creates a vector using the input float as values
 * @param: v0, first element of vector
 * @param: v1, second element of vector
 * @param: v2, third element of vector
 * @param: vec, pointer to 3x1 vector equal to [v0, v1, v2]
 * @return: magnitude of vector
 * v0, v1, and v2 are not modified by this function. vec is
 */
void VectorSet(float v0, float v1, float v2, float vec[3])
{
    vec[0] = v0;
    vec[1] = v1;
    vec[2] = v2;
}
