"""
Helper file to solve the problems in Homework 2 Problem 2 (Yaw Control)

@author Elkaim
@date 30-Jan-2023

@note created file (the other one is too complicated)
"""

import sys
sys.path.append("..")

import math
from ece163.Constants import VehiclePhysicalConstants as VPC

#%% Problem 2 Quadrotor Yaw Control, Part (a)

m = 2.5 # kg, given in problem
CT = 0.5 # given in problem
CQ = 0.05 # given in problem

Omega2 = (m*VPC.g0*math.pi*math.pi)/(VPC.rho*VPC.D_prop**4*CT)
print("Omega^2 is "+repr(Omega2))
print("Omega is "+repr(math.sqrt(Omega2)))

#%% Problem 2 Quadrotor Yaw Control, Part (b)

wdot = -0.5 # rad/s, given in problem
Izz = 0.075 # kg/m^2, given in problem
Mtot = wdot*Izz
print("Total Moment required is: "+repr(Mtot))

OmegaDiff2 = (4*math.pi*math.pi*Mtot/(2*CQ*VPC.D_prop**5*VPC.rho))
print("Difference in Omega^2's is: "+repr(OmegaDiff2))

#%% Problem 2 Quadrotor Yaw Control, Part (c)

Omega2Sum = 2*Omega2
print(f"The sum of Omega_ccw^2 and Omega_cw^2 is {Omega2Sum}")
TwoOmegaCCW = Omega2Sum + OmegaDiff2
print(f"Adding together and solving gets you: Omega_ccw^2 =  {TwoOmegaCCW/2}, or Omega_ccw = {math.sqrt(TwoOmegaCCW/2)}")
Omega2CW = -OmegaDiff2 + TwoOmegaCCW/2
print(f"And this Omega_cw^2 = {Omega2CW}, and thus the prop speed is {math.sqrt(Omega2CW)} rad/s")

#%% Problem 2 Quadrotor Yaw Control, Part (d)

Omega2term = (VPC.R_motor/VPC.KQ) * (VPC.rho*VPC.D_prop**5*CQ/(4*math.pi*math.pi))
Omegaterm = VPC.KQ
ConstTerm = VPC.R_motor * VPC.i0
Vin_ccw = Omega2term * TwoOmegaCCW/2 + math.sqrt(TwoOmegaCCW/2)*Omegaterm + ConstTerm
Vin_cw = Omega2term * Omega2CW + math.sqrt(Omega2CW)*Omegaterm + ConstTerm
print(f"Vin = {Omega2term} Omega^2 + {Omegaterm} Omega + {ConstTerm}")
print(f"Subbing in our numbers: Vin_ccw = {Vin_ccw} V.") 
print(f"Subbing in our numbers: Vin_cw {Vin_cw} V.") 
print(f"Finally, dividing by Vmax gets a PWM signal for each at\n: PWM_ccw = {100 * Vin_ccw/VPC.V_max} and PWM_cw = {100 * Vin_cw/VPC.V_max}")

#%% Problem 3 - Gliding Flight Plots
# Note that we are going to be using MATPLOTLIB here and also NUMPY (we hates numpy)
import matplotlib.pyplot as plt
import numpy as np

# derive free-fall terminal velocity, e.g.: CL = 0, find Va.

deltae_freefall = ((VPC.CM0*VPC.CLalpha)/VPC.CMalpha - VPC.CL0) / (VPC.CLdeltaE - (VPC.CLalpha * VPC.CMdeltaE / VPC.CMalpha)) # in radians
CD_freefall = (VPC.CDp + VPC.CDdeltaE*deltae_freefall)
print(f"Actual elevator angle to vertical dive: {deltae_freefall*180/math.pi}")
# Vff = np.linspace(40,85,150)
# Cdpff = (2 * VPC.mass * VPC.g0) / (VPC.rho * Vff ** 2 * VPC.S) - VPC.CDdeltaE * deltae_freefall
# Vffplot = plt.figure("Trying to figure out CDp")
# plt.plot(Vff, Cdpff)
# plt.xlabel("Terminal Speed in [m/s]")
# plt.ylabel("CDp [unitless]")
# plt.show()

alphaplot = plt.figure("Plot of alpha vs delta_e")
de = np.linspace(-40,7,1800) # sweep elevator from -30 to 8 in 0.3 degree increments
derad = de*math.pi/180.
alpha = [-(VPC.CM0 + VPC.CMdeltaE * dele)/VPC.CMalpha for dele in derad]
alphadeg = [math.degrees(a) for a in alpha]
# alpha = -(VPC.CM0 + VPC.CMdeltaE * derad)/VPC.CMalpha
CL = [VPC.CL0 + VPC.CLalpha * a + VPC.CLdeltaE * dele for (a,dele) in zip(alpha,derad)]
#CL = VPC.CL0 + VPC.CLalpha * alpha + VPC.CLdeltaE * derad
CD = [VPC.CDp + Cl*Cl/(math.pi * VPC.AR * VPC.e) + VPC.CDdeltaE * dele for (Cl,dele) in zip(CL,derad)]
#CD = VPC.CDp + np.square(CL)/(math.pi * VPC.AR * VPC.e) + VPC.CDdeltaE * derad
gamma = [math.atan(Cd/Cl) for (Cd,Cl) in zip(CD,CL)]
gammadeg = [math.degrees(gam) for gam in gamma]
#gamma = np.arctan(CD / CL)
Va = [math.sqrt(2 * VPC.mass * VPC.g0 * math.sin(gam)/ (VPC.rho * VPC.S *Cd)) for (gam,Cd) in zip(gamma,CD)]
#Va = np.sqrt(2 * VPC.mass * VPC.g0 * np.sin(gamma)/ (VPC.rho * VPC.S *CL))

plt.plot(de,alphadeg,"b-",label="alpha in degrees")
plt.plot(de,gammadeg,"r-",label="gamma in degrees")
plt.xlabel("elevator angle in degrees")
plt.title("Alpha and Gamma vs Delta_e")
plt.grid()
plt.show()

plt.plot(de,CL,"b-",label="CL")
plt.plot(de,CD,"r-",label="CD")
plt.xlabel("elevator angle in degrees")
plt.title("CL and CD vs Delta_e")
plt.grid()
plt.show()

plt.plot(de,Va,"b-",label="Va in [m/s]")
plt.xlabel("elevator angle in degrees")
plt.title("Va vs Delta_e")
plt.grid()
plt.show()




