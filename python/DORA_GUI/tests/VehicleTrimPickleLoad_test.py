"""
Quick file to test the pickle load from vehicle trim.
"""

import os
import sys
import math
import pickle

sys.path.append('..')

import ece163.Containers.States as States
import ece163.Containers.Inputs as Inputs

vTestState = States.vehicleState()
vTestInput = Inputs.controlInputs()

try:
	with open('../VehicleTrim_Data.pickle', 'rb') as f:
		vTestState, vTestInput = pickle.load(f)
except FileNotFoundError:
	print('Test file not found, exiting')
	sys.exit(-1)

print(vTestState)
print(vTestInput)
