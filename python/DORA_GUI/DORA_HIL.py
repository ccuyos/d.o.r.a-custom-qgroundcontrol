'''
/*
 * File:   DORA_HIL.py
 * Author: Carl Vincent Cuyos
 * Brief:  main .py file that runs controller for Dynamically-Operated Reconfigurable Autopilot (D.O.R.A)
 * project
 * Created on May 1, 2023
 *
'''

import math
import sys

import PyQt5.QtWidgets as QtWidgets

import ece163.Display.baseInterface as baseInterface
import ece163.Display.GridVariablePlotter
import ece163.Display.SliderWithValue
import ece163.Simulation.Chapter7Simulate
import ece163.Display.DataExport
import ece163.Display.doubleInputWithLabel
import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Constants.VehicleSensorConstants as VSC
import ece163.Display.WindControl as WindControl
from ece163.Display.vehicleTrimWidget import vehicleTrimWidget
from ece163.Display.controlGainsWidget import controlGainsWidget
from ece163.Display.ReferenceControlWidget import ReferenceControlWidget

# DORA Addition #
import ece163.Constants.MAVLINKConstants as mvc


stateNamesofInterest = ['pn', 'pe', 'pd', 'yaw', 'pitch', 'roll', 'u', 'v', 'w', 'p', 'q', 'r', 'alpha', 'beta']

positionRange = 200

defaultTrimParameters = [('Airspeed', VPC.InitialSpeed), ('Climb Angle', 0), ('Turn Radius', math.inf)]


class Chapter6(baseInterface.baseInterface):

	def __init__(self, parent=None):
		self.simulateInstance = ece163.Simulation.Chapter7Simulate.Chapter7Simulate()
		super().__init__(parent)
		self.setWindowTitle("DORA UAV HIL GUI")
		stateplotElements = [[x] for x in stateNamesofInterest]
		stateplotElements.append(['Va', 'Vg'])
		statetitleNames = list(stateNamesofInterest)
		statetitleNames.append('Va & Vg')
		legends = [False] * len(stateNamesofInterest) + [True]
		self.stateGrid = ece163.Display.GridVariablePlotter.GridVariablePlotter(5, 3, stateplotElements, titles=statetitleNames, useLegends=legends)
		# self.outPutTabs.addTab(self.stateGrid, "States")

		ControlPlotNames = ['Course', 'Speed', 'Height', 'Pitch', 'Roll']
		controlplotElements = [['Reference', 'Actual', 'Estimate'] for x in ControlPlotNames]
		trimPlotNames = ['Throttle', 'Aileron', 'Elevator', 'Rudder']
		controlplotElements.extend([['Trim', 'Actual'] for x in trimPlotNames])
		ControlPlotNames.extend(trimPlotNames)

		self.controlResponseGrid = ece163.Display.GridVariablePlotter.GridVariablePlotter(3, 4, controlplotElements, titles=ControlPlotNames, useLegends=True)
		self.afterUpdateDefList.append(self.updateControlResponsePlots)

		SensorPlotNames = ["Gyro X (deg/s)", "Gyro Y (deg/s)", "Gyro Z (deg/s)", "Baro (N/m^2)"]
		SensorPlotNames.extend(["Accel X (m/s^2)", "Accel Y (m/s^2)", "Accel Z (m/s^2)", "Pitot (N/m^2)"])
		SensorPlotNames.extend(["Mag X (nT)", "Mag Y (nT)", "Mag Z (nT)", "GPS Cog (deg)"])
		SensorPlotNames.extend(["GPS N (m)", "GPS E (m)", "GPS Alt (m)", "GPS Sog (m/s)"])

		sensorPlotElements = [['True', 'Noisy'] for x in SensorPlotNames]

		self.sensorsPlotGrid = ece163.Display.GridVariablePlotter.GridVariablePlotter(4, 4, sensorPlotElements, titles=SensorPlotNames, useLegends=True)
		self.afterUpdateDefList.append(self.updateSensorPlots)

		self.outPutTabs.setCurrentIndex(2)
		self.stateUpdateDefList.append(self.updateStatePlots)

		self.exportWidget = ece163.Display.DataExport.DataExport(self.simulateInstance, 'Chapter7')



		self.trimCalcWidget = vehicleTrimWidget(self, self.trimCalcComplete)

		# self.inputTabs.

		self.gainCalcWidget = controlGainsWidget(self, self.gainCalcComplete, parent=self)
		self.gainCalcWidget.createLinearizedModels(self.trimCalcWidget.currentTrimState, self.trimCalcWidget.currentTrimControls)

		# self.gainCalcWidget.createLinearizedModels(self.trimCalcWidget.currentTrimState, self.trimCalcWidget.currentTrimControls)


		self.referenceControl = ReferenceControlWidget()
		self.inputTabs.addTab(self.referenceControl, "Reference Control")

		self.windControl = WindControl.WindControl(self.simulateInstance.underlyingModel.getVehicleAerodynamicsModel())
		self.inputTabs.addTab(self.windControl, WindControl.widgetName)

		self.simulateInstance.underlyingModel.setControlGains(self.gainCalcWidget.curGains)
		self.simulateInstance.underlyingModel.setTrimInputs(self.trimCalcWidget.currentTrimControls)



		self.outPutTabs.addTab(self.trimCalcWidget, "Trim")
		self.outPutTabs.addTab(self.gainCalcWidget, "Gains")
		self.outPutTabs.addTab(self.stateGrid, "States")
		self.outPutTabs.addTab(self.sensorsPlotGrid, "Sensors")
		self.outPutTabs.addTab(self.controlResponseGrid, "Control Response")
		self.outPutTabs.addTab(self.exportWidget, "Export Data")
		#self.outPutTabs.addTab(exportTab, exportText)

		self.outPutTabs.setCurrentIndex(5)

		self.showMaximized()

		### Simulation Update Code ###
		# Updates the simulation when tab is being changed
		self.outPutTabs.currentChanged.connect(self.newTabClicked)
		# Default the simulation to be set to homepage
		self.outPutTabs.setCurrentIndex(0)
		# Default for all graphs to be turned off
		self.updatePlotsOn()
		self.updatePlotsOff()
		# Start QTimer for updating
		self.simulationTimedThread.timeout.connect(self.UpdateSimulationPlots)

		# Ensuring that only plot widgets get disabled
		self.plotWidgets = [self.sensorsPlotGrid, self.controlResponseGrid, self.stateGrid]

		## DORA Update Code ###
		self.MAVLINK_init()

		return

	def updateStatePlots(self, newState):
		self.updatePlotsOff()
		stateList = list()
		for key in stateNamesofInterest:
			newVal = getattr(newState, key)
			if key in ['yaw', 'pitch', 'roll', 'p', 'q', 'r', 'alpha', 'beta']:
				newVal = math.degrees(newVal)
			stateList.append([newVal])

		stateList.append([newState.Va, math.hypot(newState.u, newState.v, newState.w)])

		self.stateGrid.addNewAllData(stateList, [self.simulateInstance.time]*(len(stateNamesofInterest) + 1))
		self.updatePlotsOn()
		## MAVLINK Update ##
		self.MAVLINK_storeStates(newState)
		return

	def getVehicleState(self):
		return self.simulateInstance.underlyingModel.getVehicleState()

	def runUpdate(self):
		self.simulateInstance.takeStep(self.referenceControl.currentReference)
		return

	def resetSimulationActions(self):

		self.simulateInstance.reset()
		self.stateGrid.clearDataPointsAll()
		self.vehicleInstance.reset(self.simulateInstance.underlyingModel.getVehicleState())
		self.updateNumericStateBox(self.simulateInstance.underlyingModel.getVehicleState())
		self.vehicleInstance.removeAllAribtraryLines()
		self.controlResponseGrid.clearDataPointsAll()
		self.sensorsPlotGrid.clearDataPointsAll()
		self.outPutTabs.setCurrentIndex(0)

	def trimCalcComplete(self, **kwargs):
		"""
		if we have valid trim conditions we calculate the linear model
		"""
		self.gainCalcWidget.createLinearizedModels(self.trimCalcWidget.currentTrimState, self.trimCalcWidget.currentTrimControls)
		self.simulateInstance.underlyingModel.setTrimInputs(self.trimCalcWidget.currentTrimControls)
		return

	def gainCalcComplete(self):
		self.simulateInstance.underlyingModel.setControlGains(self.gainCalcWidget.curGains)
		self.simulateInstance.underlyingModel.setTrimInputs(self.trimCalcWidget.currentTrimControls)
		print(self.simulateInstance.underlyingModel.getControlGains())

	def updateControlResponsePlots(self):
		self.updatePlotsOff()
		inputToGrid = list()
		Commanded = self.referenceControl.currentReference
		vehicleState = self.simulateInstance.getVehicleState()
		inputToGrid.append([math.degrees(Commanded.commandedCourse), math.degrees(vehicleState.chi), math.degrees(self.mavlinkTab.estStates[mvc.est_chi])])  # Course
		inputToGrid.append([Commanded.commandedAirspeed, vehicleState.Va, self.mavlinkTab.estStates[mvc.est_Va]])  # Speed
		inputToGrid.append([Commanded.commandedAltitude, -vehicleState.pd, -self.mavlinkTab.estStates[mvc.est_pd]])  # Height
		inputToGrid.append([math.degrees(x) for x in [Commanded.commandedPitch, vehicleState.pitch, self.mavlinkTab.estStates[mvc.est_pitch]]])  # pitch
		inputToGrid.append([math.degrees(x) for x in [Commanded.commandedRoll, vehicleState.roll, self.mavlinkTab.estStates[mvc.est_roll]]])  # pitch
		ActualControl = self.simulateInstance.underlyingModel.getVehicleControlSurfaces()
		trimSettings = self.trimCalcWidget.currentTrimControls
		inputToGrid.append([trimSettings.Throttle, ActualControl.Throttle])  # Throttle
		inputToGrid.append([math.degrees(x) for x in [trimSettings.Aileron, ActualControl.Aileron]])  # Throttle
		inputToGrid.append([math.degrees(x) for x in [trimSettings.Elevator, ActualControl.Elevator]])  # Throttle
		inputToGrid.append([math.degrees(x) for x in [trimSettings.Rudder, ActualControl.Rudder]])  # Throttle
		self.controlResponseGrid.addNewAllData(inputToGrid, [self.simulateInstance.time]*len(inputToGrid))


		self.updatePlotsOn()

		## MAVLINK Update ##
		self.MAVLINK_storeControlResponse(Commanded)
		return

	def updateSensorPlots(self):
		self.updatePlotsOff()
		inputToGrid = list()
		noisySensors = self.simulateInstance.sensorModel.getSensorsNoisy()
		trueSensors = self.simulateInstance.sensorModel.getSensorsTrue()

		inputToGrid.append([math.degrees(trueSensors.gyro_x), math.degrees(noisySensors.gyro_x)])
		inputToGrid.append([math.degrees(trueSensors.gyro_y), math.degrees(noisySensors.gyro_y)])
		inputToGrid.append([math.degrees(trueSensors.gyro_z), math.degrees(noisySensors.gyro_z)])
		inputToGrid.append([trueSensors.baro, noisySensors.baro])

		inputToGrid.append([trueSensors.accel_x, noisySensors.accel_x])
		inputToGrid.append([trueSensors.accel_y, noisySensors.accel_y])
		inputToGrid.append([trueSensors.accel_z, noisySensors.accel_z])
		inputToGrid.append([trueSensors.pitot, noisySensors.pitot])

		inputToGrid.append([trueSensors.mag_x, noisySensors.mag_x])
		inputToGrid.append([trueSensors.mag_y, noisySensors.mag_y])
		inputToGrid.append([trueSensors.mag_z, noisySensors.mag_z])

		inputToGrid.append([math.degrees(trueSensors.gps_cog), math.degrees(noisySensors.gps_cog)])
		inputToGrid.append([trueSensors.gps_n, noisySensors.gps_n])
		inputToGrid.append([trueSensors.gps_e, noisySensors.gps_e])
		inputToGrid.append([trueSensors.gps_alt, noisySensors.gps_alt])
		inputToGrid.append([trueSensors.gps_sog, noisySensors.gps_sog])
		self.sensorsPlotGrid.addNewAllData(inputToGrid, [self.simulateInstance.time]*len(inputToGrid))
		self.updatePlotsOn()

		# MAVLINK Update
		self.MAVLINK_storeSensors(noisySensors)
		return

	### MAVLINK Helper Functions ###
	def MAVLINK_init(self):
		if mvc.GUI_mode > mvc.defaultMode:
			# Fetch each widget's pointer and give to MAVLINK for inspection
			self.mavlinkTab.setGainsWidget(self.gainCalcWidget) # To fetch gain values
			self.mavlinkTab.setRefWidget(self.referenceControl) # To fetch reference controls
			self.mavlinkTab.setSimulation(self.simulateInstance) # To fetch simulation related values
			self.yaw = 0
			self.pitch = 0
			self.roll = 0
			self.counter = 0
	def MAVLINK_storeStates(self, newState):
		# Update autopilot commands on the simulator
		if mvc.HIL_mode == mvc.autopilotMode:
			# Update states based on simulator calculation
			self.mavlinkTab.newStates = [newState.pn,
										 newState.pe,
										 newState.pd,
										 newState.u,
										 newState.v,
										 newState.w,
										 newState.yaw,
										 newState.pitch,
										 newState.roll,
										 newState.p,
										 newState.q,
										 newState.r,
										 newState.chi,
										 newState.alpha,
										 newState.beta,
										 newState.Va
										 ]
			self.simulateInstance.underlyingModel.UpdateAutopilotCommands(self.mavlinkTab.thr,
																		  self.mavlinkTab.ele,
																		  self.mavlinkTab.ail,
																		  self.mavlinkTab.rud)

	def MAVLINK_storeSensors(self, noisySensors):
		# Check if gps has updated
		cond1 = self.mavlinkTab.gps_lat != noisySensors.gps_n
		cond2 = self.mavlinkTab.gps_lon != noisySensors.gps_e
		cond3 = self.mavlinkTab.gps_altitude != noisySensors.gps_alt
		cond4 = self.mavlinkTab.gps_speed != noisySensors.gps_sog
		cond5 = self.mavlinkTab.gps_course != noisySensors.gps_cog
		# Update gps update flag if gps has updated or not
		gpsUpdateFlag = 0.0
		if cond1 or cond2 or cond3 or cond4 or cond5:
			gpsUpdateFlag = 1.0

		if self.mavlinkTab.ackGPS != 0:
			self.mavlinkTab.gpsList.append(self.simulateInstance.time)
		self.mavlinkTab.checkGPS = gpsUpdateFlag
		# Update gps readings
		self.mavlinkTab.gps_lat = noisySensors.gps_n
		self.mavlinkTab.gps_lon = noisySensors.gps_e
		self.mavlinkTab.gps_altitude = noisySensors.gps_alt
		self.mavlinkTab.gps_speed = noisySensors.gps_sog
		self.mavlinkTab.gps_course = noisySensors.gps_cog

		alt_baro = (noisySensors.baro - VSC.Pground) / (VPC.rho * VPC.g0)

		self.mavlinkTab.noisy_sensor = [
			noisySensors.accel_x,
			noisySensors.accel_y,
			noisySensors.accel_z,
			noisySensors.gyro_x,
			noisySensors.gyro_y,
			noisySensors.gyro_z,
			noisySensors.mag_x,
			noisySensors.mag_y,
			noisySensors.mag_z,
			alt_baro,
			noisySensors.gps_n,
			noisySensors.gps_e,
			noisySensors.gps_alt,
			noisySensors.gps_sog,
			noisySensors.gps_cog,
			gpsUpdateFlag]

	def MAVLINK_storeControlResponse(self, Commanded):
		# Store the update status of the commanded
		cond1 = Commanded.commandedCourse != self.mavlinkTab.commandedCourse
		cond2 = Commanded.commandedAltitude != self.mavlinkTab.commandedAltitude
		cond3 = Commanded.commandedAirspeed != self.mavlinkTab.commandedAirspeed
		self.mavlinkTab.commandedFlag = False
		if cond1 or cond2 or cond3:
			self.mavlinkTab.commandedFlag = True

		# Store all course, altitude, airspeed to the MAVLINK
		if mvc.HIL_command_mode == mvc.ece129_mode:
			self.mavlinkTab.commandedCourse = Commanded.commandedCourse
			self.mavlinkTab.commandedAltitude = Commanded.commandedAltitude
			self.mavlinkTab.commandedAirspeed = Commanded.commandedAirspeed


	###########################
	# Turns all simulation plots for a single instance (only if the widget is a plot widget)
	def UpdateSimulationPlots(self):
		currentWidget = self.outPutTabs.currentWidget()
		# Ensure that that the timer is only enabled for states, sensors, and control response widgets
		if (currentWidget in self.plotWidgets):
			self.updatePlotsOff()
		return

	# Updates a simulation widget when new tab clicked
	def newTabClicked(self):
		self.updatePlotsOn()
		self.updatePlotsOff()
		return

	# toggles the sensor plot widget
	def toggleSensorsPlot(self, toggleIn):
		self.sensorsPlotGrid.setUpdatesEnabled(toggleIn)
		return

	# toggles the control response widget
	def togglecontrolResponsePlot(self, toggleIn):
		self.controlResponseGrid.setUpdatesEnabled(toggleIn)
		return

	# toggles the state grid widget
	def togglestateGridPlot(self, toggleIn):
		self.stateGrid.setUpdatesEnabled(toggleIn)
		return

	# Turns on all simulation plots
	def updatePlotsOn(self):
		# print("Turning on plot update")
		self.toggleSensorsPlot(True)
		self.togglecontrolResponsePlot(True)
		self.togglestateGridPlot(True)
		return

	# Turns off all simulation plots
	def updatePlotsOff(self):
		# print("Turning off plot update")
		self.toggleSensorsPlot(False)
		self.togglecontrolResponsePlot(False)
		self.togglestateGridPlot(False)
		return
#######################

sys._excepthook = sys.excepthook

def my_exception_hook(exctype, value, tracevalue):
	# Print the error and traceback
	import traceback
	with open("LastCrash.txt", 'w') as f:
		# f.write(repr(exctype))
		# f.write('\n')
		# f.write(repr(value))
		# f.write('\n')
		traceback.print_exception(exctype, value, tracevalue, file=f)
		# traceback.print_tb(tracevalue, file=f)
	print(exctype, value, tracevalue)
	# Call the normal Exception hook after
	sys._excepthook(exctype, value, tracevalue)
	sys.exit(0)

# Set the exception hook to our wrapping function
sys.excepthook = my_exception_hook



qtApp = QtWidgets.QApplication(sys.argv)
ourWindow = Chapter6()
ourWindow.show()
qtApp.exec()