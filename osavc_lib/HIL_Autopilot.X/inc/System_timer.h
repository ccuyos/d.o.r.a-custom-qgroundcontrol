/* 
 * File: System_timer.h
 * Author: Aaron Hunter
 * Brief: 
 * Created on Jan 12 2021 10:13 am
 * Modified on <month> <day>, <year>, <hour> <pm/am>
 */

#ifndef SYSTEM_TIMER_H // Header guard
#define	SYSTEM_TIMER_H//

/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/

#include <stdint.h> 

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/


/*******************************************************************************
 * PUBLIC TYPEDEFS                                                             *
 ******************************************************************************/


/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/


/**
 * @Function void Sys_timer_init(void)
 * @param none
 * @return None.
 * @brief  Initializes the timer module 
 * @author Aaron Hunter*/
void Sys_timer_init(void);

/**
 * Function: Sys_timer_get_msec(void)
 * @param None
 * @return the current millisecond counter value
   */
uint32_t Sys_timer_get_msec(void);

/**
 * Function: Sys_timer_get_usec(void)
 * @param None
 * @return the current microsecond counter value
 * @author Aaron Hunter
   */
uint32_t Sys_timer_get_usec(void);


/**
 * 
 * @function serviceReady
 * @param tick, number of ticks currently for respective service
 *        period, period for the respective service
 * @note example use 
 *      if (serviceReady(&time.printTick, PRINT_PERIOD)){
 *          //do something
        }
 * @brief Returns SUCCESS if ready for servicing
 * @author ccuyos
 */
uint8_t serviceReady(uint32_t* tick, int period);

#endif	/* SYSTEM_TIMER_H */ // End of header guard

