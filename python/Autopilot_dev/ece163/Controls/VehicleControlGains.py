'''
    Author: Margaret Silva
    This file contains the functions used to compute gains and tuning parameters
'''

import math
import pickle
from ece163.Modeling import VehicleAerodynamicsModel
from ece163.Constants import VehiclePhysicalConstants as VPC
from ece163.Containers import States
from ece163.Containers import Inputs
from ece163.Containers import Controls
from ece163.Containers import Linearized
from ece163.Utilities import MatrixMath
from ece163.Utilities import Rotations


def computeGains(tuningParameters=Controls.controlTuning(), linearizedModel=Linearized.transferFunctions()):
    ''' Uses the desired natural frequency and damping coefficient to determine control gains '''
    controlGains = Controls.controlGains()
    tP = tuningParameters
    lM = linearizedModel

    # roll controller - ki is computed manually
    try:
        controlGains.kp_roll = (tP.Wn_roll**2) / lM.a_phi2
        controlGains.kd_roll = (2*tP.Zeta_roll*tP.Wn_roll - lM.a_phi1) / lM.a_phi2
    except:
        controlGains.kp_roll = 0
        controlGains.kd_roll = 0
    controlGains.ki_roll = 0.001        # ki should be tuned manually 

    # course controller 
    controlGains.kp_course = (2*tP.Zeta_course*tP.Wn_course*lM.Va_trim) / VPC.g0
    controlGains.ki_course = ((tP.Wn_course**2)*lM.Va_trim) / VPC.g0

    # sideslip controller 
    try:
        controlGains.ki_sideslip = (tP.Wn_sideslip**2) / lM.a_beta2
        controlGains.kp_sideslip = (2*tP.Zeta_sideslip*tP.Wn_sideslip - lM.a_beta1) / lM.a_beta2
    except:
        controlGains.ki_sideslip = 0
        controlGains.kp_sideslip = 0

    # pitch controller
    try:
        controlGains.kp_pitch = (tP.Wn_pitch**2 - lM.a_theta2) / lM.a_theta3
        controlGains.kd_pitch = (2*tP.Zeta_pitch*tP.Wn_pitch - lM.a_theta1) / lM.a_theta3
    except:
        controlGains.kp_pitch = 0
        controlGains.kd_pitch = 0

    # altitude hold using pitch 
    try:
        K_theta_DC = (controlGains.kp_pitch*lM.a_theta3) / (lM.a_theta2 + controlGains.kp_pitch*lM.a_theta3)
        controlGains.ki_altitude = (tP.Wn_altitude**2) / (K_theta_DC*lM.Va_trim)
        controlGains.kp_altitude = (2*tP.Zeta_altitude*tP.Wn_altitude) / (K_theta_DC*lM.Va_trim)
    except:
        controlGains.ki_altitude = 0
        controlGains.kp_altitude = 0

    # airspeed hold using pitch
    try:
        controlGains.ki_SpeedfromElevator = -(tP.Wn_SpeedfromElevator**2)/(K_theta_DC*VPC.g0)
        # print("tP.Wn_SpeedfromElevator**2", tP.Wn_SpeedfromElevator**2)
        # print("K_theta_DC", K_theta_DC)
        controlGains.kp_SpeedfromElevator = (lM.a_V1 - 2*tP.Zeta_SpeedfromElevator*tP.Wn_SpeedfromElevator)/(K_theta_DC*VPC.g0)
    except:
        controlGains.ki_SpeedfromElevator = 0
        controlGains.kp_SpeedfromElevator = 0

    # airspeed hold using throttle
    try:
        controlGains.ki_SpeedfromThrottle = (tP.Wn_SpeedfromThrottle**2)/lM.a_V2
        controlGains.kp_SpeedfromThrottle = (2*tP.Zeta_SpeedfromThrottle*tP.Wn_SpeedfromThrottle - lM.a_V1)/lM.a_V2
    except:
        controlGains.ki_SpeedfromThrottle = 0
        controlGains.kp_SpeedfromThrottle = 0

    return controlGains



def computeTuningParameters(controlGains=Controls.controlGains(), linearizedModel=Linearized.transferFunctions()):
    ''' Computes the natural frequency and damping coefficient from tuning params '''
    tuningParams = Controls.controlTuning()
    cG = controlGains
    lM = linearizedModel

    # roll controller
    tuningParams.Wn_roll = math.sqrt(abs(cG.kp_roll*lM.a_phi2))
    try:
        tuningParams.Zeta_roll = (lM.a_phi1 + lM.a_phi2*cG.kd_roll) / (2*tuningParams.Wn_roll)
    except:
        # it should technically be undefined, but set to 0 to prevent divide by 0
        tuningParams.Zeta_roll = 0

    # course controller 
    try:
        tuningParams.Wn_course = math.sqrt(abs((VPC.g0/lM.Va_trim)*cG.ki_course))
        tuningParams.Zeta_course = (VPC.g0*cG.kp_course)/(2*lM.Va_trim*tuningParams.Wn_course)
    except:
        tuningParams.Wn_course = 0
        tuningParams.Zeta_course = 0

    # sideslip hold
    tuningParams.Wn_sideslip = math.sqrt(abs(lM.a_beta2*cG.ki_sideslip))
    try:
        tuningParams.Zeta_sideslip = (lM.a_beta1 + lM.a_beta2*cG.kp_sideslip) / (2*tuningParams.Wn_sideslip)
    except:
        tuningParams.Zeta_sideslip = 0

    # pitch controller
    tuningParams.Wn_pitch = math.sqrt(abs(lM.a_theta2 + cG.kp_pitch*lM.a_theta3))
    try:
        tuningParams.Zeta_pitch = (lM.a_theta1 + cG.kd_pitch*lM.a_theta3) / (2*tuningParams.Wn_pitch)
    except:
        tuningParams.Zeta_pitch = 0

    # altitude hold using pitch 
    try:
        K_theta_DC = (cG.kp_pitch*lM.a_theta3) / (lM.a_theta2 + cG.kp_pitch*lM.a_theta3)
    except:
        K_theta_DC = 0
    tuningParams.Wn_altitude = math.sqrt(abs(K_theta_DC*lM.Va_trim*cG.ki_altitude))
    try:
        tuningParams.Zeta_altitude = (K_theta_DC*lM.Va_trim*cG.kp_altitude) / (2*tuningParams.Wn_altitude)
    except:
        tuningParams.Zeta_altitude = 0

    # airspeed hold using pitch
    tuningParams.Wn_SpeedfromElevator = math.sqrt(abs(-K_theta_DC*VPC.g0*cG.ki_SpeedfromElevator))
    try:
        tuningParams.Zeta_SpeedfromElevator = (lM.a_V1 - K_theta_DC*VPC.g0*cG.kp_SpeedfromElevator) / (2*tuningParams.Wn_SpeedfromElevator)
    except:
        tuningParams.Zeta_SpeedfromElevator = 0

    # airspeed hold using throttle
    tuningParams.Wn_SpeedfromThrottle = math.sqrt(abs(lM.a_V2*cG.ki_SpeedfromThrottle))
    try:
        tuningParams.Zeta_SpeedfromThrottle = (lM.a_V1 + lM.a_V2*cG.kp_SpeedfromThrottle) / (2*tuningParams.Wn_SpeedfromThrottle)
    except:
        tuningParams.Zeta_SpeedfromThrottle = 0

    return tuningParams