clc
clear all 
close all
%Conversions
default = 1;
knotsToMetersSeconds = 0.514444;
% Index corresponding to the packet frame
time = 1;
accX = 2;
accY = 3;
accZ = 4;
gyroX = 5;
gyroY = 6;
gyroZ = 7;
magX = 8;
magY = 9;
magZ = 10;
gpsLat = 13;
gpsLon = 14;
gpsSpd = 15;
gpsCog = 16;
temperature = 17;
pressure = 18;

dT = 0.01; % Enter dT in secs 
data = importdata('5_10_2023_C1.txt');

% Plot accX
PlotSensors(data, time, accX, dT)
xlabel('time (sec)')
ylabel('accX (g)')
title('Accelerometer X-axis')

% Plot accY
PlotSensors(data, time, accY, dT)
xlabel('time (sec)')
ylabel('accY (g)')
title('Accelerometer Y-axis')

% Plot accZ
PlotSensors(data, time, accZ, dT)
xlabel('time (sec)')
ylabel('accZ (g)')
title('Accelerometer Z-axis')

% Plot gyroX
PlotSensors(data, time, gyroX, dT)
xlabel('time (sec)')
ylabel('gyroX (deg/sec)')
title('Gyro X-axis')

%Plot gyroY
PlotSensors(data, time, gyroY, dT)
xlabel('time (sec)')
ylabel('gyroY (deg/sec)')
title('Gyro Y-axis')

%Plot gyroZ
PlotSensors(data, time, gyroZ, dT)
xlabel('time (sec)')
ylabel('gyroZ (deg/sec)')
title('Gyro Z-axis')

%Plot magX
PlotSensors(data, time, magX, dT)
xlabel('time (sec)')
ylabel('magX (uT)')
title('Magnetometer X-axis')

%Plot magY
PlotSensors(data, time, magY, dT)
xlabel('time (sec)')
ylabel('magY (uT)')
title('Magnetometer Y-axis')

%Plot magZ
PlotSensors(data, time, magZ, dT)
xlabel('time (sec)')
ylabel('magZ (uT)')
title('Magnetometer Z-axis')

%Plot gpsLat
PlotSensors(data, time, gpsLat, dT)
xlabel('time (sec)')
ylabel('gpsLat (degrees)')
title('GPS Latitude')

%Plot gpsLon
PlotSensors(data, time, gpsLon, dT)
xlabel('time (sec)')
ylabel('gpsLon (degrees)')
title('GPS Longtitude')
%Plot gpsSpd
PlotSensors(data, time, gpsSpd, dT)
xlabel('time (sec)')
ylabel('speed (m/s)')
title('GPS Speed Over Ground')

%Plot gpsCog
PlotSensors(data, time, gpsCog, dT)
xlabel('time (sec)')
ylabel('course (m/s)')
title('GPS Course Over Ground')
% Plot temperature
PlotSensors(data, time, temperature, dT)
xlabel('time (sec)')
ylabel('temperature (C^{\circ})')
title('Barometer Temperature')
%Plot pressure
PlotSensors(data, time, pressure, dT)
xlabel('time (sec)')
ylabel('pressure (hPa)')
title('Barometer Temperature')


