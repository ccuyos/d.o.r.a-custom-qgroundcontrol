/* 
 * File:   AttitudeEstimation.c
 * Author: Margaret Silva
 *
 * Created on May 14, 2023, 2:15 PM
 */

#include <math.h>
#include "AttitudeEstimation.h"
#include "MatrixMath.h"
#include "Lin_alg_float.h"

/*******************************************************************************
 * PRIVATE #DEFINES                                                            *
 ******************************************************************************/

// general physical constants
#define g0 9.81
#define MAGFIELD_VEC {22750.0, 5286.8, 41426.3} // update this for specific area

/*******************************************************************************
 * PRIVATE TYPEDEFS                                                            *
 ******************************************************************************/
struct estimation_biases {
    float b_p; // p bias
    float b_q; // q bias
    float b_r; // r bias
};

struct filter_gains {
    float Kp_acc; // Kp attitude from accels
    float Ki_acc; // Ki attitude from accels
    float Kp_mag; // Kp attitude from mag
    float Ki_mag; // Ki attitude from mag
};

struct IMU_state {
    float p; // body roll rate about body-x axis [rad/s]
    float q; // body pitch rate about body-y axis [rad/s]
    float r; // body yaw rate about body-z axis [rad/s]
    float R[3][3]; // inertial to body DCM rotation matrix
};

/*******************************************************************************
 * STATIC VARIABLES                                                            *
 ******************************************************************************/

static struct IMU_state current_state = {
    .p = 0,
    .q = 0,
    .r = 0,
    .R =
    {
        {1, 0, 0},
        {0, 1, 0},
        {0, 0, 1}
    }
};

static struct IMU_state ol_current_state = {
    .p = 0,
    .q = 0,
    .r = 0,
    .R =
    {
        {1, 0, 0},
        {0, 1, 0},
        {0, 0, 1}
    }
};

static struct estimation_biases current_biases = {
    .b_p = 0.0,
    .b_q = 0.0,
    .b_r = 0.0
};

static struct filter_gains gains = {
    .Kp_acc = 2.0,
    .Ki_acc = 0.2,
    .Kp_mag = 0.8,
    .Ki_mag = 0.08
};


/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATION                                              *
 ******************************************************************************/

// State Estimation Main Functions

/**
 * @Function initEstimation()
 * @param none
 * @return none
 * @brief  inits all attitude estimation values to default
 * @author Margaret Silva */
void initEstimation(){
    int i, j;
    float I[3][3] = {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    };
    
    // reset estimated state from open loop
    ol_current_state.p = 0;
    ol_current_state.q = 0;
    ol_current_state.r = 0;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            ol_current_state.R[i][j] = I[i][j];
        }
    }
    
    // reset estimated state from closed loop
    current_state.p = 0;
    current_state.q = 0;
    current_state.r = 0;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            current_state.R[i][j] = I[i][j];
        }
    }
    
    // reset estimated biases
    current_biases.b_p = 0;
    current_biases.b_q = 0;
    current_biases.b_r = 0;
}

/**
 * @Function setEstimationGains()
 * @param Kp_acc, Ki_acc - Kp and Ki gains for accelerometer values
 * @param Kp_mag, Ki_mag - Kp and Ki gains for magnetometer values
 * @return none
 * @brief  sets gains of estimation filters
 * @author Margaret Silva */
void setEstimationGains(float Kp_acc, float Ki_acc, float Kp_mag, float Ki_mag){
    gains.Kp_acc = Kp_acc;
    gains.Ki_acc = Ki_acc;
    gains.Kp_mag = Kp_mag;
    gains.Ki_mag = Ki_mag;
}

/**
 * @Function openLoopAttitudeEstimation()
 * @param IMU_data - data from the imu, including acc, gyro, and mag
 * @param dT - time between measurements, used for integration
 * @param yaw - pointer to yaw angle calculated by attitude estimation
 * @param pitch - pointer to pitch angle calculated by attitude estimation
 * @param roll - pointer to roll angle calculated by attitude estimation
 * @return none
 * @brief  estimates current attitude using gyros without feedback from accels or mag
 * @author Margaret Silva */
void openLoopAttitudeEstimation(struct IMU_out IMU_data, float dT, float * yaw, float * pitch, float * roll){
    // sensor data vector
    float gyros[3] = {IMU_data.gyro.x, IMU_data.gyro.y, IMU_data.gyro.z};
    float exp[3][3];
    float R_hat_plus[3][3];
    float I[3][3] = {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    };
    
    // calculate new values for R 
    R_exp(gyros, dT, exp);
    MatrixMultiply(exp, ol_current_state.R, R_hat_plus); // R_plus = Rexp(gyro_readings) * R_hat
    MatrixMultiply(R_hat_plus, I, ol_current_state.R); // write new matrix to stored one
    
    // record euler angles from dcm
    dcmToEuler(ol_current_state.R, yaw, pitch, roll);
}

/**
 * @Function closedLoopAttitudeEstimation()
 * @param IMU_data - data from the imu, including acc, gyro, and mag
 * @param dT - time between measurements, used for integration
 * @param yaw - pointer to yaw angle calculated by attitude estimation
 * @param pitch - pointer to pitch angle calculated by attitude estimation
 * @param roll - pointer to roll angle calculated by attitude estimation
 * @return none
 * @brief  using the current sensor vals, estimates current attitude
 * @author Margaret Silva */
void closedLoopAttitudeEstimation(struct IMU_out IMU_data, float dT, float * yaw, float * pitch, float * roll) {
     // init all vars used
    int i;
    float b_hat[3] = {current_biases.b_p, current_biases.b_q, current_biases.b_r};
    // outputs
    float b_plus[3]; // updated biases for gyros
    float gyros_biased[3]; // biased gyro values, used for euler angle estimation
    float R_hat_plus[3][3]; // new dcm matrix
    // intermediate values
    float norm = 0.0; // magnitude of a vector
    float temp[3];
    float temp_mat[3][3];
    float a[3]; // gyro feedback term
    float b[3]; // gyro feedback term
    float c[3]; // bias term
    float d[3]; // bias term
    float gyro_feedback[3]; // feedback term using biased gyro values used to find Rplus
    float b_dot[3]; // change in bias
    float acc_mag; // magnitude of accelerometer 
    // errors from accels and mags
    float w_meas_a[3];
    float w_meas_m[3];
    // sensor data vectors
    float gyros[3] = {IMU_data.gyro.x, IMU_data.gyro.y, IMU_data.gyro.z};
    float acc_b[3] = {IMU_data.acc.x, IMU_data.acc.y, IMU_data.acc.z};
    float mag_b[3] = {IMU_data.mag.x, IMU_data.mag.y, IMU_data.mag.z};
    // intertial frame sensor readings
    float acc_i[3] = {0, 0, -g0};
    float mag_i[3] = MAGFIELD_VEC; // get this for specific area
    // identity matrix
    float I[3][3] = {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    };

    // normalize the following vectors
    norm = lin_alg_v_norm(acc_b); // body accelerometer
    acc_mag = norm;
    if (fabs(norm) > 0.0001) {
        for (i = 0; i < 3; i++) {
            acc_b[i] = acc_b[i] / norm;
        }
    }
    norm = lin_alg_v_norm(mag_b); // body magnetometer
    if (fabs(norm) > 0.0001) {
        for (i = 0; i < 3; i++) {
            mag_b[i] = mag_b[i] / norm;
        }
    }
    norm = lin_alg_v_norm(acc_i); // inertial accelerometer
    if (fabs(norm) > 0.0001) {
        for (i = 0; i < 3; i++) {
            acc_i[i] = acc_i[i] / norm;
        }
    }
    norm = lin_alg_v_norm(mag_i); // inertial magnetometer
    if (fabs(norm) > 0.0001) {
        for (i = 0; i < 3; i++) {
            mag_i[i] = mag_i[i] / norm;
        }
    }

    // bias gyros
    lin_alg_v_v_sub(gyros, b_hat, gyros_biased);

    // make estimate using accels and magnetometer
    lin_alg_m_v_mult(current_state.R, acc_i, temp); // transform from interial->body
    lin_alg_cross(acc_b, temp, w_meas_a); // get cross product to find error
    lin_alg_m_v_mult(current_state.R, mag_i, temp);
    lin_alg_cross(mag_b, temp, w_meas_m);

    // gyro feedback terms
    lin_alg_s_v_mult(gains.Kp_acc, w_meas_a, a); // from accelerometer
    lin_alg_s_v_mult(gains.Kp_mag, w_meas_m, b); // from magnetometer

    // bias terms
    lin_alg_s_v_mult(-gains.Ki_acc, w_meas_a, c); // from accelerometer
    lin_alg_s_v_mult(-gains.Ki_mag, w_meas_m, d); // from magnetometer

    // ensure that accelerometer readings aren't thrown off by high acceleration
    if ((acc_mag < (1.1)) && (acc_mag > (0.9))) {
        lin_alg_v_v_add(a, b, temp);
        lin_alg_v_v_add(gyros_biased, temp, gyro_feedback); // gyro_feedback = gyros_biased + a + b
        lin_alg_v_v_add(c, d, b_dot); // b_dot = c + d
    } else {
        lin_alg_v_v_add(gyros_biased, b, gyro_feedback); // gyro_feedback = gyros_biased + b
        lin_alg_set_v(d[0], d[1], d[2], b_dot); // b_dot = d
    }

    // calculate new values for R 
    R_exp(gyro_feedback, dT, temp_mat);
    lin_alg_m_m_mult(temp_mat, current_state.R, R_hat_plus); // R_plus = Rexp(gyro_feedback) * R_hat
    // calculate new values for bias
    lin_alg_s_v_mult(dT, b_dot, temp);
    lin_alg_v_v_add(b_hat, temp, b_plus); // b_plus = b_hat + dT*b_dot

    // add outputs to current state and biases
    current_state.p = gyros_biased[0];
    current_state.q = gyros_biased[1];
    current_state.r = gyros_biased[2];
    current_biases.b_p = b_plus[0];
    current_biases.b_q = b_plus[1];
    current_biases.b_r = b_plus[2];
    lin_alg_m_m_mult(I, R_hat_plus, current_state.R);
    // record euler angles from dcm
    dcmToEuler(current_state.R, yaw, pitch, roll);
}


// Helper Conversion Functions

/**
 * @Function dcmToEuler(float dcm[3][3], float * yaw, float * pitch, float * roll)
 * @param dcm - DCM matrix from which to extract Euler angles
 *        yaw - yaw angle (degrees)
 *        pitch - pitch angle (degrees)
 *        roll - roll angle (degrees)
 * @return none
 * @brief  Extracts Euler angles from DCM matrix, and writes them to pointer locations
 *         passed in as arguments
 * @author Margaret Silva */
void dcmToEuler(float dcm[3][3], float * yaw, float * pitch, float * roll) {
    if (dcm[0][2] < -1.0) {
        dcm[0][2] = -1.0;
    } else if (dcm[0][2] > 1.0) {
        dcm[0][2] = 1.0;
    }

    *pitch = RAD_TO_DEG(-1.0 * asin(dcm[0][2]));

    *roll = RAD_TO_DEG(atan2(dcm[1][2], dcm[2][2]));

    *yaw = RAD_TO_DEG(atan2(dcm[0][1], dcm[0][0]));
}

/**
 * @Function R_exp()
 * @param w[3] - vector of rotation rates
 *        dT - integration time constants
 *        exp_mat - exponential matrix
 * @return none
 * @brief  helper function which calculates the matrix exponential exp(-[wx]dT)
 * @author Margaret Silva */
void R_exp(float w[3], float dT, float exp_mat[3][3]) {
    // define variables
    float wx[3][3] = {
        {0.0, -w[2], w[1]},
        {w[2], 0.0, -w[0]},
        {-w[1], w[0], 0.0}
    };
    float I[3][3] = {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {0.0, 0.0, 1.0}
    };
    float w_mag = VectorNorm(w);
    float matA[3][3];
    float matB[3][3];
    float temp[3][3];
    float a;
    float b;

    // calculate coefficients, use an approximation if m_mag is small
    if (w_mag < 0.2) {
        a = dT - ((pow(dT, 3) * pow(w_mag, 2)) / 6.0) + ((pow(dT, 5) * pow(w_mag, 4)) / 120.0);
        b = (pow(dT, 2) / 2.0) - ((pow(dT, 4) * pow(w_mag, 2)) / 24.0) + ((pow(dT, 6) * pow(w_mag, 4)) / 720.0);
    } else {
        a = (sin(w_mag * dT) / w_mag);
        b = (1.0 - cos(w_mag * dT)) / pow(w_mag, 2);
    }

    // multiply [wx] by the coefficients
    MatrixScalarMultiply(a, wx, matA);
    MatrixMultiply(wx, wx, temp);
    MatrixScalarMultiply(b, temp, matB);

    // combine matrices (I - a*[wx]) + b*[wx]**2
    MatrixSub(I, matA, temp);
    MatrixAdd(temp, matB, exp_mat);
}
