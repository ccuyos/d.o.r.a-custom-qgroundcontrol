/* 
 * File:   PID.c
 * Author: Aaron Hunter
 * Brief: PID controller module
 * Created on 8/15/2022 3:40 pm
 * Modified 
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

#include "PID.h" // The header file for this source file. 
#include <stdio.h>

/*******************************************************************************
 * PRIVATE #DEFINES                                                            *
 ******************************************************************************/
#define CUR_ERROR 0
#define PREV_ERROR 1
#define INTEGRAL_ERROR 2

/*******************************************************************************
 * PRIVATE TYPEDEFS                                                            *
 ******************************************************************************/

/*******************************************************************************
 * PRIVATE FUNCTIONS PROTOTYPES                                                 *
 ******************************************************************************/

/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATIONS                                             *
 ******************************************************************************/

/**
 * @Function PID_init(*pid);
 * @param *pid, pointer to PID_controller type
 * @brief initializes the PID_controller struct
 * @note computes the c0, c1, c2 constants of the controller and initializes
 * error array
 * @author Aaron Huter,
 * @modified Margaret Silva 2/27/2023 - modified to match 163 architecture*/
void PID_init(PID_controller *pid) {
    int i;
    pid->u = 0;
    pid->u_calc = 0;
    /* initialize error*/
    for (i = 0; i < 3; i++) {
        pid->error[i] = 0;
    }
}

/**
 * @Function PID_update(PID_controller *pid, float reference, float measurement, float dot)
 * @param *pid, pointer to PID_controller type
 * @param, reference, the current process setpoint
 * @param measurmeent, the current process measurement
 * @param dot, the derivative of the current process measurement
 * @brief implements a standard parallel PID
 * @note derivative filtering is not implemented
 * @author Aaron Hunter,
 * @modified Margaret Silva 2/27/2023 - modified to match 163 architecture*/
void PID_update(PID_controller *pid, float reference, float measurement, float dot){
    // record current and previous error
    pid->error[PREV_ERROR] = pid->error[CUR_ERROR];
    pid->error[CUR_ERROR] = reference - measurement;
    // take integral of error
    pid->error[INTEGRAL_ERROR] += (pid->dt * 0.5)*(pid->error[CUR_ERROR] + pid->error[PREV_ERROR]);
    /* compute new output */
    pid->u_calc = pid->trim + pid->kp * pid->error[CUR_ERROR] + pid->ki * pid->error[INTEGRAL_ERROR] - pid->kd * dot;
    /* clamp outputs within actuator limits*/
    if (pid->u_calc > pid->u_max) {
        pid->u = pid->u_max;
        pid->error[INTEGRAL_ERROR] -= (pid->dt * 0.5)*(pid->error[CUR_ERROR] + pid->error[PREV_ERROR]); // de-accumulate
    } else if (pid->u_calc < pid->u_min) {
        pid->u = pid->u_min;
        pid->error[INTEGRAL_ERROR] -= (pid->dt * 0.5)*(pid->error[CUR_ERROR] + pid->error[PREV_ERROR]);
    } else {
        pid->u = pid->u_calc;
    }
}

/**
 * @Function PID_reset_integrator(PID_controller *pid)
 * @param *pid, pointer to PID_controller type
 * @brief resets the integrator of this pid controller to 0
 * @author Margaret Silva
 * @modified  3/1/23 */
void PID_reset_integrator(PID_controller *pid){
    pid->error[INTEGRAL_ERROR] = 0;
}

/*******************************************************************************
 * PRIVATE FUNCTION IMPLEMENTATIONS                                            *
 ******************************************************************************/


#ifdef PID_TESTING
#include "Board.h"
#include "SerialM32.h"

void main(void) {
    PID_controller controller;
    controller.dt = .02;
    controller.kp = 1;
    controller.ki = 0.5;
    controller.kd = 0.2;
    controller.u_max = 2000;
    controller.u_min = -2000;

    float ref;
    float y;



    Board_init();
    Serial_init();
    printf("PID test harness %s, %s\r\n", __DATE__, __TIME__);
    PID_init(&controller);
    printf("Controller initialized to: \r\n");
    printf("dt %f\r\n", controller.dt);
    printf("kp: %f\r\n", controller.kp);
    printf("ki: %f\r\n", controller.ki);
    printf("kd: %f\r\n", controller.kd);
    printf("max output: %f\r\n", controller.u_max);
    printf("min output: %f\r\n", controller.u_min);




}
#endif //PID_TESTING