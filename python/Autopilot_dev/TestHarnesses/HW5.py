'''
    Author: Margaret Silva

    Code related to homework 5
'''


import sys
sys.path.append("..") #python is horrible, no?
from ece163 . Utilities import MatrixMath as mm
from ece163.Constants import VehiclePhysicalConstants as VPC
import math
import matplotlib.pyplot as plt
import numpy as np
import random


def GaussMarkovProcess(vi, tau, sig, dT):
    ''' Calculates the value of v for the next timestep using the gauss markov process '''
    return np.exp(-dT/tau)*vi + random.gauss(0, sig)


# calculate characteristics of weathercock system
va = 10
a_1 = - ((VPC.rho*(va**2)*VPC.S*VPC.b)/(2*VPC.Jzz))*VPC.Cnr*(VPC.b/(va*2))
a_2 = ((VPC.rho*(va**2)*VPC.S*VPC.b)/(2*VPC.Jzz))*VPC.Cnbeta
b_2 = a_2
w_n =  math.sqrt(a_2)
z = a_1/(2*w_n)
DC = 1/(1 - 5*(VPC.CndeltaR/VPC.Cnbeta))
# print(a_1, a_2, b_2)
# print(VPC.CndeltaR/VPC.Cnbeta)

# state space matrices
A = [[ - a_1 , - a_2 ] ,[1 , 0]]
B = [[1] , [0]]
C = [[0 , b_2 ]]

# constants
dT = 0.001
T_tot = 16
n_steps = int ( T_tot / dT )
tau = 400
sig = 0.0013

# fill in data arrays
t_data = [i* dT for i in range ( n_steps )]
yaw_data = [0 for i in range ( n_steps )]
rudder_data = [0 for i in range ( n_steps )]
wind_data = [ (0 if t < 1 else 10* math . pi /180) for t in t_data ] # step wind
# wind_data = [0 for i in range(n_steps)] # no wind

# tuning params for PI controller
Kp = -5
# Ki = -1.2
Ki = -0.5
acccumlator = 0
error_dl = 0
acc_max = 50
acc_min = -50
ideal_resp = [0 for i in range(n_steps)]
wc = 0

# controller with actual yaw input
x = [[0] ,[0]]
for i in range ( n_steps ):
    # record our data
    yaw_data [i ] = mm . multiply (C , x) [0][0]
    # find u(t):
    ideal_resp[i] = 0
    error = ideal_resp[i] - yaw_data[i]
    acccumlator +=  (dT/2)*(error + error_dl)
    if acccumlator > acc_max:
        acccumlator = acc_max
    elif acccumlator < acc_min:
        acccumlator = acc_min
    rudder_data[i] = Kp*error + Ki*acccumlator
    u = wind_data[i] +(VPC.CndeltaR/VPC.Cnbeta)*(rudder_data[i])    # yes feedback controller
    error_dl = error
    # calculate derivative :
    x_dot = mm . add (
     mm . multiply (A ,x) ,
    mm . scalarMultiply (u ,B))
    # forward euler update :
    x = mm . add (x ,mm . scalarMultiply ( dT , x_dot ) )
# record behavior of controller
actual_yaw_behavior = yaw_data[:]

# controller with estimated yaw input
yaw_data = [0 for i in range ( n_steps )]
est_yaw_data = [0 for i in range(n_steps)]
rudder_data = [0 for i in range ( n_steps )]
v = [0 for i in range(n_steps + 1)]
x = [[0] ,[0]]
for i in range ( n_steps ):
    # record our data
    yaw_data [i] = mm . multiply (C , x) [0][0]
    # find u(t):
    ideal_resp[i] = 0
    v[i+1] = GaussMarkovProcess(v[i], tau, sig, dT)
    est_yaw_data[i] = yaw_data[i] + v[i+1]
    error = ideal_resp[i] - est_yaw_data[i]
    acccumlator +=  (dT/2)*(error + error_dl)
    if acccumlator > acc_max:
        acccumlator = acc_max
    elif acccumlator < acc_min:
        acccumlator = acc_min
    rudder_data[i] = Kp*error + Ki*acccumlator
    u = wind_data[i] +(VPC.CndeltaR/VPC.Cnbeta)*(rudder_data[i])    # yes feedback controller
    error_dl = error
    # calculate derivative :
    x_dot = mm . add (
     mm . multiply (A ,x) ,
    mm . scalarMultiply (u ,B))
    # forward euler update :
    x = mm . add (x ,mm . scalarMultiply ( dT , x_dot ) )
est_yaw_behavior = yaw_data[:]
est_c1_yaw_data = est_yaw_data[:]

# # controller with estimated yaw input from gyro
yaw_data = [0 for i in range ( n_steps )]
gyro_data = [0 for i in range(n_steps)]
est_yaw_data = [0 for i in range(n_steps + 1)]
est_gyro_data = [0 for i in range(n_steps)]
rudder_data = [0 for i in range ( n_steps )]
# set up transfer functions to include derivative term
C2 = [[b_2 , 0 ]] # includes extra s term
v = [0 for i in range(n_steps + 1)]
x = [[0] ,[0]]
for i in range ( n_steps ):
    # record our data
    yaw_data [i] = mm . multiply (C , x) [0][0]
    gyro_data [i] = mm.multiply(C2, x)[0][0]
    # find u(t):
    ideal_resp[i] = 0
    v[i+1] = GaussMarkovProcess(v[i], tau, sig, dT)
    est_gyro_data[i] = gyro_data[i] + v[i+1]
    # integrate gyro output to create yaw value
    est_yaw_data[i+1] = est_yaw_data[i] + est_gyro_data[i]
    error = ideal_resp[i] - est_yaw_data[i+1]
    acccumlator +=  (dT/2)*(error + error_dl)
    if acccumlator > acc_max:
        acccumlator = acc_max
    elif acccumlator < acc_min:
        acccumlator = acc_min
    rudder_data[i] = Kp*error + Ki*acccumlator
    u = wind_data[i] +(VPC.CndeltaR/VPC.Cnbeta)*(rudder_data[i])    # yes feedback controller
    error_dl = error
    # calculate derivative :
    x_dot = mm . add (
     mm . multiply (A ,x) ,
    mm . scalarMultiply (u ,B))
    # forward euler update :
    x = mm . add (x ,mm . scalarMultiply ( dT , x_dot ) )
est_d_yaw_behavior = yaw_data[:]
est_d_yaw_data = est_yaw_data[:]

# graph behavior of both controllers compared to wind angle and ideal response
plt . close (" all ")
fig , ax = plt . subplots ()
plt.title("Controller comparison")
ax . plot ( t_data , wind_data , label = " wind angle")
ax . plot ( t_data , ideal_resp , label = " ideal response")
ax . plot ( t_data , actual_yaw_behavior , label = "using actual yaw values")
ax . plot ( t_data , est_yaw_behavior , label = "using estimated yaw values")
ax . plot ( t_data , est_d_yaw_behavior , label = "using estimated gyro values")
ax . set_xlabel (" time (s)")
ax . set_ylabel (" angle ( rad )")
ax . legend ()
plt.show()

# graph behavior of yaw estimator controller actual and estimated yaw
plt . close (" all ")
fig , ax = plt . subplots ()
plt.title("Actual and estimated yaw from controller using estimated yaw")
ax . plot ( t_data , wind_data , label = " wind angle")
ax . plot ( t_data , ideal_resp , label = " ideal response")
ax . plot ( t_data , est_c1_yaw_data , label = "estimated yaw value")
ax . plot ( t_data , est_yaw_behavior , label = "actual yaw value")
ax . set_xlabel (" time (s)")
ax . set_ylabel (" angle ( rad )")
ax . legend ()
plt.show()

# graph behavior of yaw from gyro estimator controller actual and estimated yaw
plt . close (" all ")
fig , ax = plt . subplots ()
plt.title("Actual and estimated yaw from controller using estimated yaw from gryo")
ax . plot ( t_data , wind_data , label = " wind angle")
ax . plot ( t_data , ideal_resp , label = " ideal response")
ax . plot ( t_data , est_d_yaw_data[1:] , label = "estimated yaw value")
ax . plot ( t_data , est_d_yaw_behavior , label = "actual yaw value")
ax . set_xlabel (" time (s)")
ax . set_ylabel (" angle ( rad )")
ax . legend ()
plt.show()




