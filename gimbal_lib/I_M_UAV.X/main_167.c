/* 
 * File:   main_167.c
 * Author: ccuyos, maansilv
 * Brief: Main code for I.M.U Final Project
 * Created on May 27, 2023, 1:57 PM
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "BOARD.h"
#include "serial.h"
#include "timers.h"
#include "BNO055.h"
#include "IMU.h"
#include "AttitudeEstimation.h"
#include "RC_Servo.h"
#include <math.h>

/*******************************************************************************
 * #DEFINES                                                                    *
 ******************************************************************************/

//Credits to Max Dunne
#define DELAY(x)    {int wait; for (wait = 0; wait <= x; wait++) {asm("nop");}}
#define A_BIT       18300
#define A_LOT       183000

#define POWERPIN_LAT LATFbits.LATF1
#define POWERPIN_TRIS TRISFbits.TRISF1
#define PRINT_PERIOD 100
#define SENSOR_PERIOD 20
#define HEARTBEAT_PERIOD 1000
#define RESPONSE_PERIOD 100
#define CONTROL_PERIOD 20
#define SENSOR_DT 0.02


//Defines for gimbal
#define NUM_OF_ANGLES 3
#define GIMBAL_RATE_RAD 0.002 //radians
#define GIMBAL_RATE_DEG 0.25 //degrees
#define GIMBAL_YAW 0
#define GIMBAL_PITCH 1
#define GIMBAL_ROLL 2

// used to determine what data is being sent in an HIL actuator commands packet
#define HIL_SEND_GAINS_TO_SIM 1 // send received gains back to sim as ack
#define HIL_RECEIVE_STATE 2 // receive state data from sim
#define HIL_RECEIVE_COMMANDS 3 // receive commanded course and height
#define HIL_RECEIVE_SENSORS 4 // receive sensor data from sim
#define HIL_SEND_STATE_TO_SIM 5 // send estimated states to sim
#define HIL_ACK_COMMANDS 6 // acknowledge new commands
#define STATE_EST_RECEIVE_GAINS 7 // receive state estimation gains from sim
#define HIL_ACK_STATE_EST_GAINS 8 // acknowledge received state estimation gains
#define STABILITY_CONTROLLER_RECEIVE_GAINS 9 // receives stability controller gains from sim
#define HEARTBEAT_HIL_ACTUATOR_ERROR 255

#define ACTUATOR_PERIOD (1 * 1000)

// for head tracking mapping to reference commands
#define XC_DEFAULT 0.0
#define HC_DEFAULT 100.0
#define HEARTBEAT_DEFAULT 11

enum {
    ZERO_HEAD_TRACKING, CAPTURE_HEAD_TRACKING
};

/*******************************************************************************
 * VARIABLES                                                                   *
 ******************************************************************************/

/*Variables*/
struct IMU_out IMU_est;
struct FRT curr_time;
struct IMU IMU;
struct IMU_euler angleCL;
struct IMU_euler angleOL;

static float angles[NUM_OF_ANGLES] = {0, 0, 0};

/*******************************************************************************
 * FUNCTIONS                                                                   *
 ******************************************************************************/
/**
 * @function TransferIMU(void)
 * @param none
 * @returns none
 * @brief transfers the calibrated sensor values from the IMU library to 
 *        state estimation library
 * @author Carl Cuyos
 */
void TransferIMU(void) {
    //Transfer sensor values from sensor libraries to attitude estimation library

    /*Accelerometer*/
    IMU_est.acc.x = -IMU.acc_true.x;
    IMU_est.acc.y = -IMU.acc_true.y;
    IMU_est.acc.z = -IMU.acc_true.z;

    /*Gyroscope*/
    IMU_est.gyro.x = IMU.gyro_rad.x;
    IMU_est.gyro.y = IMU.gyro_rad.y;
    IMU_est.gyro.z = IMU.gyro_rad.z;

}

/*******************************************************************************
 * MAIN                                                                        *
 ******************************************************************************/

int main(void) {
    int cur_time = 0;
    int actuator_start_time = 0;
    int i;
    float set_angles[NUM_OF_ANGLES] = {0, 0, 0};


    //Initialize required modules
    BOARD_Init();
    SERIAL_Init();
    TIMERS_Init();
    initEstimation();
    RCServo_Init(SERVO_PWM_1);
    RCServo_Init(SERVO_PWM_2);
    RCServo_Init(SERVO_PWM_3);
    // printf("I.M.U main compiled on %s %s. \n", __TIME__, __DATE__);
    POWERPIN_LAT = 0;
    POWERPIN_TRIS = 0;
    POWERPIN_LAT = 1;
    DELAY(100);
    if (BNO055_Init() != TRUE) {
        printf("IMU Initialization failed\n");
        while (1);
    }

    //Set State Estimation Gains
    setEstimationGains(KP_ACC, KI_ACC, KP_MAG, KI_MAG);
    
    //Initialize all timers for different periods
    actuator_start_time = TIMERS_GetMilliSeconds();
   
    
    //Set yaw, pitch, roll to center angle for servo
    RCServo_SetPulseFromAngle(0, YAW_SERVO);
    RCServo_SetPulseFromAngle(0, PITCH_SERVO);
    RCServo_SetPulseFromAngle(0, ROLL_SERVO);

    while (1) {
        //Get current time in ms
        cur_time = TIMERS_GetMilliSeconds();

        //Get IMU values from the IMU library
        IMU = GetIMU();

        if ((cur_time - actuator_start_time) > CONTROL_PERIOD) {
            // gradually move towards desired angle
            for (i = 0; i < NUM_OF_ANGLES; i++) {
                if (angles[i] > set_angles[i]) {
                    set_angles[i] += GIMBAL_RATE_DEG; 
                } else if (angles[i] < set_angles[i]) {
                    set_angles[i] -= GIMBAL_RATE_DEG;
                }
            }
            // move RC servos
            RCServo_SetPulseFromAngle(set_angles[GIMBAL_YAW], YAW_SERVO);
            RCServo_SetPulseFromAngle(set_angles[GIMBAL_PITCH], PITCH_SERVO);
            RCServo_SetPulseFromAngle(set_angles[GIMBAL_ROLL], ROLL_SERVO);

            //Reset control period
            actuator_start_time = cur_time;
        }

        if (serviceReady(&curr_time.filterTick, SENSOR_PERIOD)) {
            //Process the IMU values for calibration
            ProcessIMU();
            //Transfer the IMU values for parsing
            TransferIMU();
            //Run closed loop attitude estimation
            closedLoopAttitudeEstimation(IMU_est, SENSOR_DT, &(angleCL.yaw), &(angleCL.pitch), &(angleCL.roll));

            //Set the closed loop angles to the gimbal angles
            angles[GIMBAL_YAW] = -angleCL.yaw;
            angles[GIMBAL_PITCH] = -angleCL.pitch;
            angles[GIMBAL_ROLL] = angleCL.roll;
        }
    }

    while (1);
    BOARD_End();

    while (1);
}

