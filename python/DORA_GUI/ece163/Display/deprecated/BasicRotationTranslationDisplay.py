import sys
# this is deprecated code no longer in use
sys.exit(0)
import baseDisplay
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import vehicleState
import numpy
import math
import VehiclePhysicalConstants as VPC

class chapter2Simple(baseDisplay.mainInterface):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Chapter 2 Sliders")

        self.vehicle = vehicleState.vehicleState()

        sliderBox = QHBoxLayout()

        self.mainLayout.addLayout(sliderBox)
        movementSliders = QVBoxLayout()
        rotationSliders = QVBoxLayout()
        sliderBox.addLayout(movementSliders)
        sliderBox.addLayout(rotationSliders)

        self.xSlider = QSlider(Qt.Horizontal)
        compression = QHBoxLayout()
        compression.addWidget(QLabel("North   "))
        compression.addWidget(self.xSlider)
        movementSliders.addLayout(compression)
        self.xSlider.setRange(-VPC.LINEARMAX, VPC.LINEARMAX)
        self.xSlider.valueChanged.connect(self.updateDisplay)

        self.ySlider = QSlider(Qt.Horizontal)
        compression = QHBoxLayout()
        compression.addWidget(QLabel("East     "))
        compression.addWidget(self.ySlider)
        movementSliders.addLayout(compression)
        self.ySlider.setRange(-VPC.LINEARMAX, VPC.LINEARMAX)
        self.ySlider.valueChanged.connect(self.updateDisplay)

        self.zSlider = QSlider(Qt.Horizontal)
        compression = QHBoxLayout()
        compression.addWidget(QLabel("Altitude"))
        compression.addWidget(self.zSlider)
        movementSliders.addLayout(compression)
        self.zSlider.setRange(-VPC.LINEARMAX, VPC.LINEARMAX)
        self.zSlider.valueChanged.connect(self.updateDisplay)

        self.yawSlider = QSlider(Qt.Horizontal)
        compression = QHBoxLayout()
        compression.addWidget(QLabel("Yaw  "))
        compression.addWidget(self.yawSlider)
        rotationSliders.addLayout(compression)
        self.yawSlider.setRange(-VPC.ROTATEMAX, VPC.ROTATEMAX)
        self.yawSlider.valueChanged.connect(self.updateDisplay)

        self.pitchSlider = QSlider(Qt.Horizontal)
        compression = QHBoxLayout()
        compression.addWidget(QLabel("Pitch"))
        compression.addWidget(self.pitchSlider)
        rotationSliders.addLayout(compression)
        self.pitchSlider.setRange(-VPC.ROTATEMAX, VPC.ROTATEMAX)
        self.pitchSlider.valueChanged.connect(self.updateDisplay)

        self.rollSlider = QSlider(Qt.Horizontal)
        compression = QHBoxLayout()
        compression.addWidget(QLabel("Roll  "))
        compression.addWidget(self.rollSlider)
        rotationSliders.addLayout(compression)
        self.rollSlider.setRange(-VPC.ROTATEMAX, VPC.ROTATEMAX)
        self.rollSlider.valueChanged.connect(self.updateDisplay)

        return

    def updateDisplay(self, newValue):
        # print('hi', newValue)
        self.vehicle.pn = float(self.xSlider.value())
        self.vehicle.pe = float(self.ySlider.value())
        self.vehicle.pd = -float(self.zSlider.value())  # up to down in NED coords
        self.vehicle.yaw = math.radians(float(self.yawSlider.value()))
        self.vehicle.pitch = math.radians(float(self.pitchSlider.value()))
        self.vehicle.roll = math.radians(float(self.rollSlider.value()))
        newVertices = numpy.array(self.vehicleDrawInstance.getNewPoints(self.vehicle.pn, self.vehicle.pe, self.vehicle.pd,
                                                                        self.vehicle.yaw, self.vehicle.pitch,
                                                                        self.vehicle.roll), numpy.float32)
        # print(newVertices)
        print("North: {: 7.3f}\tEast: {: 7.3f}\tAlt: {: 7.3f}\t:yaw: {: 7.3f}\tpitch: {: 7.3f}\troll: {: 7.3f}".format(
            self.vehicle.pn, self.vehicle.pe, -self.vehicle.pd,
            math.degrees(self.vehicle.yaw), math.degrees(self.vehicle.pitch), math.degrees(self.vehicle.roll)))
        self.updateVehiclePosition([self.vehicle.pn, self.vehicle.pe, self.vehicle.pd,
                                    self.vehicle.yaw, self.vehicle.pitch, self.vehicle.roll])

        return


sys._excepthook = sys.excepthook


def my_exception_hook(exctype, value, tracevalue):
    # Print the error and traceback
    import traceback
    with open("LastCrash.txt", 'w') as f:
        # f.write(repr(exctype))
        # f.write('\n')
        # f.write(repr(value))
        # f.write('\n')
        traceback.print_exception(exctype, value, tracevalue, file=f)
    # traceback.print_tb(tracevalue, file=f)
    print(exctype, value, tracevalue)
    # Call the normal Exception hook after
    sys._excepthook(exctype, value, tracevalue)
    sys.exit(0)


# Set the exception hook to our wrapping function
sys.excepthook = my_exception_hook

qtApp = QApplication(sys.argv)
ourWindow = chapter2Simple()
ourWindow.show()
qtApp.exec()
