"""This file is a test harness for the module ece163.Utilities.Rotations,
and for the method ece163.Modeling.VehicleGeometry.getNewPoints(). 

It is meant to be run from the Testharnesses directory of the repo with:

python ./TestHarnesses/testChapter2.py (from the root directory) -or-
python testChapter2.py (from inside the TestHarnesses directory)

at which point it will execute various tests on the Rotations module"""

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Utilities.MatrixMath as mm
import ece163.Utilities.Rotations as Rotations
import ece163.Modeling.VehicleGeometry as VG

"""math.isclose doesn't work well for comparing things near 0 unless we 
use an absolute tolerance, so we make our own isclose:"""
isclose = lambda  a,b : math.isclose(a, b, abs_tol= 1e-12)

def compareVectors(a, b):
	"""A quick tool to compare two vectors"""
	el_close = [isclose(a[i][0], b[i][0]) for i in range(3)]
	return all(el_close)

def compareAngleVectors(a, b):
	"""
	A quick tool to compare two vectors of angles
	Checks to see if the angles are within plus/mins 2pi radians of eachother
	"""
	el_close = [isclose(a[i], b[i]) or isclose(a[i] + 2*math.pi, b[i]) or isclose(a[i] - 2*math.pi, b[i]) for i in range(3)]
	return all(el_close)

def compareMatrices(A, B):
	"""Compare two matrices"""
	for i in range(len(A)):
		for j in range(len(A[0])):
			if not isclose(A[i][j], B[i][j]):
				return False
	return True

#of course, you should test your testing tools too:
assert(compareVectors([[0], [0], [-1]],[[1e-13], [0], [-1+1e-9]]))
assert(not compareVectors([[0], [0], [-1]],[[1e-11], [0], [-1]]))
assert(not compareVectors([[1e8], [0], [-1]],[[1e8+1], [0], [-1]]))



failed = []
passed = []
def evaluateTest(test_name, boolean):
	"""evaluateTest prints the output of a test and adds it to one of two 
	global lists, passed and failed, which can be printed later"""
	if boolean:
		print(f"   passed {test_name}")
		passed.append(test_name)
	else:
		print(f"   failed {test_name}")
		failed.append(test_name)
	return boolean


#%% Euler2dcm():
print("Beginning testing of Rotations.Euler2dcm()")

cur_test = "Euler2dcm basic"
R = Rotations.euler2DCM(0, 0, 0)
orig_vec = [[1],[0],[0]]
expected_vec = [[1],[0],[0]]
actual_vec = mm.multiply(R, orig_vec)
if not evaluateTest(cur_test, compareVectors(expected_vec, actual_vec) ):
	print(f"{expected_vec} != {actual_vec}")

cur_test = "Euler2dcm yaw test 1"
#we know that rotating [1,0,0] by 90 degrees about Z should produce [0,-1,0], so
R = Rotations.euler2DCM(90*math.pi/180, 0, 0)
orig_vec = [[1],[0],[0]]
expected_vec = [[0],[-1],[0]]
actual_vec = mm.multiply(R, orig_vec)
if not evaluateTest(cur_test, compareVectors(expected_vec, actual_vec) ):
	print(f"{expected_vec} != {actual_vec}")

cur_test = "Euler2dcm pitch test 1"
R = Rotations.euler2DCM(0, 90*math.pi/180, 0)
orig_vec = [[1],[0],[0]]
expected_vec = [[0],[0],[1]]
actual_vec = mm.multiply(R, orig_vec)
if not evaluateTest(cur_test, compareVectors(expected_vec, actual_vec) ):
	print(f"{expected_vec} != {actual_vec}")

cur_test = "Euler2dcm pitch test 2"
R = Rotations.euler2DCM(0, math.pi, 0)
orig_vec = [[1],[0],[0]]
expected_vec = [[-1],[0],[0]]
actual_vec = mm.multiply(R, orig_vec)
if not evaluateTest(cur_test, compareVectors(expected_vec, actual_vec) ):
	print(f"{expected_vec} != {actual_vec}")

cur_test = "Euler2dcm roll test 1"
R = Rotations.euler2DCM(0, 0, 90*math.pi/180)
orig_vec = [[1],[0],[0]]
expected_vec = [[1],[0],[0]]
actual_vec = mm.multiply(R, orig_vec)
if not evaluateTest(cur_test, compareVectors(expected_vec, actual_vec) ):
	print(f"{expected_vec} != {actual_vec}")

cur_test = "Euler2dcm complex test 1"
R = Rotations.euler2DCM(-(3/4)*math.pi, (1/3)*math.pi, (1/4)*math.pi)
orig_vec = [[1],[0],[0]]
expected_vec = [[-0.353553390593274],[0.066987298107781],[ -0.933012701892219]]
actual_vec = mm.multiply(R, orig_vec)
if not evaluateTest(cur_test, compareVectors(expected_vec, actual_vec) ):
	print(f"{expected_vec} != {actual_vec}")

cur_test = "Euler2dcm complex test 2"
R = Rotations.euler2DCM(-(3/2)*math.pi, (17/5)*math.pi, -(1/9)*math.pi)
orig_vec = [[1],[2],[3]]
expected_vec = [[2.235135560135564],[0.027938461360811],[ -3.000564691886223]]
actual_vec = mm.multiply(R, orig_vec)
if not evaluateTest(cur_test, compareVectors(expected_vec, actual_vec) ):
	print(f"{expected_vec} != {actual_vec}")


#%%  

"""
Students, add more tests here.  
You aren't required to use the testing framework we've started here, 
but it will work just fine.
"""

#%% dcm2Euler():

cur_test = "dcm2Euler yaw test 1"
R = Rotations.euler2DCM(math.pi, 0, 0)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [math.pi, 0, 0]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler yaw test 2"
R = Rotations.euler2DCM(math.pi/2, 0, 0)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [math.pi/2, 0, 0]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler yaw test 3"
R = Rotations.euler2DCM(-(11*math.pi)/9, 0, 0)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [-(11*math.pi)/9, 0, 0]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler yaw test 4"
R = Rotations.euler2DCM(-(1/3)*math.pi, 0, 0)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [-(1/3)*math.pi, 0, 0]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler pitch test 1"
# this results in 2 sets of euler angles that result in the same transformation, ie rotation matrix
R = Rotations.euler2DCM(0, math.pi, 0)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [0, math.pi, 0]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler pitch test 2"
R = Rotations.euler2DCM(0, math.pi/2, 0)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [0, math.pi/2, 0]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler pitch test 3"
R = Rotations.euler2DCM(0, math.pi/4, 0)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [0, math.pi/4, 0]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler roll test 1"
# this results in 2 sets of euler angles that result in the same transformation, ie rotation matrix
R = Rotations.euler2DCM(0, 0, math.pi)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [0, 0, math.pi]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler roll test 2"
R = Rotations.euler2DCM(0, 0, math.pi/2)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [0, 0, math.pi/2]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler roll test 3"
R = Rotations.euler2DCM(0, 0, -math.pi/4)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [0, 0, -math.pi/4]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler complex test 1"
# see if complicated dcm matrices return correct angles
R = Rotations.euler2DCM(-(3/4)*math.pi, (1/3)*math.pi, (1/4)*math.pi)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [-(3/4)*math.pi, (1/3)*math.pi, (1/4)*math.pi]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler complex test 2"
# see if complicated dcm matrices return correct angles
R = Rotations.euler2DCM(-(1/3)*math.pi, (4/5)*math.pi, -(1/9)*math.pi)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [-(1/3)*math.pi, (4/5)*math.pi, -(1/9)*math.pi]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)

cur_test = "dcm2Euler complex test 3"
# see if complicated dcm matrices return correct angles
R = Rotations.euler2DCM(-(1/3)*math.pi, (4/5)*math.pi, -(1/11)*math.pi)
actual_vec = Rotations.dcm2Euler(R)
expected_vec = [-(1/3)*math.pi, (4/5)*math.pi, -(1/11)*math.pi]
R1 = Rotations.euler2DCM(actual_vec[0], actual_vec[1], actual_vec[2])
if not evaluateTest(cur_test, compareMatrices(R, R1) ):
	print(f"{expected_vec} != {actual_vec}")
	mm.matrixPrint(R)
	print("---")
	mm.matrixPrint(R1)


#%% ned2enu():
cur_test = "ned2enu() simple test 1 - x"
ned_points = [[1, 0, 0]]
actual_enu_points = Rotations.ned2enu(ned_points)
expected_enu_points = [[0, 1, 0]]
if not evaluateTest(cur_test, compareMatrices(expected_enu_points, actual_enu_points) ):
	print(f"{expected_vec} != {actual_vec}")

cur_test = "ned2enu() simple test 2 - y"
ned_points = [[0,1,0]]
actual_enu_points = Rotations.ned2enu(ned_points)
expected_enu_points = [[1,0,0]]
if not evaluateTest(cur_test, compareMatrices(expected_enu_points, actual_enu_points) ):
	print(f"{expected_vec} != {actual_vec}")

cur_test = "ned2enu() simple test 3 - z"
ned_points = [[0,0,1]]
actual_enu_points = Rotations.ned2enu(ned_points)
expected_enu_points = [[0,0,-1]]
if not evaluateTest(cur_test, compareMatrices(expected_enu_points, actual_enu_points) ):
	print(f"{expected_vec} != {actual_vec}")

cur_test = "ned2enu() complicated test 1 - weird numbers"
ned_points = [[-47, 300, .72]]
actual_enu_points = Rotations.ned2enu(ned_points)
expected_enu_points = [[300,-47,-.72]]
if not evaluateTest(cur_test, compareMatrices(expected_enu_points, actual_enu_points) ):
	print(f"{expected_vec} != {actual_vec}")

cur_test = "ned2enu() complicated test 2 - multidimensional"
ned_points = [[.003, -10, 100283], [-47, 300, .72], [0,0,-1]]
actual_enu_points = Rotations.ned2enu(ned_points)
expected_enu_points = [[-10,.003,-100283], [300,-47,-.72], [0,0,1]]
if not evaluateTest(cur_test,  compareMatrices(actual_enu_points, expected_enu_points)):
	print(f"{actual_enu_points} != {expected_enu_points}")


#%% Print results:

total = len(passed) + len(failed)
print(f"\n---\nPassed {len(passed)}/{total} tests")
[print("   " + test) for test in passed]

if failed:
	print(f"Failed {len(failed)}/{total} tests:")
	[print("   " + test) for test in failed]



