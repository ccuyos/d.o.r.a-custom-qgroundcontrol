/*
 * File:   main.c
 * Author: Aaron Hunter
 * Brief:  Minimal application to test MAVLink communication with QGC 
 * project
 * Created on January 27, 2021 at 2:00 pm
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "xc.h"
#include "inc/Board.h"
#include "inc/SerialM32.h"
#include "inc/System_timer.h"
#include "inc/Radio_serial.h"
#include "inc/lib/common/mavlink.h"
#include "inc/NEO_M8N.h"
#include "inc/RC_RX.h"
#include "inc/RC_servo.h"
#include "inc/ICM_20948.h"
#include "inc/AS5047D.h"



/*******************************************************************************
 * #DEFINES                                                                    *
 ******************************************************************************/
#define HEARTBEAT_PERIOD 1000 //1 sec interval for hearbeat update
#define CONTROL_PERIOD 20 //Period for control loop in msec #20
#define CONTROL_FREQUENCY (1000/CONTROL_PERIOD) //frequency in Hz
#define BUFFER_SIZE 1024
#define RAW 1
#define SCALED 2
#define CTS_2_USEC 1
#define SAFE_HIGH 1790  // normal val 1809
#define SAFE_LOW 1000    // normal val 992

/*******************************************************************************
 * VARIABLES                                                                   *
 ******************************************************************************/
mavlink_system_t mavlink_system = {
    1, // System ID (1-255)
    MAV_COMP_ID_AUTOPILOT1 // Component ID (a MAV_COMPONENT value)
};

enum RC_channels {
    AIL,
    ELE,
    THR,
    RUD,
    HASH,
    SAFE,
    SWITCH_A,
    SWITCH_B,
    SWITCH_C,
    SWITCH_D,
    SWITCH_E
}; //map to the car controls from the RC receiver

enum mav_output_type {
    USB,
    RADIO
};

enum flight_mode {
    MANUAL, AUTONOMOUS
};

static enum flight_mode mode = MANUAL;

const uint16_t RC_raw_fs_scale = RC_RAW_TO_FS;

RCRX_channel_buffer RC_channels[CHANNELS];
static uint8_t pub_RC_servo = FALSE;
static uint8_t pub_RC_signals = TRUE;
static uint16_t ail_prev = 0;
static uint16_t ele_prev = 0;
static uint16_t thr_prev = 0;
static uint16_t rud_prev = 0;
static int safe = 69;
/*******************************************************************************
 * TYPEDEFS                                                                    *
 ******************************************************************************/


/*******************************************************************************
 * FUNCTION PROTOTYPES                                                         *
 ******************************************************************************/
/**
 * @function RC_channels_init(void)
 * @param none
 * @brief set all RC channels to RC_RX_MID_COUNTS
 * @author Aaron Hunter
 */
void RC_channels_init(void);
/**
 * @function check_RC_events(void)
 * @param none
 * @brief checks for RC messages and stores data in RC channel buffer
 * @author Aaron Hunter
 */
void check_RC_events();
/**
 * @function check_radio_events(void)
 * @param none
 * @brief looks for messages sent over the radio serial port to OSAVC, parses
 * them and provides responses, if needed
 * @note currently only pushing information to usb-serial port
 * @author Aaron Hunter
 */
void check_radio_events(void);

/**
 * @function publish_RC_signals_raw(void)
 * @param none
 * @brief scales raw RC signals
 * @author Aaron Hunter
 */
void publish_RC_signals_raw(void);

/**
 * @Function publish_heartbeat(void)
 * @param none
 * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
 * @author aaron hunter
 */
void publish_heartbeat(void);

/**
 * @Function publish_parameter(uint8_t param_id[16])
 * @param parameter ID
 * @brief invokes mavlink helper to send out stored parameter 
 * @author aaron hunter
 */
void publish_parameter(uint8_t param_id[16]);

/**
 * @Function calc_pw(uint16_t raw_counts)
 * @param raw counts from the radio transmitter (11 bit unsigned int)
 * @return pulse width in microseconds
 * @brief converts the RC input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author aahunter
 * @modified <Your Name>, <year>.<month>.<day> <hour> <pm/am> */
static uint16_t calc_pw(uint16_t raw_counts);

/**
 * @Function set_control_output(void)
 * @param none
 * @return none
 * @brief converts RC input signals to pulsewidth values and sets the actuators
 * (servos and ESCs) to those values
 * @author Aaron Hunter
 */
void set_control_output(void);

/*******************************************************************************
 * FUNCTIONS                                                                   *
 ******************************************************************************/

/**
 * @function RC_channels_init(void)
 * @param none
 * @brief set all RC channels to RC_RX_MID_COUNTS
 * @author Aaron Hunter
 */
void RC_channels_init(void) {
    uint8_t i;
    for (i = 0; i < CHANNELS; i++) {
        RC_channels[i] = RC_RX_MID_COUNTS;
    }
}

/**
 * @function check_RC_events(void)
 * @param none
 * @brief checks for RC messages and stores data in RC channel buffer
 * @author Aaron Hunter
 */
void check_RC_events() {
    if (RCRX_new_cmd_avail()) {
        RCRX_get_cmd(RC_channels);
    }
}

/**
 * @function check_radio_events(void)
 * @param none
 * @brief looks for messages sent over the radio serial port to OSAVC, parses
 * them and provides responses, if needed
 * @note currently only pushing information to usb-serial port
 * @author Aaron Hunter
 */
void check_radio_events(void) {
    uint8_t channel = MAVLINK_COMM_0;
    uint8_t msg_byte;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    mavlink_message_t msg_rx;
    mavlink_status_t msg_rx_status;

    //MAVLink command structs
    mavlink_heartbeat_t heartbeat;
    mavlink_command_long_t command_qgc;
    mavlink_param_request_read_t param_read;

    if (Radio_data_available()) {
        msg_byte = Radio_get_char();
        if (mavlink_parse_char(channel, msg_byte, &msg_rx, &msg_rx_status)) {
            switch (msg_rx.msgid) {
                case MAVLINK_MSG_ID_HEARTBEAT:
                    mavlink_msg_heartbeat_decode(&msg_rx, &heartbeat);
                    if (heartbeat.type)
                        printf("heartbeat received type(%d)\r\n", heartbeat.type);
                    break;
                case MAVLINK_MSG_ID_COMMAND_LONG:
                    mavlink_msg_command_long_decode(&msg_rx, &command_qgc);
                    //                    printf("Command ID %d received from Ground Control\r\n", command_qgc.command);
                    break;
                case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
                    mavlink_msg_param_request_read_decode(&msg_rx, &param_read);
                    //                    printf("Parameter request ID %s received from Ground Control\r\n", param_read.param_id);
                    publish_parameter(param_read.param_id);
                    break;
                default:
                    //                    printf("Received message with ID %d, sequence: %d from component %d of system %d\r\n",
                    //                            msg_rx.msgid, msg_rx.seq, msg_rx.compid, msg_rx.sysid);
                    break;
            }
        }
    }
}

///**
// * @function publish_RC_signals(void)
// * @param none
// * @brief scales raw RC signals into +/- 10000
// * @author Aaron Hunter
// */

void publish_RC_signals(void) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t RC_port = 0; //first 8 channels 
    int16_t scaled_channels[CHANNELS];
    uint8_t rssi = 255; //unknown--may be able to extract from receiver
    for (index = 0; index < CHANNELS; index++) {
        scaled_channels[index] = (RC_channels[index] - RC_RX_MID_COUNTS) * RC_raw_fs_scale;
    }
    mavlink_msg_rc_channels_scaled_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            Sys_timer_get_msec(),
            RC_port,
            scaled_channels[0],
            scaled_channels[1],
            scaled_channels[2],
            scaled_channels[3],
            scaled_channels[4],
            scaled_channels[5],
            scaled_channels[6],
            scaled_channels[7],
            rssi);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        Radio_put_char(msg_buffer[index]);
    }
}
//
///**
// * @function publish_RC_signals_raw(void)
// * @param none
// * @brief scales raw RC signals
// * @author Aaron Hunter
// */

void publish_RC_signals_raw(void) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t RC_port = 0; //first 8 channels 
    uint8_t rssi = 255; //unknown--may be able to extract from receiver
    mavlink_msg_rc_channels_raw_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            Sys_timer_get_msec(),
            RC_port,
            RC_channels[0],
            RC_channels[1],
            RC_channels[2],
            RC_channels[3],
            RC_channels[4],
            RC_channels[5],
            RC_channels[6],
            RC_channels[7],
            rssi);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        Radio_put_char(msg_buffer[index]);
    }
}

/**
 * @Function publish_heartbeat(void)
 * @param none
 * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
 * @author aaron hunter
 */
void publish_heartbeat(void) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t mode = MAV_MODE_FLAG_MANUAL_INPUT_ENABLED | MAV_MODE_FLAG_SAFETY_ARMED;
    uint32_t custom = 0;
    uint8_t state = MAV_STATE_STANDBY;
    mavlink_msg_heartbeat_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC,
            mode,
            custom,
            state);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        Radio_put_char(msg_buffer[index]);
    }
}

/**
 * @Function publish_parameter(uint8_t param_id[16])
 * @param parameter ID
 * @brief invokes mavlink helper to send out stored parameter 
 * @author aaron hunter
 */
void publish_parameter(uint8_t param_id[16]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    float param_value = 320.0; // value of the requested parameter
    uint8_t param_type = MAV_PARAM_TYPE_INT16; // onboard mavlink parameter type
    uint16_t param_count = 1; // total number of onboard parameters
    uint16_t param_index = 1; //index of this value
    mavlink_msg_param_value_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            param_id,
            param_value,
            param_type,
            param_count,
            param_index
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        Radio_put_char(msg_buffer[index]);
    }
}

/**
 * @Function calc_pw(uint16_t raw_counts)
 * @param raw counts from the radio transmitter (11 bit unsigned int)
 * @return pulse width in microseconds
 * @brief converts the RC input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author aahunter
 * @modified <Your Name>, <year>.<month>.<day> <hour> <pm/am> */
static uint16_t calc_pw(uint16_t raw_counts) {
    int16_t normalized_pulse; //converted to microseconds and centered at 0
    uint16_t pulse_width; //servo output in microseconds
    normalized_pulse = (raw_counts - RC_RX_MID_COUNTS) >> CTS_2_USEC;
    pulse_width = normalized_pulse + RC_SERVO_CENTER_PULSE;
    return pulse_width;
}

/**
 * @Function set_control_output(void)
 * @param none
 * @return none
 * @brief converts RC input signals to pulsewidth values and sets the actuators
 * (servos and ESCs) to those values
 * @author Aaron Hunter
 */
void set_control_output(void) {

    char message[BUFFER_SIZE];
    uint8_t msg_len = 0;
    int index;
    int hash, ail, ele, thr, rud;
    int hash_check;
    const int tol = 4;
    int INTOL;

    /* Check switch state*/
    /*if switch state == up*/
    /* Manual control enabled */
    /* if switch state == down*/
    /* Autonomous mode enabled*/
    /* get RC commanded values*/


    safe = RC_channels[SAFE]; //1807 when ON, 240 when OFF
    hash = RC_channels[HASH]; //1807 when ON, 240 when OFF
    ail = RC_channels[AIL];
    ele = RC_channels[ELE];
    thr = RC_channels[THR];
    rud = RC_channels[RUD];


    /*For validating if there's corruption*/
    //     int cond1 = ail != ail_prev;
    //    int cond2 = ele != ele_prev;
    //    int cond3 = thr != thr_prev;
    //    int cond4 = rud != rud_prev;
    //    
    //    if (cond3){
    //       printf("Hash Val: %d\n"
    //      "Ail Val: %d\n"
    //      "Ele val: %d\n"
    //      "Thr val: %d\n"
    //      "Rud val: %d\n", hash, ail, ele, thr, rud);
    //      ail_prev = ail;
    //      ele_prev = ele;
    //      thr_prev = thr;
    //      rud_prev = rud;
    //    }

    hash_check = (thr >> 2) + (ail >> 2) + (ele >> 2) + (rud >> 2);
    
    // corruption check
    if (abs(hash_check - hash) <= tol) {
        INTOL = TRUE;
        
        // check if mode switching occurs
        if (safe <= SAFE_LOW) {
            mode = MANUAL;
        } else if (safe >= SAFE_HIGH) {
            mode = AUTONOMOUS;
        }
        
        /* send commands to motor outputs*/
        if (mode == MANUAL) {
            RC_servo_set_pulse(calc_pw(ail), SERVO_PWM_1);
            RC_servo_set_pulse(calc_pw(ele), SERVO_PWM_2);
            RC_servo_set_pulse(calc_pw(thr), SERVO_PWM_3);
            RC_servo_set_pulse(calc_pw(rud), SERVO_PWM_4);
        } else{
            printf("I am autonomous! \r\n");
        }

    } else {
        INTOL = FALSE;
        msg_len = sprintf(message, "%d, %d, %d, %d, %d, %d, %d \r\n", thr, ail, ele, rud, hash, hash_check, INTOL);
        for (index = 0; index < msg_len; index++) {
            Radio_put_char(message[index]);
        }
    }

}




int main(void) {
    uint32_t cur_time = 0;
    uint32_t warmup_time = 250; //time in ms to allow subsystems to stabilize (IMU))
    uint32_t control_start_time = 0;
    uint32_t heartbeat_start_time = 0;
    uint8_t index;
    uint8_t error_report = 50;
    RCRX_channel_buffer channels[CHANNELS];

    //    //Initialization routines
    Board_init(); //board configuration
    Serial_init(); //start debug terminal (USB)
    Sys_timer_init(); //start the system timer
    /*small delay to get all the subsystems time to get online*/
    while (cur_time < warmup_time) {
        cur_time = Sys_timer_get_msec();
    }
    Radio_serial_init(); //start the radios

    RCRX_init(); //initialize the radio control system
    RC_channels_init(); //set channels to midpoint of RC system
    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_1); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_2); // start the servo subsystem
    RC_servo_init(ESC_UNIDIRECTIONAL_TYPE, SERVO_PWM_3); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_4); // start the servo subsystem

    printf("\r\nMinimal Mavlink application %s, %s \r\n", __DATE__, __TIME__);
    //printf("I am here\n");
    while (1) {
        //printf("I am here\n");
        cur_time = Sys_timer_get_msec();

        //check for all events
        check_radio_events(); //detect and process MAVLink incoming messages
        check_RC_events(); //check incoming RC commands

        //publish control and sensor signals
        if (cur_time - control_start_time >= CONTROL_PERIOD) {
            control_start_time = cur_time; //reset control loop timer
            set_control_output(); // set actuator outputs

            /*publish high speed sensors*/
            if (pub_RC_signals == TRUE) {
                //publish_RC_signals_raw();
                //printf("Channels: %d" );
            }
        }


        //publish heartbeat
        if (cur_time - heartbeat_start_time >= HEARTBEAT_PERIOD) {
            heartbeat_start_time = cur_time; //reset the timer
            //publish_heartbeat();
        }
//        int i;
//        unsigned char msgt[2048];
//        sprintf(msgt, "Hi brendan\r\n");
//        for(i = 0;i < strlen(msgt); i++ ){
//            Radio_put_char(msgt[i]);
//        }
    }
    return 0;
}