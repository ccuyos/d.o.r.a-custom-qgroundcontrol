/* 
 * File:   RC_Servo.h
 * Author: Margaret Silva
 * 
 * Library used to control pulse width. Needed to work with gimbal servos
 *
 * Created on May 28, 2023, 4:08 PM
 */

#include "RC_Servo.h"
#include "BOARD.h"
#include <sys/attribs.h>
#include <proc/p32mx340f512h.h>

// pin defines 
#define OC3_TRIS TRISDbits.TRISD2 // pin 6
#define OC3_LAT LATDbits.LATD2

// timer constants
#define T3_CYCLES_PER_50MS 62500
#define T3_CYCLES_PER_20MS 25000

// module level static vars
static unsigned int pulse_width[3] = {1875, 1875, 1875}; // width of output capture pulse, set to 1.5 ms

/*******************************************************************************
 * PUBLIC FUNCTIONS                                                           *
 ******************************************************************************/

/**
 * @Function RCServo_Init(uint8_t output_channel)
 * @param None
 * @return SUCCESS or ERROR
 * @brief initializes hardware required and set it to the CENTER PULSE */
int RCServo_Init(uint8_t output_channel) {
    // set up timer 3
    T3CON = 0; // turn off free running timer
    TMR3 = 0; // clear timer value
    T3CONbits.TCKPS = 0b101; // set the pre scale to 1:32, ie frequency is 1.25 MHz
    IFS0bits.T3IF = 0; // clear interrupt flag
    IEC0bits.T3IE = 1; // enable timer trigger interrupts
    IPC3bits.T3IP = 3; // set priority
    IPC3bits.T3IS = 1;
    PR3 = T3_CYCLES_PER_50MS; // set period match to the correct number of cycles

    switch (output_channel) {
        case SERVO_PWM_1:
            // set up output capture
            OC1CON = 0; // disable OC1 and clear control register
            OC1CONbits.OCTSEL = 1; // have timer 3 in use
            OC1RS = pulse_width[0]; // configure duty cycle
            OC1R = RC_SERVO_CENTER_PULSE; // set initial pulse width
            OC1CONbits.OCM = 0b110; // set to pulse width modulation mode
            //IEC0bits.OC3IE = 0; // disable output capture interrupts

            OC1CONbits.ON = 1; // enable output capture
            break;
        case SERVO_PWM_2:
            // set up output capture
            OC2CON = 0; // disable OC2 and clear control register
            OC2CONbits.OCTSEL = 1; // have timer 3 in use
            OC2RS = pulse_width[1]; // configure duty cycle
            OC2R = RC_SERVO_CENTER_PULSE; // set initial pulse width
            OC2CONbits.OCM = 0b110; // set to pulse width modulation mode
            //IEC0bits.OC3IE = 0; // disable output capture interrupts

            OC2CONbits.ON = 1; // enable output capture
            break;
        case SERVO_PWM_3:
            // set up output capture
            OC3CON = 0; // disable OC3 and clear control register
            OC3CONbits.OCTSEL = 1; // have timer 3 in use
            OC3RS = pulse_width[2]; // configure duty cycle
            OC3R = RC_SERVO_CENTER_PULSE; // set initial pulse width
            OC3CONbits.OCM = 0b110; // set to pulse width modulation mode
            //IEC0bits.OC3IE = 0; // disable output capture interrupts

            OC3CONbits.ON = 1; // enable output capture
            break;
        default:
            break;
    }
    T3CONbits.ON = 1; // turn on free running timer

    return SUCCESS;
}

/**
 * @Function int RCServo_SetPulse(unsigned int inPulse)
 * @param inPulse, integer representing number of microseconds
 * @return SUCCESS or ERROR
 * @brief takes in microsecond count, converts to ticks and updates the internal variables
 * @warning This will update the timing for the next pulse, not the current one */
int RCServo_SetPulse(unsigned int inPulse, uint8_t output_channel) {
    // adjust input if it falls outside range of safety
    if (inPulse < RC_SERVO_MIN_PULSE) {
        inPulse = RC_SERVO_MIN_PULSE;
    }
    if (inPulse > RC_SERVO_MAX_PULSE) {
        inPulse = RC_SERVO_MAX_PULSE;
    }
    pulse_width[output_channel] = (5 * inPulse) / 4; // conversion rate: 5 ticks/4 microsec
    switch (output_channel) {
        case SERVO_PWM_1:
            OC1RS = pulse_width[output_channel]; //load new PWM value into OCxRS
            break;
        case SERVO_PWM_2:
            OC2RS = pulse_width[output_channel]; //load new PWM value into OCxRS
            break;
        case SERVO_PWM_3:
            OC3RS = pulse_width[output_channel]; //load new PWM value into OCxRS
            break;
        default:
            break;
    }
    return SUCCESS;
}

/**
 * @Function int RCServo_SetPulseFromAngle(float angle, uint8_t output_channel)
 * @param angle - angle in degrees to set rc servo to
 *        output_channel - RC servo to output to
 * @return SUCCESS or ERROR
 * @brief takes in microsecond count, converts to ticks and updates the internal variables
 * @warning This will update the timing for the next pulse, not the current one */
int RCServo_SetPulseFromAngle(float angle, uint8_t output_channel) {
    unsigned int inPulse;

    // map angle to pulse, keeping in mind angle limits
    switch (output_channel) {
        case YAW_SERVO:
            angle = -angle;
            if (angle > YAW_MAX_ANGLE) {
                angle = YAW_MAX_ANGLE;
            } else if (angle < YAW_MIN_ANGLE) {
                angle = YAW_MIN_ANGLE;
            }
            inPulse = RCServo_MapDegreesToPulse(angle);
            break;
        case PITCH_SERVO:
            angle = -angle;
            if (angle > PITCH_MAX_ANGLE) {
                angle = PITCH_MAX_ANGLE;
            } else if (angle < PITCH_MIN_ANGLE) {
                angle = PITCH_MIN_ANGLE;
            }
            inPulse = RCServo_MapDegreesToPulse(angle);
            break;
        case ROLL_SERVO:
            angle -= 2;
            if (angle > ROLL_MAX_ANGLE) {
                angle = ROLL_MAX_ANGLE;
            } else if (angle < ROLL_MIN_ANGLE) {
                angle = ROLL_MIN_ANGLE;
            }
            inPulse = RCServo_MapDegreesToPulse(angle);
            break;
        default:
            break;
    }
    
    // set RC servo pulse using mapped pulse from angle
    return RCServo_SetPulse(inPulse, output_channel);

}

/**
 * @Function int RCServo_GetPulse(void)
 * @param None
 * @return Pulse in microseconds currently set */
unsigned int RCServo_GetPulse(uint8_t output_channel) {
    return (4 * pulse_width[output_channel]) / 5; // conversion rate: 4 microsec/5 ticks
}

/**
 * @Function int RCServo_GetRawTicks(void)
 * @param None
 * @return raw timer ticks required to generate current pulse. */
unsigned int RCServo_GetRawTicks(uint8_t output_channel) {
    return pulse_width[output_channel];
}

/**
 * @Function unsigned int RCServo_MapDegreesToPulse(int angle)
 * @param angle - angle in degrees to set rc servo to
 * @return required pulse width */
unsigned int RCServo_MapDegreesToPulse(float angle) {
    return (unsigned int) (angle * 10.516 + RC_SERVO_CENTER_PULSE);
}

/*******************************************************************************
 * ISRS                                                       *
 ******************************************************************************/

/*
void __ISR(_OUTPUT_COMPARE_3_VECTOR) __OC3Interrupt(void)
{
    // set pulse width to current pulse width
    OC3RS = pulse_width;

    // clear interrupt flag
    IFS0bits.OC3IF = 0;
}
 */

void __ISR(_TIMER_3_VECTOR, ipl3auto) Timer3IntHandler(void) {
    // set pulse width to current pulse width
    OC1RS = pulse_width[0];
    OC2RS = pulse_width[1];
    OC3RS = pulse_width[2];

    // clear timer interrupt
    IFS0bits.T3IF = 0;
}

/*******************************************************************************
 * TEST HARNESSES                                                       *
 ******************************************************************************/

//#define RC_SERVO_TEST_HARNESS
#ifdef RC_SERVO_TEST_HARNESS
#include <stdio.h>
#include <stdlib.h>

int main() {
    BOARD_Init();
    Protocol_Init();
    LEDS_INIT();
    RCServo_Init();

    // send current date and time
    unsigned char date_time_str[100];
    sprintf(date_time_str, "RC Servo command response test harness: %s %s", __DATE__, __TIME__);
    Protocol_SendDebugMessage(date_time_str);

    // wait for ping message and prepare to respond to it
    unsigned int servo_command, prev_pulse_width;
    unsigned char trash_this_payload[128];
    while (1) {
        if (Protocol_IsMessageAvailable() == TRUE) {
            if (Protocol_ReadNextID() == ID_COMMAND_SERVO_PULSE) {
                Protocol_GetPayload(&servo_command);
                // swap endedness of servo command
                servo_command = Protocol_IntEndednessConversion(servo_command);
                prev_pulse_width = pulse_width; // store previous pulse width
                RCServo_SetPulse(servo_command); // set new pulse width
                if (prev_pulse_width != pulse_width) {
                    // if the pulse width has been changed, send it to the console
                    prev_pulse_width = Protocol_IntEndednessConversion(RCServo_GetPulse());
                    Protocol_SendMessage(4, ID_SERVO_RESPONSE, &prev_pulse_width);
                }
            } else {
                // throw out the message
                Protocol_GetPayload(trash_this_payload);
            }
        }
    }

    while (1);
}

#endif
