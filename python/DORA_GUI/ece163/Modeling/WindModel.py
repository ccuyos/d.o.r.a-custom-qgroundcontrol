"""
WindModel.py implements the Dryden gust model and allows for an update of the wind at every time step when driven by white
noise. The constants for the wind model are embedded in the init function, and an init flag tells the __init__ function which
set of paramters to grab.

All of the constants that are used in the Dryden Wind Gust Models. These are detailed on Beard Chapter 4.4. The Dryden
models are typically implemented assuming a constant nominal airspeed (taken as initial speed from physical constants).
The parameters for the Dryden gust model are defined in MIL-F-8785C. See ECE163_DrydenWindModel writeup for details.
"""

import math
import random
from ..Containers import States
from ..Utilities import MatrixMath
from ..Constants import VehiclePhysicalConstants as VPC


class WindModel():
    def __init__(self, dT=VPC.dT, Va=VPC.InitialSpeed, drydenParamters=VPC.DrydenNoWind):
        """__init__(self, dT=VPC.dT, Va=VPC.InitialSpeed, drydenParamters=VPC.DrydenNoWind)
        function to initialize the wind model code. Will load the appropriate constants that parameterize the wind gusts
        from the Dryden gust model. Creates the discrete transfer functions for the gust models that are used to update
        the local wind gusts in the wind frame. These are added to the inertial wind (Wn, We, Wd) that are simply constants.
        Discrete models are held in self and used in the Update function.

        To turn off gusts completely, use the DrydenNoGusts parameters.

        :param dT: time step [s] for numerical integration of wind
        :param Va: nominal flight speed [m/s]
        :param drydenParamters: class from Inputs
        :return: none
        """

        self.Wind = States.windState()
        self.x_u = [[0.0]]
        self.x_v = [[0.0], [0.0]]
        self.x_w = [[0.0], [0.0]]

        self.Phi_u = [[1.0]]
        self.Gamma_u = [[0.0]]
        self.H_u = [[1.0]]

        self.Phi_v = [[1.0, 0.0], [0.0, 1.0]]
        self.Gamma_v = [[0.0], [0.0]]
        self.H_v = [[1.0, 1.0]]

        self.Phi_w = [[1.0, 0.0], [0.0, 1.0]]
        self.Gamma_w = [[0.0], [0.0]]
        self.H_w = [[1.0, 1.0]]
        
        self.dT = dT

        self.CreateDrydenTransferFns(dT, Va, drydenParamters)
        return

#     def getWindState(self):
#         """
#         Wrapper function to return wind state from module

#         :return: wind state class
#         """
#         return self.windModel.Wind

    def setWindModelParameters(self, Wn=0.0, We=0.0, Wd=0.0, drydenParamters=VPC.DrydenNoWind):
        """setWindModelParameters(self, Wn=0.0, We=0.0, Wd=0.0, drydenParamters=VPC.DrydenNoWind)
        Wrapper function that will inject parameters into the wind model.
        This class models wind according to the Dryden Wind Model, where winds are 
        a static component plus stocahstically derived gusts.

        :param Wn: Steady wind in inertial North direction [m/s]
        :param We: Steady wind in inertial East direction [m/s]
        :param Wd: Steady wind in inertial Down direction [m/s], should usually be zero
        :param drydenParamters: model of Dryden parameters taken from constants
        :return: none
        """
        self.Wind.Wn = Wn
        self.Wind.We = We
        self.Wind.Wd = Wd
        self.CreateDrydenTransferFns(self.dT, VPC.InitialSpeed, drydenParamters)
#         return

    def reset(self):
        """
        Wrapper function that resets the wind model code (but does not reset the model parameters). This will
        reset both steady and gust winds to zero, and re-set the internal states of the stochastic Dyden wind
        model to zero. To change the model transfer functions you need to use CreateDrydenTranferFns().

        :return: none
        """
        zeroWind = States.windState()
        self.Wind = zeroWind
        self.x_u = [[0.0]]
        self.x_v = [[0.0], [0.0]]
        self.x_w = [[0.0], [0.0]]
        return

    def setWind(self, windState):
        """
        Wrapper function that allows for injecting constant wind and gust values into the class
        :param windState: class from vehicleStates with inertial constant wind and wind frame gusts

        :return: none
        """
        self.Wind = windState
        return

    def getWind(self):
        """
        Wrapper function to return the wind state from the module

        :return: windState class
        """
        return self.Wind

    def CreateDrydenTransferFns(self, dT, Va, drydenParamters):
        """
        Function creates the Dryden transfer functions in discrete form. These are used in generating the gust models for
        wind gusts (in wind frame). If the input is DrydenNoWind, then set \Phi_[u,v,w] to Identity, \Gamma_{u,v,w] to zero,
        and H_[u,v,w] to a column of ones. If input Va is less than or 0, raise an arithmetic error since your wind models are 
        undefined for zero or negative airspeeds.

        :param dT: time step [s]
        :param Va: nominal flight speed [m/s]
        :param drydenParamters: Dryden Wing Gust Model from VehiclePhysicalConstants
        :return: none, gust models are created internally
        """
        if drydenParamters == VPC.DrydenNoWind:
            self.Phi_u = [[1.0]]
            self.Gamma_u = [[0.0]]
            self.H_u = [[1.0]]

            self.Phi_v = [[1.0, 0.0], [0.0, 1.0]]
            self.Gamma_v = [[0.0], [0.0]]
            self.H_v = [[1.0, 1.0]]

            self.Phi_w = [[1.0, 0.0], [0.0, 1.0]]
            self.Gamma_w = [[0.0], [0.0]]
            self.H_w = [[1.0, 1.0]]
            return
        if (Va <= 0.0):
            raise ArithmeticError('Transfer Functions for Wind Gusts undefined for Va = 0.0')

        expVadt_Lu = math.exp(-Va * dT / drydenParamters.Lu)
        self.Phi_u = [[expVadt_Lu]]
        self.Gamma_u = [[(drydenParamters.Lu / Va) * (1 - expVadt_Lu)]]
        self.H_u = [[drydenParamters.sigmau * math.sqrt(2 * Va /(math.pi * drydenParamters.Lu))]]

        Vadt_Lv = Va * dT / drydenParamters.Lv
        Phi_v = [[1 - Vadt_Lv, -(Va / drydenParamters.Lv) ** 2 * dT],
                 [dT, 1 + Vadt_Lv]]
        self.Phi_v = MatrixMath.scalarMultiply(math.exp(-Vadt_Lv), Phi_v)
        Gam_v = [[dT], [(drydenParamters.Lv / Va) ** 2 * (math.exp(Vadt_Lv) - 1) - (dT * drydenParamters.Lv / Va)]]
        self.Gamma_v = MatrixMath.scalarMultiply(math.exp(-Vadt_Lv), Gam_v)
        self.H_v = MatrixMath.scalarMultiply(drydenParamters.sigmav * math.sqrt(3 * Va /(math.pi * drydenParamters.Lv)),
                                                   [[1,Va / (math.sqrt(3) * drydenParamters.Lv)]])

        Vadt_Lw = Va * dT / drydenParamters.Lw
        Phi_w = [[1 - Vadt_Lw, -(Va / drydenParamters.Lw) ** 2 * dT],
                 [dT, 1 + Vadt_Lw]]
        self.Phi_w = MatrixMath.scalarMultiply(math.exp(-Vadt_Lw), Phi_w)
        Gam_w = [[dT], [(drydenParamters.Lw / Va) ** 2 * (math.exp(Vadt_Lw) - 1) - (dT * drydenParamters.Lw / Va)]]
        self.Gamma_w = MatrixMath.scalarMultiply(math.exp(-Vadt_Lw), Gam_w)
        self.H_w = MatrixMath.scalarMultiply(drydenParamters.sigmaw * math.sqrt(3 * Va /(math.pi * drydenParamters.Lw)),
                                                   [[1,Va / (math.sqrt(3) * drydenParamters.Lw)]])
        return

    def getDrydenTransferFns(self):
        """
        Wrapper function to return the internals of the Dryden Transfer function in order to be able to test the code without
        requiring consistent internal names. Returns the discretized version of the Drydem gust model as outlined in the
        ECE163_DrydenWindModel handout (Phi_u, Gamma_u, H_u, Phi_v, Gamma_v, H_v, Phi_w, Gamma_w, H_w).

        :param None:
        :return: Phi_u, Gamma_u, H_u, Phi_v, Gamma_v, H_v, Phi_w, Gamma_w, H_w
        """
        return self.Phi_u, self.Gamma_u, self.H_u, self.Phi_v, self.Gamma_v, self.H_v, self.Phi_w, self.Gamma_w, self.H_w

    def Update(self, uu=None, uv=None, uw=None):
        """
        def Update(self, uu=None, uv=None, uw=None):
        Function that updates the wind gusts and inserts them back into the .Wind portion of self. This is done by running
        white noise [Gaussian(0,1)] through the coloring filters of the Dryden Wind Gust model.

        :param uu: optional argument for injecting input to Hu(s), defaults to None
        :param uv: optional argument for injecting input to Hv(s), defaults to None
        :param uw: optional argument for injecting input to Hw(s), defaults to None
        :return: none, gust values are updated internally
        """
        if uu is None:
            uu = random.gauss(0, 1)
        if uv is None:
            uv = random.gauss(0, 1)
        if uw is None:
            uw = random.gauss(0, 1)
			
        xu_one = MatrixMath.multiply(self.Phi_u,self.x_u)
        xu_two = MatrixMath.multiply(self.Gamma_u,[[uu]])
        xuPlus = MatrixMath.add(xu_one,xu_two)
        gust_u = MatrixMath.multiply(self.H_u,xuPlus)
        self.x_u = xuPlus
        self.Wind.Wu = gust_u[0][0]

        xv_one = MatrixMath.multiply(self.Phi_v,self.x_v)
        xv_two = MatrixMath.multiply(self.Gamma_v,[[uv]])
        xvPlus = MatrixMath.add(xv_one,xv_two)
        gust_v = MatrixMath.multiply(self.H_v,xvPlus)
        self.x_v = xvPlus
        self.Wind.Wv = gust_v[0][0]

        xw_one = MatrixMath.multiply(self.Phi_w,self.x_w)
        xw_two = MatrixMath.multiply(self.Gamma_w,[[uw]])
        xwPlus = MatrixMath.add(xw_one,xw_two)
        gust_w = MatrixMath.multiply(self.H_w,xwPlus)
        self.x_w = xwPlus
        self.Wind.Ww = gust_w[0][0]
        return
