/* 
 * File:   main_capstone.c
 * Author: ccuyos, maansilv
 * Brief: Main code for I.M.U Final Project
 * Created on May 27, 2023, 1:57 PM
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "BOARD.h"
#include "serial.h"
#include "timers.h"
#include "BNO055.h"
#include "IMU.h"
#include "AttitudeEstimation.h"
#include "PublishMavlinkMsg.h"
#include "RC_Servo.h"
#include <math.h>

/*******************************************************************************
 * #DEFINES                                                                    *
 ******************************************************************************/

//Credits to Max Dunne
#define DELAY(x)    {int wait; for (wait = 0; wait <= x; wait++) {asm("nop");}}
#define A_BIT       18300
#define A_LOT       183000

//Defines for sensors
#define POWERPIN_LAT LATFbits.LATF1
#define POWERPIN_TRIS TRISFbits.TRISF1
#define PRINT_PERIOD 100
#define SENSOR_PERIOD 20
#define CONTROL_PERIOD 20
#define HEARTBEAT_PERIOD 1000
#define SENSOR_DT 0.02

//Defines for gimbal
#define NUM_OF_ANGLES 3
#define GIMBAL_RATE_RAD 0.002 //radians
#define GIMBAL_YAW 0
#define GIMBAL_PITCH 1
#define GIMBAL_ROLL 2

// used to determine what data is being sent in an HIL actuator commands packet
#define HIL_SEND_GAINS_TO_SIM 1 // send received gains back to sim as ack
#define HIL_RECEIVE_STATE 2 // receive state data from sim
#define HIL_RECEIVE_COMMANDS 3 // receive commanded course and height
#define HIL_RECEIVE_SENSORS 4 // receive sensor data from sim
#define HIL_SEND_STATE_TO_SIM 5 // send estimated states to sim
#define HIL_ACK_COMMANDS 6 // acknowledge new commands
#define STATE_EST_RECEIVE_GAINS 7 // receive state estimation gains from sim
#define HIL_ACK_STATE_EST_GAINS 8 // acknowledge received state estimation gains
#define STABILITY_CONTROLLER_RECEIVE_GAINS 9 // receives stability controller gains from sim
#define HEARTBEAT_HIL_ACTUATOR_ERROR 255
#define ACTUATOR_PERIOD (1 * 1000)

// for head tracking mapping to reference commands
#define XC_DEFAULT 0.0
#define HC_DEFAULT 100.0
#define HEARTBEAT_DEFAULT 11

//Enum for states for head tracking
enum {
    ZERO_HEAD_TRACKING, CAPTURE_HEAD_TRACKING
};

/*******************************************************************************
 * VARIABLES                                                                   *
 ******************************************************************************/

/*Variables*/
struct IMU_out IMU_est;
struct FRT curr_time;
struct IMU IMU;
struct IMU_euler angleCL;
struct IMU_euler angleOL;
static float angles[NUM_OF_ANGLES] = {0, 0, 0};
static int new_state_received = 0;

/*******************************************************************************
 * FUNCTIONS                                                                   *
 ******************************************************************************/

/**
 * @function TransferIMU(void)
 * @param none
 * @returns none
 * @brief transfers the calibrated sensor values from the IMU library to 
 *        state estimation library
 * @author Carl Cuyos
 */
void TransferIMU(void) {
    //Transfer sensor values from sensor libraries to attitude estimation library
    
    /*Accelerometer*/
    IMU_est.acc.x = -IMU.acc_true.x;
    IMU_est.acc.y = -IMU.acc_true.y;
    IMU_est.acc.z = -IMU.acc_true.z;
    /*Gyroscope*/
    IMU_est.gyro.x = IMU.gyro_rad.x;
    IMU_est.gyro.y = IMU.gyro_rad.y;
    IMU_est.gyro.z = IMU.gyro_rad.z;
}

/**
 * @function checkHILEvents(void)
 * @param none
 * @returns none
 * @brief looks for messages sent over the radio serial port to OSAVC
 * @author Margaret Silva
 */
void checkHILEvents(void) {
    uint8_t channel = MAVLINK_COMM_0;
    uint8_t msg_byte;
    mavlink_message_t msg_rx;
    mavlink_status_t msg_rx_status;
    mavlink_hil_actuator_controls_t hil_sim_state;
    mavlink_statustext_t status_text;

    if (DATA_AVAILABLE()) {
        msg_byte = GET_CHAR();
        if (mavlink_parse_char(channel, msg_byte, &msg_rx, &msg_rx_status)) {
            switch (msg_rx.msgid) {
                case MAVLINK_MSG_ID_HIL_ACTUATOR_CONTROLS:
                    // used to pass in state data from simulator to controller
                    mavlink_msg_hil_actuator_controls_decode(&msg_rx, &hil_sim_state);
                    switch (hil_sim_state.flags) {
                        case HIL_RECEIVE_STATE:
                            angles[0] = hil_sim_state.controls[6];
                            angles[1] = hil_sim_state.controls[7];
                            angles[2] = hil_sim_state.controls[8];
                            new_state_received = TRUE;
                            publish_hil_act_controls(HIL_RECEIVE_STATE, hil_sim_state.controls);
                            break;
                        default:
                            break;
                    }
                    break;
                case MAVLINK_MSG_ID_STATUSTEXT:
                    // debug message
                    mavlink_msg_statustext_decode(&msg_rx, &status_text);
                    if (strcmp(status_text.text, "I AM SIM") == 0) {
                        publish_status_text("I AM AUTOPILOT");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

/*******************************************************************************
 * MAIN                                                                        *
 ******************************************************************************/

int main(void) {
    int cur_time = 0;
    int heartbeat_start = 0;
    int actuator_start_time = 0;
    int i;
    float set_angles[NUM_OF_ANGLES] = {0, 0, 0};

    //Initialize required modules
    BOARD_Init();
    SERIAL_Init();
    TIMERS_Init();
    initEstimation();
    RCServo_Init(SERVO_PWM_1);
    RCServo_Init(SERVO_PWM_2);
    RCServo_Init(SERVO_PWM_3);
    POWERPIN_LAT = 0;
    POWERPIN_TRIS = 0;
    POWERPIN_LAT = 1;
    DELAY(100);

    //Set state estimation gains
    setEstimationGains(KP_ACC, KI_ACC, KP_MAG, KI_MAG);
    
    //Initialize all timers for the heartbeat
    heartbeat_start = TIMERS_GetMilliSeconds();
    actuator_start_time = TIMERS_GetMilliSeconds();

    //Set yaw, pitch, roll to center angle for servo
    RCServo_SetPulseFromAngle(0, YAW_SERVO);
    RCServo_SetPulseFromAngle(0, PITCH_SERVO);
    RCServo_SetPulseFromAngle(0, ROLL_SERVO);

    while (1) {
        //Get current time in ms
        cur_time = TIMERS_GetMilliSeconds();

        //Check for HIL events from the simulator
        checkHILEvents();

        
        if ((cur_time - heartbeat_start) > HEARTBEAT_PERIOD) {
            publish_heartbeat(HEARTBEAT_DEFAULT);
            heartbeat_start = cur_time;
        }

        if ((cur_time - actuator_start_time) > CONTROL_PERIOD) {
            // gradually move towards desired angle
            for (i = 0; i < NUM_OF_ANGLES; i++) {
                if (angles[i] > set_angles[i]) {
                    set_angles[i] += GIMBAL_RATE_RAD;
                } else if (angles[i] < set_angles[i]) {
                    set_angles[i] -= GIMBAL_RATE_RAD;
                }
            }
            // move RC servos
            RCServo_SetPulseFromAngle(RAD_TO_DEG(set_angles[GIMBAL_YAW]), YAW_SERVO);
            RCServo_SetPulseFromAngle(RAD_TO_DEG(set_angles[GIMBAL_PITCH]), PITCH_SERVO);
            RCServo_SetPulseFromAngle(RAD_TO_DEG(set_angles[GIMBAL_ROLL]), ROLL_SERVO);
            
            
            //Reset control period
            actuator_start_time = cur_time;
        }
    }

    while (1);
    BOARD_End();

    while (1);
}