/* 
 * File:   PublishMavlinkMsg.c
 * Author: Margaret Silva
 * 
 * This file contains all the mavlink publish functions used in the main file
 *
 * Created on May 26, 2023, 2:42 PM
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/
#include "PublishMavlinkMsg.h"

#define SEND_HEAD_TRACK_CMDS 1
#define SEND_REF_AND_ACTUAL_ANGLES 2
#define SEND_EULER_ANGLES_FROM_GIMBAL 3

/*******************************************************************************
 * STATIC VARIABLES                                                            *
 ******************************************************************************/
static mavlink_system_t mavlink_system = {
    1, // System ID (1-255)
    MAV_COMP_ID_AUTOPILOT1 // Component ID (a MAV_COMPONENT value)
};

/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATIONS                                             *
 ******************************************************************************/

/**
 * @Function publish_controller_output(float output, char controller_type[10])
 * @param none
 * @brief invokes mavlink helper to send controller output over radio
 * @author aaron hunter
 */
void publish_controller_output(float output, char controller_type[10]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_named_value_float_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            0,
            controller_type,
            output);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_status_text(void)
 * @param none
 * @brief invokes mavlink helper to send out status text
 * @author aaron hunter
 */
void publish_status_text(char text[50]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t severity = 5;
    uint16_t id = 0;
    uint8_t chunk_seq = 0;
    mavlink_msg_statustext_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            severity,
            text,
            id,
            chunk_seq
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_heartbeat(void)
 * @param none
 * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
 * @author aaron hunter
 */
void publish_heartbeat(uint8_t status) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t mode = MAV_MODE_FLAG_MANUAL_INPUT_ENABLED | MAV_MODE_FLAG_SAFETY_ARMED;
    uint32_t custom = 0;
    mavlink_msg_heartbeat_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC,
            mode,
            custom,
            status);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_hil_act_controls(uint8_t mode, float outputs[16])
 * @param mode - flag that determines what info is transmitted
 *      outputs - data to be sent over mavlink
 * @brief invokes mavlink helper to generate hil act controls and sends out via the radio
 * @author aaron hunter
 */
void publish_hil_act_controls(uint64_t flags, float outputs[16]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_hil_actuator_controls_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            0,
            outputs,
            1,
            flags);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_euler_angles(float angles[3])
 * @param angles[3] - yaw, pitch, and roll angles received from sim
 * @brief invokes mavlink helper to send actuator commands over radio
 * @author Margaret Silva
 */
void publish_euler_angles(float angles[3]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_hil_controls_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            0, // time (unused)
            angles[0],
            angles[1],
            angles[2],
            0.0,
            0.0, 0.0, 0.0, 0.0, // unused aux commands
            0,
            0
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_euler_angles(float angles[3])
 * @param angles[3] - yaw, pitch, and roll angles received from sim
 * @brief invokes mavlink helper to send actuator commands over radio
 * @author Margaret Silva
 */
void publish_euler_angles_redux(float angles[3]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_hil_controls_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            0, // time (unused)
            angles[0],
            angles[1],
            angles[2],
            0.0,
            0.0, 0.0, 0.0, 0.0, // unused aux commands
            SEND_EULER_ANGLES_FROM_GIMBAL,
            0
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_est_and_gimbal_angles(float est_angles[3], float gimbal_angle[3])
 * @param est angles[3] - yaw, pitch, and roll angles received from attitude est
 * @param gimbal angles[3] - yaw, pitch, roll angles from gimbal settings
 * @brief invokes mavlink helper to send actuator commands over radio
 * @author Margaret Silva
 */
void publish_est_and_gimbal_angles(float est_angles[3], float gimbal_angle[3]){
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_hil_controls_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            0, // time (unused)
            est_angles[0],
            est_angles[1],
            est_angles[2],
            gimbal_angle[0],
            gimbal_angle[1],
            gimbal_angle[2],
            0.0, 0.0, // unused aux commands
            SEND_REF_AND_ACTUAL_ANGLES,
            0
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_reference_cmds_angles(float X_c, float h_c)
 * @param X_c - commanded course
 *        h_c - commanded height
 * @brief invokes mavlink helper to send reference commands over radio
 * @author Margaret Silva
 */
void publish_reference_cmds_angles(float X_c, float h_c) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_hil_controls_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            0, // time (unused)
            X_c,
            h_c,
            0.0,
            0.0,
            0.0, 0.0, 0.0, 0.0, // unused aux commands
            SEND_HEAD_TRACK_CMDS,
            0
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}
