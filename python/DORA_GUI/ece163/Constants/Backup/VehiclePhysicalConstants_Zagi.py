"""All the vehicle constants that define the various constants that are used in the simulation and control
   model. These are physical constants, mathematical constants, and the rest."""

import math
from ece163.Utilities import MatrixMath

LINEARMAX = 250
ROTATEMAX = 180

# parameters for Zagi flying wing model

mass = 1.56  # [kg]
rho = 1.226  # [kg / m^3]
g0 = 9.81  # gravity, [m/s^2]
b = 1.4224  # wing-span [m]
c = 0.3302  # wing chord [m]
S = 0.2589  # wing area [m^2]
e = 0.9  # Oswald's Efficiency Factor []

AR = b ** 2 / S

M = 50.  # barrier function coefficient for angle of attack
alpha0 = math.radians(27.)  # angle at which stall occurs [deg]

Jxx = 0.1147  # [kg m^2]
Jyy = 0.0576  # [kg m^2]
Jzz = 0.1712  # [kg m^2]
Jxz = 0.0015  # [kg m^2]

Jbody = [[Jxx, 0., -Jxz], [0., Jyy, 0.], [-Jxz, 0., Jzz]]
Jdet = (Jxx * Jzz - Jxz ** 2)
JinvBody = MatrixMath.scalarMultiply(1. / Jdet, [[Jzz, 0., Jxz], [0., Jdet / Jyy, 0.], [Jxz, 0., Jxx]])

# Aerodynamic Partial Derivatives for Forces

# Lift
CL0 = 0.09167  # zero angle of attack lift coefficient
CLalpha = math.pi * AR / (1 + math.sqrt(1 + (AR / 2.) ** 2))
CLalpha = 3.5016  # given in book
CLq = 2.8932  # needs to be normalized by c/2*Va
CLdeltaE = 0.2724  # lift due to elevator deflection

# Drag
CDp = 0.0254  # minimum drag
CDalpha = 0.2108  # drag slope
CD0 = 0.01631  # intercept of linarized drag slope
CDq = 0  # drag wrt pitch rate
CDdeltaE = 0.3045  # drag due to elevator deflection

# Pitching Moment
CM0 = -0.02338  # intercept of pitching moment
CMalpha = -0.5675  # pitching moment slope
CMq = -1.3990  # pitching moment wrt q
CMdeltaE = -0.3254  # pitching moment from elevator

# Sideforce
CY0 = 0.
CYbeta = -0.07359
CYp = 0.
CYr = 0.
CYdeltaA = 0.
CYdeltaR = 0.

# Rolling Moment
Cl0 = 0.
Clbeta = -0.02854
Clp = -0.3209
Clr = 0.03066
CldeltaA = 0.1682
CldeltaR = 0.

# Yawing Moment
Cn0 = 0.
Cnbeta = -0.00040
Cnp = -0.01297
Cnr = -0.00434
CndeltaA = -0.00328
CndeltaR = 0.

# Propeller Thrust
Sprop = 0.0314  # propellor area [m^2]
kmotor = 20.  # motor constant
kTp = 0.  # motor torque constant
kOmega = 0.  # motor speed constant
Cprop = 1.0  # propeller thrust coefficient

# Alternate Propellor Model
# D_prop = 5. * (0.0254)  # prop diameter in m
#
# # Motor parameters (taken from Turnigy D2836/8 1100KV Brushless Outrunner Motor)
# KV = 1100.  # from datasheet RPM/V
# KQ = (1. / KV) * 60. / (2. * math.pi)  # KQ in N-m/A, V-s/rad
# R_motor = 0.107  # ohms
# i0 = 1.0  # no-load (zero-torque) current (A)
#
# # Inputs
# ncells = 3.
# V_max = 3.7 * ncells  # max voltage for specified number of battery cells
#
# # Coeffiecients from prop_data fit (from lecture slide)
# C_Q2 = -0.015729
# C_Q1 = 0.0031409
# C_Q0 = 0.006199
# C_T2 = -0.047394
# C_T1 = -0.13803
# C_T0 = 0.11221
