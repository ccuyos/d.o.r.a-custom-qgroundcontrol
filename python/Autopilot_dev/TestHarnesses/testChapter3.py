"""This file is a test harness for the module VehicleDynamicsModel. 

It is meant to be run from the Testharnesses directory of the repo with:

python ./TestHarnesses/testChapter3.py (from the root directory) -or-
python testChapter3.py (from inside the TestHarnesses directory)

at which point it will execute various tests on the VehicleDynamicsModel module"""

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Utilities.MatrixMath as mm
import ece163.Utilities.Rotations as Rotations
import ece163.Modeling.VehicleDynamicsModel as VDM
import ece163.Containers.Inputs as Inputs
import ece163.Containers.States as States
from ece163.Constants import VehiclePhysicalConstants as VPC

"""math.isclose doesn't work well for comparing things near 0 unless we 
use an absolute tolerance, so we make our own isclose:"""
isclose = lambda  a,b : math.isclose(a, b, abs_tol= 1e-12)

def compareVectors(a, b):
	"""A quick tool to compare two vectors"""
	el_close = [isclose(a[i][0], b[i][0]) for i in range(3)]
	return all(el_close)

#of course, you should test your testing tools too:
assert(compareVectors([[0], [0], [-1]],[[1e-13], [0], [-1+1e-9]]))
assert(not compareVectors([[0], [0], [-1]],[[1e-11], [0], [-1]]))
assert(not compareVectors([[1e8], [0], [-1]],[[1e8+1], [0], [-1]]))



failed = []
passed = []
def evaluateTest(test_name, boolean):
	"""evaluateTest prints the output of a test and adds it to one of two 
	global lists, passed and failed, which can be printed later"""
	if boolean:
		print(f"   passed {test_name}")
		passed.append(test_name)
	else:
		print(f"   failed {test_name}")
		failed.append(test_name)
	return boolean

def compareMatrices(A, B):
	"""Compare two matrices"""
	for i in range(len(A)):
		for j in range(len(A[0])):
			if not math.isclose(A[i][j], B[i][j], abs_tol= 1e-7):
				return False
	return True


#%% Euler2dcm():
# print("Beginning testing of Rotations.Euler2dcm()")

# cur_test = "Derivative test p_dot x dir"

# testVdm = VDM.VehicleDynamicsModel()
# # testState = InputsVS()
# testState = States.vehicleState()
# # testFm = FM()
# testFm = States.vehicleState()
# testState.pitch = 30*math.pi/180
# testState.u = 10
# # testDot = testVDM.derivative(testState, testFm)
# testDot = testVdm.dot

# if testDot.pd < 0:
# 	print("passed!")
# else:
# 	print("failed :(")



#%%  

"""
Students, add more tests here.  
You aren't required to use the testing framework we've started here, 
but it will work just fine.
"""

#%% Basic set/get/reset tests
print("Begin Basic Functionality Tests:")
cur_test = "Init Vehicle State test"
testVdm = VDM.VehicleDynamicsModel()
if not evaluateTest(cur_test, (testVdm.dot.pn == 0) ):
	print("test failed")

cur_test = "Set/Get vehicle state test"
testVdm = VDM.VehicleDynamicsModel()
newState = States.vehicleState(yaw=0.284)
testVdm.setVehicleState(newState)
if not evaluateTest(cur_test, (testVdm.state.yaw == testVdm.getVehicleState().yaw == 0.284)):
	print("test failed. expected:", newState.yaw, "returned:", testVdm.getVehicleState().yaw)

cur_test = "Set/Get vehicle derivative test"
testVdm = VDM.VehicleDynamicsModel()
newState = States.vehicleState(pitch=0.123)
testVdm.setVehicleDerivative(newState)
if not evaluateTest(cur_test, (testVdm.dot.pitch == testVdm.getVehicleDerivative().pitch == 0.123)):
	print("test failed. expected:", newState.pitch, "returned:", testVdm.getVehicleDerivative().pitch)

cur_test = "Reset test"
testVdm.reset()
if not evaluateTest(cur_test, (testVdm.dot.pitch == testVdm.getVehicleDerivative().pitch == 0)):
	print("test failed. expected:", newState.pitch, "returned:", testVdm.getVehicleDerivative().pitch)


#%% Rexp tests
print("Begin matrix exponential tests:")

cur_test = "Rexp() test - Simple"
testVdm = VDM.VehicleDynamicsModel()
rexp_test = testVdm.Rexp(testVdm.dT, testVdm.state, testVdm.dot)
rexp_expected = [[1,0,0], [0,1,0], [0,0,1]]
if not evaluateTest(cur_test, compareMatrices(rexp_test, rexp_expected)):
	print("test failed")

cur_test = "Rexp() test for w_mag > 0.2"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(p=(math.pi/6), q=(-math.pi/3), r=(math.pi/11))
rexp_test = testVdm.Rexp(testVdm.dT, testState, testVdm.dot)
rexp_expected = [[0.99994109,0.0028285,0.01047919], [-0.002883339,0.9999822,0.005220907], [-0.01046424,-0.00525081,0.9999314]]
if not evaluateTest(cur_test, compareMatrices(rexp_test, rexp_expected)):
	print("test failed")
	print("expected:")
	mm.matrixPrint(rexp_expected)
	print("returned:")
	mm.matrixPrint(rexp_test)

cur_test = "Rexp() test for w_mag < 0.2"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(p=(-math.pi/46), q=(-math.pi/30), r=(math.pi/47))
rexp_test = testVdm.Rexp(testVdm.dT, testState, testVdm.dot)
rexp_expected = [[0.999999228293473,0.000668781339247,0.001046968948731], [-0.000668066150642,0.999999543391061,-0.000683304681835], [-0.001047425452096,0.000682604710009,0.999999218475061]]
if not evaluateTest(cur_test, compareMatrices(rexp_test, rexp_expected)):
	print("test failed")
	print("expected:")
	mm.matrixPrint(rexp_expected)
	print("returned:")
	mm.matrixPrint(rexp_test)

cur_test = "Rexp() test for dT = 0.1"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(p=(math.pi/6), q=(-math.pi/3), r=(math.pi/11))
rexp_test = testVdm.Rexp(0.1, testState, testVdm.dot)
rexp_expected = [[0.994116177,0.025752611,0.1052132498], [-0.03122909,0.9982235383,0.0507396428], [-0.103719664,-0.0537268,0.993154399]]
if not evaluateTest(cur_test, compareMatrices(rexp_test, rexp_expected)):
	print("test failed")
	print("expected:")
	mm.matrixPrint(rexp_expected)
	print("returned:")
	mm.matrixPrint(rexp_test)



#%% Derivative tests
print("Begin derivative tests:")

cur_test = "Euler angle derivative test - Simple"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState()
test_result = testVdm.derivative(testState, Inputs.forcesMoments())
test_result = [[test_result.roll], [test_result.pitch], [test_result.yaw]]
expected = [[0], [0], [0]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Euler angle derivative test 1"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(p=.1, q=0.4, r=0.9, yaw=math.pi/4, pitch=-math.pi/12, roll=-math.pi/8)
test_result = testVdm.derivative(testState, Inputs.forcesMoments())
test_result = [[test_result.roll], [test_result.pitch], [test_result.yaw]]
expected = [[-0.08178161051243979], [0.7139669021330955], [0.7023502093535441]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Euler angle derivative test 2"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(p=.85, q=2, r=3.5, yaw=(-3*math.pi)/4, pitch=(11*math.pi)/12, roll=math.pi/3)
test_result = testVdm.derivative(testState, Inputs.forcesMoments())
test_result = [[test_result.roll], [test_result.pitch], [test_result.yaw]]
expected = [[-0.083012701892220], [-2.031088913245535], [-3.604884260053753]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)


cur_test = "Position derivative test - Simple"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState()
test_result = testVdm.derivative(testState, Inputs.forcesMoments())
test_result = [[test_result.pn], [test_result.pe], [test_result.pd]]
expected = [[0], [0], [0]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Position derivative test 1"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(u=22.5, yaw=math.pi/3, pitch=-math.pi/6, roll=math.pi/5)
test_result = testVdm.derivative(testState, Inputs.forcesMoments())
test_result = [[test_result.pn], [test_result.pe], [test_result.pd]]
expected = [[9.742785792574939], [16.875], [11.249999999999998]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Position derivative test 2"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(u=3, v=-100, w=65, yaw=(8*math.pi)/3, pitch=(11*math.pi)/6, roll=-math.pi/5)
test_result = testVdm.derivative(testState, Inputs.forcesMoments())
test_result = [[test_result.pn], [test_result.pe], [test_result.pd]]
expected = [[63.517643852848479], [-24.624470253230665], [97.944598544945080]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)


cur_test = "Ground speed derivative test - Simple"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState()
test_result = testVdm.derivative(testState, Inputs.forcesMoments())
test_result = [[test_result.u], [test_result.v], [test_result.w]]
expected = [[0],[0],[0]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Ground speed derivative test 1"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState()
test_result = testVdm.derivative(testState, Inputs.forcesMoments(Fx=9.81*VPC.mass, Fy=0, Fz=0))
test_result = [[test_result.u], [test_result.v], [test_result.w]]
expected = [[9.81],[0],[0]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Ground speed derivative test 2"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState()
test_result = testVdm.derivative(testState, Inputs.forcesMoments(Fx=9.81*VPC.mass, Fy=-10*VPC.mass, Fz=.99*VPC.mass))
test_result = [[test_result.u], [test_result.v], [test_result.w]]
expected = [[9.81],[-10],[0.99]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Ground speed derivative test 3"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(q=.5, u=20)
test_result = testVdm.derivative(testState, Inputs.forcesMoments(Fx=9.81*VPC.mass, Fy=-10*VPC.mass, Fz=.99*VPC.mass))
test_result = [[test_result.u], [test_result.v], [test_result.w]]
expected = [[9.81],[-10],[10.99]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Ground speed derivative test 4"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(q=.5, p=5, r=-2, u=20, v=-5)
test_result = testVdm.derivative(testState, Inputs.forcesMoments(Fx=9.81*VPC.mass, Fy=-10*VPC.mass, Fz=.99*VPC.mass))
test_result = [[test_result.u], [test_result.v], [test_result.w]]
expected = [[19.81],[30],[35.99]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)


cur_test = "Angular rate derivative test - Simple"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState()
test_result = testVdm.derivative(testState, Inputs.forcesMoments())
test_result = [[test_result.p], [test_result.q], [test_result.r]]
expected = [[0],[0],[0]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Angular rate derivative test 1"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState()
test_result = testVdm.derivative(testState, Inputs.forcesMoments(Mx=0.5))
test_result = [[test_result.p], [test_result.q], [test_result.r]]
expected = [[0.6126258289], [0.0], [0.041933001595]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Angular rate derivative test 2"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(p = 10)
test_result = testVdm.derivative(testState, Inputs.forcesMoments(Mx=0.5))
test_result = [[test_result.p], [test_result.q], [test_result.r]]
expected = [[0.6126258289], [-10.6079295154], [0.041933001595]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)

cur_test = "Angular rate derivative test 3"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(p = 10, q = -4, r= 15)
test_result = testVdm.derivative(testState, Inputs.forcesMoments(Mx=0.5, My=-66, Mz=103))
test_result = [[test_result.p], [test_result.q], [test_result.r]]
expected = [[50.871233476], [78.6255506607], [73.2080139343]]
if not evaluateTest(cur_test, compareVectors(test_result, expected)):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected)



#%% Forward Euler test

cur_test = "Forward Euler test - Simple"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState()
testDot = testVdm.derivative(testState, Inputs.forcesMoments())
test_result = testVdm.ForwardEuler(0.01, testState, testDot)
if not evaluateTest(cur_test, testState==test_result):
	print("test failed")
	print("returned:", test_result)
	print("expected:", testState)

cur_test = "Forward Euler test 1"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(u=0.5, v=0.1, w=0.2)
testDot = States.vehicleState(u=12/.01, v=-42/.01, w=3/.01)
test_result = testVdm.ForwardEuler(0.01, testState, testDot)
expected_result = States.vehicleState(u=12.5, v=-41.9, w=3.2)
if not evaluateTest(cur_test, expected_result==test_result):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected_result)

cur_test = "Forward Euler test 2"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(p=-11, q=.5, r=-7)
testDot = States.vehicleState(p=12/.01, q=-42/.01, r=8/.01)
test_result = testVdm.ForwardEuler(0.01, testState, testDot)
expected_result = States.vehicleState(p=1, q=-41.5, r=1)
if not evaluateTest(cur_test, expected_result==test_result):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected_result)

cur_test = "Forward Euler test 3"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(pn=-8, pe=7.8, pd=55)
testDot = States.vehicleState(pn=-12/.01, pe=72.2/.01, pd=-15/.01)
test_result = testVdm.ForwardEuler(0.01, testState, testDot)
expected_result = States.vehicleState(pn=-20, pe=80, pd=40)
if not evaluateTest(cur_test, expected_result==test_result):
	print("test failed")
	print("returned:", test_result)
	print("expected:", expected_result)



#%% Integration tests

cur_test = "IntegrateState test - Stationary"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(pn=7, pd=-.65, yaw=(math.pi)/3, roll=(2*math.pi)/5)
testDot = States.vehicleState()
test_result = testVdm.IntegrateState(0.01, testState, testDot)
expected_result = States.vehicleState(pn=7, pd=-.65, yaw=(math.pi)/3, roll=(2*math.pi)/5)
if not evaluateTest(cur_test, test_result==expected_result):
	print("test failed")
	print("expected:")
	print(expected_result)
	print("returned:")
	print(test_result)


cur_test = "IntegrateState test - R mat testing"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(p=(math.pi/6), q=(-math.pi/3), r=(math.pi/11))
testDot = States.vehicleState()
test_result = testVdm.IntegrateState(0.01, testState, testDot)
rexp_test = test_result.R
rexp_expected = [[0.99994109,0.0028285,0.01047919], [-0.002883339,0.9999822,0.005220907], [-0.01046424,-0.00525081,0.9999314]]
if not evaluateTest(cur_test, compareMatrices(rexp_test, rexp_expected)):
	print("test failed")
	print("expected:")
	mm.matrixPrint(rexp_expected)
	print("returned:")
	mm.matrixPrint(rexp_test)


cur_test = "IntegrateState test - Full state test"
testVdm = VDM.VehicleDynamicsModel()
testState = States.vehicleState(pn=10, pd=-5, v=25, w=-.5, yaw=math.pi/3, roll=(-4*math.pi)/5, p=8, q=-.6)
testDot = States.vehicleState(pe=9, u=-14, pitch=-3, q=4, r=-5)
test_result = testVdm.IntegrateState(0.01, testState, testDot)
expected_result = States.vehicleState(pn=10, pe=0.09, pd=-5, u=-.14, v=25, w=-.5, yaw=math.radians(60.217110886), pitch=math.radians(0.25187884), roll=math.radians(-139.4158475), p=math.radians(458.366236), q=math.radians(-32.085636), r=math.radians(-2.86478897))
if not evaluateTest(cur_test, test_result==expected_result):
	print("test failed")
	print("expected:")
	print(expected_result)
	print("returned:")
	print(test_result)





#%% Print results:

total = len(passed) + len(failed)
print(f"\n---\nPassed {len(passed)}/{total} tests")
[print("   " + test) for test in passed]

if failed:
	print(f"Failed {len(failed)}/{total} tests:")
	[print("   " + test) for test in failed]