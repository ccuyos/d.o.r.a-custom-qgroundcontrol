'''
    File: testChapter6.py
    Author: Margaret Silva
    Notes: Contains tests for functions withing the vehicle closed loop control and vehicle control gains files
'''

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Utilities.MatrixMath as mm
import ece163.Utilities.Rotations as Rotations
import ece163.Modeling.VehicleDynamicsModel as VDM
import ece163.Controls.VehiclePerturbationModels as VPM
import ece163.Modeling.WindModel as WM
import ece163.Controls.VehicleTrim as VehicleTrim
import ece163.Containers.Inputs as Inputs
import ece163.Containers.States as States
import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Containers.Linearized as Linearized
import ece163.Containers.Controls as Controls
import ece163.Controls.VehicleControlGains as VCG
import ece163.Controls.VehicleClosedLoopControl as VCLC
import matplotlib.pyplot as plt

"""math.isclose doesn't work well for comparing things near 0 unless we 
use an absolute tolerance, so we make our own isclose:"""
isclose = lambda  a,b : math.isclose(a, b, abs_tol= 1e-12)

def compareVectors(a, b):
	"""A quick tool to compare two vectors"""
	el_close = [isclose(a[i][0], b[i][0]) for i in range(3)]
	return all(el_close)

#of course, you should test your testing tools too:
assert(compareVectors([[0], [0], [-1]],[[1e-13], [0], [-1+1e-9]]))
assert(not compareVectors([[0], [0], [-1]],[[1e-11], [0], [-1]]))
assert(not compareVectors([[1e8], [0], [-1]],[[1e8+1], [0], [-1]]))



failed = []
passed = []
def evaluateTest(test_name, boolean):
	"""evaluateTest prints the output of a test and adds it to one of two 
	global lists, passed and failed, which can be printed later"""
	if boolean:
		print(f"   passed {test_name}")
		passed.append(test_name)
	else:
		print(f"   failed {test_name}")
		failed.append(test_name)
	return boolean

# get a transfer function
vTrim = VehicleTrim.VehicleTrim()
Vastar = 25.0
Gammastar = math.radians(6.0)
Kappastar = -1.0 / 150.0

check = vTrim.computeTrim(Vastar, Kappastar, Gammastar)
if check:
 print("Optimization successful")
else:
 print("Model converged outside of valid inputs, change parameters and try again")
	   
 
tF = VPM.CreateTransferFunction(
	vTrim.getTrimState(), 
	vTrim.getTrimControls())


#%% PD Controller Testing
print("Testing PD controller")

cur_test = "PD zeros test"
pd = VCLC.PDControl()
result = pd.Update()
expected = 0
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "PD simple update test"
pd = VCLC.PDControl(-2, 5, .4, -1, 1)
result = pd.Update()
expected = .4
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "PD complex test 1"
pd = VCLC.PDControl(-2, 5, .4, -1, 1)
result = pd.Update(8, 8.5, .3)
expected = -.1
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "PD complex test 2"
pd = VCLC.PDControl(.29, -1, 9, 0, 10)
result = pd.Update(-2, 47, 3)
expected = 0
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)



#%% PI Controller Testing
print("Testing PI controller")

cur_test = "PI zeros test"
pi = VCLC.PIControl()
result = pi.Update()
expected = 0
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "PI simple update test"
pi = VCLC.PIControl(-2, 5, .3, -1, 1)
result = pi.Update()
expected = 1
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "PI complex test 1"
pi = VCLC.PIControl(.001, 8, .8, 4, -2, 5)
result = pi.Update(8, 8.5)
expected = -0.0002
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "PI complex test 2"
pi = VCLC.PIControl(.29, -1, 9, 0, 10)
result = pi.Update(-2, 47)
expected = 10
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)



#%% PID Controller Testing
print("Testing PID controller")

cur_test = "PID zeros test"
pid = VCLC.PIDControl()
result = pid.Update()
expected = 0
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "PID simple update test"
pid = VCLC.PIDControl(-2, 5, .3, -1, 1)
result = pid.Update()
expected = 0
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "PID complex test 1"
pid = VCLC.PIDControl(.001, 8, .8, 4, -2, 5)
result = pid.Update(8, 8.5, .3)
expected = 5
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)

cur_test = "PID complex test 2"
pid = VCLC.PIDControl(.29, -1, 9, 0, 10)
result = pid.Update(-2, 47, 19)
expected = 0
if not evaluateTest(cur_test, isclose(result,expected)):
	print("expected:", expected)
	print("resulted:", result)



#%% Test Vehicle Control Gains functions

print("Testing compute gains and compute parameters together")
cur_test = "Compute combo - zeros test"
plant = Linearized.transferFunctions()
test_gains = Controls.controlGains(ki_roll=0.001)
result_params = VCG.computeTuningParameters(test_gains, plant)
result_gains = VCG.computeGains(result_params, plant)
if not evaluateTest(cur_test, test_gains == result_gains):
	print("expected:", test_gains)
	print("resulted:", result_gains)
	
cur_test = "Compute combo - ones test, gains->params->gains"
plant = Linearized.transferFunctions(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
test_gains = Controls.controlGains(1, 1, .001, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1)
result_params = VCG.computeTuningParameters(test_gains, plant)
result_gains = VCG.computeGains(result_params, plant)
if not evaluateTest(cur_test, test_gains == result_gains):
	print("expected:", test_gains)
	print("resulted:", result_gains)
	
cur_test = "Compute combo - ones test, params->gains->params"
plant = Linearized.transferFunctions(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
test_params = Controls.controlTuning(Wn_roll=1.0, Zeta_roll=1.0, Wn_course=1.0, Zeta_course=1.0, Wn_sideslip=1.0, Zeta_sideslip=1.0, Wn_pitch=1.0, Zeta_pitch=1.0, Wn_altitude=0.0, Zeta_altitude=0, Wn_SpeedfromThrottle=1.0, Zeta_SpeedfromThrottle=1.0, Wn_SpeedfromElevator=0.0, Zeta_SpeedfromElevator=0)
result_gains = VCG.computeGains(test_params, plant)
result_params = VCG.computeTuningParameters(result_gains, plant)
if not evaluateTest(cur_test, test_params == result_params):
	print("expected:", test_params)
	print("resulted:", result_params)



#%% Aircraft behavior graphs

print("Testing aircraft behavior")
plant = tF
test_params = Controls.controlTuning(Wn_roll=7.5, Zeta_roll=1, Wn_course=.75, Zeta_course=.8, 
	Wn_sideslip=.5, Zeta_sideslip=1, 
	Wn_pitch=20, Zeta_pitch=.8, Wn_altitude=1, Zeta_altitude=0.75, 
	Wn_SpeedfromThrottle=10, Zeta_SpeedfromThrottle=.7, 
	Wn_SpeedfromElevator=10, Zeta_SpeedfromElevator=.5)
control_gains = VCG.computeGains(test_params, plant)

# control_gains = Controls.controlGains(
# 					kp_roll=70, kd_roll=40, ki_roll=2, 
# 				    kp_course=10, ki_course=.7,
# 				    kp_sideslip=15, ki_sideslip=.95,
# 					kp_pitch=-15, kd_pitch=.5, 
# 					kp_altitude=25, ki_altitude=.5,
# 					kp_SpeedfromElevator=50, ki_SpeedfromElevator=1,
# 					kp_SpeedfromThrottle=50, ki_SpeedfromThrottle=1)

vehicleController = VCLC.VehicleClosedLoopControl()
vehicleController.setControlGains(control_gains)
vehicleController.setTrimInputs()

commands = Controls.referenceCommands()
throttle = []
ailerons = []
elevator = []
rudder = []
course = []
Va = []
h = []
beta = []
roll = []
roll_cmd = []
course_cmd = []
Va_cmd = []
h_cmd = []
pitch_cmd = []
pitch = []
n = 2500
for i in range(n):
	commands = Controls.referenceCommands()
	vehicleController.Update(commands)
	throttle.append(vehicleController.outputControls.Throttle)
	ailerons.append(vehicleController.outputControls.Aileron)
	elevator.append(vehicleController.outputControls.Elevator)
	rudder.append(vehicleController.outputControls.Rudder)
	course.append(vehicleController.getVehicleState().chi)
	Va.append(vehicleController.getVehicleState().Va)
	h.append(-vehicleController.getVehicleState().pd)
	beta.append(vehicleController.getVehicleState().beta)
	roll_cmd.append(commands.commandedRoll)
	course_cmd.append(commands.commandedCourse)
	Va_cmd.append(commands.commandedAirspeed)
	h_cmd.append(commands.commandedAltitude)
	roll.append(vehicleController.getVehicleState().roll)
	pitch_cmd.append(commands.commandedPitch)
	pitch.append(vehicleController.getVehicleState().pitch)


time = [VPC.dT*i for i in range(n)]
# course_cmd = [commands.commandedCourse for i in range(n)]
# Va_cmd = [commands.commandedAirspeed for i in range(n)]
# h_cmd = [commands.commandedAltitude for i in range(n)]
beta_cmd = [0 for i in range(n)]

plt.subplot(4, 2, 1)
plt.plot(time, throttle, label='dt')
plt.plot(time, ailerons, label='da')
plt.plot(time, elevator, label='de')
plt.plot(time, rudder, label='dr')
plt.legend()

plt.subplot(4, 2, 2)
plt.plot(time, course, label='actual course')
plt.plot(time, course_cmd, label='commanded course')
plt.legend()

plt.subplot(4, 2, 3)
plt.plot(time, Va, label='actual airspeed')
plt.plot(time, Va_cmd, label='commanded airspeed')
plt.legend()

plt.subplot(4, 2, 4)
plt.plot(time, h, label='actual altitude')
plt.plot(time, h_cmd, label='commanded altitude')
plt.legend()

plt.subplot(4, 2, 5)
plt.plot(time, beta, label='actual beta')
plt.plot(time, beta_cmd, label='commanded beta')
plt.legend()

plt.subplot(4, 2, 6)
plt.plot(time, roll, label='actual roll')
plt.plot(time, roll_cmd, label='commanded roll')
plt.legend()

plt.subplot(4, 2, 7)
plt.plot(time, pitch, label='actual pitch')
plt.plot(time, pitch_cmd, label='commanded pitch')
plt.legend()

plt.show()


#%% Print results:

total = len(passed) + len(failed)
print(f"\n---\nPassed {len(passed)}/{total} tests")
[print("   " + test) for test in passed]

if failed:
	print(f"Failed {len(failed)}/{total} tests:")
	[print("   " + test) for test in failed]