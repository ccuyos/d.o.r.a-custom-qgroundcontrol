/* 
 * File:   main_waypoint_following_test.c
 * Author: Margaret Silva
 * Brief: main file for demonstrating way point following functionality 
 *
 * Created on June 3, 2023, 9:14 PM
 */


/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

// standard libraries
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/attribs.h>  //for ISR definitions
#include "xc.h"
#include "p32mx795f512l.h"

// DORA libraries
#include "AirframeConstants.h"
#include "StabilityController.h"
#include "StateEstimation.h"
#include "WaypointFollowing.h"
#include "PD.h"
#include "PI.h"
#include "PublishMavlinkMsg.h"
#include "RC_Control.h"

// OSAVC libraries
#include "SerialM32.h"
#include "Board.h"
#include "System_timer.h"
#include "Radio_serial.h"
#include "common/mavlink.h"
#include "NEO_M8N.h"
#include "RC_RX.h"
#include "RC_servo.h"
#include "ICM_20948.h"
#include "AS5047D.h"
#include "PID.h"

/*******************************************************************************
 * #DEFINES                                                                    *
 ******************************************************************************/
#define HEARTBEAT_PERIOD 1000 //1 sec interval for hearbeat update
#define SENDING_PERIOD 20 // 20 ms between 
#define ENCODER_PERIOD 10 //re-initiate encoder at 100 Hz
#define CONTROL_FREQUENCY (1000/CONTROL_PERIOD) //frequency in Hz
#define GPS_PERIOD 100 //10 Hz update rate
#define RAW 1
#define SCALED 2

// used to determine what data is being sent in an HIL actuator commands packet
#define HIL_SEND_GAINS_TO_SIM 1 // send received gains back to sim as ack
#define HIL_RECEIVE_STATE 2 // receive state data from sim
#define HIL_RECEIVE_COMMANDS 3 // receive commanded course and height
#define HIL_RECEIVE_SENSORS 4 // receive sensor data from sim
#define HIL_SEND_STATE_TO_SIM 5 // send estimated states to sim
#define HIL_ACK_COMMANDS 6 // acknowledge new commands
#define STATE_EST_RECEIVE_GAINS 7 // receive state estimation gains from sim
#define HIL_ACK_STATE_EST_GAINS 8 // acknowledge received state estimation gains
#define STABILITY_CONTROLLER_RECEIVE_GAINS 9 // receives stability controller gains from sim
#define RECEIVE_SQUARE_WAYPOINTS 10 // receive 4 waypoints, arranged in a square, from sim
#define HIL_ACK_WAYPOINTS 11 // acknowledge waypoint sequence
#define HEARTBEAT_HIL_ACTUATOR_ERROR 255

// used to control printing speed of messages
#define CONSOLE_OUTPUT_FREQUENCY 50

// flags used to show which mode system is in
#define MANUAL_STATUS_FLAG 7
#define AUTO_STATUS_FLAG 5

/*******************************************************************************
 * VARIABLES                                                                   *
 ******************************************************************************/

// controller commands
static float X_c = 0;
static float h_c = 100;
static float Va_c = 20;

// new state flag, used to command controller to only update when new state data is received
static uint8_t new_state_received = FALSE;

static uint8_t waypoint_seq_received = FALSE;

enum mav_output_type {
    USB,
    RADIO
};

// sensor data structs
static struct IMU_out IMU_data;
static float Baro_data;
static struct GPS_data GPS_out;
static uint8_t ready_sensors_flag = 0;
static uint8_t autonomous_status_flag = MANUAL_STATUS_FLAG;
static uint8_t first_gps_flag = 0;

// debug variables
static int gps_received_check = 0;

/*******************************************************************************
 * FUNCTION PROTOTYPES                                                         *
 ******************************************************************************/

/**
 * @function checkHILEvents(void)
 * @param none
 * @returns none
 * @brief looks for messages sent over the radio serial port to OSAVC
 * @author Margaret Silva
 */
void checkHILEvents(void);

/*******************************************************************************
 * FUNCTIONS                                                                   *
 ******************************************************************************/

/**
 * @function checkHILEvents(void)
 * @param none
 * @returns none
 * @brief looks for messages sent over the radio serial port to OSAVC
 * @author Margaret Silva
 */
void checkHILEvents(void) {
    uint8_t channel = MAVLINK_COMM_0;
    uint8_t msg_byte;
    mavlink_message_t msg_rx;
    mavlink_status_t msg_rx_status;
    mavlink_hil_actuator_controls_t hil_sim_state;
    mavlink_statustext_t status_text;

    if (DATA_AVAILABLE()) {
        msg_byte = GET_CHAR();
        if (mavlink_parse_char(channel, msg_byte, &msg_rx, &msg_rx_status)) {
            switch (msg_rx.msgid) {
                case MAVLINK_MSG_ID_HIL_ACTUATOR_CONTROLS:
                    // used to pass in state data from simulator to controller
                    mavlink_msg_hil_actuator_controls_decode(&msg_rx, &hil_sim_state);
                    switch (hil_sim_state.flags) {
                        case HIL_RECEIVE_STATE:
                            setTrueState(hil_sim_state.controls);
                            new_state_received = TRUE;
                            break;
                        case HIL_RECEIVE_COMMANDS:
                            X_c = hil_sim_state.controls[0];
                            h_c = hil_sim_state.controls[1];
                            Va_c = hil_sim_state.controls[2];
                            publish_hil_act_controls(HIL_ACK_COMMANDS, hil_sim_state.controls);
                            break;
                        case HIL_RECEIVE_SENSORS:
                            // check to see if gps has updated or not
                            gps_received_check = 0;
                            if (hil_sim_state.controls[15]) {
                                // GPS 
                                GPS_out.lat = hil_sim_state.controls[10];
                                GPS_out.lon = hil_sim_state.controls[11];
                                // note: gps data struct is currently missing alt field
                                // GPS_out.spd = hil_sim_state.controls[12];
                                GPS_out.spd = hil_sim_state.controls[13];
                                GPS_out.cog = hil_sim_state.controls[14];
                                ready_sensors_flag = ready_sensors_flag | GPS_AVAILABLE;
                                first_gps_flag = GPS_FIRST_DATA_RECEIVED;
                            } else {
                                if (GPS_out.cog != hil_sim_state.controls[14]) {
                                    gps_received_check = 1;
                                }
                            }
                            // accelerometer
                            IMU_data.acc.x = hil_sim_state.controls[0];
                            IMU_data.acc.y = hil_sim_state.controls[1];
                            IMU_data.acc.z = hil_sim_state.controls[2];
                            // gyro
                            IMU_data.gyro.x = hil_sim_state.controls[3];
                            IMU_data.gyro.y = hil_sim_state.controls[4];
                            IMU_data.gyro.z = hil_sim_state.controls[5];
                            // mag
                            IMU_data.mag.x = hil_sim_state.controls[6];
                            IMU_data.mag.y = hil_sim_state.controls[7];
                            IMU_data.mag.z = hil_sim_state.controls[8];
                            // baro
                            Baro_data = hil_sim_state.controls[9];
                            // set flags
                            ready_sensors_flag = ready_sensors_flag | IMU_AVAILABLE;
                            ready_sensors_flag = ready_sensors_flag | BARO_AVAILABLE;
                            ready_sensors_flag = ready_sensors_flag | first_gps_flag;
                            break;
                        case STATE_EST_RECEIVE_GAINS: // state estimation gains
                            setStateEstimationGains(hil_sim_state.controls);
                            initStabilityController();
                            initStateEstimation();
                            first_gps_flag = 0;
                            publish_hil_act_controls(HIL_ACK_STATE_EST_GAINS, hil_sim_state.controls);
                            break;
                        case STABILITY_CONTROLLER_RECEIVE_GAINS: // controller gains
                            initStabilityController();
                            initStateEstimation();
                            first_gps_flag = 0;
                            setStabilityControllerGains(hil_sim_state.controls);
                            publish_hil_act_controls(HIL_SEND_GAINS_TO_SIM, hil_sim_state.controls);
                            break;
                        case RECEIVE_SQUARE_WAYPOINTS:
                            unpackSquareWaypointTransmission(hil_sim_state.controls);
                            publish_hil_act_controls(HIL_ACK_WAYPOINTS, hil_sim_state.controls);
                            waypoint_seq_received = TRUE;
                            break;
                        default:
                            publish_heartbeat(HEARTBEAT_HIL_ACTUATOR_ERROR);
                            break;
                    }
                    break;
                case MAVLINK_MSG_ID_STATUSTEXT:
                    // debug message
                    mavlink_msg_statustext_decode(&msg_rx, &status_text);
                    if (strcmp(status_text.text, "I AM SIM") == 0) {
                        publish_status_text("I AM AUTOPILOT");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

/*******************************************************************************
 * MAIN                                                                        *
 ******************************************************************************/

int main() {
    // system values
    uint32_t cur_time = 0;
    uint32_t warmup_time = 250; //time in ms to allow subsystems to stabilize (IMU))
    uint32_t heartbeat_start_time = 0;
    uint32_t control_start_time = 0;
    uint8_t wakeup_sim_flag = TRUE;
    struct vehicle_state current_state;
//    struct waypoint w1 = {.pn=0, .pe=0, .pd=-100};
//    struct waypoint w2 = {.pn=100, .pe=0, .pd=-100};
//    struct waypoint w3 = {.pn=0, .pe=100, .pd=-100};
//    struct waypoint w4 = {.pn=100, .pe=100, .pd=-100};
//    struct waypoint W[9] = {w1, w2, w3, w4, w1, w2, w3, w4, w1};

    struct controller_outputs act_out = DEFAULT_CONTROLLER_OUTPUTS;
    float est_state_arr[16] = {0};

    // initialize board, serial, and radio
    Board_init(); //board configuration
    Serial_init(); //start debug terminal (USB)
    Sys_timer_init(); //start the system timer
    /*small delay to get all the subsystems time to get online*/
    while (cur_time < warmup_time) {
        cur_time = Sys_timer_get_msec();
    }

    //Initialize communication and actuators
    Radio_serial_init(); //start the radios
    RCRX_init(); //initialize the radio control system
    RC_channels_init(); //set channels to midpoint of RC system
    RC_servo_init(RC_SERVO_TYPE, AIL_PWM); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, ELE_PWM); // start the servo subsystem
    RC_servo_init(ESC_UNIDIRECTIONAL_TYPE, THR_PWM); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, RUD_PWM); // start the servo subsystem

    printf("\r\n\nDemo application %s, %s \r\n", __DATE__, __TIME__);

    initStabilityController();
    initStateEstimation();

    uint8_t heartbeat_sensor = 0;
    
//    inputWaypoints(W, 9);

    while (1) {
        cur_time = Sys_timer_get_msec();
        checkHILEvents();
        check_RC_events(); // uncomment this if you don't want to use actuators

        // publish heartbeat
        if (cur_time - heartbeat_start_time >= HEARTBEAT_PERIOD) {
            heartbeat_start_time = cur_time; //reset the timer
            // publish_heartbeat(autonomous_status_flag);
            publish_heartbeat(heartbeat_sensor);
            heartbeat_sensor = 0;
        }

        // move actuators either using RC or autopilot commands
        if (cur_time - control_start_time >= CONTROL_PERIOD) {
            control_start_time = cur_time; //reset control loop timer
            set_control_output(act_out); // set actuator outputs
        }

#ifdef HIL_STATE_TESTING // using true states
        // only update controller when a new state is received from the sim
        if (new_state_received == TRUE) {
            act_out = updateStabilityController(getTrueState(), X_c, h_c, Va_c);
            publish_actuator_cmds(act_out);
            // set_control_output(act_out); // set actuator outputs irl
            new_state_received = FALSE;
        }

#else // using simulated sensors and state estimation
        if ((ready_sensors_flag != 0) && waypoint_seq_received){
            wakeup_sim_flag = FALSE;
            heartbeat_sensor = 6;
            updateStateEstimation(IMU_data, GPS_out, Baro_data, 0.0, ready_sensors_flag);
            current_state = getCurrentState();

            waypointFollowingUpdateReferenceCommands(current_state, &X_c, &h_c);
                    
            act_out = updateStabilityController(current_state, X_c, h_c, Va_c);

            // send estimated states to the simulator
            getCurrentStateArray(est_state_arr);
            // add act_out actuator commands to est_state_arr array
            est_state_arr[0] = act_out.d_aileron;
            est_state_arr[1] = act_out.d_elevator;
            est_state_arr[2] = act_out.d_rudder;
            est_state_arr[3] = act_out.d_throttle;
            est_state_arr[13] = ready_sensors_flag & GPS_AVAILABLE;
            est_state_arr[14] = gps_received_check;

            publish_hil_act_controls(HIL_SEND_STATE_TO_SIM, est_state_arr);

            ready_sensors_flag = 0;
        }
#endif
    }
}
