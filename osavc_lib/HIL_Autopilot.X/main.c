/*
 * File:   main.c
 * Author: Aaron Hunter
 * Brief:  application for Open Logger 
 * project
 * Created on April 10, 2023 
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

//Standard Libraries
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "xc.h"
#include <math.h>
#include "inc/Board.h"

//OSAVC - Helper Libraries
#include "inc/SerialM32.h"
#include "inc/System_timer.h"
#include "inc/lib/common/mavlink.h"
#include "inc/p32mx795f512l.h"

//Sensors
//IMU
#include "inc/ICM_20948.h"
#include "inc/ICM_20948_registers.h"  //register definitions for the device
#include "inc/NEO_M8N.h"
#include "DPS310.h"

//Autopilot 
#include "AirframeConstants.h"
#include "PD.h"
#include "PI.h"
#include "PID.h"
#include "StabilityController.h"
#include "StateEstimation.h"

//Communication Method
#include "inc/Radio_serial.h"
#include "inc/RC_RX.h"
#include "inc/RC_servo.h"
#include "SerialM32.h" // used for SIL b/w autopilot and 163 sim




/*******************************************************************************
 * #DEFINES                                                                    *
 ******************************************************************************/
//Helper Macros
#define DELAY(x)    {int wait; for (wait = 0; wait <= x; wait++) {asm("nop");}}
#define A_BIT       18300
#define A_LOT       183000
#define BUFFER_SIZE 2048

//Timing Macros
#define HEARTBEAT_PERIOD 1000 //1 sec interval for hearbeat update
#define FREQUENCY_PERIOD 1000
#define ACTUATOR_PERIOD 10000 //10s to characterize
#define SENSOR_PERIOD 20
#define PUBLISH_PERIOD 50 // Period for publishing data (msec)

//Sensor Macros
#define INTERFACE_MODE IMU_SPI_MODE
#define RAW 1
#define SCALED 2

//Communication Macros
#define CTS_2_USEC 1
#define SAFE_HIGH 1790  // normal val 1809
#define SAFE_LOW 1000    // normal val 992
#define KILL_HIGH 1790
#define KILL_LOW 1000
#define GET_CHAR() get_char()
#define PUT_CHAR(c) put_char(c)
#define DATA_AVAILABLE() data_available()

//Actuator Macros
#define AIL_PWM SERVO_PWM_1
#define ELE_PWM SERVO_PWM_2
#define THR_PWM SERVO_PWM_3
#define RUD_PWM SERVO_PWM_4

//HIL Macros
#define HIL_DEFAULT 0
// used to determine what data is being sent in an HIL actuator commands packet
#define HIL_SEND_GAINS_TO_SIM 1 // send received gains back to sim as ack
#define HIL_RECEIVE_STATE 2 // receive state data from sim
#define HIL_RECEIVE_COMMANDS 3 // receive commanded course and height
#define HIL_RECEIVE_SENSORS 4 // receive sensor data from sim
#define HIL_SEND_STATE_TO_SIM 5 // send estimated states to sim
#define HIL_ACK_COMMANDS 6 // acknowledge new commands
#define STATE_EST_RECEIVE_GAINS 7 // receive state estimation gains from sim
#define HIL_ACK_STATE_EST_GAINS 8 // acknowledge received state estimation gains
#define STABILITY_CONTROLLER_RECEIVE_GAINS 9 // receives stability controller gains from sim
#define HEARTBEAT_HIL_ACTUATOR_ERROR 255 



//Autopilot mode flags
#define MANUAL_STATUS_FLAG 7
#define AUTO_STATUS_FLAG 5



/*******************************************************************************
 * VARIABLES                                                                   *
 ******************************************************************************/


//MAVLINK Variables
mavlink_system_t mavlink_system = {
    1, // System ID (1-255)
    MAV_COMP_ID_AUTOPILOT1 // Component ID (a MAV_COMPONENT value)
};

//Communication Method
enum RC_channels {
    AIL,
    ELE,
    THR,
    RUD,
    HASH,
    SAFE,
    KILL,
    SWITCH_A,
    SWITCH_B,
    SWITCH_C,
    SWITCH_D,
    SWITCH_E
}; //map to the car controls from the RC receiver

enum mav_output_type {
    USB,
    RADIO
};

enum flight_mode {
    MANUAL, AUTONOMOUS
};

static enum flight_mode mode = MANUAL;
const uint16_t RC_raw_fs_scale = RC_RAW_TO_FS;

RCRX_channel_buffer RC_channels[CHANNELS];
static uint8_t pub_RC_servo = FALSE;
static uint8_t pub_RC_signals = TRUE;
static uint16_t ail_prev = 0;
static uint16_t ele_prev = 0;
static uint16_t thr_prev = 0;
static uint16_t rud_prev = 0;
static int safe = 69;
static int kill_plane = 420;
static uint8_t is_data_valid = FALSE;
static uint8_t is_data_new = FALSE;
static uint8_t logOutBuffer[BUFFER_SIZE]; 
//Sensor Variables
struct IMU_out IMU_raw = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //container for raw IMU data
struct IMU_out IMU_scaled = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //container for scaled IMU data
static struct IMU_out IMU_data; //container for IMU data from the HIL simulator

/* IMU data arrays */
float gyro_cal[MSZ] = {0, 0, 0};
float acc_cal[MSZ] = {0, 0, 0};
float mag_cal[MSZ] = {0, 0, 0};

/*IMU data*/
int IMU_err = 0;
struct IMU_out IMU_data_raw = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
struct IMU_out IMU_data_scaled = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
/* test values: mag cal from Dorveaux*/
//Aaron's thing
//float A_acc[MSZ][MSZ] = {
//    6.01180201773358e-05, -6.28352073406424e-07, -3.91326747595870e-07,
//    -1.18653342135860e-06, 6.01268083773005e-05, -2.97010157797952e-07,
//    -3.19011230800348e-07, -3.62174516629958e-08, 6.04564465269327e-05
//};
float A_acc[MSZ][MSZ] = {
   5.88931773891380e-05,    -3.40087724536171e-07,    -2.21250367678547e-07,
    -2.14849934547173e-07,    5.77741624302809e-05,    -2.38134184919193e-07,
    -3.17815515376590e-07,    -4.13416865644758e-07,    5.82643536075151e-05
};
float A_mag_fl[3][3] = {
    0.00336730429836004,    4.04769849350429e-07,    -2.15029629397491e-05,
    -1.17610824162755e-05,    0.00327613186929661,    -4.12516874120987e-05,
    3.07540892216210e-05,    -3.41842355787250e-05,    0.00321920085094393
};
float b_acc[MSZ] = {0.0286685331274404, 0.0237008031916020
, 0.00573737607323846};
float b_mag_fl[3] = {-0.112169627617264, -0.146234392448288, 1.17491873266617};
float A_test[MSZ][MSZ];
float b_test[MSZ];


/* GPS data */
struct GPS_data GPSdata;
static struct GPS_data GPS_out;

/*Baro Data*/ 
struct BARO_out BARO_scaled = {0, 0}; //from actual barometer data
static float Baro_data;   //for simulated barometer data

//Data Logger Variables
const float deg2rad = M_PI / 180.0;
static uint8_t imuReady = FALSE;
static uint8_t baroReady = FALSE;
static uint8_t gpsReady = FALSE;
static uint8_t imu_buffer[BUFFER_SIZE];
static uint8_t gps_buffer[BUFFER_SIZE];
static uint8_t baro_buffer[BUFFER_SIZE];
static uint8_t log_buffer[BUFFER_SIZE];
int counter = 0;
static int printCount = 0;
static float all_sensors[16] = {0};

//Stability Controller Variables
/*Sim Gains*/
static struct controller_gains simulator_gains = DEFAULT_CONTROLLER_GAINS;
/*Sim State*/
static struct vehicle_state simulator_state = DEFAULT_VEHICLE_STATE;
/*Sim Controller Commands*/
static float X_c = 0;
static float h_c = 100;
static float Va_c = 20;

// new state flag, used to command controller to only update when new state data is received
static uint8_t new_state_received= FALSE;
static uint8_t ready_sensors_flag = 0;
static uint8_t first_gps_flag = 0;


//Control output test
static int controlCount = 0;
static struct controller_outputs act_out = DEFAULT_CONTROLLER_OUTPUTS;



/*******************************************************************************
 * TYPEDEFS                                                                    *
 ******************************************************************************/


/*******************************************************************************
 * FUNCTION PROTOTYPES                                                         *
 ******************************************************************************/
/**
 * @function RC_channels_init(void)
 * @param none
 * @brief set all RC channels to RC_RX_MID_COUNTS
 * @author Aaron Hunter
 */
void RC_channels_init(void);
/**
 * @function check_RC_events(void)
 * @param none
 * @brief checks for RC messages and stores data in RC channel buffer
 * @author Aaron Hunter
 */
void check_RC_events();
/**
 * @function check_IMU_events(void)
 * @param none
 * @brief detects when IMU SPI transaction completes and then publishes data over Mavlink
 * @author Aaron Hunter
 */
void check_IMU_events(void);
/*
 * @function check_GPS_events(void)
 * @param none
 * @brief detect when GPS transaction completes  
 * @author ccuyos
 */
void check_GPS_events(void);
/*
 * @function check_BARO_EVENTS(void)
 * @param none
 * @brief detect when barometer transcation completes
 * @author ccuyos 
 */
void check_BARO_events(void);
/**
 * @function check_HIL_events(void)
 * @param none
 * @returns none
 * @brief looks for messages sent over the radio serial port to OSAVC
 * @author Margaret Silva
 */
void check_HIL_events(void);


void CalibrateSensors(void);
/**
 * @function publish_RC_signals_raw(void)
 * @param none
 * @brief scales raw RC signals
 * @author Aaron Hunter
 */
void publish_RC_signals_raw(void);
/**
 * @Function publish_heartbeat(uint8_t dest)
 * @param dest, either USB or RADIO
 * @brief publishes heartbeat message 
 * @return none
 * @author Aaron Hunter
 */
// send actuator commands to the simulator
void publish_actuator_cmds(struct controller_outputs actuators);
void publish_heartbeat(uint8_t status, uint8_t dest);
/**
 * @Function publish_h(uint8_t param_id[16], uint8_t dest)
 * @param parameter ID
 * @param dest, USB or RADIO
 * @brief invokes mavlink helper to send out stored parameter 
 * @author aaron hunter
 */
void publish_parameter(uint8_t param_id[16], uint8_t dest);

/**
 * @Function publish_status_text(void)
 * @param none
 * @brief invokes mavlink helper to send out status text
 * @author Margaret Silva
 */
void publish_status_text(char text[50]);


/**
 * @Function publish_hil_act_controls(uint8_t mode, float outputs[16])
 * @param mode - flag that determines what info is transmitted
 *      outputs - data to be sent over mavlink
 * @brief invokes mavlink helper to generate hil act controls and sends out via the radio
 * @author Margaret Silva
 */
void publish_hil_act_controls(uint64_t flags, float outputs[16]);
/**
 * @function publish_IMU_data()
 * @param data_type RAW or SCALED
 * @brief reads module level IMU data and publishes over radio serial in Mavlink
 * @author Aaron Hunter
 */
void publish_IMU_data(uint8_t data_type, uint8_t dest);
/**
 * @Function mavprint(char msg_buffer[], int8_t msg_length, int8_t output);
 * @param msg_buffer, string of bytes to send to receiver
 * @param msg_length, length of msg_buffer
 * @param output, either USB or RADIO, which peripheral to send the message from
 * @return SUCCESS or ERROR
 */
int8_t mavprint(uint8_t msg_buffer[], uint8_t msg_length, uint8_t output);
/**
 * @Function calc_pw(uint16_t raw_counts)
 * @param raw counts from the radio transmitter (11 bit unsigned int)
 * @return pulse width in microseconds
 * @brief converts the RC input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author aahunter
 * @modified <Your Name>, <year>.<month>.<day> <hour> <pm/am> */
static uint16_t calc_pw(uint16_t raw_counts);
/**
 * @Function calc_pw_from_angle(float angle, float min, float max)
 * @param angle - desired angle (radians)
 * @param min - minimum angle (radians)
 * @param max - maximum angle (radians)
 * @return pulse width in microseconds
 * @brief converts the angle input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author Margaret Silva
 */
uint16_t calc_pw_from_angle(float angle, float slope, float intercept);
/**
 * @Function calc_pw_from_throttle(float throttle)
 * @param throttle - throttle command
 * @return pulse width in microseconds
 * @brief converts the throttle input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author Margaret Silva
 */
uint16_t calc_pw_from_throttle(float throttle); 
/**
 * @Function set_control_output(void)
 * @param control - controller output struct
 * @return none
 * @brief converts RC input signals to pulsewidth values and sets the actuators
 * (servos and ESCs) to those values
 * @author Aaron Hunter
 */
void set_control_output(struct controller_outputs controls);
/*DATA LOGGER FUNCTIONS*/
/**
 * @Function log_IMU(void)
 * @param time, time in msec provided by the microcontroller
 * @return none
 * @brief creates an IMU packet frame for the Sparkfun OpenLogger 
 * @author ccuyos
 */
void log_IMU(uint32_t time);
/**
 * @Function log_GPS(void)
 * @param time, time in msec provided by the microcontroller
 * @return none
 * @brief creates an GPS packet frame for the Sparkfun OpenLogger 
 * @author ccuyos
 */
void log_GPS(uint32_t time);
/**
 * @Function log_BARO(void)
 * @param time, time in msec provided by the microcontroller
 * @return none
 * @brief creates an BARO packet frame for the Sparkfun OpenLogger 
 * @author ccuyos
 */
void log_BARO(uint32_t time);
/**
 * @Function log_sensors(void)
 * @param time, time in msec provided by the microcontroller
 * @return none
 * @brief creates an sensor packet frame for the Sparkfun OpenLogger 
 * @author ccuyos
 */
void log_sensors(uint32_t time);

void ConvertSensors(void);





/*******************************************************************************
 * ACTUATOR FUNCTIONS                                                                   *
 ******************************************************************************/
/**
 * @function check_RC_events(void)
 * @param none
 * @brief checks for RC messages and stores data in RC channel buffer
 * @author Aaron Hunter
 */
void check_RC_events() {
    if (RCRX_new_cmd_avail()) {
        RCRX_get_cmd(RC_channels);
    }
}
/**
 * @function RC_channels_init(void)
 * @param none
 * @brief set all RC channels to RC_RX_MID_COUNTS
 * @author Aaron Hunter
 */
void RC_channels_init(void) {
    uint8_t i;
    for (i = 0; i < CHANNELS; i++) {
        RC_channels[i] = RC_RX_MID_COUNTS;
    }
}
///**
// * @function publish_RC_signals(void)
// * @param none
// * @brief scales raw RC signals into +/- 10000
// * @author Aaron Hunter
// */
void publish_RC_signals(void) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t RC_port = 0; //first 8 channels 
    int16_t scaled_channels[CHANNELS];
    uint8_t rssi = 255; //unknown--may be able to extract from receiver
    for (index = 0; index < CHANNELS; index++) {
        scaled_channels[index] = (RC_channels[index] - RC_RX_MID_COUNTS) * RC_raw_fs_scale;
    }
    mavlink_msg_rc_channels_scaled_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            Sys_timer_get_msec(),
            RC_port,
            scaled_channels[0],
            scaled_channels[1],
            scaled_channels[2],
            scaled_channels[3],
            scaled_channels[4],
            scaled_channels[5],
            scaled_channels[6],
            scaled_channels[7],
            rssi);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        Radio_put_char(msg_buffer[index]);
    }
}
/**
 * @function publish_RC_signals_raw(void)
 * @param none
 * @brief scales raw RC signals
 * @author Aaron Hunter
 */
void publish_RC_signals_raw(void) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t RC_port = 0; //first 8 channels 
    uint8_t rssi = 255; //unknown--may be able to extract from receiver
    mavlink_msg_rc_channels_raw_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            Sys_timer_get_msec(),
            RC_port,
            RC_channels[0],
            RC_channels[1],
            RC_channels[2],
            RC_channels[3],
            RC_channels[4],
            RC_channels[5],
            RC_channels[6],
            RC_channels[7],
            rssi);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        Radio_put_char(msg_buffer[index]);
    }
}
/**
 * @function publish_actuator_cmds(void)
 * @param none
 * @brief publishes actuator commands to the HIL simulator
 * @author Margaret Silva
 */
void publish_actuator_cmds(struct controller_outputs actuators) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_hil_controls_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            0, // time (unused)
            actuators.d_aileron,
            actuators.d_elevator,
            actuators.d_rudder,
            actuators.d_throttle,
            0.0, 0.0, 0.0, 0.0, // unused aux commands
            0,
            0
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}
/**
 * @Function calc_pw(uint16_t raw_counts)
 * @param raw counts from the radio transmitter (11 bit unsigned int)
 * @return pulse width in microseconds
 * @brief converts the RC input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author aahunter
 * @modified <Your Name>, <year>.<month>.<day> <hour> <pm/am> */
uint16_t calc_pw(uint16_t raw_counts) {
    int16_t normalized_pulse; //converted to microseconds and centered at 0
    uint16_t pulse_width; //servo output in microseconds
    normalized_pulse = (raw_counts - RC_RX_MID_COUNTS) >> CTS_2_USEC;
    pulse_width = normalized_pulse + RC_SERVO_CENTER_PULSE;
    return pulse_width;
}

/**
 * @Function calc_pw_from_angle(float angle, float min, float max)
 * @param angle - desired angle (radians)
 * @param min - minimum angle (radians)
 * @param max - maximum angle (radians)
 * @return pulse width in microseconds
 * @brief converts the angle input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author Margaret Silva
 */
uint16_t calc_pw_from_angle(float angle, float slope, float intercept) {
    float angle_degrees = (angle * 180.0) / M_PI;
    uint16_t pulse_width = (uint16_t) (slope * angle_degrees + intercept);

    if (pulse_width > RC_SERVO_MAX_PULSE) {
        pulse_width = RC_SERVO_MAX_PULSE;
    } else if (pulse_width < RC_SERVO_MIN_PULSE) {
        pulse_width = RC_SERVO_MIN_PULSE;
    }

    return pulse_width;
}


uint16_t calc_pw_from_throttle(float throttle) {
    float limiter = 0.25;
    uint16_t pulse_width = (uint16_t) (limiter * throttle * (RC_SERVO_MAX_PULSE - RC_SERVO_MIN_PULSE) + RC_SERVO_MIN_PULSE);
    
    if (pulse_width > RC_SERVO_MAX_PULSE) {
        pulse_width = RC_SERVO_MAX_PULSE;
    } else if (pulse_width < RC_SERVO_MIN_PULSE) {
        pulse_width = RC_SERVO_MIN_PULSE;
    }
    return pulse_width;
}

/**
 * @Function set_control_output(struct controller_outputs controls)
 * @param controls, generated by the stability controller
 * @return none
 * @brief converts control inputs into servo outputs for each control surface
 * @author Margaret Silva
 */
void set_control_output(struct controller_outputs controls) {

    char message[BUFFER_SIZE];
    uint8_t msg_len = 0;
    int index;
    int hash, ail, ele, thr, rud;
    int hash_check;
    const int tol = 4;
    int INTOL;

    /* Check switch state*/
    /*if switch state == up*/
    /* Manual control enabled */
    /* if switch state == down*/
    /* Autonomous mode enabled*/
    /* get RC commanded values*/

    safe = RC_channels[SAFE]; //1807 when ON, 240 when OFF
    hash = RC_channels[HASH]; //1807 when ON, 240 when OFF
    ail = RC_channels[AIL];
    ele = RC_channels[ELE];
    thr = RC_channels[THR];
    rud = RC_channels[RUD];
    kill_plane = RC_channels[KILL]; //Kill switch to kill throttle
    
    

    hash_check = (thr >> 2) + (ail >> 2) + (ele >> 2) + (rud >> 2);
    if (abs(hash_check - hash) <= tol) {
        INTOL = TRUE;
        // check if mode switching occurs
        if (safe <= SAFE_LOW) {
            mode = MANUAL;
        } else if (safe >= SAFE_HIGH) {
            mode = AUTONOMOUS;
        }

        /* send commands to motor outputs*/
        if (mode == MANUAL) {
            RC_servo_set_pulse(calc_pw(ail), AIL_PWM);  
            RC_servo_set_pulse(calc_pw(ele), ELE_PWM);
            if (kill_plane <= KILL_LOW){
                RC_servo_set_pulse(calc_pw(thr), THR_PWM);
            }
            else{
                 RC_servo_set_pulse(RC_SERVO_MIN_PULSE, THR_PWM);
            }
            RC_servo_set_pulse(calc_pw(rud), RUD_PWM); 
            
        } else {
#ifdef USE_AUTONOMOUS_CONTROLS  
            RC_servo_set_pulse(calc_pw_from_angle(controls.d_aileron, AILERON_SLOPE, AILERON_INTERCEPT), AIL_PWM);
            RC_servo_set_pulse(calc_pw_from_angle(-controls.d_elevator, ELEVATOR_SLOPE, ELEVATOR_INTERCEPT), ELE_PWM);
            
            if (kill_plane <= KILL_LOW){
                RC_servo_set_pulse(calc_pw_from_throttle(controls.d_throttle), THR_PWM);
            }
            else{
                RC_servo_set_pulse(RC_SERVO_MIN_PULSE, THR_PWM);
            }
            RC_servo_set_pulse(RC_SERVO_MIN_PULSE, THR_PWM); // turn off throttle
            RC_servo_set_pulse(calc_pw_from_angle(controls.d_rudder, RUDDER_SLOPE, RUDDER_INTERCEPT), RUD_PWM);
#endif
        }

    } else {
        INTOL = FALSE;
//        msg_len = sprintf(message, "%d, %d, %d, %d, %d, %d, %d \r\n", thr, ail, ele, rud, hash, hash_check, INTOL);
//        for (index = 0; index < msg_len; index++) {
//            Radio_put_char(message[index]);
//        }
    }

}
/*******************************************************************************
 * COMMUNICATION FUNCTIONS                                                                   *
 ******************************************************************************/
/**
 * @function checkHILEvents(void)
 * @param none
 * @returns none
 * @brief looks for messages sent over the radio serial port to OSAVC
 * @author Margaret Silva
 */
void check_HIL_events() {
    uint8_t channel = MAVLINK_COMM_0;
    uint8_t msg_byte;
    mavlink_message_t msg_rx;
    mavlink_status_t msg_rx_status;

    mavlink_hil_actuator_controls_t hil_sim_state;
    mavlink_hil_sensor_t hil_sensors_imu;
    mavlink_altitude_t hil_sensors_gps;
//    mavlink_named_value_float_t named_float;
    mavlink_statustext_t status_text;
    // mavlink_hil_controls_t hil_controls_ping_pong;

    // struct controller_outputs act_out;

//    static float gains[16];

    if (DATA_AVAILABLE()) {
        msg_byte = GET_CHAR();
        if (mavlink_parse_char(channel, msg_byte, &msg_rx, &msg_rx_status)) {
            switch (msg_rx.msgid) {
                case MAVLINK_MSG_ID_HIL_ACTUATOR_CONTROLS:
                    // used to pass in state data from simulator to controller
                    mavlink_msg_hil_actuator_controls_decode(&msg_rx, &hil_sim_state);
                    switch (hil_sim_state.flags) {
                        case HIL_RECEIVE_STATE:
                            setTrueState(hil_sim_state.controls);
                            new_state_received = TRUE;
                            break;
                        case HIL_RECEIVE_COMMANDS:
                            X_c = hil_sim_state.controls[0];
                            h_c = hil_sim_state.controls[1];
                            Va_c = hil_sim_state.controls[2];
                            publish_hil_act_controls(HIL_ACK_COMMANDS, hil_sim_state.controls);
                            break;
                        case HIL_RECEIVE_SENSORS:
                            // check to see if gps has updated or not
                            if (hil_sim_state.controls[15]) {
                                // GPS 
                                GPS_out.lat = hil_sim_state.controls[10];
                                GPS_out.lon = hil_sim_state.controls[11];
                                // note: gps data struct is currently missing alt field
                                // GPS_out.spd = hil_sim_state.controls[12];
                                GPS_out.spd = hil_sim_state.controls[13];
                                GPS_out.cog = hil_sim_state.controls[14];
                                ready_sensors_flag = ready_sensors_flag | GPS_AVAILABLE;
                                first_gps_flag = GPS_FIRST_DATA_RECEIVED;
                            }
                            // accelerometer
                            IMU_data.acc.x = hil_sim_state.controls[0];
                            IMU_data.acc.y = hil_sim_state.controls[1];
                            IMU_data.acc.z = hil_sim_state.controls[2];
                            // gyro
                            IMU_data.gyro.x = hil_sim_state.controls[3];
                            IMU_data.gyro.y = hil_sim_state.controls[4];
                            IMU_data.gyro.z = hil_sim_state.controls[5];
                            // mag
                            IMU_data.mag.x = hil_sim_state.controls[6];
                            IMU_data.mag.y = hil_sim_state.controls[7];
                            IMU_data.mag.z = hil_sim_state.controls[8];
                            // baro
                            Baro_data = hil_sim_state.controls[9];
                            // set flags
                            ready_sensors_flag = ready_sensors_flag | IMU_AVAILABLE;
                            ready_sensors_flag = ready_sensors_flag | BARO_AVAILABLE;
                            ready_sensors_flag = ready_sensors_flag | first_gps_flag;
                            break;
                        case STATE_EST_RECEIVE_GAINS: // state estimation gains
                            setStateEstimationGains(hil_sim_state.controls);
                            publish_hil_act_controls(HIL_ACK_STATE_EST_GAINS, hil_sim_state.controls);
                            break;
                        case STABILITY_CONTROLLER_RECEIVE_GAINS: // controller gains
                            initController();
                            initStateEstimation();
                            first_gps_flag = 0;
                            setControllerGains(hil_sim_state.controls);
                            publish_hil_act_controls(HIL_SEND_GAINS_TO_SIM, hil_sim_state.controls);
                            break;
                        default:
                            publish_heartbeat(HEARTBEAT_HIL_ACTUATOR_ERROR, USB);
                            break;
                    }
                    break;
                case MAVLINK_MSG_ID_HIL_SENSOR:
                    // used to pass in simulated IMU and baro data from sim to controller
                    mavlink_msg_hil_sensor_decode(&msg_rx, &hil_sensors_imu);
                    // accelerometer
                    IMU_data.acc.x = hil_sensors_imu.xacc;
                    IMU_data.acc.y = hil_sensors_imu.yacc;
                    IMU_data.acc.z = hil_sensors_imu.zacc;
                    // gyro
                    IMU_data.gyro.x = hil_sensors_imu.xgyro;
                    IMU_data.gyro.y = hil_sensors_imu.ygyro;
                    IMU_data.gyro.z = hil_sensors_imu.zgyro;
                    // mag
                    IMU_data.mag.x = hil_sensors_imu.xmag;
                    IMU_data.mag.y = hil_sensors_imu.ymag;
                    IMU_data.mag.z = hil_sensors_imu.zmag;
                    // baro
                    Baro_data = hil_sensors_imu.abs_pressure;
                    // set flags
                    ready_sensors_flag = ready_sensors_flag | IMU_AVAILABLE;
                    ready_sensors_flag = ready_sensors_flag | BARO_AVAILABLE;
                    break;
                case MAVLINK_MSG_ID_ALTITUDE: // use to receive GPS sensor data
                    mavlink_msg_altitude_decode(&msg_rx, &hil_sensors_gps);
                    GPS_out.lat = hil_sensors_gps.altitude_monotonic;
                    GPS_out.lon = hil_sensors_gps.altitude_amsl;
                    // note: gps data struct is currently missing alt field
                    // GPS_out.spd = hil_sensors_gps.altitude_local;
                    GPS_out.spd = hil_sensors_gps.altitude_relative;
                    GPS_out.cog = hil_sensors_gps.altitude_terrain;
                    ready_sensors_flag = ready_sensors_flag | GPS_AVAILABLE;
                    break;
                case MAVLINK_MSG_ID_HIL_CONTROLS:
                    // used for debug to validate that hil controls are being sent properly
                    break;
                    // mavlink_msg_hil_controls_decode(&msg_rx, &hil_controls_ping_pong);
                    // act_out.d_aileron = hil_controls_ping_pong.roll_ailerons;
                    // act_out.d_elevator = hil_controls_ping_pong.pitch_elevator;
                    // act_out.d_rudder = hil_controls_ping_pong.yaw_rudder;
                    // act_out.d_throttle = hil_controls_ping_pong.throttle;
                    break;
                case MAVLINK_MSG_ID_STATUSTEXT:
                    // debug message
                    mavlink_msg_statustext_decode(&msg_rx, &status_text);
                    if (strcmp(status_text.text, "I AM SIM") == 0) {
                        publish_status_text("I AM AUTOPILOT");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

/**
 * @Function publish_heartbeat(void)
 * @param none
 * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
 * @author aaron hunter
 */
void publish_heartbeat(uint8_t status, uint8_t dest) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t mode = MAV_MODE_FLAG_MANUAL_INPUT_ENABLED | MAV_MODE_FLAG_SAFETY_ARMED;
    uint32_t custom = 0;
    mavlink_msg_heartbeat_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC,
            mode,
            custom,
            status);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    mavprint(msg_buffer, msg_length, dest);
//    for (index = 0; index < msg_length; index++) {
//        PUT_CHAR(msg_buffer[index]);
//    }
}

/*******************************************************************************
 * SENSOR FUNCTIONS                                                                   *
 ******************************************************************************/
/**
 * @function check_IMU_events(void)
 * @param none
 * @brief detects when IMU SPI transaction completes and then publishes data over Mavlink
 * @author Aaron Hunter
 */
void check_IMU_events(void) {
    if (IMU_is_data_ready() == TRUE) {
        imuReady = TRUE;
        IMU_get_raw_data(&IMU_data_raw);
        IMU_get_norm_data(&IMU_scaled);
    }
}

void CalibrateSensors(void){
    //IMU_get_norm_data(&IMU_scaled);
    //Calibrating accelerometer
    IMU_scaled.acc.x = ((IMU_data_raw.acc.x - BIAS_ACCX) * SCALE_ACCX) * ms2;
    IMU_scaled.acc.y = ((IMU_data_raw.acc.y - BIAS_ACCY) * SCALE_ACCY) * ms2;
    IMU_scaled.acc.z = ((IMU_data_raw.acc.z - BIAS_ACCZ) * SCALE_ACCZ) * ms2;
    
    //Calibrating gyros
    float angularX = (IMU_data_raw.gyro.x/CONV_GYRO) - OFFSET_GYROX;
    float angularY = (IMU_data_raw.gyro.y/CONV_GYRO) - OFFSET_GYROY;
    float angularZ = (IMU_data_raw.gyro.z/CONV_GYRO) - OFFSET_GYROZ;
    IMU_scaled.gyro.x += angularX * dT_GYRO - DRIFT_GYROX;
    IMU_scaled.gyro.y += angularY * dT_GYRO - DRIFT_GYROY;
    IMU_scaled.gyro.z += angularZ * dT_GYRO - DRIFT_GYROZ;
    
  
    //Calibrating magnetometer
    IMU_scaled.mag.x = IMU_scaled.mag.x * 22.6338;
    IMU_scaled.mag.y = IMU_scaled.mag.y * 5.1879;
    IMU_scaled.mag.z = IMU_scaled.mag.z * 41.2284;
 
}
/*
 * @function check_GPS_events(void)
 * @param none
 * @brief detect when GPS transaction completes  
 * @author ccuyos
 */
void check_GPS_events(void){
    if (GPS_is_msg_avail() == TRUE) {
        GPS_parse_stream();
    }
    if (GPS_is_data_avail() == TRUE){
        GPS_get_data(&GPSdata);
        gpsReady = TRUE;
    }
}
/*
 * @function check_BARO_EVENTS(void)
 * @param none
 * @brief detect when barometer transcation completes
 * @author ccuyos 
 */
void check_BARO_events(void){
    if (Barometer_is_data_ready()){
        Barometer_get_scaled_data(&BARO_scaled);
        baroReady = TRUE;
        ready_sensors_flag = ready_sensors_flag | BARO_AVAILABLE;
    }
}
/**
 * @function publish_IMU_data()
 * @param none
 * @brief reads module level IMU data and publishes over radio serial in Mavlink
 * @author Aaron Hunter
 */
void publish_IMU_data(uint8_t data_type, uint8_t dest) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint8_t IMU_id = 0;
    if (data_type == RAW) {
        mavlink_msg_raw_imu_pack(mavlink_system.sysid,
                mavlink_system.compid,
                &msg_tx,
                Sys_timer_get_usec(),
                (int16_t) IMU_raw.acc.x,
                (int16_t) IMU_raw.acc.y,
                (int16_t) IMU_raw.acc.z,
                (int16_t) IMU_raw.gyro.x,
                (int16_t) IMU_raw.gyro.y,
                (int16_t) IMU_raw.gyro.z,
                (int16_t) IMU_raw.mag.x,
                (int16_t) IMU_raw.mag.y,
                (int16_t) IMU_raw.mag.z,
                IMU_id,
                (int16_t) IMU_raw.temp
                );
    } else if (data_type == SCALED) {
        mavlink_msg_highres_imu_pack(mavlink_system.sysid,
                mavlink_system.compid,
                &msg_tx,
                Sys_timer_get_usec(),
                (float) IMU_scaled.acc.x,
                (float) IMU_scaled.acc.y,
                (float) IMU_scaled.acc.z,
                (float) IMU_scaled.gyro.x,
                (float) IMU_scaled.gyro.y,
                (float) IMU_scaled.gyro.z,
                (float) IMU_scaled.mag.x,
                (float) IMU_scaled.mag.y,
                (float) IMU_scaled.mag.z,
                0.0, //no pressure
                0.0, //no diff pressure
                0.0, //no pressure altitude
                (float) IMU_scaled.temp,
                0, //bitfields updated
                IMU_id
                );
    }
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    mavprint(msg_buffer, msg_length, dest);
}
/**
 * @Function log_IMU(void)
 * @param time, time in msec provided by the microcontroller
 * @return none
 * @brief creates an IMU packet frame for the Sparkfun OpenLogger 
 * @author ccuyos
 */
void log_IMU(uint32_t time){
     if (imuReady){
         imuReady = FALSE;
        sprintf(imu_buffer, ",%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%0.1f,%x",
                (float) IMU_scaled.acc.x, (float) IMU_scaled.acc.y, (float) IMU_scaled.acc.z,
                (float) IMU_scaled.gyro.x, (float) IMU_scaled.gyro.y, (float) IMU_scaled.gyro.z,
                (float) (IMU_scaled.mag.x), (float) (IMU_scaled.mag.y), (float) (IMU_scaled.mag.z),
                (float) 0.0, 0x00);
     }
     else{
         sprintf(imu_buffer, "");
     }
}
/**
 * @Function log_GPS(void)
 * @param time, time in msec provided by the microcontroller
 * @return none
 * @brief creates an GPS packet frame for the Sparkfun OpenLogger 
 * @author ccuyos
 */
void log_GPS(uint32_t time){
    if (gpsReady){
        gpsReady = FALSE;
//        sprintf(gps_buffer, ",%0.6f,%0.6f,%3.3f,%0.6f", GPSdata.lat,
//                     GPSdata.lon, GPSdata.spd, GPSdata.cog);
            sprintf(gps_buffer, ",%0.1f,%0.1f,%3.3f,%0.6f", 0.0,
                 0.0, GPSdata.spd, GPSdata.cog);
    }
    else{
        sprintf(gps_buffer, "");
    }
}
/**
 * @Function log_BARO(void)
 * @param time, time in msec provided by the microcontroller
 * @return none
 * @brief creates an BARO packet frame for the Sparkfun OpenLogger 
 * @author ccuyos
 */
void log_BARO(uint32_t time){
    if (baroReady){
        baroReady = FALSE;
        sprintf(baro_buffer, ",%0.3f,%0.3f", BARO_scaled.calibrated_temp, BARO_scaled.calibrated_pressure);
    }
    else{
       sprintf(baro_buffer, "");
    }
    
}
/**
 * @Function log_sensors(void)
 * @param time, time in msec provided by the microcontroller
 * @return none
 * @brief creates an sensor packet frame for the Sparkfun OpenLogger 
 * @author ccuyos
 */
void log_sensors(uint32_t time){
    
    uint8_t imuAvailable = (strcmp(imu_buffer, ""));
    uint8_t gpsAvailable = (strcmp(gps_buffer, ""));
    uint8_t baroAvailable = (strcmp(baro_buffer, ""));
    
    uint8_t sensorAvailable = !(!imuAvailable) << 2 | !(!gpsAvailable) << 1 | !(!baroAvailable);
    uint8_t timeVal[BUFFER_SIZE];
    sprintf(timeVal, "%x", time);
    
    //For debugging sensors/logging
    sprintf(log_buffer, "%d,%x,%x%s%s%s\n", 0, time, sensorAvailable, imu_buffer, gps_buffer, baro_buffer);
    //sprintf(log_buffer, "%x,%x%s%s%s\n", time, sensorAvailable, imu_buffer, gps_buffer, baro_buffer);    
    mavprint(log_buffer, strlen(log_buffer), RADIO);
}

void ConvertSensors(void){
    
    /*IMU CONVERSIONS*/
    // Convert all accelerometer data into m/s^2
    IMU_data.acc.x = IMU_scaled.acc.x * ms2;
    IMU_data.acc.y = IMU_scaled.acc.y * ms2;
    IMU_data.acc.z = IMU_scaled.acc.z * ms2;
    
    //Convert all magnetometer data into nT
    IMU_data.mag.x = IMU_scaled.mag.x * nT;
    IMU_data.mag.y = IMU_scaled.mag.y * nT;
    IMU_data.mag.z = IMU_scaled.mag.z * nT;
    
    //Convert all gyroscope data into rad/sec
//    IMU_data.gyro.x = IMU_scaled.gyro.x;
//    IMU_data.gyro.y = IMU_scaled.gyro.y;
//    IMU_data.gyro.z = IMU_scaled.gyro.z;
    IMU_data.gyro.x = 0;
    IMU_data.gyro.y = 0;
    IMU_data.gyro.z = 0;
    
    /*GPS CONVERSIONS*/
    GPS_out.lat = GPSdata.lat;
    GPS_out.lon = GPSdata.lon;
    GPS_out.spd = GPSdata.spd * knots_to_ms;
    GPS_out.cog = GPSdata.cog * rad;
    
    /*BARO CONVERSIONS*/
    Baro_data = BARO_scaled.calibrated_pressure;
    
//    float store_sensors[16] = {IMU_data.acc.x, IMU_data.acc.y, IMU_data.acc.z, 
//                    IMU_data.mag.x, IMU_data.mag.y, IMU_data.mag.z, 
//                    IMU_data.gyro.x, IMU_data.gyro.y, IMU_data.gyro.z,
//                    GPS_out.lat, GPS_out.lon, GPS_out.spd, GPS_out.cog, 
//                    Baro_data};
    
//    all_sensors[0] = IMU_data.acc.x;
//    all_sensors[1] = IMU_data.acc.y;
//    all_sensors[2] = IMU_data.acc.z;
    
    all_sensors[9] = Baro_data;
    
    //memcpy(all_sensors, store_sensors, 16*sizeof(float));
    // all_sensors = store_sensors;
}


/*******************************************************************************
 * HELPER FUNCTIONS                                                                   *
 ******************************************************************************/
/**
 * @Function publish_parameter(uint8_t param_id[16])
 * @param parameter ID
 * @brief invokes mavlink helper to send out stored parameter 
 * @author aaron hunter
 */
void publish_parameter(uint8_t param_id[16], uint8_t dest) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    float param_value = 320.0; // value of the requested parameter
    uint8_t param_type = MAV_PARAM_TYPE_INT16; // onboard mavlink parameter type
    uint16_t param_count = 1; // total number of onboard parameters
    uint16_t param_index = 1; //index of this value
    mavlink_msg_param_value_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            param_id,
            param_value,
            param_type,
            param_count,
            param_index
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    mavprint(msg_buffer, msg_length, USB);
}


/**
 * @Function publish_status_text(void)
 * @param none
 * @brief invokes mavlink helper to send out status text
 * @author aaron hunter
 */
void publish_status_text(char text[50]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t severity = 5;
    uint16_t id = 0;
    uint8_t chunk_seq = 0;
    mavlink_msg_statustext_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            severity,
            text,
            id,
            chunk_seq
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}


/**
 * @Function publish_hil_act_controls(uint8_t mode, float outputs[16])
 * @param mode - flag that determines what info is transmitted
 *      outputs - data to be sent over mavlink
 * @brief Used for sending HIL actuator controls
 * @note Team also uses this function as a method for sending out huge float values
 *      The following defines are the flags used to indicate which packet is sent
 *      #define HIL_SEND_GAINS_TO_SIM 1
        #define HIL_RECEIVE_STATE 2
        #define HIL_RECEIVE_COMMANDS 3
        #define HIL_GPS_SENSOR 4 
 * @author Margaret Silva
 */
void publish_hil_act_controls(uint64_t flags, float outputs[16]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t mode = MAV_MODE_FLAG_MANUAL_INPUT_ENABLED | MAV_MODE_FLAG_SAFETY_ARMED;
    uint32_t custom = 0;
    uint8_t state = MAV_STATE_STANDBY;
    mavlink_msg_hil_actuator_controls_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            0,
            outputs,
            1,
            flags);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    uint8_t dest = USB;
    mavprint(msg_buffer, msg_length, dest);
}
/**
 * @Function mavprint(char msg_buffer[], int8_t msg_length, int8_t output);
 * @param msg_buffer, string of bytes to send to receiver
 * @param msg_length, length of msg_buffer
 * @param output, either USB or RADIO, which peripheral to send the message from
 * @return SUCCESS or ERROR
 */
int8_t mavprint(uint8_t msg_buffer[], uint8_t msg_length, uint8_t output) {
    uint8_t i;
    if (output == USB) {
        for (i = 0; i < msg_length; i++) {
            
            printCount++;
            putchar(msg_buffer[i]);
        }
    } else if (output == RADIO) {
        for (i = 0; i < msg_length; i++) {
            printCount++;
           Radio_put_char(msg_buffer[i]);
        }
    } else {
        return ERROR;
    }
//    printf("Print Count: %d\n", printCount);
//    printCount = 0;
    return SUCCESS;
}



/**
 *@function main
 *@note: Make sure to add a print statement after each sensor initialization
 *      or it breaks the function 
 */

#ifdef HIL_TESTING

int main(void) {
    // timing variables
    uint32_t cur_time = 0;
    uint32_t warmup_time = 250; //time in ms to allow subsystems to stabilize (IMU))
    uint32_t control_start_time = 0;
    uint32_t heartbeat_start_time = 0;
    uint32_t publish_start_time = 0;
    uint32_t log_start_time = 0;
    uint32_t actuator_start_time = 0;
    uint8_t index;
    uint8_t error_report = 50;
    RCRX_channel_buffer channels[CHANNELS];
    uint32_t pwm_signal = RC_SERVO_MIN_PULSE;
    char pwm_dir = 1; //0 for backward, 1 for forward
    uint8_t wakeup_sim_flag = TRUE;
    float est_state_arr[16] = {0};
    //IMU variables
    int value = 0;
    int i;
    int row;
    int col;
    int IMU_err = 0;
    
    //GPS variables
    
    //Initialization routines
    Board_init(); //board configuration
    Serial_init(); //start debug terminal (USB)
    printf("HIL_Autopilot compiled on: %s, %s \r\n", __DATE__, __TIME__);
    Sys_timer_init(); //start the system timer
    
    //Initialize sensors
    IMU_err = IMU_init(INTERFACE_MODE);
    printf("IMU Initialized Success\n");
    GPS_init();
    printf("GPS Initialized Success\n");
    Barometer_init();
    printf("Barometer Initialized Success\n");
     while (cur_time < warmup_time) {
        cur_time = Sys_timer_get_msec();
    }
    IMU_set_mag_cal(A_mag_fl, b_mag_fl);
    IMU_set_acc_cal(A_acc, b_acc);

    //Initialize communication and actuators
    Radio_serial_init(); //start the radios
    RCRX_init(); //initialize the radio control system
    RC_channels_init(); //set channels to midpoint of RC system
    RC_servo_init(RC_SERVO_TYPE, AIL_PWM); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, ELE_PWM); // start the servo subsystem
    RC_servo_init(ESC_UNIDIRECTIONAL_TYPE, THR_PWM); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, RUD_PWM); // start the servo subsystem

    // Autopilot systems inits
    initController();
    initStateEstimation();
    uint8_t heartbeat_sensor = HIL_DEFAULT;
    while (1) {
        cur_time = Sys_timer_get_msec();
        check_HIL_events(); 
        //check for all events
        check_RC_events(); //check incoming RC commands
        //Check sensors
        check_IMU_events();
        check_GPS_events();
        check_BARO_events();
        //publish control and sensor signals (20 ms)
        
        if (cur_time - control_start_time >= CONTROL_PERIOD) {
            control_start_time = cur_time; //reset control loop timer  
            set_control_output(act_out); // set actuator outputs irl
        }

        /* publish high speed sensors */
        if (cur_time - publish_start_time >= PUBLISH_PERIOD) {
            publish_start_time = cur_time; //reset publishing timer
        }        
        //publish heartbeat
        if (cur_time - heartbeat_start_time >= HEARTBEAT_PERIOD) {
            heartbeat_start_time = cur_time; //reset the timer
            if (ready_sensors_flag & GPS_AVAILABLE) {
                publish_heartbeat(HIL_RECEIVE_STATE, USB);
                ready_sensors_flag ^= GPS_AVAILABLE;
            }   
            else{
                publish_heartbeat(heartbeat_sensor, USB);
                heartbeat_sensor = HIL_DEFAULT; 
            }
        }
        if (cur_time - log_start_time >= SENSOR_PERIOD){ //every 10 ms
            IMU_start_data_acq();
            log_start_time = cur_time;
            CalibrateSensors();
        }
        
//        if (new_state_received == TRUE) {
//            
//            act_out = updateController(getCurrentState(), X_c, h_c, Va_c);
//            publish_actuator_cmds(act_out);
//            set_control_output(act_out); // set actuator outputs irl
//            new_state_received = FALSE;
//        }

        if (ready_sensors_flag != 0) {
            wakeup_sim_flag = FALSE;
            heartbeat_sensor = 6;
            updateStateEstimation(IMU_data, GPS_out, Baro_data, 0.0, ready_sensors_flag);

            act_out = updateController(getCurrentState(), X_c, h_c, Va_c);

            // send estimated states to the simulator
            getCurrentStateArray(est_state_arr);
            // add act_out actuator commands to est_state_arr array
            est_state_arr[0] = act_out.d_aileron;
            est_state_arr[1] = act_out.d_elevator;
            est_state_arr[2] = act_out.d_rudder;
            est_state_arr[3] = act_out.d_throttle;
            //publish_hil_act_controls(HIL_SEND_STATE_TO_SIM, est_state_arr);

            ready_sensors_flag = 0;
        } 
        
       
    }
    return 0;
}
#endif