/* 
 * File:   AttitudeEstimation.h
 * Author: Margaret Silva
 *
 * Created on May 14, 2023, 2:15 PM
 */


#ifndef ATTITUDE_ESTIMATION_H
#define ATTITUDE_ESTIMATION_H

#include <math.h>

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
// function macros
#define RAD_TO_DEG(rad) (rad * ((180.0)/(M_PI)))
#define DEG_TO_RAD(deg) (deg * ((M_PI)/(180.0)))

/*******************************************************************************
 * PUBLIC TYPEDEFS                                                             *
 ******************************************************************************/
struct IMU_euler {
    float yaw;
    float pitch;
    float roll;
};


struct IMU_axis {
    float x;
    float y;
    float z;
};

struct IMU_out {
    struct IMU_axis acc;
    struct IMU_axis gyro;
    struct IMU_axis mag;
};

/*Estimation Gains*/
#define KP_ACC 0.1
#define KI_ACC 0.01
#define KP_MAG 0.0
#define KI_MAG 0.0


/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

// State Estimation Main Functions

/**
 * @Function initEstimation()
 * @param none
 * @return none
 * @brief  inits all attitude estimation values to default
 * @author Margaret Silva */
void initEstimation();

/**
 * @Function setEstimationGains()
 * @param Kp_acc, Ki_acc - Kp and Ki gains for accelerometer values
 * @param Kp_mag, Ki_mag - Kp and Ki gains for magnetometer values
 * @return none
 * @brief  sets gains of estimation filters
 * @author Margaret Silva */
void setEstimationGains(float Kp_acc, float Ki_acc, float Kp_mag, float Ki_mag);

/**
 * @Function openLoopAttitudeEstimation()
 * @param IMU_data - data from the imu, including acc, gyro, and mag
 * @param dT - time between measurements, used for integration
 * @param yaw - pointer to yaw angle calculated by attitude estimation
 * @param pitch - pointer to pitch angle calculated by attitude estimation
 * @param roll - pointer to roll angle calculated by attitude estimation
 * @return none
 * @brief  estimates current attitude using gyros without feedback from accels or mag
 * @author Margaret Silva */
void openLoopAttitudeEstimation(struct IMU_out IMU_data, float dT, float * yaw, float * pitch, float * roll);

/**
 * @Function closedLoopAttitudeEstimation()
 * @param IMU_data - data from the imu, including acc, gyro, and mag
 * @param dT - time between measurements, used for integration
 * @param yaw - pointer to yaw angle calculated by attitude estimation
 * @param pitch - pointer to pitch angle calculated by attitude estimation
 * @param roll - pointer to roll angle calculated by attitude estimation
 * @return none
 * @brief  using the current sensor vals, estimates current attitude with feedback
 * @author Margaret Silva */
void closedLoopAttitudeEstimation(struct IMU_out IMU_data, float dT, float * yaw, float * pitch, float * roll);

// Helper Conversion Functions

/**
 * @Function dcmToEuler(float dcm[3][3], float * yaw, float * pitch, float * roll)
 * @param dcm - DCM matrix from which to extract Euler angles
 *        yaw - yaw angle
 *        pitch - pitch angle
 *        roll - roll angle
 * @return none
 * @brief  Extracts Euler angles from DCM matrix, and writes them to pointer locations
 *         passed in as arguments
 * @author Margaret Silva */
void dcmToEuler(float dcm[3][3], float * yaw, float * pitch, float * roll);

/**
 * @Function R_exp()
 * @param w[3] - vector of rotation rates
 *        dT - integration time constants
 *        exp_mat - exponential matrix
 * @return none
 * @brief  helper function which calculates the matrix exponential exp(-[wx]dT)
 * @author Margaret Silva */
void R_exp(float w[3], float dT, float exp_mat[3][3]);

#endif // ATTITUDE_ESTIMATION_H


