"""
Uses the VehicleAerodynamics to test out the sensors generation and ensure that we are getting the correct sensor outputs and
realistic noise out of them.
"""

import math
import csv
import sys

sys.path.append('..')

import ece163.Modeling.VehicleAerodynamicsModel as VehicleAerodynamicsModel
import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Constants.VehicleSensorConstants as VSC
import ece163.Sensors.SensorsModel as SensorsModel
import ece163.Containers.States as vehicleState
import ece163.Containers.Inputs as Inputs
import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
import random
#
# gpsGaussMarkov=SensorsModel.GaussMarkovXYZ(1.0 / VSC.GPS_rate, VSC.GPS_tau, VSC.GPS_etaHorizontal, VSC.GPS_tau, VSC.GPS_etaHorizontal,
# 										   VSC.GPS_tau, VSC.GPS_etaVertical)
# gpsGM = list()
# for x in range(0,1000):
# 	vx,vy,vz=gpsGaussMarkov.update()
# 	vx += random.gauss(0.0, VSC.GPS_sigmaHorizontal)
# 	vy += random.gauss(0.0, VSC.GPS_sigmaHorizontal)
# 	vz += random.gauss(0.0, VSC.GPS_sigmaVertical)
# 	gpsGM.append([vx,vy,vz,x])
#
# vx = [row[0] for row in gpsGM]
# vy = [row[1] for row in gpsGM]
# vz = [row[2] for row in gpsGM]
# t = [row[3] for row in gpsGM]
#
# mpl.rcParams['legend.fontsize'] = 10
# fig = plt.figure(1)
# plt.plot(t, vx, t, vy, t, vz)
# plt.show()
# exit(1)

fm = VehicleAerodynamicsModel.VehicleAerodynamicsModel()  # forces and moments
sm = SensorsModel.SensorsModel(fm)
controls = Inputs.controlInputs()
myState = vehicleState.vehicleState()
myDot = vehicleState.vehicleState()

fm.reset()
sm.reset()

Hz = 100
fm.vehicleDynamics.dT = 1 / Hz
time = 0.

Va = 25.

fm.setWindModel(12.0, -5.2, 0.0, VPC.DrydenLowAltitudeLight)

myState.u = Va
myState.w = -5.0
fm.setVehicleState(myState)

controls.Throttle = 0.5
controls.Elevator = math.radians(-5.)
controls.Aileron = math.radians(0.1)
fm.vehicleDynamics.state.pd = -100

fm.CalculateAirspeed(fm.vehicleDynamics.state, fm.windModel.Wind)
print("Starting Iteration\n\n")
print(
	"time\t gyro_x\t gyro_y\t gyro_z\t accel_x\t accel_y\t accel_z\t mag_x\t mag_y\t mag_z\t baro\t pitot\t "
	"gps_n\t gps_e\t gps_alt\t gps_sog\t gps_cog\t gyro_x\t gyro_y\t gyro_z\t accel_x\t accel_y\t accel_z\t "
	"mag_x\t mag_y\t mag_z\t baro\t pitot\t gps_n\t gps_e\t gps_alt\t gps_sog\t gps_cog")

dataList = [[time, fm.vehicleDynamics.state.pn, fm.vehicleDynamics.state.pe, -fm.vehicleDynamics.state.pd,
			 fm.vehicleDynamics.state.u, fm.vehicleDynamics.state.v, fm.vehicleDynamics.state.w,
			 math.degrees(fm.vehicleDynamics.state.roll), math.degrees(fm.vehicleDynamics.state.pitch),
			 math.degrees(fm.vehicleDynamics.state.yaw), math.degrees(fm.vehicleDynamics.state.p),
			 math.degrees(fm.vehicleDynamics.state.q), math.degrees(fm.vehicleDynamics.state.r),
			 fm.vehicleDynamics.state.Va, math.degrees(fm.vehicleDynamics.state.alpha),
			 math.degrees(fm.vehicleDynamics.state.beta), math.degrees(fm.vehicleDynamics.state.chi)]]

sensorList = [[time, sm.sensorsTrue.gyro_x, sm.sensorsTrue.gyro_y, sm.sensorsTrue.gyro_z, sm.sensorsTrue.accel_x, sm.sensorsTrue.accel_y,
			   sm.sensorsTrue.accel_z, sm.sensorsTrue.mag_x, sm.sensorsTrue.mag_y, sm.sensorsTrue.mag_z, sm.sensorsTrue.baro,
			   sm.sensorsTrue.pitot, sm.sensorsTrue.gps_n, sm.sensorsTrue.gps_e, sm.sensorsTrue.gps_alt, sm.sensorsTrue.gps_sog,
			   sm.sensorsTrue.gps_cog, sm.sensorsNoisy.gyro_x, sm.sensorsNoisy.gyro_y, sm.sensorsNoisy.gyro_z, sm.sensorsNoisy.accel_x,
			   sm.sensorsNoisy.accel_y, sm.sensorsNoisy.accel_z, sm.sensorsNoisy.mag_x, sm.sensorsNoisy.mag_y, sm.sensorsNoisy.mag_z,
			   sm.sensorsNoisy.baro, sm.sensorsNoisy.pitot, sm.sensorsNoisy.gps_n, sm.sensorsNoisy.gps_e, sm.sensorsNoisy.gps_alt,
			   sm.sensorsNoisy.gps_sog, sm.sensorsNoisy.gps_cog]]

for x in range(0, 100 * Hz):
	time += fm.vehicleDynamics.dT
	fm.Update(controls)
	sm.update()

	s = [time, sm.sensorsTrue.gyro_x, sm.sensorsTrue.gyro_y, sm.sensorsTrue.gyro_z, sm.sensorsTrue.accel_x, sm.sensorsTrue.accel_y,
			   sm.sensorsTrue.accel_z, sm.sensorsTrue.mag_x, sm.sensorsTrue.mag_y, sm.sensorsTrue.mag_z, sm.sensorsTrue.baro,
			   sm.sensorsTrue.pitot, sm.sensorsTrue.gps_n, sm.sensorsTrue.gps_e, sm.sensorsTrue.gps_alt, sm.sensorsTrue.gps_sog,
			   sm.sensorsTrue.gps_cog, sm.sensorsNoisy.gyro_x, sm.sensorsNoisy.gyro_y, sm.sensorsNoisy.gyro_z, sm.sensorsNoisy.accel_x,
			   sm.sensorsNoisy.accel_y, sm.sensorsNoisy.accel_z, sm.sensorsNoisy.mag_x, sm.sensorsNoisy.mag_y, sm.sensorsNoisy.mag_z,
			   sm.sensorsNoisy.baro, sm.sensorsNoisy.pitot, sm.sensorsNoisy.gps_n, sm.sensorsNoisy.gps_e, sm.sensorsNoisy.gps_alt,
			   sm.sensorsNoisy.gps_sog, sm.sensorsNoisy.gps_cog]

	d = [time, fm.vehicleDynamics.state.pn, fm.vehicleDynamics.state.pe, -fm.vehicleDynamics.state.pd,
			 fm.vehicleDynamics.state.u, fm.vehicleDynamics.state.v, fm.vehicleDynamics.state.w,
			 math.degrees(fm.vehicleDynamics.state.roll), math.degrees(fm.vehicleDynamics.state.pitch),
			 math.degrees(fm.vehicleDynamics.state.yaw), math.degrees(fm.vehicleDynamics.state.p),
			 math.degrees(fm.vehicleDynamics.state.q), math.degrees(fm.vehicleDynamics.state.r),
			 fm.vehicleDynamics.state.Va, math.degrees(fm.vehicleDynamics.state.alpha),
			 math.degrees(fm.vehicleDynamics.state.beta), math.degrees(fm.vehicleDynamics.state.chi)]

	print(" ".join('{: 7.3f}'.format(t) for t in s))
	sensorList.append(s)

with open('data.csv', 'w', newline='') as f:
	writer = csv.writer(f)
	writer.writerows(sensorList)
