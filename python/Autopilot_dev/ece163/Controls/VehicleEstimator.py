'''
    Author: Margaret Silva
    This file contains the classes and function needed to estimate the current state from sensor measurements
'''

import math
import sys
import ece163.Utilities.MatrixMath as MM
import ece163.Utilities.Rotations as Rotations
import ece163.Containers.States as States
import ece163.Containers.Sensors as Sensors
import ece163.Constants.VehiclePhysicalConstants as VPC
from ..Constants import VehicleSensorConstants as VSC
import ece163.Modeling.VehicleAerodynamicsModel as VAM
import ece163.Sensors.SensorsModel as SensorsModel


class VehicleEstimator():

    def __init__(self, dT=VPC.dT, aeroModel=VAM.VehicleAerodynamicsModel()):
        ''' Inits an instance of vehicle estimator object, which contains a aero model and sensor model '''
        self.dT = dT
        self.aeroModel = aeroModel # updated externally
        self.sensorModel = SensorsModel.SensorsModel(aeroModel=aeroModel) # sensors model is completely self contained
        self.estimate = States.vehicleState(dcm=[[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])
        # used in airspeed estimation
        self.prev_estimate = States.vehicleState()
        self.prev_sensors = Sensors.vehicleSensors()
        # biases are used to store accumulated values
        self.biases = States.vehicleState()
        # arrays used for making moving averages for airspeed and altitude
        self.Va_gps_arr = [] 
        self.pd_arr = []
        # set complementary filter gains to default values
        self.Kp_att_a = 5
        self.Kp_att_m = 15
        self.Ki_att_a = .5
        self.Ki_att_m = 1.5
        self.Kp_alt = .8
        self.Ki_alt = .08
        self.Kp_spd = .11
        self.Ki_spd = .011
        self.Kp_course = .8
        self.Ki_course = .08
        # specifically for use when using real sensors
        self.first_gps_packet_received = False
        return

    def reset(self):
        ''' Resets the sensors model, state estimation, bias estimation, and filter gains '''
        self.sensorModel.reset()
        self.biases = States.vehicleState()
        self.estimate = States.vehicleState(dcm=[[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])
        self.prev_estimate = States.vehicleState()
        self.prev_sensors = Sensors.vehicleSensors()
        self.setFilterGains()
        self.Va_gps_arr = []
        self.pd_arr = []
        # specifically for use when using real sensors
        self.first_gps_packet_received = False
        return
    
    def setFilterGains(self, Kp_att_a=5, Kp_att_m=15, Kp_alt=0.8, Kp_spd=.11, Kp_course=.8,
                       Ki_att_a=None, Ki_att_m=None, Ki_alt=None, Ki_spd=None, Ki_course=None):
        ''' 
            Sets the gains of the complementary filters. The Ki values default to be 1/10 the value of the Kp
            gains when not specified.
        '''
        self.Kp_att_a = Kp_att_a
        self.Kp_att_m = Kp_att_m
        self.Kp_alt = Kp_alt
        self.Kp_spd = Kp_spd
        self.Kp_course = Kp_course
        if Ki_att_a == None:
            self.Ki_att_a = Kp_att_a/10
        else:
            self.Ki_att_a = Ki_att_a
        if Ki_att_m == None:
            self.Ki_att_m = Kp_att_m/10
        else:
            self.Ki_att_m = Ki_att_m
        if Ki_alt == None:
            self.Ki_alt = Kp_alt/10
        else:
            self.Ki_alt = Ki_alt
        if Ki_spd == None:
            self.Ki_spd = Kp_spd/10
        else:
            self.Ki_spd = Ki_spd
        if Ki_course == None:
            self.Ki_course = Kp_course/10
        else:
            self.Ki_course = Ki_course
        return
    
    def getFilterGains(self):
        ''' Returns the current filter gains '''
        return self.Kp_att_a, self.Kp_att_m, self.Kp_alt, self.Ki_att_a, self.Ki_att_m, self.Ki_alt

    def getStateEstimate(self):
        ''' Return current state estimate '''
        return self.estimate
    
    def getVehicleState(self):
        ''' Return state from internal model '''
        return self.aeroModel.getVehicleState()

    def getAllSensors(self):
        ''' Return all sensor measurements '''
        return self.sensorModel.getSensorsNoisy()
    
    def getGyroValues(self):
        ''' Return current gyro measurements '''
        return [self.sensorModel.getSensorsNoisy().gyro_x, self.sensorModel.getSensorsNoisy().gyro_y, 
                self.sensorModel.getSensorsNoisy().gyro_z]
    
    def getAccelValues(self):
        ''' Return current gyro measurements '''
        return [self.sensorModel.getSensorsNoisy().accel_x, self.sensorModel.getSensorsNoisy().accel_y, 
                self.sensorModel.getSensorsNoisy().accel_z]
    
    def getMagValues(self):
        ''' Return current gyro measurements '''
        return [self.sensorModel.getSensorsNoisy().mag_x, self.sensorModel.getSensorsNoisy().mag_y, 
                self.sensorModel.getSensorsNoisy().mag_z]

    def update(self, sensors=None):
        ''' Update sensors model and calculates new state estimate '''
        if sensors == None:
            self.sensorModel.update()
            self.estimate, self.biases = self.estimateState(self.sensorModel.getSensorsNoisy())
        else:
            self.estimate, self.biases = self.estimateState(sensors, True)
        return

    def estimateState(self, sensors=Sensors.vehicleSensors(), use_true_sensors=False):
        ''' Return state estimate and bias estimate based on sensor readings '''
        # create containers
        estimate = States.vehicleState()
        biases = States.vehicleState()

        # attitude estimation
        b_hat, w_hat, R_hat = self.compFilterAttitude(sensors)
        estimate.p, estimate.q, estimate.r = w_hat[0][0], w_hat[1][0], w_hat[2][0]
        estimate.R = R_hat
        estimate.yaw, estimate.pitch, estimate.roll = Rotations.dcm2Euler(R_hat)
        biases.p, biases.q, biases.r = b_hat[0][0], b_hat[1][0], b_hat[2][0]

        # airspeed estimation
        estimate.Va, estimate.u, estimate.v, biases.Va = self.estimateAirspeed(self.estimate.R, sensors)

        # altitude estimation
        estimate.pd, biases.pd = self.estimateAltitude(sensors=sensors, use_true_sensors=use_true_sensors)

        # course estimation
        if use_true_sensors == False:
            estimate.chi, biases.chi = self.estimateCourse(sensors)
        else:
            estimate.chi, biases.chi = self.estimateCourseFromTrueSensors(sensors)

        # position estimation
        estimate.pn, estimate.pe, biases.pn, biases.pe = self.estimatePosition(sensors)

        return estimate, biases
    
    def estimateAirspeed(self, R_hat2, sensors2=Sensors.vehicleSensors()):
        ''' 
            Estimate the airspeed using GPS speed and course over ground, the estimated DCM matrix, and 
            accelerometer values 
        '''
        # extract old sensor values, estimated quantities, etc
        Va_hat = self.estimate.Va
        Va_hat_accels = self.estimate.u
        Va_hat_gps = self.estimate.v
        b_hat = self.biases.Va
        sensors1 = self.prev_sensors # stores gps sog and cog 
        R_hat1 = self.prev_estimate.R 
        # filter gains
        Kp = self.Kp_spd
        Ki = self.Ki_spd

        # get estimate for airspeed from GPS
        s_n = sensors2.gps_sog*math.cos(sensors2.gps_cog) - sensors1.gps_sog*math.cos(sensors1.gps_cog)
        s_e = sensors2.gps_sog*math.sin(sensors2.gps_cog) - sensors1.gps_sog*math.sin(sensors1.gps_cog)
        num_mag = math.hypot(s_n, s_e)
        s_n = MM.multiply(MM.transpose(R_hat2), [[1], [0], [0]])
        s_e = MM.multiply(MM.transpose(R_hat1), [[1], [0], [0]])
        den = MM.subtract(s_n, s_e)
        den_mag = math.hypot(den[0][0], den[1][0], den[2][0])

        # only update gps speed when the difference is large enough to be measured
        if not math.isclose(den_mag, 0, abs_tol=1e-12) and not math.isclose(num_mag, 0, abs_tol=1e-12):
            # calculate new airspeed from gps
            Va_hat_gps = (num_mag/den_mag)
            self.prev_sensors = sensors2
            self.prev_estimate.R = R_hat2

            # use the average airspeed calculated by the gps (moving average filter)
            self.Va_gps_arr.append(Va_hat_gps)
            if (len(self.Va_gps_arr) < 50):
                n = len(self.Va_gps_arr)
            else:
                n = 50
            Va_hat_gps = sum(self.Va_gps_arr[-n:])/len(self.Va_gps_arr[-n:])

            # calculate bias only when gps has updated
            # for some reason, using the actual time difference instead of dT breaks everything; do not do that
            b_dot = -Ki*(Va_hat_gps - Va_hat)
            b_hat += b_dot*self.dT

        # get estimate for airspeed from accelerometers
        u_dot = sensors2.accel_x - VPC.g0*math.sin(self.estimate.pitch)
        Va_hat_accels += u_dot*self.dT

        # complementary filter with accelerometer (high pass) and gps (low pass)
        Va_hat = Va_hat_accels - b_hat + Kp*(Va_hat_gps - Va_hat)

        return Va_hat, Va_hat_accels, Va_hat_gps, b_hat
    
    def estimateCourse(self, sensors=Sensors.vehicleSensors()):
        ''' 
            Estimate the course angle of the UAV 
            Used estimated values of airspeed, roll, and yaw
        '''
        # extract previous estimations
        chi_hat = self.estimate.chi
        # other estimates
        Va_hat = self.estimate.Va
        phi_hat = self.estimate.roll
        psi_hat = self.estimate.yaw
        # sensors
        chi_hat_gps = sensors.gps_cog
        # accumulation
        b_hat = self.biases.chi
        # gains
        Kp = self.Kp_course
        Ki = self.Ki_course

        # get estimate for change in course
        try:
            chi_dot = (VPC.g0/sensors.gps_sog) * math.tan(phi_hat) * math.cos(chi_hat - psi_hat)
            if (sensors.gps_sog > 50) :
                # the estimate for airspeed is most likely wrong
                chi_dot = 0
        except:
            chi_dot = 0

        # ignore estimate for chi dot before gps has updated for the first time
        if self.sensorModel.updateTicks < self.sensorModel.gpsTickUpdate:
            chi_dot = 0
            chi_hat_gps = 0
            chi_hat = 0

        # get cog from gps
        if (int(self.sensorModel.updateTicks -1) % int(self.sensorModel.gpsTickUpdate)) == 0:
            chi_diff = chi_hat_gps - chi_hat
            if abs(chi_diff) < 0.34906: # don't trust a turn faster than 20 deg/sec
                b_hat += self.dT*chi_diff
                chi_hat = chi_hat_gps     
            else:
                # b_hat += 0
                b_hat += self.dT*chi_diff
                chi_hat = chi_hat_gps 
        
        # add to chi dot estimate
        chi_dot += Ki*b_hat + Kp*(chi_hat_gps - chi_hat)

        chi_hat += self.dT*(chi_dot)

        return chi_hat, b_hat
    
    def estimateCourseFromTrueSensors(self, sensors=Sensors.vehicleSensors()):
        ''' 
            Estimate the course angle of the UAV 
            Used estimated values of airspeed, roll, and yaw
        '''
        # extract previous estimations
        chi_hat = self.estimate.chi
        # other estimates
        Va_hat = self.estimate.Va
        phi_hat = self.estimate.roll
        psi_hat = self.estimate.yaw
        # sensors
        chi_hat_gps = sensors.gps_cog
        # accumulation
        b_hat = self.biases.chi
        # gains
        Kp = self.Kp_course
        Ki = self.Ki_course

        # get estimate for change in course
        if sensors.gps_sog > 5:
            chi_dot = (VPC.g0/sensors.gps_sog) * math.tan(phi_hat) * math.cos(chi_hat - psi_hat)
            if (sensors.gps_sog > 50) :
                # the estimate for airspeed is most likely wrong
                chi_dot = 0
        else:
            chi_dot = 0

        # ignore estimate for chi dot before gps has updated for the first time
        if self.first_gps_packet_received == False:
            chi_dot = 0
            chi_hat_gps = 0
            chi_hat = 0

        # get cog from gps
        if sensors.gps_alt == True:
            self.first_gps_packet_received = True
            chi_diff = chi_hat_gps - chi_hat
            if (abs(chi_diff) < 0.34906) and (sensors.gps_sog > 5.0): # don't trust a turn faster than 20 deg/sec
                b_hat += self.dT*chi_diff
                chi_hat = chi_hat_gps     
            else:
                # b_hat = 0
                # chi_hat_gps = 0
                # chi_hat = 0
                # b_hat += 0
                b_hat += self.dT*chi_diff
                chi_hat = chi_hat_gps 
        
        # add to chi dot estimate
        # chi_dot += Ki*b_hat + Kp*(chi_hat_gps - chi_hat)

        # chi_hat += self.dT*(chi_dot)

        return chi_hat, b_hat
    
    def estimatePosition(self, sensors=Sensors.vehicleSensors()):
        ''' Estimates position north and east using gps position and ground speed, and estimated course '''
        Vg = sensors.gps_sog
        chi_hat = self.estimate.chi
        pn_hat = self.estimate.pn
        pe_hat = self.estimate.pe
        b_pn = self.biases.pn
        b_pe = self.biases.pe
        Kp = 0.8
        Ki = 0.08

        # get estimate for change in course
        pn_dot = Vg*math.cos(chi_hat)
        pe_dot = Vg*math.sin(chi_hat)

        # ignore estimate for chi dot before gps has updated for the first time
        if self.sensorModel.updateTicks < self.sensorModel.gpsTickUpdate:
            pn_dot = Vg*math.cos(chi_hat)
            pe_dot = Vg*math.sin(chi_hat)
            pn_hat = 0
            pe_hat = 0

        # get cog from gps
        if (int(self.sensorModel.updateTicks -1) % int(self.sensorModel.gpsTickUpdate)) == 0:
            pn_diff = sensors.gps_n - pn_hat
            pe_diff = sensors.gps_e - pe_hat

            b_pn += self.dT*pn_diff
            pn_hat = sensors.gps_n

            b_pe += self.dT*pe_diff
            pe_hat = sensors.gps_e
        
        # add to chi dot estimate
        pn_dot += Ki*b_pn + Kp*(sensors.gps_n - pn_hat)
        pn_hat += self.dT*pn_dot

        pe_dot += Ki*b_pe + Kp*(sensors.gps_e - pe_hat)
        pe_hat += self.dT*pe_dot

        return pn_hat, pe_hat, b_pn, b_pe
        


    def estimateAltitude(self, sensors=Sensors.vehicleSensors(), use_true_sensors=False):
        '''  Estimate altitude using moving average filter on barometer output '''
        # extract stored values
        b = self.biases.pd

        # values related to altitude from sensors
        h_baro = -(sensors.baro - VSC.Pground)/(VPC.rho*VPC.g0)

        # try adding moving average filter 
        self.pd_arr.append(h_baro)
        h_baro = sum(self.pd_arr[-10:])/len(self.pd_arr[-10:])

        if (use_true_sensors):
            if (abs(-h_baro - self.estimate.pd) > 10) and (abs(self.estimate.pd) > 1.0):
                h_baro = -self.estimate.pd

        return -h_baro, b
    
    def compFilterAttitude(self, sensors=Sensors.vehicleSensors()):
        ''' Returns estimated pqr biases, estimated pqr, and estimated dcm matrix '''
        # similar to filter in state estimation cheat sheet 
        # https://ahrs.readthedocs.io/en/latest/filters/mahony.html#explicit-complementary-filter
        
        # get values stored by class
        R_hat = self.estimate.R
        b_hat = [[self.biases.p], [self.biases.q], [self.biases.r]]
        # filter gains
        Kp_a = self.Kp_att_a
        Kp_m = self.Kp_att_m
        Ki_a = self.Ki_att_a
        Ki_m = self.Ki_att_m

        acc_b = MM.vectorNorm([[sensors.accel_x], [sensors.accel_y], [sensors.accel_z]])
        mag_b = MM.vectorNorm([[sensors.mag_x], [sensors.mag_y], [sensors.mag_z]])

        mag_I = MM.vectorNorm(VSC.magfield)
        acc_I = MM.vectorNorm([[0], [0], [-VPC.g0]])

        gyros = [[sensors.gyro_x], [sensors.gyro_y], [sensors.gyro_z]]
        gyro_biased = MM.subtract(gyros, b_hat)
        w_meas_a = MM.crossProduct(acc_b, MM.multiply(R_hat, acc_I))
        w_meas_m = MM.crossProduct(mag_b, MM.multiply(R_hat, mag_I))

        # gyro feedback terms
        a = MM.scalarMultiply(Kp_a, w_meas_a)
        b = MM.scalarMultiply(Kp_m, w_meas_m)
        # bias terms
        c = MM.scalarMultiply(-Ki_a, w_meas_a)
        d = MM.scalarMultiply(-Ki_m, w_meas_m)
        if (math.hypot(sensors.accel_x, sensors.accel_y, sensors.accel_z) < 1.1*VPC.g0) and \
            (math.hypot(sensors.accel_x, sensors.accel_y, sensors.accel_z) > .9*VPC.g0):
            gyro_feedback = MM.add(gyro_biased, MM.add(a, b))
            b_dot = MM.add(c, d)
        else:
            # ignore acceleration when it's much larger or smaller than 1 g
            gyro_feedback = MM.add(gyro_biased, b)
            b_dot = d

        R_plus = MM.multiply(self.R_exp(gyro_feedback, self.dT), R_hat)
        b_plus = MM.add(b_hat, MM.scalarMultiply(self.dT, b_dot))

        return b_plus, gyro_biased, R_plus

    def R_exp(self, w, dT):
        ''' Takes in a rotation matrix and rotation rate vector and returns the matrix exponential exp(-[wx]dT) '''
        # extract p, q, r
        p = w[0][0]
        q = w[1][0]
        r = w[2][0]

        # calculate the [wx] matrix
        wx = [[0.0, -r, q], [r, 0.0, -p], [-q, p, 0.0]]
        w_mag = math.hypot(p, q, r)

        # calculate the coefficients, and use an approximation if the value of w_mag is small
        if w_mag < 0.2:
            A = dT - (((dT**3) * (w_mag**2))/6.0) + (((dT**5) * (w_mag**4))/ 120.0)
            B = ((dT**2)/2.0) - (((dT**4) * (w_mag**2))/24.0) + (((dT**6) * (w_mag**4))/ 720.0)
        else:
            A = (math.sin(w_mag*dT)/w_mag)
            B = (1.0 - math.cos(w_mag*dT))/(w_mag**2)

        I = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]
        # multiply [wx] by the coefficients
        matA = MM.scalarMultiply(A, wx)
        matB = MM.multiply(wx, wx)
        matB = MM.scalarMultiply(B, matB)
        # combine matrices (I - A*[wx]) + B*[wx]**2
        temp = MM.subtract(I, matA)
        exp_mat = MM.add(temp, matB)

        return exp_mat
