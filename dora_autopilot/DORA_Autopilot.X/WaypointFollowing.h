/* ************************************************************************** */
/** Way Point Following Library

  @File Name
    WaypointFollowing.h

  @Summary
    This library contains all the functions used to allow the autopilot to 
    follow a series of user defined way points

  @Description
    Provides way point following functionality, along with straight line 
    following
 
 * Created on February 8, 2023, 1:52 PM
 */
/* ************************************************************************** */

#ifndef _WAYPOINT_FOLLOWING_H    /* Guard against multiple inclusion */
#define _WAYPOINT_FOLLOWING_H

/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/

#include "StateEstimation.h"

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/
#define MAXIMUM_WAYPOINTS 100

/*******************************************************************************
 * PUBLIC TYPEDEFS                                                             *
 ******************************************************************************/

struct waypoint {
    float pn; // position of way point north
    float pe; // position of way point east
    float pd; // position of way point down (upwards is negative)
};

/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

/**
 * @Function inputWaypoints()
 * @param Waypoints - array of way points (array of way point structs), 
 *        n - number of way points (int)
 * @return none
 * @brief  Takes in an array of way points to be used to define the aircraft's
 *          path. This way point array is stored in a static array within the
 *          file
 * @author Margaret Silva */
void inputWaypoints(struct waypoint W[], int n);

/**
 * @Function unpackSquareWaypointTransmission(float points[16])
 * @param points - array of 16 float for sim, arranged in a square
 * @return none
 * @brief  Takes in an array of way points, then configures them in such a way
 *      that the algorithm works properly
 * @author Margaret Silva */
void unpackSquareWaypointTransmission(float points[16]);

/**
 * @Function waypointFollowingSetTuningParams()
 * @param X_inf - maximum turn towards the desired path 
 *        Kpath - tuning param that determines sharpness of turn (approx 1/max radius)
 * @return none
 * @brief Takes in the tuning parameters needed for waypoint following
 * @author Margaret Silva */
void waypointFollowingSetTuningParams(float X_inf, float Kpath);

/**
 * @Function waypointFollowingSetTuningParamsFromWaypointArray()
 * @param none
 * @return none
 * @brief Uses the current way point array to calculate the distance between 
 *  adjacent points and use this to tune Kp
 * @author Margaret Silva */
void waypointFollowingSetTuningParamsFromWaypointArray();

/**
 * @Function waypointFollowingUpdateReferenceCommands()
 * @param cur_state - the current state of the vehicle
 *        X_c - pointer to float that holds commanded course
 *        h_c - pointer to float that hold commanded altitude
 * @return none
 * @brief Calculates and returns new course and altitude commands based on the 
 *      set of way points and the current vehicle state. These commands can then
 *      be fed to the stability controller to command the UAV
 * @source chapter 10 and 11 in Beard, R. W., & McLain, T. W. (2012). 
 *    Small unmanned aircraft : Theory and practice. Princeton University Press.
 * @author Margaret Silva */
void waypointFollowingUpdateReferenceCommands(struct vehicle_state cur_state, float * X_c, float * h_c);

#endif /* _WAYPOINT_FOLLOWING_H */
