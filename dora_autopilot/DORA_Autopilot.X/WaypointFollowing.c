/* 
 * File:   WaypointFollowing.c
 * Author: Margaret Silva
 * Brief: Contains all functions needed to find the course and pitch angle needed 
 *  for the aircraft to follow a set of way points
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

#include <math.h>

#include "WaypointFollowing.h"
#include "Lin_alg_float.h"
#include "AirframeConstants.h"

/*******************************************************************************
 * PRIVATE #DEFINES                                                            *
 ******************************************************************************/

/*******************************************************************************
 * PRIVATE STATIC VARIABLES                                                    *
 ******************************************************************************/
// waypoint array data
static struct waypoint Waypoints[MAXIMUM_WAYPOINTS];
static int num_waypoints = 0;
static int wp_idx = 1;

// tuning params
static float X_inf = M_PI_2; // maximum turn angle
static float Kpath = 0.01; // sharpness of turn (approx 1/max radius) 

/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATIONS                                             *
 ******************************************************************************/

/**
 * @Function inputWaypoints()
 * @param Waypoints - array of way points (array of way point structs), 
 *        n - number of way points (int)
 * @return none
 * @brief  Takes in an array of way points to be used to define the aircraft's
 *          path. This way point array is stored in a static array within the
 *          file
 * @author Margaret Silva */
void inputWaypoints(struct waypoint W[], int n) {
    int i;

    // only record up to the maximum number of waypoints
    if (n > MAXIMUM_WAYPOINTS) {
        n = MAXIMUM_WAYPOINTS;
    }

    // record waypoints in static array
    for (i = 0; i < n; i++) {
        Waypoints[i].pn = W[i].pn;
        Waypoints[i].pe = W[i].pe;
        Waypoints[i].pd = W[i].pd;
    }

    // record number of waypoints and reset index
    num_waypoints = n;
    wp_idx = 1;
}

/**
 * @Function unpackSquareWaypointTransmission(float points[16])
 * @param points - array of 16 float for sim, arranged in a square
 * @return none
 * @brief  Takes in an array of way points, then configures them in such a way
 *      that the algorithm works properly
 * @author Margaret Silva */
void unpackSquareWaypointTransmission(float points[16]) {
    struct waypoint wp_init = {.pn = INITIAL_PN, .pe = INITIAL_PE, .pd = INITIAL_PD};
    struct waypoint wp1 = {.pn = points[0], .pe = points[1], .pd = points[2]};
    struct waypoint wp2 = {.pn = points[3], .pe = points[4], .pd = points[5]};
    struct waypoint wp3 = {.pn = points[6], .pe = points[7], .pd = points[8]};
    struct waypoint wp4 = {.pn = points[9], .pe = points[10], .pd = points[11]};
    struct waypoint array[7] = {wp_init, wp1, wp2, wp3, wp4, wp1, wp1};
    
    inputWaypoints(array, 7);
}

/**
 * @Function waypointFollowingSetTuningParams()
 * @param X_inf - maximum turn towards the desired path 
 *        Kpath - tuning param that determines sharpness of turn (approx 1/max radius)
 * @return none
 * @brief Takes in the tuning parameters needed for waypoint following
 * @author Margaret Silva */
void waypointFollowingSetTuningParams(float X_max, float Kp) {
    X_inf = X_max;
    Kpath = Kp;
}

/**
 * @Function waypointFollowingSetTuningParamsFromWaypointArray()
 * @param none
 * @return none
 * @brief Uses the current way point array to calculate the distance between 
 *  adjacent points and use this to tune Kp
 * @author Margaret Silva */
void waypointFollowingSetTuningParamsFromWaypointArray() {
    float w_minus[3] = {Waypoints[wp_idx - 1].pn, Waypoints[wp_idx - 1].pe, Waypoints[wp_idx - 1].pd};
    float w_plus[3] = {Waypoints[wp_idx + 1].pn, Waypoints[wp_idx + 1].pe, Waypoints[wp_idx + 1].pd};
    float dist_v[3];
    float dist;

    lin_alg_v_v_sub(w_plus, w_minus, dist_v);
    dist = 0.5 * lin_alg_v_norm(dist_v);

    if (fabs(dist) > 0.001) {
        Kpath = 1 / dist;
    } else {
        Kpath = 0.1;
    }
}

/**
 * @Function waypointFollowingUpdateReferenceCommands()
 * @param cur_state - the current state of the vehicle
 *        X_c - pointer to float that holds commanded course
 *        h_c - pointer to float that hold commanded altitude
 * @return none
 * @brief Calculates and returns new course and altitude commands based on the 
 *      set of way points and the current vehicle state. These commands can then
 *      be fed to the stability controller to command the UAV
 * @source chapter 10 and 11 of Beard, R. W., & McLain, T. W. (2012). 
 *    Small unmanned aircraft : Theory and practice. Princeton University Press.
 * @author Margaret Silva */
void waypointFollowingUpdateReferenceCommands(struct vehicle_state cur_state, float * X_c, float * h_c) {
    // define important way points
    float w_start[3] = {Waypoints[wp_idx - 1].pn, Waypoints[wp_idx - 1].pe, Waypoints[wp_idx - 1].pd};
    float w_target[3] = {Waypoints[wp_idx].pn, Waypoints[wp_idx].pe, Waypoints[wp_idx].pd};
    float w_next[3] = {Waypoints[wp_idx + 1].pn, Waypoints[wp_idx + 1].pe, Waypoints[wp_idx + 1].pd};
    float pos[3] = {cur_state.pn, cur_state.pe, cur_state.pd};
    // define starting point and path vectors
    float r[3] = {Waypoints[wp_idx - 1].pn, Waypoints[wp_idx - 1].pe, Waypoints[wp_idx - 1].pd};
    float q_minus[3];
    float q_i[3];
    float n[3];
    // define values used in determining direction to take
    float s[3];
    float Xq;
    // define temporary values
    float norm;
    float decide;
    float dot;
    float temp;
    float temp_v[3];
    float error_v[3];
    float k[3] = {0, 0, 1};
    int i;

    // set tuning params
    waypointFollowingSetTuningParamsFromWaypointArray();
    
    // decide whether or not to swap way points
    // algorithm source: chapter 11 of Small unmanned aircraft

    // vector from starting point to target
    lin_alg_v_v_sub(w_target, w_start, q_minus);
    norm = lin_alg_v_norm(q_minus);
    for (i = 0; i < 3; i++) {
        q_minus[i] = q_minus[i] / norm;
    }

    // vector from target to following way point
    lin_alg_v_v_sub(w_next, w_target, q_i);
    norm = lin_alg_v_norm(q_i);
    for (i = 0; i < 3; i++) {
        q_i[i] = q_i[i] / norm;
    }

    // addition between current and next path
    lin_alg_v_v_add(q_minus, q_i, n);
    norm = lin_alg_v_norm(n);
    for (i = 0; i < 3; i++) {
        n[i] = n[i] / norm;
    }

    // determine if the current target way point has been passed or not
    lin_alg_v_v_sub(pos, w_target, temp_v);
    // transpose temp v, and multiply n by it
    decide = 0.0;
    for (i = 0; i < 3; i++) {
        decide += temp_v[i] * n[i];
    }
    if (decide >= 0) {
        // autopilot will continue in direction of last way point when end of list is reached
        if (wp_idx < num_waypoints - 2) {
            wp_idx++;
        }
    }

    // calculate commanded course and altitude to reach target way point
    // algorithm source: chapter 10 of Small unmanned aircraft

    // calculate vector normal to the plane created by the path vector and downwards vector k
    lin_alg_v_v_sub(pos, r, error_v);
    lin_alg_cross(q_minus, k, n);
    norm = lin_alg_v_norm(n);
    for (i = 0; i < 3; i++) {
        n[i] = n[i] / norm;
    }

    // s = e_p - (e_p . n) n
    dot = lin_alg_dot(error_v, n);
    lin_alg_s_v_mult(dot, n, temp_v);
    lin_alg_v_v_sub(error_v, temp_v, s);

    // compute commanded altitude
    temp = sqrt(q_minus[0] * q_minus[0] + q_minus[1] * q_minus[1]);
    if (fabs(temp) > 0.0001) {
        *h_c = -r[2] - sqrt(s[0] * s[0] + s[1] * s[1]) * ((q_minus[2]) / (temp));
    } else {
        *h_c = -r[2];
    }

    // compute commanded course
    Xq = atan2(q_minus[1], q_minus[0]);
    while ((Xq - cur_state.chi) < M_PI) {
        Xq += 2 * M_PI;
    }
    while ((Xq - cur_state.chi) > M_PI) {
        Xq -= 2 * M_PI;
    }

    // error from the path in the path y axis
    temp = -sin(Xq) * (pos[0] - r[0]) + cos(Xq) * (pos[1] - r[1]);
    *X_c = Xq - X_inf * (2.0 / M_PI) * atan(Kpath * temp);
}
