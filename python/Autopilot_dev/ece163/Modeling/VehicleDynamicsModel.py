'''
    Author: Margaret Silva
    This file contains the functions used to model the vehicle dynamics
'''

import math
from ..Containers import States
from ..Utilities import MatrixMath
from ..Utilities import Rotations
from ..Constants import VehiclePhysicalConstants as VPC


class VehicleDynamicsModel():
    def __init__(self, dT=VPC.dT):
        ''' Define all initial values for the state variables '''
        self.dT = dT

        self.state = States.vehicleState()
        self.dot = States.vehicleState()

    def setVehicleState(self, newState): 
        ''' Set vehicle's current state to the state passed as argument '''
        self.state = newState

    def getVehicleState(self):
        ''' Return the vehicle's current state '''
        return self.state

    def setVehicleDerivative(self, newDerivative):
        ''' Set the vehicle's derivative state to the one passed as argument '''
        self.dot = newDerivative

    def getVehicleDerivative(self):
        ''' Return the derivative of the vehicle's current state '''
        return self.dot

    def reset(self):
        ''' Reset all values to default '''
        self.state = States.vehicleState()
        self.dot = States.vehicleState()

    def Update(self, forcesMoments):
        ''' Updates the current vehicle state using the forces passed in as forcesMoments '''
        self.dot = self.derivative(self.state, forcesMoments)
        self.state = self.IntegrateState(self.dT, self.state, self.dot)

    def Rexp(self, dT, state, dot):
        ''' Calculate matrix exponential for use in matrix differentiation '''
        # find values of p, q, and r at the current time step
        p = state.p + dot.p * (dT/2.0)
        q = state.q + dot.q * (dT/2.0)
        r = state.r + dot.r * (dT/2.0)

        # calculate the [wx] matrix
        wx = [[0.0, -r, q], [r, 0.0, -p], [-q, p, 0.0]]
        w_mag = math.sqrt(p**2 + q**2 + r**2)

        # calculate the coefficients, and use an approximation if the value of w_mag is small
        if w_mag < 0.2:
            A = dT - (((dT**3) * (w_mag**2))/6.0) + (((dT**5) * (w_mag**4))/ 120.0)
            B = ((dT**2)/2.0) - (((dT**4) * (w_mag**2))/24.0) + (((dT**6) * (w_mag**4))/ 720.0)
        else:
            A = (math.sin(w_mag*dT)/w_mag)
            B = (1.0 - math.cos(w_mag*dT))/(w_mag**2)

        I = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]
        # multiply [wx] by the coefficients
        matA = MatrixMath.scalarMultiply(A, wx)
        matB = MatrixMath.multiply(wx, wx)
        matB = MatrixMath.scalarMultiply(B, matB)
        # combine matrices (I - A*[wx]) + B*[wx]**2
        temp = MatrixMath.subtract(I, matA)
        exp_mat = MatrixMath.add(temp, matB)

        return exp_mat

    def derivative(self, state, forcesMoments):
        ''' Calculates the derivative of each parameter of the state passed in the function '''

        d_state = States.vehicleState()

        # position derivative dt(pn, pe, pd)
        v = [[state.u], [state.v], [state.w]]
        invR = MatrixMath.transpose(state.R)
        d_pos = MatrixMath.multiply(invR, v)
        d_state.pn = d_pos[0][0]
        d_state.pe = d_pos[1][0]
        d_state.pd = d_pos[2][0]

        # vehicle ground speed derivative dt(u, v, w)
        wx = [[0.0, -state.r, state.q], [state.r, 0.0, -state.p], [-state.q, state.p, 0.0]]
        forces = [[forcesMoments.Fx], [forcesMoments.Fy], [forcesMoments.Fz]]
        temp1 = MatrixMath.scalarDivide(VPC.mass, forces)       # (1/m)Fb
        temp2 = MatrixMath.multiply(wx, v)                      # [wx]v
        dv = MatrixMath.subtract(temp1, temp2)                  # (1/m)Fb - [wx]v
        d_state.u = dv[0][0]
        d_state.v = dv[1][0]
        d_state.w = dv[2][0]

        # euler angle derivatives dt(yaw, pitch, roll)
        R = [[1.0, math.sin(state.roll)*math.tan(state.pitch), math.cos(state.roll)*math.tan(state.pitch)], 
            [0.0, math.cos(state.roll), -math.sin(state.roll)],
            [0.0, math.sin(state.roll)/math.cos(state.pitch), math.cos(state.roll)/math.cos(state.pitch)]]
        pqr_vec = [[state.p], [state.q], [state.r]]
        dt_euler = MatrixMath.multiply(R, pqr_vec)   # convert from angular rates to euler angle derivatives
        d_state.roll = dt_euler[0][0]
        d_state.pitch = dt_euler[1][0]
        d_state.yaw = dt_euler[2][0]

        # euler angle rate derivatives dt(p, q, r)
        J_inv = [[VPC.Jzz, 0.0, VPC.Jxz], 
            [0.0, (VPC.Jxx*VPC.Jzz - VPC.Jxz**2)/VPC.Jyy, 0.0], 
            [VPC.Jxz, 0.0, VPC.Jxx]]
        coefficient = 1.0/(VPC.Jxx*VPC.Jzz - VPC.Jxz**2)
        J_inv = MatrixMath.scalarMultiply(coefficient, J_inv)
        J = [[VPC.Jxx, 0.0, -VPC.Jxz], [0.0, VPC.Jyy, 0.0], [-VPC.Jxz, 0.0, VPC.Jzz]]
        Mb = [[forcesMoments.Mx], [forcesMoments.My], [forcesMoments.Mz]]
        w = [[state.p],[state.q],[state.r]]
        tempa = MatrixMath.multiply(J, w)           # J*w
        tempb = MatrixMath.multiply(wx, tempa)      # [wx]*J*w
        tempc = MatrixMath.subtract(Mb, tempb)      # Mb - [wx]*J*w
        dw = MatrixMath.multiply(J_inv, tempc)      # J^-1 (Mb - [wx]*J*w)
        d_state.p = dw[0][0]
        d_state.q = dw[1][0]
        d_state.r = dw[2][0]

        return d_state

    def ForwardEuler(self, dT, state, dot):
        ''' 
        Performs forward euler integration on the state state using the timestep dT and state derivative dot 
        Only integrates the independent variables (pn,pe,pd), (u,v,w), and (p,q,r)
        '''

        newState = States.vehicleState()

        newState.pn = state.pn + dT*dot.pn
        newState.pe = state.pe + dT*dot.pe
        newState.pd = state.pd + dT*dot.pd

        newState.u = state.u + dT*dot.u
        newState.v = state.v + dT*dot.v
        newState.w = state.w + dT*dot.w

        newState.p = state.p + dT*dot.p
        newState.q = state.q + dT*dot.q
        newState.r = state.r + dT*dot.r

        return newState

    def IntegrateState(self, dT, state, dot):
        ''' Updates the input state given the state, a time step, and the state's derivative '''

        # update matrix R
        R_mat = self.Rexp(dT, state, dot)
        R_mat = MatrixMath.multiply(R_mat, state.R)
        R_old = state.R     # preserve this so that state variable isn't perminently altered
        state.R = R_mat

        # update remaining independent variables of the state
        newState = self.ForwardEuler(dT, state, dot)
        newState.R = R_mat
        state.R = R_old     # reset state variable to original state

        # derive euler angles from the new rotation matrix
        euler_angles = Rotations.dcm2Euler(R_mat)
        newState.yaw = euler_angles[0]
        newState.pitch = euler_angles[1]
        newState.roll = euler_angles[2]

        # make sure remaining variables are preserved/updated
        newState.alpha = state.alpha
        newState.beta = state.beta
        newState.Va = state.Va
        newState.chi = math.atan2(dot.pe, dot.pn)

        return newState
