///*
// * File:   main.c
// * Author: Aaron Hunter
// * Brief:  application for Open Logger 
// * project
// * Created on April 10, 2023 
// */
//
///*******************************************************************************
// * #INCLUDES                                                                   *
// ******************************************************************************/
//#include <stdio.h>
//#include <stdint.h>
//#include <stdlib.h>
//#include <math.h>
//#include "xc.h"
//#include "inc/Board.h"
//#include "inc/SerialM32.h"
//#include "inc/System_timer.h"
//#include "inc/Radio_serial.h"
//#include "inc/lib/common/mavlink.h"
//#include "inc/NEO_M8N.h"
//#include "inc/RC_RX.h"
//#include "inc/RC_servo.h"
//#include "inc/ICM_20948.h"
//#include "inc/AS5047D.h"
//#include "inc/ICM_20948_registers.h"  //register definitions for the device
//#include "inc/NEO_M8N.h"
//#include "DPS310.h"
//
//
//
///*******************************************************************************
// * #DEFINES                                                                    *
// ******************************************************************************/
//#define HEARTBEAT_PERIOD 1000 //1 sec interval for hearbeat update
//#define FREQUENCY_PERIOD 1000
//#define CONTROL_PERIOD 20 //Period for control loop in msec #20
//#define LOG_PERIOD 10 //10 //18 for flight day
//#define PUBLISH_PERIOD 50 // Period for publishing data (msec)
//#define CONTROL_FREQUENCY (1000/CONTROL_PERIOD) //frequency in Hz
//#define BUFFER_SIZE 2048
//#define RAW 1
//#define SCALED 2
//#define CTS_2_USEC 1
//#define SAFE_HIGH 1790  // normal val 1809
//#define SAFE_LOW 1000    // normal val 992
//#define INTERFACE_MODE IMU_SPI_MODE
//#define DELAY(x)    {int wait; for (wait = 0; wait <= x; wait++) {asm("nop");}}
//#define A_BIT       18300
//#define A_LOT       183000
//
///*******************************************************************************
// * VARIABLES                                                                   *
// ******************************************************************************/
//mavlink_system_t mavlink_system = {
//    1, // System ID (1-255)
//    MAV_COMP_ID_AUTOPILOT1 // Component ID (a MAV_COMPONENT value)
//};
//
//enum RC_channels {
//    AIL,
//    ELE,
//    THR,
//    RUD,
//    HASH,
//    SAFE,
//    SWITCH_A,
//    SWITCH_B,
//    SWITCH_C,
//    SWITCH_D,
//    SWITCH_E
//}; //map to the car controls from the RC receiver
//
//enum mav_output_type {
//    USB,
//    RADIO
//};
//
//enum flight_mode {
//    MANUAL, AUTONOMOUS
//};
//
//static enum flight_mode mode = MANUAL;
//
//const uint16_t RC_raw_fs_scale = RC_RAW_TO_FS;
//
//RCRX_channel_buffer RC_channels[CHANNELS];
//static uint8_t pub_RC_servo = FALSE;
//static uint8_t pub_RC_signals = TRUE;
//static uint16_t ail_prev = 0;
//static uint16_t ele_prev = 0;
//static uint16_t thr_prev = 0;
//static uint16_t rud_prev = 0;
//static int safe = 69;
//static uint8_t is_data_valid = FALSE;
//static uint8_t is_data_new = FALSE;
//static uint8_t logOutBuffer[BUFFER_SIZE]; 
//struct IMU_out IMU_raw = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //container for raw IMU data
//struct IMU_out IMU_scaled = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //container for scaled IMU data
//
///* IMU data arrays */
//float gyro_cal[MSZ] = {0, 0, 0};
//float acc_cal[MSZ] = {0, 0, 0};
//float mag_cal[MSZ] = {0, 0, 0};
//
///* GPS data */
//struct GPS_data GPSdata;
//
///*Baro Data*/
//struct BARO_out BARO_scaled = {0, 0};
//
//const float deg2rad = M_PI / 180.0;
//static uint8_t imuReady = FALSE;
//static uint8_t baroReady = FALSE;
//static uint8_t gpsReady = FALSE;
//uint8_t imu_buffer[BUFFER_SIZE];
//uint8_t gps_buffer[BUFFER_SIZE];
//uint8_t baro_buffer[BUFFER_SIZE];
//uint8_t log_buffer[BUFFER_SIZE];
//
//int counter = 0;
//static int printCount = 0;
///*******************************************************************************
// * TYPEDEFS                                                                    *
// ******************************************************************************/
//
//
///*******************************************************************************
// * FUNCTION PROTOTYPES                                                         *
// ******************************************************************************/
///**
// * @function RC_channels_init(void)
// * @param none
// * @brief set all RC channels to RC_RX_MID_COUNTS
// * @author Aaron Hunter
// */
//void RC_channels_init(void);
///**
// * @function check_RC_events(void)
// * @param none
// * @brief checks for RC messages and stores data in RC channel buffer
// * @author Aaron Hunter
// */
//void check_RC_events();
//
//
///**
// * @function check_IMU_events(void)
// * @param none
// * @brief detects when IMU SPI transaction completes and then publishes data over Mavlink
// * @author Aaron Hunter
// */
//void check_IMU_events(void);
//
//
///*
// * @function check_GPS_events(void)
// * @param none
// * @brief detect when GPS transaction completes  
// * @author ccuyos
// */
//void check_GPS_events(void);
//
//
///*
// * @function check_BARO_EVENTS(void)
// * @param none
// * @brief detect when barometer transcation completes
// * @author ccuyos 
// */
//void check_BARO_events(void);
//
///**
// * @function check_radio_events(void)
// * @param none
// * @brief looks for messages sent over the radio serial port to OSAVC, parses
// * them and provides responses, if needed
// * @note currently only pushing information to usb-serial port
// * @author Aaron Hunter
// */
//
//void check_radio_events(void);
//
///**
// * @function publish_RC_signals_raw(void)
// * @param none
// * @brief scales raw RC signals
// * @author Aaron Hunter
// */
//void publish_RC_signals_raw(void);
//
///**
// * @Function publish_heartbeat(uint8_t dest)
// * @param dest, either USB or RADIO
// * @brief publishes heartbeat message 
// * @return none
// * @author Aaron Hunter
// */
//void publish_heartbeat(uint8_t dest);
////void publish_heartbeat(void);
///**
// * @Function publish_h(uint8_t param_id[16], uint8_t dest)
// * @param parameter ID
// * @param dest, USB or RADIO
// * @brief invokes mavlink helper to send out stored parameter 
// * @author aaron hunter
// */
//void publish_parameter(uint8_t param_id[16], uint8_t dest);
//
///**
// * @Function mavprint(char msg_buffer[], int8_t msg_length, int8_t output);
// * @param msg_buffer, string of bytes to send to receiver
// * @param msg_length, length of msg_buffer
// * @param output, either USB or RADIO, which peripheral to send the message from
// * @return SUCCESS or ERROR
// */
//int8_t mavprint(uint8_t msg_buffer[], uint8_t msg_length, uint8_t output);
//
///**
// * @Function calc_pw(uint16_t raw_counts)
// * @param raw counts from the radio transmitter (11 bit unsigned int)
// * @return pulse width in microseconds
// * @brief converts the RC input into the equivalent pulsewidth output for servo
// * and ESC control
// * @author aahunter
// * @modified <Your Name>, <year>.<month>.<day> <hour> <pm/am> */
//static uint16_t calc_pw(uint16_t raw_counts);
//
///**
// * @Function set_control_output(void)
// * @param none
// * @return none
// * @brief converts RC input signals to pulsewidth values and sets the actuators
// * (servos and ESCs) to those values
// * @author Aaron Hunter
// */
//void set_control_output(void);
//
///**
// * @function publish_IMU_data()
// * @param data_type RAW or SCALED
// * @brief reads module level IMU data and publishes over radio serial in Mavlink
// * @author Aaron Hunter
// */
//
//void publish_IMU_data(uint8_t data_type, uint8_t dest);
///*DATA LOGGER FUNCTIONS*/
//
//void log_IMU(uint32_t time);
//void log_GPS(uint32_t time);
//void log_BARO(uint32_t time);
//void log_sensors(uint32_t time);
//
///*IMU data*/
//int IMU_err = 0;
//struct IMU_out IMU_data_raw = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//struct IMU_out IMU_data_scaled = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
///* test values: mag cal from Dorveaux*/
//
//float A_acc[MSZ][MSZ] = {
//    6.01180201773358e-05, -6.28352073406424e-07, -3.91326747595870e-07,
//    -1.18653342135860e-06, 6.01268083773005e-05, -2.97010157797952e-07,
//    -3.19011230800348e-07, -3.62174516629958e-08, 6.04564465269327e-05
//};
//float A_mag_fl[3][3] = {
//    0.00351413733554131, -1.74599042407869e-06, -1.62761272908763e-05,
//    6.73767225208446e-06, 0.00334531206332366, -1.35302929502152e-05,
//    -3.28233797524166e-05, 9.29337701972177e-06, 0.00343350080131375
//};
//float b_acc[MSZ] = {-0.0156750747576770, -0.0118720194488050, -0.0240128301624044};
//float b_mag_fl[3] = {-0.809679246097106, 0.700742334522691, -0.571694648765172};
//float A_test[MSZ][MSZ];
//float b_test[MSZ];
//
//
//
///*******************************************************************************
// * FUNCTIONS                                                                   *
// ******************************************************************************/
//
///**
// * @function RC_channels_init(void)
// * @param none
// * @brief set all RC channels to RC_RX_MID_COUNTS
// * @author Aaron Hunter
// */
//void RC_channels_init(void) {
//    uint8_t i;
//    for (i = 0; i < CHANNELS; i++) {
//        RC_channels[i] = RC_RX_MID_COUNTS;
//    }
//}
//
///**
// * @function check_RC_events(void)
// * @param none
// * @brief checks for RC messages and stores data in RC channel buffer
// * @author Aaron Hunter
// */
//void check_RC_events() {
//    if (RCRX_new_cmd_avail()) {
//        RCRX_get_cmd(RC_channels);
//    }
//}
//
//void check_USB_events(void) {
//    uint8_t channel = MAVLINK_COMM_0;
//    uint8_t msg_byte;
//    uint16_t msg_length;
//    uint8_t msg_buffer[BUFFER_SIZE];
//    mavlink_message_t msg_rx;
//    mavlink_status_t msg_rx_status;
//
//    //MAVLink command structs
//    mavlink_heartbeat_t heartbeat;
//    mavlink_command_long_t command_qgc;
//    mavlink_param_request_read_t param_read;
//
//    if (msg_byte = get_char()) {
//        if (mavlink_parse_char(channel, msg_byte, &msg_rx, &msg_rx_status)) {
//            switch (msg_rx.msgid) {
//                case MAVLINK_MSG_ID_HEARTBEAT:
//                    mavlink_msg_heartbeat_decode(&msg_rx, &heartbeat);
//                    if (heartbeat.type) {
//                        msg_length = sprintf(msg_buffer, "heartbeat received type(%d)\r\n", heartbeat.type);
//                        mavprint(msg_buffer, msg_length, RADIO);
//                    }
//                    break;
//                case MAVLINK_MSG_ID_COMMAND_LONG:
//                    mavlink_msg_command_long_decode(&msg_rx, &command_qgc);
//                    msg_length = sprintf(msg_buffer, "Command ID %d received from Ground Control\r\n", command_qgc.command);
//                    mavprint(msg_buffer, msg_length, RADIO);
//                    break;
//                case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
//                    mavlink_msg_param_request_read_decode(&msg_rx, &param_read);
//                    msg_length = sprintf(msg_buffer, "Parameter request ID %s received from Ground Control\r\n", param_read.param_id);
//                    mavprint(msg_buffer, msg_length, RADIO);
//                    publish_parameter(param_read.param_id, USB);
//                    break;
//                default:
//                    msg_length = sprintf(msg_buffer, "Received message with ID %d, sequence: %d from component %d of system %d\r\n",
//                            msg_rx.msgid, msg_rx.seq, msg_rx.compid, msg_rx.sysid);
//                    mavprint(msg_buffer, msg_length, RADIO);
//                    break;
//            }
//        }
//    }
//}
///**
// * @function check_IMU_events(void)
// * @param none
// * @brief detects when IMU SPI transaction completes and then publishes data over Mavlink
// * @author Aaron Hunter
// */
//void check_IMU_events(void) {
//    if (IMU_is_data_ready() == TRUE) {
//        imuReady = TRUE;
//        IMU_get_raw_data(&IMU_raw);
//        IMU_get_norm_data(&IMU_scaled);
//
//        acc_cal[0] = (float) IMU_scaled.acc.x;
//        acc_cal[1] = (float) IMU_scaled.acc.y;
//        acc_cal[2] = (float) IMU_scaled.acc.z;
//        mag_cal[0] = (float) IMU_scaled.mag.x;
//        mag_cal[1] = (float) IMU_scaled.mag.y;
//        mag_cal[2] = (float) IMU_scaled.mag.z;
//        /*scale gyro readings into rad/sec */
//        gyro_cal[0] = (float) IMU_scaled.gyro.x * deg2rad;
//        gyro_cal[1] = (float) IMU_scaled.gyro.y * deg2rad;
//        gyro_cal[2] = (float) IMU_scaled.gyro.z * deg2rad;
//        
//    }
//}
//
//
///*
// * @function check_GPS_events(void)
// * @param none
// * @brief detect when GPS transaction completes  
// * @author ccuyos
// */
//void check_GPS_events(void){
//    if (GPS_is_msg_avail() == TRUE) {
//        GPS_parse_stream();
//    }
//    if (GPS_is_data_avail() == TRUE){
//        GPS_get_data(&GPSdata);
//        gpsReady = TRUE;
//    }
//}
//
///*
// * @function check_BARO_EVENTS(void)
// * @param none
// * @brief detect when barometer transcation completes
// * @author ccuyos 
// */
//void check_BARO_events(void){
//    if (Barometer_is_data_ready()){
//        Barometer_get_scaled_data(&BARO_scaled);
//        baroReady = TRUE;
//    }
//}
//
//
//
//
///**
// * @function check_radio_events(void)
// * @param none
// * @brief looks for messages sent over the radio serial port to OSAVC, parses
// * them and provides responses, if needed
// * @note currently only pushing information to usb-serial port
// * @author Aaron Hunter
// */
//void check_radio_events(void) {
//    uint8_t channel = MAVLINK_COMM_0;
//    uint8_t msg_byte;
//    uint16_t msg_length;
//    uint8_t msg_buffer[BUFFER_SIZE];
//    mavlink_message_t msg_rx;
//    mavlink_status_t msg_rx_status;
//
//    //MAVLink command structs
//    mavlink_heartbeat_t heartbeat;
//    mavlink_command_long_t command_qgc;
//    mavlink_param_request_read_t param_read;
//
//    if (Radio_data_available()) {
//        msg_byte = Radio_get_char();
//        if (mavlink_parse_char(channel, msg_byte, &msg_rx, &msg_rx_status)) {
//            switch (msg_rx.msgid) {
//                case MAVLINK_MSG_ID_HEARTBEAT:
//                    mavlink_msg_heartbeat_decode(&msg_rx, &heartbeat);
//                    if (heartbeat.type) {
//                        msg_length = sprintf(msg_buffer, "heartbeat received type(%d)\r\n", heartbeat.type);
//                        mavprint(msg_buffer, msg_length, RADIO);
//                    }
//                    break;
//                case MAVLINK_MSG_ID_COMMAND_LONG:
//                    mavlink_msg_command_long_decode(&msg_rx, &command_qgc);
//                    msg_length = sprintf(msg_buffer, "Command ID %d received from Ground Control\r\n", command_qgc.command);
//                    mavprint(msg_buffer, msg_length, RADIO);
//                    break;
//                case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
//                    mavlink_msg_param_request_read_decode(&msg_rx, &param_read);
//                    msg_length = sprintf(msg_buffer, "Parameter request ID %s received from Ground Control\r\n", param_read.param_id);
//                    mavprint(msg_buffer, msg_length, RADIO);
//                    publish_parameter(param_read.param_id, USB);
//                    break;
//                default:
//                    msg_length = sprintf(msg_buffer, "Received message with ID %d, sequence: %d from component %d of system %d\r\n",
//                            msg_rx.msgid, msg_rx.seq, msg_rx.compid, msg_rx.sysid);
//                    mavprint(msg_buffer, msg_length, RADIO);
//                    break;
//            }
//        }
//    }
//}
//
/////**
//// * @function publish_RC_signals(void)
//// * @param none
//// * @brief scales raw RC signals into +/- 10000
//// * @author Aaron Hunter
//// */
//
//void publish_RC_signals(void) {
//    mavlink_message_t msg_tx;
//    uint16_t msg_length;
//    uint8_t msg_buffer[BUFFER_SIZE];
//    uint16_t index = 0;
//    uint8_t RC_port = 0; //first 8 channels 
//    int16_t scaled_channels[CHANNELS];
//    uint8_t rssi = 255; //unknown--may be able to extract from receiver
//    for (index = 0; index < CHANNELS; index++) {
//        scaled_channels[index] = (RC_channels[index] - RC_RX_MID_COUNTS) * RC_raw_fs_scale;
//    }
//    mavlink_msg_rc_channels_scaled_pack(mavlink_system.sysid,
//            mavlink_system.compid,
//            &msg_tx,
//            Sys_timer_get_msec(),
//            RC_port,
//            scaled_channels[0],
//            scaled_channels[1],
//            scaled_channels[2],
//            scaled_channels[3],
//            scaled_channels[4],
//            scaled_channels[5],
//            scaled_channels[6],
//            scaled_channels[7],
//            rssi);
//    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
//    for (index = 0; index < msg_length; index++) {
//        Radio_put_char(msg_buffer[index]);
//    }
//}
////
/////**
//// * @function publish_RC_signals_raw(void)
//// * @param none
//// * @brief scales raw RC signals
//// * @author Aaron Hunter
//// */
//
//void publish_RC_signals_raw(void) {
//    mavlink_message_t msg_tx;
//    uint16_t msg_length;
//    uint8_t msg_buffer[BUFFER_SIZE];
//    uint16_t index = 0;
//    uint8_t RC_port = 0; //first 8 channels 
//    uint8_t rssi = 255; //unknown--may be able to extract from receiver
//    mavlink_msg_rc_channels_raw_pack(mavlink_system.sysid,
//            mavlink_system.compid,
//            &msg_tx,
//            Sys_timer_get_msec(),
//            RC_port,
//            RC_channels[0],
//            RC_channels[1],
//            RC_channels[2],
//            RC_channels[3],
//            RC_channels[4],
//            RC_channels[5],
//            RC_channels[6],
//            RC_channels[7],
//            rssi);
//    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
//    for (index = 0; index < msg_length; index++) {
//        Radio_put_char(msg_buffer[index]);
//    }
//}
/////**
//// * @Function publish_heartbeat(void)
//// * @param none
//// * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
//// * @author aaron hunter
//// */
////void publish_heartbeat(void) {
////    mavlink_message_t msg_tx;
////    uint16_t msg_length;
////    uint8_t msg_buffer[BUFFER_SIZE];
////    uint16_t index = 0;
////    uint8_t mode = MAV_MODE_FLAG_MANUAL_INPUT_ENABLED | MAV_MODE_FLAG_SAFETY_ARMED;
////    uint32_t custom = 0;
////    uint8_t state = MAV_STATE_STANDBY;
////    mavlink_msg_heartbeat_pack(mavlink_system.sysid
////            , mavlink_system.compid,
////            &msg_tx,
////            MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC,
////            mode,
////            custom,
////            state);
////    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
////    for (index = 0; index < msg_length; index++) {
////        Radio_put_char(msg_buffer[index]);
////    }
////}
///**
// * @Function publish_heartbeat(mav_output_type dest)
// * @param dest, either USB or RADIO
// * @brief publishes heartbeat message 
// * @return none
// * @author Aaron Hunter
// */
//void publish_heartbeat(uint8_t dest) {
//    mavlink_message_t msg_tx;
//    uint16_t msg_length;
//    uint8_t msg_buffer[BUFFER_SIZE];
//    uint8_t mode = MAV_MODE_FLAG_MANUAL_INPUT_ENABLED | MAV_MODE_FLAG_SAFETY_ARMED;
//    uint32_t custom = 0;
//    uint8_t state = MAV_STATE_STANDBY;
//    mavlink_msg_heartbeat_pack(mavlink_system.sysid
//            , mavlink_system.compid,
//            &msg_tx,
//            MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC,
//            mode,
//            custom,
//            state);
//    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
//    mavprint(msg_buffer, msg_length, dest);
//}
//
///**
// * @Function publish_parameter(uint8_t param_id[16])
// * @param parameter ID
// * @brief invokes mavlink helper to send out stored parameter 
// * @author aaron hunter
// */
//void publish_parameter(uint8_t param_id[16], uint8_t dest) {
//    mavlink_message_t msg_tx;
//    uint16_t msg_length;
//    uint8_t msg_buffer[BUFFER_SIZE];
//    uint16_t index = 0;
//    float param_value = 320.0; // value of the requested parameter
//    uint8_t param_type = MAV_PARAM_TYPE_INT16; // onboard mavlink parameter type
//    uint16_t param_count = 1; // total number of onboard parameters
//    uint16_t param_index = 1; //index of this value
//    mavlink_msg_param_value_pack(mavlink_system.sysid,
//            mavlink_system.compid,
//            &msg_tx,
//            param_id,
//            param_value,
//            param_type,
//            param_count,
//            param_index
//            );
//    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
//    mavprint(msg_buffer, msg_length, USB);
//}
//
//
///**
// * @function publish_IMU_data()
// * @param none
// * @brief reads module level IMU data and publishes over radio serial in Mavlink
// * @author Aaron Hunter
// */
//void publish_IMU_data(uint8_t data_type, uint8_t dest) {
//    mavlink_message_t msg_tx;
//    uint16_t msg_length;
//    uint8_t msg_buffer[BUFFER_SIZE];
//    uint8_t IMU_id = 0;
//    if (data_type == RAW) {
//        mavlink_msg_raw_imu_pack(mavlink_system.sysid,
//                mavlink_system.compid,
//                &msg_tx,
//                Sys_timer_get_usec(),
//                (int16_t) IMU_raw.acc.x,
//                (int16_t) IMU_raw.acc.y,
//                (int16_t) IMU_raw.acc.z,
//                (int16_t) IMU_raw.gyro.x,
//                (int16_t) IMU_raw.gyro.y,
//                (int16_t) IMU_raw.gyro.z,
//                (int16_t) IMU_raw.mag.x,
//                (int16_t) IMU_raw.mag.y,
//                (int16_t) IMU_raw.mag.z,
//                IMU_id,
//                (int16_t) IMU_raw.temp
//                );
//    } else if (data_type == SCALED) {
//        mavlink_msg_highres_imu_pack(mavlink_system.sysid,
//                mavlink_system.compid,
//                &msg_tx,
//                Sys_timer_get_usec(),
//                (float) IMU_scaled.acc.x,
//                (float) IMU_scaled.acc.y,
//                (float) IMU_scaled.acc.z,
//                (float) IMU_scaled.gyro.x,
//                (float) IMU_scaled.gyro.y,
//                (float) IMU_scaled.gyro.z,
//                (float) IMU_scaled.mag.x,
//                (float) IMU_scaled.mag.y,
//                (float) IMU_scaled.mag.z,
//                0.0, //no pressure
//                0.0, //no diff pressure
//                0.0, //no pressure altitude
//                (float) IMU_scaled.temp,
//                0, //bitfields updated
//                IMU_id
//                );
//    }
//    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
//    mavprint(msg_buffer, msg_length, dest);
//}
//
//
//
///**
// * @Function calc_pw(uint16_t raw_counts)
// * @param raw counts from the radio transmitter (11 bit unsigned int)
// * @return pulse width in microseconds
// * @brief converts the RC input into the equivalent pulsewidth output for servo
// * and ESC control
// * @author aahunter
// * @modified <Your Name>, <year>.<month>.<day> <hour> <pm/am> */
//static uint16_t calc_pw(uint16_t raw_counts) {
//    int16_t normalized_pulse; //converted to microseconds and centered at 0
//    uint16_t pulse_width; //servo output in microseconds
//    normalized_pulse = (raw_counts - RC_RX_MID_COUNTS) >> CTS_2_USEC;
//    pulse_width = normalized_pulse + RC_SERVO_CENTER_PULSE;
//    return pulse_width;
//}
//
///**
// * @Function set_control_output(void)
// * @param none
// * @return none
// * @brief converts RC input signals to pulsewidth values and sets the actuators
// * (servos and ESCs) to those values
// * @author Aaron Hunter
// */
//void set_control_output(void) {
//
//    char message[BUFFER_SIZE];
//    uint8_t msg_len = 0;
//    int index;
//    int hash, ail, ele, thr, rud;
//    int hash_check;
//    const int tol = 10;
//    int INTOL;
//
//    /* Check switch state*/
//    /*if switch state == up*/
//    /* Manual control enabled */
//    /* if switch state == down*/
//    /* Autonomous mode enabled*/
//    /* get RC commanded values*/
//
//
//    safe = RC_channels[SAFE]; //1807 when ON, 240 when OFF
//    hash = RC_channels[HASH]; //1807 when ON, 240 when OFF
//    ail = RC_channels[AIL];
//    ele = RC_channels[ELE];
//    thr = RC_channels[THR];
//    rud = RC_channels[RUD];
//
//
//    hash_check = (thr >> 2) + (ail >> 2) + (ele >> 2) + (rud >> 2);
//    
//    // corruption check
//    if (abs(hash_check - hash) <= tol) {
//        INTOL = TRUE;
//        
//        // check if mode switching occurs
//        if (safe <= SAFE_LOW) {
//            mode = MANUAL;
//        } else if (safe >= SAFE_HIGH) {
//            mode = AUTONOMOUS;
//        }
//        
//        /* send commands to motor outputs*/
//        if (mode == MANUAL) {
//            RC_servo_set_pulse(calc_pw(ail), SERVO_PWM_1);
//            RC_servo_set_pulse(calc_pw(ele), SERVO_PWM_2);
//            RC_servo_set_pulse(calc_pw(thr), SERVO_PWM_3);
//            RC_servo_set_pulse(calc_pw(rud), SERVO_PWM_4);
//        } else{
////            printf("I am autonomous! \r\n");
//        }
//
//    } else {
//        INTOL = FALSE;
////        msg_len = sprintf(message, "%d, %d, %d, %d, %d, %d, %d \r\n", thr, ail, ele, rud, hash, hash_check, INTOL);
////        for (index = 0; index < msg_len; index++) {
////            Radio_put_char(message[index]);
////        }
//    }
//
//}
///**

// * @Function mavprint(char msg_buffer[], int8_t msg_length, int8_t output);
// * @param msg_buffer, string of bytes to send to receiver
// * @param msg_length, length of msg_buffer
// * @param output, either USB or RADIO, which peripheral to send the message from
// * @return SUCCESS or ERROR
// */
//int8_t mavprint(uint8_t msg_buffer[], uint8_t msg_length, uint8_t output) {
//    uint8_t i;
//    if (output == USB) {
//        for (i = 0; i < msg_length; i++) {
//            
//            printCount++;
//            putchar(msg_buffer[i]);
//        }
//    } else if (output == RADIO) {
//        for (i = 0; i < msg_length; i++) {
//            printCount++;
//           Radio_put_char(msg_buffer[i]);
//        }
//    } else {
//        return ERROR;
//    }
////    printf("Print Count: %d\n", printCount);
////    printCount = 0;
//    return SUCCESS;
//}
//
//
///**
// * @Function log_IMU(void)
// * @param none
// * @return none
// * @brief creates an IMU packet frame for the Sparkfun OpenLogger 
// * @author ccuyos
// */
//void log_IMU(uint32_t time){
//     if (imuReady){
//         imuReady = FALSE;
//            sprintf(imu_buffer, ",%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%0.1f,%x",
//                    (float) IMU_scaled.acc.x, (float) IMU_scaled.acc.y, (float) IMU_scaled.acc.z,
//                    (float) IMU_scaled.gyro.x, (float) IMU_scaled.gyro.y, (float) IMU_scaled.gyro.z,
//                    (float) (IMU_scaled.mag.x), (float) (IMU_scaled.mag.y), (float) (IMU_scaled.mag.z),
//                    (float) IMU_scaled.temp, IMU_scaled.mag_status);
//            //printf("%s\n", imu_buffer);
//     }
//     else{
//         sprintf(imu_buffer, "");
//     }
//}
//void log_GPS(uint32_t time){
//    if (gpsReady){
//        gpsReady = FALSE;
//        sprintf(gps_buffer, ",%0.6f,%0.6f,%3.3f,%0.6f", GPSdata.lat,
//                     GPSdata.lon, GPSdata.spd, GPSdata.cog);
//    }
//    else{
//        sprintf(gps_buffer, "");
//    }
//}
//void log_BARO(uint32_t time){
//    if (baroReady){
//        counter++;
//        baroReady = FALSE;
//        sprintf(baro_buffer, ",%0.3f,%0.3f", BARO_scaled.calibrated_temp, BARO_scaled.calibrated_pressure);
//    }
//    else{
//       sprintf(baro_buffer, "");
//    }
//    
//}
//
//void log_sensors(uint32_t time){
//    
//    uint8_t imuAvailable = (strcmp(imu_buffer, ""));
//    uint8_t gpsAvailable = (strcmp(gps_buffer, ""));
//    uint8_t baroAvailable = (strcmp(baro_buffer, ""));
//    
//    uint8_t sensorAvailable = !(!imuAvailable) << 2 | !(!gpsAvailable) << 1 | !(!baroAvailable);
//    uint8_t timeVal[BUFFER_SIZE];
//    sprintf(timeVal, "%x", time);
//    sprintf(log_buffer, "%d,%x,%x%s%s%s\n", 0, time, sensorAvailable, imu_buffer, gps_buffer, baro_buffer);
//    mavprint(log_buffer, strlen(log_buffer), RADIO);
//}
//
//
//
//#ifdef OPENLOGGER_TEST
//int main(void) {
//    uint32_t cur_time = 0;
//    uint32_t warmup_time = 250; //time in ms to allow subsystems to stabilize (IMU))
//    uint32_t control_start_time = 0;
//    uint32_t heartbeat_start_time = 0;
//    uint32_t publish_start_time = 0;
//    uint32_t log_start_time = 0;
//    uint8_t index;
//    uint8_t error_report = 50;
//    RCRX_channel_buffer channels[CHANNELS];
//    //IMU variables
//    int value = 0;
//    int i;
//    int row;
//    int col;
//    int IMU_err = 0;
//    
//    //GPS variables
//    
//
//    //    //Initialization routines
//    Board_init(); //board configuration
//    Serial_init(); //start debug terminal (USB)
//    Sys_timer_init(); //start the system timer
//    IMU_err = IMU_init(INTERFACE_MODE);
//    //asm(nop);
//    DELAY(1);
//    GPS_init();
//    Barometer_init();
//    //printf("NOT STUCK AT GPS INIT\n");
//    /*small delay to get all the subsystems time to get online*/
//    while (cur_time < warmup_time) {
//        cur_time = Sys_timer_get_msec();
//    }
//    Radio_serial_init(); //start the radios
//
//    RCRX_init(); //initialize the radio control system
//    RC_channels_init(); //set channels to midpoint of RC system
//    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_1); // start the servo subsystem
//    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_2); // start the servo subsystem
//    RC_servo_init(ESC_UNIDIRECTIONAL_TYPE, SERVO_PWM_3); // start the servo subsystem
//    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_4); // start the servo subsystem
//
//
//    //printf("\r\nOpen Logger compiled on: %s, %s \r\n", __DATE__, __TIME__);
//    //printf("I am here\n");
//    IMU_set_mag_cal(A_mag_fl, b_mag_fl);
//    IMU_set_acc_cal(A_acc, b_acc);
//    publish_start_time = cur_time;
//    log_start_time = cur_time;
//    while (1) {
//        //printf("I am here\n");
//        cur_time = Sys_timer_get_msec();
//        //check for all events
//        check_radio_events(); //detect and process MAVLink incoming messages
//        check_RC_events(); //check incoming RC commands
//        check_IMU_events();
//        check_GPS_events();
//        check_BARO_events();
//        check_USB_events(); // look for MAVLink messages
//        //publish control and sensor signals (20 ms)
//        
//        if (cur_time - control_start_time >= CONTROL_PERIOD) {
//            control_start_time = cur_time; //reset control loop timer     
//            set_control_output();
//        }
//
//         
//        /* publish high speed sensors */
//        if (cur_time - publish_start_time > PUBLISH_PERIOD) {
//            publish_start_time = cur_time; //reset publishing timer
//            //printf("IMU dt: %d\n", IMU_get_dt());
//            
//        }        
//        //publish heartbeat
//        if (cur_time - heartbeat_start_time >= HEARTBEAT_PERIOD) {
//            heartbeat_start_time = cur_time; //reset the timer
//            //printf("Current frequency: %d\n", counter);
//            counter = 0;
//        }
//        
//        if (cur_time - log_start_time >= LOG_PERIOD){
//            IMU_start_data_acq();
//            log_start_time = cur_time;
//            log_IMU(cur_time);
//            log_GPS(cur_time);
//            log_BARO(cur_time);
//            log_sensors(cur_time);
//            //printf("IMU dt: %d\n", IMU_get_dt());
//            
//        }
//        
//        
//
//        
//    }
//    return 0;
//}
//#endif

/*
 * File:   main.c
 * Author: Aaron Hunter
 * Brief:  application for Open Logger 
 * project
 * Created on April 10, 2023 
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "xc.h"
#include "inc/Board.h"
#include "inc/SerialM32.h"
#include "inc/System_timer.h"
#include "inc/Radio_serial.h"
#include "inc/lib/common/mavlink.h"
#include "inc/NEO_M8N.h"
#include "inc/RC_RX.h"
#include "inc/RC_servo.h"
#include "inc/ICM_20948.h"
#include "inc/AS5047D.h"
#include "inc/ICM_20948_registers.h"  //register definitions for the device
#include "inc/NEO_M8N.h"
#include "DPS310.h"



/*******************************************************************************
 * #DEFINES                                                                    *
 ******************************************************************************/
#define HEARTBEAT_PERIOD 1000 //1 sec interval for hearbeat update
#define FREQUENCY_PERIOD 1000
#define CONTROL_PERIOD 20 //Period for control loop in msec #20
#define LOG_PERIOD 10 //10 //18 for flight day
#define PUBLISH_PERIOD 50 // Period for publishing data (msec)
#define CONTROL_FREQUENCY (1000/CONTROL_PERIOD) //frequency in Hz
#define BUFFER_SIZE 2048
#define RAW 1
#define SCALED 2
#define CTS_2_USEC 1
#define SAFE_HIGH 1790  // normal val 1809
#define SAFE_LOW 1000    // normal val 992
#define KILL_HIGH 1790
#define KILL_LOW 1000
//Actuator Macros
#define AIL_PWM SERVO_PWM_1
#define ELE_PWM SERVO_PWM_2
#define THR_PWM SERVO_PWM_3
#define RUD_PWM SERVO_PWM_4


#define INTERFACE_MODE IMU_SPI_MODE
#define DELAY(x)    {int wait; for (wait = 0; wait <= x; wait++) {asm("nop");}}
#define A_BIT       18300
#define A_LOT       183000




//Conversion for state estimation
#define ms2 9.8  //G -> ms2
#define nT 1000  //uT -> nT
#define rad 0.0174533  //deg -> rad

//Accelerometer calibration
#define BIAS_ACCX  68.7983
#define SCALE_ACCX 6.102025873e-05
#define BIAS_ACCY -196.5700
#define SCALE_ACCY 6.092731371e-05
#define BIAS_ACCZ -75.4633
#define SCALE_ACCZ 6.063178318e-05

//Gyroscope calibration
#define DRIFT_GYROX  -0.004070617273368
#define DRIFT_GYROY 2.369648499684174e-04//9.2847e-05
#define DRIFT_GYROZ 0.001435278922658
#define OFFSET_GYROX -2.1898
#define OFFSET_GYROY 0.8088
#define OFFSET_GYROZ -0.3489
#define CONV_GYRO 65.53400
#define dT_GYRO 0.02

/*******************************************************************************
 * VARIABLES                                                                   *
 ******************************************************************************/
mavlink_system_t mavlink_system = {
    1, // System ID (1-255)
    MAV_COMP_ID_AUTOPILOT1 // Component ID (a MAV_COMPONENT value)
};

enum RC_channels {
    AIL,
    ELE,
    THR,
    RUD,
    HASH,
    SAFE,
    KILL,
    SWITCH_A,
    SWITCH_B,
    SWITCH_C,
    SWITCH_D,
    SWITCH_E
}; //map to the car controls from the RC receiver

enum mav_output_type {
    USB,
    RADIO
};

enum flight_mode {
    MANUAL, AUTONOMOUS
};

static enum flight_mode mode = MANUAL;




const uint16_t RC_raw_fs_scale = RC_RAW_TO_FS;

RCRX_channel_buffer RC_channels[CHANNELS];
static uint8_t pub_RC_servo = FALSE;
static uint8_t pub_RC_signals = TRUE;
static uint16_t ail_prev = 0;
static uint16_t ele_prev = 0;
static uint16_t thr_prev = 0;
static uint16_t rud_prev = 0;
static int safe = 69;
static int kill_plane = 420;
static uint8_t is_data_valid = FALSE;
static uint8_t is_data_new = FALSE;
static uint8_t logOutBuffer[BUFFER_SIZE]; 
struct IMU_out IMU_raw = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //container for raw IMU data
struct IMU_out IMU_scaled = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //container for scaled IMU data

/* IMU data arrays */
float gyro_cal[MSZ] = {0, 0, 0};
float acc_cal[MSZ] = {0, 0, 0};
float mag_cal[MSZ] = {0, 0, 0};

/* GPS data */
struct GPS_data GPSdata;

/*Baro Data*/
struct BARO_out BARO_scaled = {0, 0};

const float deg2rad = M_PI / 180.0;
static uint8_t imuReady = FALSE;
static uint8_t baroReady = FALSE;
static uint8_t gpsReady = FALSE;
uint8_t imu_buffer[BUFFER_SIZE];
uint8_t gps_buffer[BUFFER_SIZE];
uint8_t baro_buffer[BUFFER_SIZE];
uint8_t log_buffer[BUFFER_SIZE];

int counter = 0;
static int printCount = 0;
/*******************************************************************************
 * TYPEDEFS                                                                    *
 ******************************************************************************/


/*******************************************************************************
 * FUNCTION PROTOTYPES                                                         *
 ******************************************************************************/
/**
 * @function RC_channels_init(void)
 * @param none
 * @brief set all RC channels to RC_RX_MID_COUNTS
 * @author Aaron Hunter
 */
void RC_channels_init(void);
/**
 * @function check_RC_events(void)
 * @param none
 * @brief checks for RC messages and stores data in RC channel buffer
 * @author Aaron Hunter
 */
void check_RC_events();


/**
 * @function check_IMU_events(void)
 * @param none
 * @brief detects when IMU SPI transaction completes and then publishes data over Mavlink
 * @author Aaron Hunter
 */
void check_IMU_events(void);


/*
 * @function check_GPS_events(void)
 * @param none
 * @brief detect when GPS transaction completes  
 * @author ccuyos
 */
void check_GPS_events(void);


/*
 * @function check_BARO_EVENTS(void)
 * @param none
 * @brief detect when barometer transcation completes
 * @author ccuyos 
 */
void check_BARO_events(void);

/**
 * @function check_radio_events(void)
 * @param none
 * @brief looks for messages sent over the radio serial port to OSAVC, parses
 * them and provides responses, if needed
 * @note currently only pushing information to usb-serial port
 * @author Aaron Hunter
 */

void check_radio_events(void);

/**
 * @function publish_RC_signals_raw(void)
 * @param none
 * @brief scales raw RC signals
 * @author Aaron Hunter
 */
void publish_RC_signals_raw(void);

/**
 * @Function publish_heartbeat(uint8_t dest)
 * @param dest, either USB or RADIO
 * @brief publishes heartbeat message 
 * @return none
 * @author Aaron Hunter
 */
void publish_heartbeat(uint8_t dest);
//void publish_heartbeat(void);
/**
 * @Function publish_h(uint8_t param_id[16], uint8_t dest)
 * @param parameter ID
 * @param dest, USB or RADIO
 * @brief invokes mavlink helper to send out stored parameter 
 * @author aaron hunter
 */
void publish_parameter(uint8_t param_id[16], uint8_t dest);

/**
 * @Function mavprint(char msg_buffer[], int8_t msg_length, int8_t output);
 * @param msg_buffer, string of bytes to send to receiver
 * @param msg_length, length of msg_buffer
 * @param output, either USB or RADIO, which peripheral to send the message from
 * @return SUCCESS or ERROR
 */
int8_t mavprint(uint8_t msg_buffer[], uint8_t msg_length, uint8_t output);

/**
 * @Function calc_pw(uint16_t raw_counts)
 * @param raw counts from the radio transmitter (11 bit unsigned int)
 * @return pulse width in microseconds
 * @brief converts the RC input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author aahunter
 * @modified <Your Name>, <year>.<month>.<day> <hour> <pm/am> */
static uint16_t calc_pw(uint16_t raw_counts);

/**
 * @Function set_control_output(void)
 * @param none
 * @return none
 * @brief converts RC input signals to pulsewidth values and sets the actuators
 * (servos and ESCs) to those values
 * @author Aaron Hunter
 */
void set_control_output(void);

/**
 * @function publish_IMU_data()
 * @param data_type RAW or SCALED
 * @brief reads module level IMU data and publishes over radio serial in Mavlink
 * @author Aaron Hunter
 */

void publish_IMU_data(uint8_t data_type, uint8_t dest);
/*DATA LOGGER FUNCTIONS*/

void log_IMU(uint32_t time);
void log_GPS(uint32_t time);
void log_BARO(uint32_t time);
void log_sensors(uint32_t time);

/*IMU data*/
int IMU_err = 0;
struct IMU_out IMU_data_raw = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
struct IMU_out IMU_data_scaled = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
/* test values: mag cal from Dorveaux*/
float A_acc[MSZ][MSZ] = {
   5.88931773891380e-05,    -3.40087724536171e-07,    -2.21250367678547e-07,
    -2.14849934547173e-07,    5.77741624302809e-05,    -2.38134184919193e-07,
    -3.17815515376590e-07,    -4.13416865644758e-07,    5.82643536075151e-05
};
float A_mag_fl[3][3] = {
    0.00336730429836004,    4.04769849350429e-07,    -2.15029629397491e-05,
    -1.17610824162755e-05,    0.00327613186929661,    -4.12516874120987e-05,
    3.07540892216210e-05,    -3.41842355787250e-05,    0.00321920085094393
};
float b_acc[MSZ] = {0.0286685331274404, 0.0237008031916020
, 0.00573737607323846};
float b_mag_fl[3] = {-0.112169627617264, -0.146234392448288, 1.17491873266617};
//float A_acc[MSZ][MSZ] = {
//    6.01180201773358e-05, -6.28352073406424e-07, -3.91326747595870e-07,
//    -1.18653342135860e-06, 6.01268083773005e-05, -2.97010157797952e-07,
//    -3.19011230800348e-07, -3.62174516629958e-08, 6.04564465269327e-05
//};
//float A_mag_fl[3][3] = {
//    0.00351413733554131, -1.74599042407869e-06, -1.62761272908763e-05,
//    6.73767225208446e-06, 0.00334531206332366, -1.35302929502152e-05,
//    -3.28233797524166e-05, 9.29337701972177e-06, 0.00343350080131375
//};
//float b_acc[MSZ] = {-0.0156750747576770, -0.0118720194488050, -0.0240128301624044};
//float b_mag_fl[3] = {-0.809679246097106, 0.700742334522691, -0.571694648765172};
float A_test[MSZ][MSZ];
float b_test[MSZ];



/*******************************************************************************
 * FUNCTIONS                                                                   *
 ******************************************************************************/

/**
 * @function RC_channels_init(void)
 * @param none
 * @brief set all RC channels to RC_RX_MID_COUNTS
 * @author Aaron Hunter
 */
void RC_channels_init(void) {
    uint8_t i;
    for (i = 0; i < CHANNELS; i++) {
        if (i == THR) {
            RC_channels[i] = RC_RX_MIN_COUNTS;
        }
        RC_channels[i] = RC_RX_MID_COUNTS;
    }
}

/**
 * @function check_RC_events(void)
 * @param none
 * @brief checks for RC messages and stores data in RC channel buffer
 * @author Aaron Hunter
 */
void check_RC_events() {
    if (RCRX_new_cmd_avail()) {
        RCRX_get_cmd(RC_channels);
    }
}

void check_USB_events(void) {
    uint8_t channel = MAVLINK_COMM_0;
    uint8_t msg_byte;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    mavlink_message_t msg_rx;
    mavlink_status_t msg_rx_status;

    //MAVLink command structs
    mavlink_heartbeat_t heartbeat;
    mavlink_command_long_t command_qgc;
    mavlink_param_request_read_t param_read;

    if (msg_byte = get_char()) {
        if (mavlink_parse_char(channel, msg_byte, &msg_rx, &msg_rx_status)) {
            switch (msg_rx.msgid) {
                case MAVLINK_MSG_ID_HEARTBEAT:
                    mavlink_msg_heartbeat_decode(&msg_rx, &heartbeat);
                    if (heartbeat.type) {
                        msg_length = sprintf(msg_buffer, "heartbeat received type(%d)\r\n", heartbeat.type);
                        mavprint(msg_buffer, msg_length, RADIO);
                    }
                    break;
                case MAVLINK_MSG_ID_COMMAND_LONG:
                    mavlink_msg_command_long_decode(&msg_rx, &command_qgc);
                    msg_length = sprintf(msg_buffer, "Command ID %d received from Ground Control\r\n", command_qgc.command);
                    mavprint(msg_buffer, msg_length, RADIO);
                    break;
                case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
                    mavlink_msg_param_request_read_decode(&msg_rx, &param_read);
                    msg_length = sprintf(msg_buffer, "Parameter request ID %s received from Ground Control\r\n", param_read.param_id);
                    mavprint(msg_buffer, msg_length, RADIO);
                    publish_parameter(param_read.param_id, USB);
                    break;
                default:
                    msg_length = sprintf(msg_buffer, "Received message with ID %d, sequence: %d from component %d of system %d\r\n",
                            msg_rx.msgid, msg_rx.seq, msg_rx.compid, msg_rx.sysid);
                    mavprint(msg_buffer, msg_length, RADIO);
                    break;
            }
        }
    }
}
/**
 * @function check_IMU_events(void)
 * @param none
 * @brief detects when IMU SPI transaction completes and then publishes data over Mavlink
 * @author Aaron Hunter
 */
void check_IMU_events(void) {
    if (IMU_is_data_ready() == TRUE) {
        imuReady = TRUE;
        IMU_get_raw_data(&IMU_data_raw);
        IMU_get_norm_data(&IMU_scaled);

        IMU_scaled.acc.x = ((IMU_data_raw.acc.x - BIAS_ACCX) * SCALE_ACCX) * ms2;
        IMU_scaled.acc.y = ((IMU_data_raw.acc.y - BIAS_ACCY) * SCALE_ACCY) * ms2;
        IMU_scaled.acc.z = ((IMU_data_raw.acc.z - BIAS_ACCZ) * SCALE_ACCZ) * ms2;

        //Calibrating gyros
        float angularX = (IMU_data_raw.gyro.x/CONV_GYRO) - OFFSET_GYROX;
        float angularY = (IMU_data_raw.gyro.y/CONV_GYRO) - OFFSET_GYROY;
        float angularZ = (IMU_data_raw.gyro.z/CONV_GYRO) - OFFSET_GYROZ;
        IMU_scaled.gyro.x += angularX * dT_GYRO - DRIFT_GYROX;
        IMU_scaled.gyro.y += angularY * dT_GYRO - DRIFT_GYROY;
        IMU_scaled.gyro.z += angularZ * dT_GYRO - DRIFT_GYROZ;


        //Calibrating magnetometer
        IMU_scaled.mag.x = IMU_scaled.mag.x * 22.6338;
        IMU_scaled.mag.y = IMU_scaled.mag.y * 5.1879;
        IMU_scaled.mag.z = IMU_scaled.mag.z * 41.2284;
        
//        acc_cal[0] = (float) IMU_scaled.acc.x;
//        acc_cal[1] = (float) IMU_scaled.acc.y;
//        acc_cal[2] = (float) IMU_scaled.acc.z;
//        mag_cal[0] = (float) IMU_scaled.mag.x;
//        mag_cal[1] = (float) IMU_scaled.mag.y;
//        mag_cal[2] = (float) IMU_scaled.mag.z;
//        /*scale gyro readings into rad/sec */
//        gyro_cal[0] = (float) IMU_scaled.gyro.x * deg2rad;
//        gyro_cal[1] = (float) IMU_scaled.gyro.y * deg2rad;
//        gyro_cal[2] = (float) IMU_scaled.gyro.z * deg2rad;
        
    }
}


/*
 * @function check_GPS_events(void)
 * @param none
 * @brief detect when GPS transaction completes  
 * @author ccuyos
 */
void check_GPS_events(void){
    if (GPS_is_msg_avail() == TRUE) {
        GPS_parse_stream();
    }
    if (GPS_is_data_avail() == TRUE){
        GPS_get_data(&GPSdata);
        gpsReady = TRUE;
    }
}

/*
 * @function check_BARO_EVENTS(void)
 * @param none
 * @brief detect when barometer transcation completes
 * @author ccuyos 
 */
void check_BARO_events(void){
    if (Barometer_is_data_ready()){
        Barometer_get_scaled_data(&BARO_scaled);
        baroReady = TRUE;
    }
}




/**
 * @function check_radio_events(void)
 * @param none
 * @brief looks for messages sent over the radio serial port to OSAVC, parses
 * them and provides responses, if needed
 * @note currently only pushing information to usb-serial port
 * @author Aaron Hunter
 */
void check_radio_events(void) {
    uint8_t channel = MAVLINK_COMM_0;
    uint8_t msg_byte;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    mavlink_message_t msg_rx;
    mavlink_status_t msg_rx_status;

    //MAVLink command structs
    mavlink_heartbeat_t heartbeat;
    mavlink_command_long_t command_qgc;
    mavlink_param_request_read_t param_read;

    if (Radio_data_available()) {
        msg_byte = Radio_get_char();
        if (mavlink_parse_char(channel, msg_byte, &msg_rx, &msg_rx_status)) {
            switch (msg_rx.msgid) {
                case MAVLINK_MSG_ID_HEARTBEAT:
                    mavlink_msg_heartbeat_decode(&msg_rx, &heartbeat);
                    if (heartbeat.type) {
                        msg_length = sprintf(msg_buffer, "heartbeat received type(%d)\r\n", heartbeat.type);
                        mavprint(msg_buffer, msg_length, RADIO);
                    }
                    break;
                case MAVLINK_MSG_ID_COMMAND_LONG:
                    mavlink_msg_command_long_decode(&msg_rx, &command_qgc);
                    msg_length = sprintf(msg_buffer, "Command ID %d received from Ground Control\r\n", command_qgc.command);
                    mavprint(msg_buffer, msg_length, RADIO);
                    break;
                case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
                    mavlink_msg_param_request_read_decode(&msg_rx, &param_read);
                    msg_length = sprintf(msg_buffer, "Parameter request ID %s received from Ground Control\r\n", param_read.param_id);
                    mavprint(msg_buffer, msg_length, RADIO);
                    publish_parameter(param_read.param_id, USB);
                    break;
                default:
                    msg_length = sprintf(msg_buffer, "Received message with ID %d, sequence: %d from component %d of system %d\r\n",
                            msg_rx.msgid, msg_rx.seq, msg_rx.compid, msg_rx.sysid);
                    mavprint(msg_buffer, msg_length, RADIO);
                    break;
            }
        }
    }
}

///**
// * @function publish_RC_signals(void)
// * @param none
// * @brief scales raw RC signals into +/- 10000
// * @author Aaron Hunter
// */

void publish_RC_signals(void) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t RC_port = 0; //first 8 channels 
    int16_t scaled_channels[CHANNELS];
    uint8_t rssi = 255; //unknown--may be able to extract from receiver
    for (index = 0; index < CHANNELS; index++) {
        scaled_channels[index] = (RC_channels[index] - RC_RX_MID_COUNTS) * RC_raw_fs_scale;
    }
    mavlink_msg_rc_channels_scaled_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            Sys_timer_get_msec(),
            RC_port,
            scaled_channels[0],
            scaled_channels[1],
            scaled_channels[2],
            scaled_channels[3],
            scaled_channels[4],
            scaled_channels[5],
            scaled_channels[6],
            scaled_channels[7],
            rssi);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        Radio_put_char(msg_buffer[index]);
    }
}
//
///**
// * @function publish_RC_signals_raw(void)
// * @param none
// * @brief scales raw RC signals
// * @author Aaron Hunter
// */

void publish_RC_signals_raw(void) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t RC_port = 0; //first 8 channels 
    uint8_t rssi = 255; //unknown--may be able to extract from receiver
    mavlink_msg_rc_channels_raw_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            Sys_timer_get_msec(),
            RC_port,
            RC_channels[0],
            RC_channels[1],
            RC_channels[2],
            RC_channels[3],
            RC_channels[4],
            RC_channels[5],
            RC_channels[6],
            RC_channels[7],
            rssi);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        Radio_put_char(msg_buffer[index]);
    }
}
///**
// * @Function publish_heartbeat(void)
// * @param none
// * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
// * @author aaron hunter
// */
//void publish_heartbeat(void) {
//    mavlink_message_t msg_tx;
//    uint16_t msg_length;
//    uint8_t msg_buffer[BUFFER_SIZE];
//    uint16_t index = 0;
//    uint8_t mode = MAV_MODE_FLAG_MANUAL_INPUT_ENABLED | MAV_MODE_FLAG_SAFETY_ARMED;
//    uint32_t custom = 0;
//    uint8_t state = MAV_STATE_STANDBY;
//    mavlink_msg_heartbeat_pack(mavlink_system.sysid
//            , mavlink_system.compid,
//            &msg_tx,
//            MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC,
//            mode,
//            custom,
//            state);
//    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
//    for (index = 0; index < msg_length; index++) {
//        Radio_put_char(msg_buffer[index]);
//    }
//}
/**
 * @Function publish_heartbeat(mav_output_type dest)
 * @param dest, either USB or RADIO
 * @brief publishes heartbeat message 
 * @return none
 * @author Aaron Hunter
 */
void publish_heartbeat(uint8_t dest) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint8_t mode = MAV_MODE_FLAG_MANUAL_INPUT_ENABLED | MAV_MODE_FLAG_SAFETY_ARMED;
    uint32_t custom = 0;
    uint8_t state = MAV_STATE_STANDBY;
    mavlink_msg_heartbeat_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC,
            mode,
            custom,
            state);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    mavprint(msg_buffer, msg_length, dest);
}

/**
 * @Function publish_parameter(uint8_t param_id[16])
 * @param parameter ID
 * @brief invokes mavlink helper to send out stored parameter 
 * @author aaron hunter
 */
void publish_parameter(uint8_t param_id[16], uint8_t dest) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    float param_value = 320.0; // value of the requested parameter
    uint8_t param_type = MAV_PARAM_TYPE_INT16; // onboard mavlink parameter type
    uint16_t param_count = 1; // total number of onboard parameters
    uint16_t param_index = 1; //index of this value
    mavlink_msg_param_value_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            param_id,
            param_value,
            param_type,
            param_count,
            param_index
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    mavprint(msg_buffer, msg_length, USB);
}


/**
 * @function publish_IMU_data()
 * @param none
 * @brief reads module level IMU data and publishes over radio serial in Mavlink
 * @author Aaron Hunter
 */
void publish_IMU_data(uint8_t data_type, uint8_t dest) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint8_t IMU_id = 0;
    if (data_type == RAW) {
        mavlink_msg_raw_imu_pack(mavlink_system.sysid,
                mavlink_system.compid,
                &msg_tx,
                Sys_timer_get_usec(),
                (int16_t) IMU_raw.acc.x,
                (int16_t) IMU_raw.acc.y,
                (int16_t) IMU_raw.acc.z,
                (int16_t) IMU_raw.gyro.x,
                (int16_t) IMU_raw.gyro.y,
                (int16_t) IMU_raw.gyro.z,
                (int16_t) IMU_raw.mag.x,
                (int16_t) IMU_raw.mag.y,
                (int16_t) IMU_raw.mag.z,
                IMU_id,
                (int16_t) IMU_raw.temp
                );
    } else if (data_type == SCALED) {
        mavlink_msg_highres_imu_pack(mavlink_system.sysid,
                mavlink_system.compid,
                &msg_tx,
                Sys_timer_get_usec(),
                (float) IMU_scaled.acc.x,
                (float) IMU_scaled.acc.y,
                (float) IMU_scaled.acc.z,
                (float) IMU_scaled.gyro.x,
                (float) IMU_scaled.gyro.y,
                (float) IMU_scaled.gyro.z,
                (float) IMU_scaled.mag.x,
                (float) IMU_scaled.mag.y,
                (float) IMU_scaled.mag.z,
                0.0, //no pressure
                0.0, //no diff pressure
                0.0, //no pressure altitude
                (float) IMU_scaled.temp,
                0, //bitfields updated
                IMU_id
                );
    }
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    mavprint(msg_buffer, msg_length, dest);
}



/**
 * @Function calc_pw(uint16_t raw_counts)
 * @param raw counts from the radio transmitter (11 bit unsigned int)
 * @return pulse width in microseconds
 * @brief converts the RC input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author aahunter
 * @modified <Your Name>, <year>.<month>.<day> <hour> <pm/am> */
static uint16_t calc_pw(uint16_t raw_counts) {
    int16_t normalized_pulse; //converted to microseconds and centered at 0
    uint16_t pulse_width; //servo output in microseconds
    normalized_pulse = (raw_counts - RC_RX_MID_COUNTS) >> CTS_2_USEC;
    pulse_width = normalized_pulse + RC_SERVO_CENTER_PULSE;
    return pulse_width;
}

/**
 * @Function set_control_output(struct controller_outputs controls)
 * @param controls, generated by the stability controller
 * @return none
 * @brief converts control inputs into servo outputs for each control surface
 * @author Margaret Silva
 */
void set_control_output(void) {

    char message[BUFFER_SIZE];
    uint8_t msg_len = 0;
    int index;
    int hash, ail, ele, thr, rud;
    int hash_check;
    const int tol = 4;
    int INTOL;

    /* Check switch state*/
    /*if switch state == up*/
    /* Manual control enabled */
    /* if switch state == down*/
    /* Autonomous mode enabled*/
    /* get RC commanded values*/

    safe = RC_channels[SAFE]; //1807 when ON, 240 when OFF
    hash = RC_channels[HASH]; //1807 when ON, 240 when OFF
    ail = RC_channels[AIL];
    ele = RC_channels[ELE];
    thr = RC_channels[THR];
    rud = RC_channels[RUD];
    kill_plane = RC_channels[KILL]; //Kill switch to kill throttle
    
    

    hash_check = (thr >> 2) + (ail >> 2) + (ele >> 2) + (rud >> 2);
    if (abs(hash_check - hash) <= tol) {
        INTOL = TRUE;
        // check if mode switching occurs
        if (safe <= SAFE_LOW) {
            mode = MANUAL;
        } else if (safe >= SAFE_HIGH) {
            mode = AUTONOMOUS;
        }

        /* send commands to motor outputs*/
        if (mode == MANUAL) {
            RC_servo_set_pulse(calc_pw(ail), AIL_PWM);  
            RC_servo_set_pulse(calc_pw(ele), ELE_PWM);
            if (kill_plane <= KILL_LOW){
                RC_servo_set_pulse(calc_pw(thr), THR_PWM);
            }
            else{
                 RC_servo_set_pulse(RC_SERVO_MIN_PULSE, THR_PWM);
            }
            RC_servo_set_pulse(calc_pw(rud), RUD_PWM); 
            
        } else {
            //RC_servo_set_pulse(RC_SERVO_MIN_PULSE, THR_PWM);
        }

    } else {
        INTOL = FALSE;
    }

}
/**
 * @Function mavprint(char msg_buffer[], int8_t msg_length, int8_t output);
 * @param msg_buffer, string of bytes to send to receiver
 * @param msg_length, length of msg_buffer
 * @param output, either USB or RADIO, which peripheral to send the message from
 * @return SUCCESS or ERROR
 */
int8_t mavprint(uint8_t msg_buffer[], uint8_t msg_length, uint8_t output) {
    uint8_t i;
    if (output == USB) {
        for (i = 0; i < msg_length; i++) {
            
            printCount++;
            putchar(msg_buffer[i]);
        }
    } else if (output == RADIO) {
        for (i = 0; i < msg_length; i++) {
            printCount++;
           Radio_put_char(msg_buffer[i]);
        }
    } else {
        return ERROR;
    }
//    printf("Print Count: %d\n", printCount);
//    printCount = 0;
    return SUCCESS;
}


/**
 * @Function log_IMU(void)
 * @param none
 * @return none
 * @brief creates an IMU packet frame for the Sparkfun OpenLogger 
 * @author ccuyos
 */
void log_IMU(uint32_t time){
     if (imuReady){
         imuReady = FALSE;
//            sprintf(imu_buffer, ",%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%0.1f,%x",
//                    (float) IMU_scaled.acc.x, (float) IMU_scaled.acc.y, (float) IMU_scaled.acc.z,
//                    (float) IMU_scaled.gyro.x, (float) IMU_scaled.gyro.y, (float) IMU_scaled.gyro.z,
//                    (float) (IMU_scaled.mag.x), (float) (IMU_scaled.mag.y), (float) (IMU_scaled.mag.z),
//                    (float) 0.0, 0x00);
            sprintf(imu_buffer, ",%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%0.1f,%x",
                    (float) IMU_scaled.acc.x, (float) IMU_scaled.acc.y, (float) IMU_scaled.acc.z,
                    (float) IMU_scaled.gyro.x, (float) IMU_scaled.gyro.y, (float) IMU_scaled.gyro.z,
                    (float) (IMU_scaled.mag.x), (float) (IMU_scaled.mag.y), (float) (IMU_scaled.mag.z),
                    (float) 0, (uint16_t) 0);
//          sprintf(imu_buffer, ",%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%+0.3f,%0.1f,%x",
//                    (float) IMU_scaled.acc.x, (float) IMU_scaled.acc.y, (float) IMU_scaled.acc.z,
//                    (float) IMU_scaled.gyro.x, (float) IMU_scaled.gyro.y, (float) IMU_scaled.gyro.z,
//                    (float) (IMU_scaled.mag.x), (float) (IMU_scaled.mag.y), (float) (IMU_scaled.mag.z),
//                    (float) IMU_scaled.temp, IMU_scaled.mag_status);


            //printf("%s\n", imu_buffer);
     }
     else{
         sprintf(imu_buffer, "");
     }
}
void log_GPS(uint32_t time){
    if (gpsReady){
        gpsReady = FALSE;
        sprintf(gps_buffer, ",%0.6f,%0.6f,%3.3f,%0.6f", GPSdata.lat,
                     GPSdata.lon, GPSdata.spd, GPSdata.cog);
    }
    else{
        sprintf(gps_buffer, "");
    }
}
void log_BARO(uint32_t time){
    if (baroReady){
        counter++;
        baroReady = FALSE;
        sprintf(baro_buffer, ",%0.3f,%0.3f", BARO_scaled.calibrated_temp, BARO_scaled.calibrated_pressure);
    }
    else{
       sprintf(baro_buffer, "");
    }
    
}

void log_sensors(uint32_t time){
    
    uint8_t imuAvailable = (strcmp(imu_buffer, ""));
    uint8_t gpsAvailable = (strcmp(gps_buffer, ""));
    uint8_t baroAvailable = (strcmp(baro_buffer, ""));
    
    uint8_t sensorAvailable = !(!imuAvailable) << 2 | !(!gpsAvailable) << 1 | !(!baroAvailable);
    uint8_t timeVal[BUFFER_SIZE];
    sprintf(timeVal, "%x", time);
    
    //sprintf(log_buffer, "Hello World\n");
    
    //sprintf(log_buffer, "%d,%x,%x%s%s%s\n", strlen(timeVal), time, sensorAvailable, imu_buffer, gps_buffer, baro_buffer);
    sprintf(log_buffer, "%d,%d,%x%s%s%s\n", 0, time, sensorAvailable, imu_buffer, gps_buffer, baro_buffer);
    mavprint(log_buffer, strlen(log_buffer), RADIO);
}




int main(void) {
    uint32_t cur_time = 0;
    uint32_t warmup_time = 250; //time in ms to allow subsystems to stabilize (IMU))
    uint32_t control_start_time = 0;
    uint32_t heartbeat_start_time = 0;
    uint32_t publish_start_time = 0;
    uint32_t log_start_time = 0;
    uint8_t index;
    uint8_t error_report = 50;
    RCRX_channel_buffer channels[CHANNELS];
    //IMU variables
    int value = 0;
    int i;
    int row;
    int col;
    int IMU_err = 0;
    
    //GPS variables
    

    //    //Initialization routines
    Board_init(); //board configuration
    Serial_init(); //start debug terminal (USB)
    Sys_timer_init(); //start the system timer
    IMU_err = IMU_init(INTERFACE_MODE);
    DELAY(1);
    GPS_init();
    Barometer_init();
    /*small delay to get all the subsystems time to get online*/
    while (cur_time < warmup_time) {
        cur_time = Sys_timer_get_msec();
    }
    Radio_serial_init(); //start the radios

    RCRX_init(); //initialize the radio control system
    RC_channels_init(); //set channels to midpoint of RC system
    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_1); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_2); // start the servo subsystem
    RC_servo_init(ESC_UNIDIRECTIONAL_TYPE, SERVO_PWM_3); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_4); // start the servo subsystem


    //printf("\r\nOpen Logger compiled on: %s, %s \r\n", __DATE__, __TIME__);
    //printf("I am here\n");
    IMU_set_mag_cal(A_mag_fl, b_mag_fl);
    IMU_set_acc_cal(A_acc, b_acc);
    publish_start_time = cur_time;
    log_start_time = cur_time;
    int curTime = 0;
    int prevTime = 0;
    int dT_time = 0;
    while (1) {
        //printf("I am here\n");
        cur_time = Sys_timer_get_msec();
        //check for all events
        check_radio_events(); //detect and process MAVLink incoming messages
        check_RC_events(); //check incoming RC commands
        check_IMU_events();
        check_GPS_events();
        check_BARO_events();
        check_USB_events(); // look for MAVLink messages
        //publish control and sensor signals (20 ms)
        
        if (cur_time - control_start_time >= CONTROL_PERIOD) {
            control_start_time = cur_time; //reset control loop timer     
            set_control_output();
        }

         
        /* publish high speed sensors */
        if (cur_time - publish_start_time > PUBLISH_PERIOD) {
            publish_start_time = cur_time; //reset publishing timer
            //printf("IMU dt: %d\n", IMU_get_dt());
            
        }        
        //publish heartbeat
        if (cur_time - heartbeat_start_time >= HEARTBEAT_PERIOD) {
            heartbeat_start_time = cur_time; //reset the timer
            //printf("Current frequency: %d\n", counter);
            counter = 0;
        }
        
        if (cur_time - log_start_time >= LOG_PERIOD){
            curTime = cur_time;
            dT_time = curTime - prevTime;
            prevTime = curTime;
            
            IMU_start_data_acq();
            log_start_time = cur_time;
            log_IMU(cur_time);
            log_GPS(cur_time);
            log_BARO(cur_time);
            log_sensors(dT_time);
            
            
        }
        
        

        
    }
    return 0;
}


        //Uncomment for actuator characterization
//         if (cur_time - actuator_start_time >= ACTUATOR_PERIOD){
//            actuator_start_time = cur_time;
//            printf("PWM: %d\n", pwm_signal);
//            RC_servo_set_pulse(pwm_signal, THR_PWM); 
//            if (pwm_dir){
//                 pwm_signal = pwm_signal + 100;
//                if (pwm_signal >= RC_SERVO_MAX_PULSE){
//                    pwm_dir = 0;
//                }
//            }
//            else if (!pwm_dir){
//                pwm_signal = pwm_signal - 100;
//                if (pwm_signal <= RC_SERVO_MIN_PULSE){
//                    pwm_dir = 1;
//                }
//            }
//        }