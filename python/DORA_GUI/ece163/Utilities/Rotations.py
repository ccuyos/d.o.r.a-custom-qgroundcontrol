"""
Library to do all of the rotation matrix math (uses the math and MatrixMath libraries). Euler angles are defined as the
standard [3-2-1] set of yaw, pitch, roll (in radians). Quaternions are defined as the unit normal attitude quaternion
with the first element as the scalar [q0] and the remaining 3 elements as the vector [q]. The standard rotation matrix
takes a vector in the inertial frame [NED] and transforms it to the body frame.
"""

import math
from . import MatrixMath

def euler2DCM(yaw, pitch, roll):
    """
    Create the direction cosine matrix, R, from the euler angles (assumed to be in radians). Angles are yaw,pitch,roll
    passed as individual arguments and in the form corresponding to the [3-2-1] Euler set. The DCM goes from inertial [I]
    vectors into the body frame [B].

    :param yaw: rotation about inertial down (rad)
    :param pitch: rotation about intermediate y-axis (rad)
    :param roll: rotation about body x-axis (rad)
    :return: Direction Cosine Matrix (DCM) [3 x 3]
    """
    cos_phi = math.cos(roll)
    sin_phi = math.sin(roll)
    cos_theta = math.cos(pitch)
    sin_theta = math.sin(pitch)
    cos_psi = math.cos(yaw)
    sin_psi = math.sin(yaw)

    R_phi = [[1, 0, 0],[0, cos_phi, sin_phi],[0, -sin_phi, cos_phi]]
    R_theta = [[cos_theta, 0, -sin_theta],[0, 1, 0],[sin_theta, 0, cos_theta]]
    R_psi = [[cos_psi, sin_psi, 0],[-sin_psi, cos_psi, 0],[0, 0, 1]]

    Result = MatrixMath.multiply(R_phi,MatrixMath.multiply(R_theta,R_psi))
    return Result

def euler2Quaternion(yaw, pitch, roll):
    """
    Create the unit quaternion, Q, from the euler angles (assumed to be in radians). Angles are yaw,pitch,roll
    passed as individual arguments and in the form corresponding to the [3-2-1] Euler set. The unit quaternion (Q'*Q = 1)
    transforms inertial [I] vectors to the body frame [B]. The quaternion is defined as [q0 | q1 q2 q3] with q0 corresponding
    to the scalar part of the quaternion and [q1 q2 q3] corresponding to the vector part.

    :param yaw: rotation about inertial down (rad)
    :param pitch: rotation about intermediate y-axis (rad)
    :param roll: rotation about body x-axis (rad)
    :return: Attitude Quaternion, Q [4 x 1]
    """
    cos_phi2 = math.cos(roll / 2.0)
    sin_phi2 = math.sin(roll / 2.0)
    cos_theta2 = math.cos(pitch / 2.0)
    sin_theta2 = math.sin(pitch / 2.0)
    cos_psi2 = math.cos(yaw / 2.0)
    sin_psi2 = math.sin(yaw / 2.0)

    q0 = cos_psi2*cos_theta2*cos_phi2 + sin_psi2*sin_theta2*sin_phi2
    q1 = cos_psi2*cos_theta2*sin_phi2 - sin_psi2*sin_theta2*cos_phi2
    q2 = cos_psi2*sin_theta2*cos_phi2 + sin_psi2*cos_theta2*sin_phi2
    q3 = sin_psi2*cos_theta2*cos_phi2 - cos_psi2*sin_theta2*sin_phi2

    return [[q0],[q1],[q2],[q3]]

def dcm2Euler(DCM):
    """
    Extracts the Euler angles from the rotation matrix, in the form of yaw, pitch, roll corresponding to the [3,2,1]
    euler set. Note that euler angles have a singularity at pitch = +/- pi/2 in that roll and yaw become indistinguishable.

    :param DCM: Rotation matrix [3 x 3]
    :return: yaw, pitch, and roll [rad]
    """
    if any([len(DCM) != 3,len(DCM[0]) != 3]):
        raise ArithmeticError('Rotation matrix only defined for 3x3 matrices')
    yaw = math.atan2(DCM[0][1],DCM[0][0])
    roll = math.atan2(DCM[1][2],DCM[2][2])
    if DCM[0][2] > 1.0:
        DCM[0][2] = 1.0
    elif DCM[0][2] < -1.0:
        DCM[0][2] = -1.0
    pitch = -1.0 * math.asin(DCM[0][2])
    return yaw, pitch, roll

def quaternion2DCM(Q):
    """
    Converts the unit quaternion (Q) to a rotation matrix (DCM)

    :param Q: attitude quaternion [4 x 1]
    :return: Direction Cosine Matrix (DCM) [3 x 3]
    """
    if any([len(Q) != 4,len(Q[0]) != 1]):
        raise ArithmeticError('Quaternion must be a 4x1 vector')
    q0 = Q[0][0]
    q1 = Q[1][0]
    q2 = Q[2][0]
    q3 = Q[3][0]
    result = [[1.0 - 2.0 * (q2*q2 + q3*q3), 2.0 * (q3*q0 + q1*q2), 2.0 * (q3*q1 - q2*q0)],
              [2.0 * (q1*q2 - q3*q0), 1.0 - 2.0 * (q3*q3 + q1*q1), 2.0 * (q2*q3 + q0*q1)],
              [2.0 * (q3*q1 + q2*q0), 2.0 * (q2*q3 - q0*q1), 1.0 - 2.0 * (q1*q1 + q2*q2)]]
    return result

def quaternion2Euler(Q):
    """
    Extracts the euler angles (in radians) from the unit quaternion in the form of yaw, pitch, roll
    corresponding to the [3-2-1] euler set.
    :param Q: attitude quaternion [4 x 1]
    :return: yaw, pitch, roll [rad]
    """
    if any([len(Q) != 4,len(Q[0]) != 1]):
        raise ArithmeticError('Quaternion must be a 4x1 vector')
    q0 = Q[0][0]
    q1 = Q[1][0]
    q2 = Q[2][0]
    q3 = Q[3][0]

    pitch = math.asin(-2.0 * (q1 * q3 - q0 * q2))
    yaw = math.atan2(2.0 * (q1*q2 + q0*q3), (q0**2 + q1**2 - q2**2 - q3**2))
    roll = math.atan2(2.0 * (q2*q3 + q0*q1), (q0**2 - q1**2 - q2**2 + q3**2))
    return yaw, pitch, roll

def ned2enu(points):
    """
    Function changes coordinates from North-East-Down (NED) to East-North-Up (ENU). This is required because while
    all of the dynamics and rotations are defined in NED, the graphics functions use ENU

    :param points: matrix of point in NED [n x 3]
    :return: same set of [n x 3] points in ENU coordinates
    """
    if len(points[0]) !=3:
        raise ArithmeticError('Must operate on an [nx3] matrix')
    result = [[pts[1],pts[0],-1.0*pts[2]] for pts in points]
    return result

# # Test Harness -- test the code with known good examples
# if __name__ == "__main__":
#     import Rotations
#
#     yaw = -135
#     pitch = 90
#     roll = -15
#     print("Euler Angles --> yaw: {}\tpitch: {}\troll: {}".format(yaw, pitch, roll))
#     R = Rotations.euler2DCM(math.radians(yaw), math.radians(pitch), math.radians(roll))
#     print("DCM = ")
#     MatrixMath.matrixPrint(R)
#     newYaw, newPitch, newRoll = Rotations.dcm2Euler(R)
#     print("Recovered Euler Angles (deg) --> yaw: {}\tpitch: {}\troll: {}".format(math.degrees(newYaw),math.degrees(newPitch),math.degrees(newRoll)))
#     Q = Rotations.euler2Quaternion(math.radians(yaw), math.radians(pitch), math.radians(roll))
#     print('Unit quaternion = ')
#     MatrixMath.matrixPrint(Q)
#     print("Check on length of Q = ",str(MatrixMath.dotProduct(Q,Q)))
#     qYaw, qPitch, qRoll = Rotations.quaternion2Euler(Q)
#     print("Euler angles recovered from Q --> yaw: {}\tpitch: {}\troll: {}".format(math.degrees(qYaw),math.degrees(qPitch),math.degrees(qRoll)))
#     print("For the DCM from quaternion: ")
#     Rq = quaternion2DCM(Q)
#     MatrixMath.matrixPrint(Rq)
#     print("Error between quaternion and Euler DCM = ")
#     MatrixMath.matrixPrint(MatrixMath.add(R,MatrixMath.scalarMultiply(-1.0,Rq)))
#     print("Checking the ned2enu conversion...")
#     Renu = MatrixMath.matrixPrint(Rotations.ned2enu(Rq))