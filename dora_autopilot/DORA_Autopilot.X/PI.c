/* 
 * File:   PI.h
 * Author: Margaret Silva
 * Brief: Interface to PI controller module
 * Uses code from Aaron Hunter's code from the OSAVC PID library to create a 
 * PI controller module
 *
 * Created on February 14, 2023, 2:03 PM
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

#include "PI.h" // The header file for this source file. 
#include <stdio.h>
#include "PI.h"

/*******************************************************************************
 * PRIVATE #DEFINES                                                            *
 ******************************************************************************/
#define CUR_ERROR 0
#define PREV_ERROR 1
#define INTEGRAL_ERROR 2

/*******************************************************************************
 * PRIVATE TYPEDEFS                                                            *
 ******************************************************************************/

/*******************************************************************************
 * PRIVATE FUNCTIONS PROTOTYPES                                                 *
 ******************************************************************************/

/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATIONS                                             *
 ******************************************************************************/

/**
 * @Function PI_init(*pi);
 * @param *pi, pointer to PI_controller type
 * @brief initializes the PI_controller struct
 * @note computes the c0, c1, c2 constants of the controller and initializes
 * error array
 * @author Aaron Huter,
 * @modified */
void PI_init(PI_controller *pi) {
    int i;
    pi->u = 0;
    pi->u_calc = 0;
    /* initialize error*/
    for (i = 0; i < 3; i++) {
        pi->error[i] = 0;
    }
}

/**
 * @Function PI_update(PI_controller *pi, float reference, float measurement)
 * @param *pi, pointer to PI_controller type
 * @param, reference, the current process setpoint
 * @param measurmeent, the current process measurement
 * @brief implements a standard parallel PI
 * @note derivative filtering is not implemented
 * @author Aaron Hunter,
 * @modified  */
void PI_update(PI_controller *pi, float reference, float measurement) {
    // record current and previous error
    pi->error[PREV_ERROR] = pi->error[CUR_ERROR];
    pi->error[CUR_ERROR] = reference - measurement;
    // take integral of error
    pi->error[INTEGRAL_ERROR] += (pi->dt * 0.5)*(pi->error[CUR_ERROR] + pi->error[PREV_ERROR]);
    /* compute new output */
    pi->u_calc = pi->trim + pi->kp * pi->error[CUR_ERROR] + pi->ki * pi->error[INTEGRAL_ERROR];
    /* clamp outputs within actuator limits*/
    if (pi->u_calc > pi->u_max) {
        pi->u = pi->u_max;
        pi->error[INTEGRAL_ERROR] -= (pi->dt * 0.5)*(pi->error[CUR_ERROR] + pi->error[PREV_ERROR]);
        ; // de-accumulate
    } else if (pi->u_calc < pi->u_min) {
        pi->u = pi->u_min;
        pi->error[INTEGRAL_ERROR] -= (pi->dt * 0.5)*(pi->error[CUR_ERROR] + pi->error[PREV_ERROR]);
        ; // de-accumulate
    } else {
        pi->u = pi->u_calc;
    }
}

/**
 * @Function PI_reset_integrator(PI_controller *pi)
 * @param *pi, pointer to PI_controller type
 * @brief resets the integrator of this pi controller to 0
 * @author Margaret Silva
 * @modified  2/24/23 */
void PI_reset_integrator(PI_controller *pi) {
    pi->error[INTEGRAL_ERROR] = 0;
}

/*******************************************************************************
 * PRIVATE FUNCTION IMPLEMENTATIONS                                            *
 ******************************************************************************/

//#define PI_TESTING
#ifdef PI_TESTING
#include "Board.h"
#include "SerialM32.h"

void main(void) {
    PI_controller controller;
    controller.dt = .02;
    controller.kp = 1;
    controller.ki = 0.5;
//    controller.kd = 0.2;
    controller.u_max = 2000;
    controller.u_min = -2000;

    float ref;
    float y;



    Board_init();
    Serial_init();
    printf("PI test harness %s, %s\r\n", __DATE__, __TIME__);
    PI_init(&controller);
    printf("Controller initialized to: \r\n");
    printf("dt %f\r\n", controller.dt);
    printf("kp: %f\r\n", controller.kp);
    printf("ki: %f\r\n", controller.ki);
//    printf("kd: %f\r\n", controller.kd);
    printf("max output: %f\r\n", controller.u_max);
    printf("min output: %f\r\n", controller.u_min);

    PI_update(&controller, 0, 5);
    printf("cur error: %f\n", controller.error[CUR_ERROR]);
    printf("integral error: %f\n", controller.error[INTEGRAL_ERROR]);

    while(1);
}
#endif //PI_TESTING
