/* 
 * File:   DPS310.c
 * Author: Kevin Mok
 * Brief: 
 * Created on 
 * Modified on
 */
/*******************************************************************************
 * Barometer 1 is white
 * Barometer 2 is black                                                             *
 ******************************************************************************/


/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

#include "DPS310.h" // The header file for this source file. 
#include "SerialM32.h"
#include "Board.h"
#include "dps310_registers.h"
#include "dps310_errors.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/attribs.h>  //for ISR definitions
#include <xc.h>
#include "p32mx795f512l.h"
#include <sys/types.h>
#include <stdfix.h> //fixed point library
#include "inc/System_timer.h" // The header file for this source file. 
/*******************************************************************************
 * PRIVATE #DEFINES                                                            *
 ******************************************************************************/
//#define BAR_SPI_FREQ 10000000ul //10MHz clock rate //10000000ul
#define BAR_SPI_FREQ 5000000ul //5MHz clock rate (can go up to 10Mhz)
#define READ 1
#define WRITE 0
/*Set up the chip select digital IOs here*/
#define CS1_TRIS TRISEbits.TRISE1 //chip select for LHS rotary encoder
#define CS1_LAT LATEbits.LATE1
#define CS2_TRIS TRISEbits.TRISE2 //chip select for RHS rotary encoder
#define CS2_LAT LATEbits.LATE2 
#define CS3_TRIS TRISEbits.TRISE3 //chip select for RHS rotary encoder
#define CS3_LAT LATEbits.LATE3 


#define INPUT 1
#define OUTPUT 0

#define RX_BARO_INIT TRISGbits.TRISG8
#define SEND_CMD_INIT TRISGbits.TRISG7 
#define RX_BARO PORTGbits.RG8 
#define SEND_CMD PORTGbits.RG7

//#define SDO2_TRIS TRISBbits.TRISB4    // SDO2 (output)
//#define SDI2_TRIS TRISGbits.TRISG7    // SDI2 (input)

/*Used for debugging the interrupt can be removed eventually*/
#define LED_OUT_TRIS TRISDbits.TRISD3
#define LED_OUT_LAT LATDbits.LATD3
/*important constants*/
#define MAX_VELOCITY 6000 //extrapolated to 100 Hz
#define TWO_PI 16384
/*AS5047D register definitions*/
//#define NOP 0x0000
#define ERRFL 0x0001
#define SETTINGS_REG 0x0018
//#define DIAAGC 0X3FFC
//#define MAG 0X3FFD
//#define ANGLEUNC 0X3FFE
#define PRESSURE 0x3FFF

#define SPITEST 0x6548


#define IMU_CS_TRIS TRISEbits.TRISE0 //chip select for IMU
#define IMU_CS_LAT LATEbits.LATE0

#define DPS310_TMP_SIZE 3

#define Barometer_Library_TESTING

/*******************************************************************************
 * PRIVATE TYPEDEFS                                                            *
 ******************************************************************************/

typedef struct {
    int16_t c0;
    int16_t c1;
    int32_t c00;
    int32_t c10;
    int16_t c01;
    int16_t c11;
    int16_t c20;
    int16_t c21;
    int16_t c30;
} calibration_coefs_t;
static int32_t C00,C10,C20,C30,C01,C11,C21, C0,C1;

static calibration_coefs_t g_coefs;
//static uint8_t g_pressure_rate = DPS310_CFG_RATE_16_MEAS;
//static uint8_t g_temperature_rate = DPS310_CFG_RATE_16_MEAS;
//static float g_last_temp_raw_sc;

static uint8_t temp1, temp2;
static uint8_t buffer[16];
static float tempVar;

static volatile int start_dt = 0;
static volatile int end_dt = 0;
static volatile int BARO_dt = 0;


struct FRT{
    uint32_t us;
    uint32_t ms;
    uint32_t printTick;
    uint32_t filterTick;
    uint32_t checkTick;
};
struct FRT timeBaro;

/*******************************************************************************
 * PRIVATE FUNCTIONS PROTOTYPES                                                 *
 ******************************************************************************/
/**
 * @Function get_two_complement_of(uint32_t value, uint8_t length)
 * @param reg, hardware address in the encoder
 * @param setting, setting data to write
 * @returns setting read from address after write
 * @author ahunter
 */
static uint32_t get_two_complement_of(uint32_t value); 
static int16_t read_coefs(void);
 
 /**
 * @Function readRegister(uint16_t address)
 * @param reg, hardware address in the encoder
 * @brief sends address
 * @returns in, the value to send to encoder
 * @author ahunter
 */
static uint8_t read_register(uint8_t address);

/*blocking function used for init only*/
/**
 * @Function writeRegister(uint16_t address, uint16_t setting)
 * @param reg, hardware address in the encoder
 * @param setting, setting data to write
 * @returns setting read from address after write
 * @author ahunter
 */
static uint16_t write_register(uint8_t address, uint8_t setting);

/**
 * @Function DPS_run_SPI_state_machine(uint8_t byte_read)
 * @return none
 * @param byte_read, the byte read from SPI 2 buffer SPI1BUF
 * @brief state machine to reads DPS data registers over SPI
 * @note called with a single SPI read of the DPS
 * @author kemok
 **/
static void DPS_run_SPI_state_machine(uint8_t byte_read);

//Credit: ChatGPT
int twosComplementToDecimal(int num, int numBits);

/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATIONS                                             *
 ******************************************************************************/
static void delay(int cycles) {
    int i;
    for (i = 0; i < cycles; i++) {
        ;
    }
}
/**
 * @Function Barometer_Init(void)
 * @param freq, frequency of SPI rate
 * @return SUCCESS or ERROR
 * @brief initializes hardware in appropriate mode along with the needed interrupts */
uint8_t Barometer_init(void) {
    uint8_t index;
    uint32_t pb_clk;
    uint16_t setting;
    uint16_t return_value;
    pb_clk = Board_get_PB_clock();
    CS2_TRIS = 0; //chip select for RHS rotary encoder
    CS2_LAT = 1; 
    
    
    /* SPI settings */
     __builtin_disable_interrupts();
    SPI2CON = 0; // disable SPI system
    SPI2BUF; // clear receive buffer*/
    SPI2STATbits.SPIROV = 0; // clear any overflow condition 
    SPI2CONbits.MSTEN = 1; // set as master
    SPI2CONbits.SSEN = 0; // manually drive CS/SS 
    SPI2CONbits.MODE32 = 0;
    SPI2CONbits.MODE16 = 0; // set to 16 bit mode
    SPI2BRG = (pb_clk / (2 * BAR_SPI_FREQ)) - 1; // 
    SPI2CONbits.SMP = 1; /* set sample at end of data*/
    /*NOTE: mode 1 SPI has CKE = 0, CKP = 1*/
    // CPOL and CPHA 1 1
    SPI2CONbits.CKP = 1; /*set clock phase to idle low 1 */ 
    SPI2CONbits.CKE = 0; /* set to read on falling edge (active --> idle) 0*/ 
    /*initialize chip select pins*/
    SPI2CONbits.ON = 1; /* enable SPI system*/
    /*Initialize encoder.*/
    setting = 0; //ABI PWM off
    //printf("Wow\n");
    
    
    __builtin_enable_interrupts();
//    printf("Temperature coeff source: %d\n", read_register(DPS310_TMP_COEF_SRCE));
    
    uint8_t reg0 = read_register(DPS310_PRODUCT_ID_REG);
    //printf("Product ID: %x\n", reg0); 
    uint8_t tempConfig = (read_register(DPS310_TMP_COEF_SRCE) & DPS310_TMP_COEF_SRCE_MASK) | 
                         DPS310_CFG_RATE_64_MEAS |
                         DPS310_TMP_CFG_TMP_PRC_2_TIMES;
            
    if (write_register(DPS310_TMP_CFG_REG, tempConfig) == tempConfig){
        //printf("DPS310 Temperature Config: Success\n");
    }    
    else{
        //printf("Error in Temperature Config\n");
    }
    
    
    uint8_t presConfig = DPS310_CFG_RATE_64_MEAS | DPS310_PRS_CFG_PM_PRC_2_TIMES;
    if (write_register(DPS310_PRS_CFG_REG, presConfig) == presConfig){
        //printf("DPS310 Pressure Config: Success\n");
    }
    else{
        //printf("Error in Pressure Config\n");
    }
    
    uint8_t opModeConfig = DPS310_MEAS_CFG_MEAS_CTRL_CONTINUOUS_PRS_TMP;
    if ((write_register(DPS310_MEAS_CFG_REG, opModeConfig) & DPS310_MEAS_CFG_MEAS_CTRL_MASK) == 
           opModeConfig){
        //printf("DPS310 Operating Mode Config: Success\n");
    }
    else{
        //printf("Error in Operating Mode Config\n");
    }
    
//    uint8_t fifoConfig = DPS310_CFG_RET_PRS_SHIFT_EN;;
//    write_register(DPS310_CFG_REG_REG, fifoConfig);
    
    

    
    //Get calibration coefficients
    
    
    C0 = read_register(0x10) << 4 | (read_register(0x11) & 0xF0) >> 4;
    C0 = twosComplementToDecimal(C0, 12);
    C1 = (read_register(0x11) & 0x0F) << 8 | read_register(0x12);
    C1 = twosComplementToDecimal(C1, 12);
    C00 = read_register(0x13) << 12  | read_register(0x14) << 4 | ((read_register(0x15) & 0xF0) >> 4);
    C00 = twosComplementToDecimal(C00, 20);
    C10 = ((read_register(0x15) & 0x0F) << 16) | read_register(0x16) << 8  | read_register(0x17);
    C10 = twosComplementToDecimal(C10, 20   );
    C01 = read_register(0x18) << 8 | read_register(0x19);
    C01 = twosComplementToDecimal(C01, 16);
    C11 = read_register(0x1A) << 8 | read_register(0x1B);
    C11 = twosComplementToDecimal(C11, 16);
    C20 = read_register(0x1C) << 8 | read_register(0x1D);
    C20 = twosComplementToDecimal(C20, 16);
    C21 = read_register(0x1E) << 8 | read_register(0x1F);
    C21 = twosComplementToDecimal(C21, 16);
    C30 = read_register(0x20) << 8 | read_register(0x21);
    C30 = twosComplementToDecimal(C30, 16);
    
//    C0 = 224;
//    C1 = -298;
//    C00 = 82392;
//    C10 = -52199;
//    C01 = -3425;
//    C11 = 1337;
//    C20 = -9786;
//    C21 = 73;
//    C30 = -1387;
    
    
//    printf("C0 = %d\n"
//            "C1 = %d\n"
//            "C00 = %d\n"
//            "C10 = %d\n"
//            "C20 = %d\n"
//            "C30 = %d\n"
//            "C01 = %d\n"
//            "C11 = %d\n"
//            "C21 = %d\n", C0, C1, C00, C10, C20, C30, C01, C11, C21);
  
    
    //Wait until sensor is ready 
    while (!dps_checkEvents(DPS310_MEAS_CFG_SENSOR_RDY_COMPLETE));
    
    
    return SUCCESS;
}


/**
 * @Function get_two_complement_of(uint32_t value, uint8_t length)
 * @param reg, hardware address in the encoder
 * @param setting, setting data to write
 * @returns setting read from address after write
 * @author ahunter
 */

uint32_t get_two_complement_of(uint32_t value){
    return ((~value) + 1) & 0xFFFFFF; 
 }

int twosComplementToDecimal(int num, int numBits) {
    int signBit = 1 << (numBits - 1);
    int mask = signBit - 1;
    return (num & signBit) ? -((~num & mask) + 1) : (num & mask);
}



/**
 * @Function read_register(uint16_t address)
 * @param address, hardware address in the encoder
 * @brief sends the next address to read
 * @returns data from previous SPI operation
 * @author kemok
 */
uint8_t read_register(uint8_t address) {
    uint8_t data;
    uint8_t reg_addr = address | (READ << 7);
    //IMU_CS_LAT = 0;
    CS2_LAT = 0;
    SPI2BUF = reg_addr;
    while (SPI2STATbits.SPIRBF == FALSE) {
        ;
    }
    data = SPI2BUF;
    reg_addr++;
    SPI2BUF = reg_addr; // We're not going to read this but need set something
    while (SPI2STATbits.SPIRBF == FALSE) {
        ;
    }
    data = SPI2BUF;
    CS2_LAT = 1;
    return (data);
}
float dps_read_temperature(void){
    uint8_t B2_reg = read_register(DPS310_TMP_B2_REG);
    uint8_t B1_reg = read_register(DPS310_TMP_B1_REG);
    uint8_t B0_reg = read_register(DPS310_TMP_B0_REG);
    //wait for measurement to complete
    uint32_t raw_temp =  (B2_reg << 16) | (B1_reg << 8) | (B0_reg << 0);
    float raw_temp_new = twosComplementToDecimal((raw_temp & 0xFFFFFF), 32);
    tempVar = raw_temp_new / 1572864;
    float out_temp = C0 * 0.5 + C1 * tempVar; //Has an offset calculated
    //printf("Temperature (C): %0.3f, %0.3f\n", tempVar, out_temp);
    
    return out_temp;
}


float dps_read_pressure(void){
    uint32_t P_B2_reg = read_register(DPS310_PSR_B2_REG);
    uint32_t P_B1_reg = read_register(DPS310_DSP_B1_REG);
    uint32_t P_B0_reg = read_register(DPS310_DSP_B0_REG);
    uint32_t raw_pressure = (P_B2_reg << 16) | (P_B1_reg << 8) | (P_B0_reg << 0);
    float raw_pressure_new = twosComplementToDecimal((raw_pressure & 0xFFFFFF), 32);
    float P_raw_sc = (raw_pressure_new * 6.35782878e-07);
    float T_raw_sc = tempVar;
    
    
    float final_pressure = C00 + P_raw_sc * (C10 + P_raw_sc *(C20 + P_raw_sc * C30)) + T_raw_sc * C01 + T_raw_sc * P_raw_sc * (C11 + P_raw_sc * C21);
    final_pressure = final_pressure * 0.01;  //Convert to hPa
    final_pressure = 0.077775483515734 * (final_pressure -(-29826.461)) + 987.62;
   
    return final_pressure;
}
/**
 * @Function dps_checkEvents
 * @param meas_desired, defined value for operating mode status of barometer
 * @return the event itself if present, else returns the operating mode status
 * @brief used for ensuring that barometer is ready for measurement 
 * @author ccuyos
 */
int dps_checkEvents(uint8_t meas_desired){
    uint8_t baroStatus = read_register(DPS310_MEAS_CFG_REG);
    switch(meas_desired){
        case DPS310_MEAS_CFG_COEF_RDY_AVAILABLE:
            return baroStatus & DPS310_MEAS_CFG_COEF_RDY_AVAILABLE;
        case DPS310_MEAS_CFG_SENSOR_RDY_COMPLETE:
            return baroStatus & DPS310_MEAS_CFG_SENSOR_RDY_COMPLETE;
        case DPS310_MEAS_CFG_TMP_RDY:
            return baroStatus & DPS310_MEAS_CFG_TMP_RDY;
        case DPS310_MEAS_CFG_PRS_RDY:
            return baroStatus & DPS310_MEAS_CFG_PRS_RDY;
        default:
            return baroStatus & meas_desired;
    }
    
}


void dps_reset(void){
    uint32_t reset = (DPS310_RESET_FIFO_FLUSH | DPS310_RESET_SOFT_RST_VALUE);
    write_register(DPS310_RESET_REG, reset);
}

/**
 * @Function write_register(uint16_t address, uint16_t value)
 * @param address, hardware address in the encoder
 * @param value: data to write
 * @brief performs single SPI write
 * @returns value after the write is performed
 * @author kemok
 */
uint16_t write_register(uint8_t address, uint8_t value) {
    
    uint8_t data;
    address = address | (WRITE << 7);
    CS2_LAT = 0;
    SPI2BUF = address;
    while (SPI2STATbits.SPIRBF == FALSE) {
        ;
    }
    data = SPI2BUF;
    SPI2BUF = value; //send register setting
    while (SPI2STATbits.SPIRBF == FALSE) {
        ;
    }
    data = SPI2BUF;
    CS2_LAT = 1;
    
    return read_register(address);
}

/*
 * @function Barometer_get_scaled_data()
 * @return pointer to BARO_output struct
 * @brief returns scaled calibrated data from the barometer
 * @author ccuyos
 */

uint8_t Barometer_get_scaled_data(struct BARO_out* BARO_data){
    BARO_data->calibrated_temp = dps_read_temperature();
    BARO_data->calibrated_pressure = dps_read_pressure();
    //printf("Pressure: %0.3f, Temperature: %0.3f\n", BARO_data->calibrated_pressure, BARO_data->calibrated_temp);
    return SUCCESS;
}


/* 
* @function serviceReady
* @param tick, number of ticks currently for respective service
*        period, period for the respective service
* @brief Returns SUCCESS if ready for servicing
*/
uint8_t serviceReady(unsigned int* tick, int period){
    //Check if period has been reached
    if (Sys_timer_get_msec() - *tick >= period){
        //Overwrite tick variable with current ms reading
        *tick = Sys_timer_get_msec();
        return SUCCESS;
    }
    return 0;
}
uint8_t Barometer_is_data_ready(void){
    if (serviceReady(&timeBaro.checkTick, 10)){
        return 1;
    }
    else{
        return 0;
    }
}

#ifdef BARO_TESTING
#define PRINT_PERIOD 10

int main(void){
    Board_init();
    Serial_init();
    printf("Barometer Testing compiled on %s, %s\n", __DATE__, __TIME__);
    Barometer_init();
    Sys_timer_init();
    //Don't move on until barometer is not done initializing 
    while (!dps_checkEvents(DPS310_MEAS_CFG_SENSOR_RDY_COMPLETE));
    printf("Barometer initialization complete\n");
    uint32_t printTick = Sys_timer_get_msec();
    
    float final_pressure = 0;
    
    while (1){
        if (Barometer_is_data_ready()){
           float pressure_raw_sc = dps_read_pressure();
           float g_last_temp_raw_sc = dps_read_temperature(); 
          final_pressure = (C00 +
          tempVar * (C10 + pressure_raw_sc * (C20 + pressure_raw_sc * C30)) +
          tempVar * ( C01 + pressure_raw_sc * (C11 + pressure_raw_sc * C21))); 
        }
        if (serviceReady(&printTick, PRINT_PERIOD)){
            printf("%d\n", BARO_dt);
            printf("Final Pressure: %0.3f\n", final_pressure);
        }
    }
    while(1);
}
#endif 
