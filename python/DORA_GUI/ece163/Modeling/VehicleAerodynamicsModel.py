"""
Vehicle Aerodynamics Model will take the parameters from the VehiclePhysicalConstants and use the vehicle state to compute
the forces and moments about the vehicle for integration. These are linearized aerodynamics using stability derivatives and
simplified aerodynamics; the only deviation from this is the post-stall lift model used to convert lift from the linear
model to a flat plate at an angle.

Note that stability derivatives are non-dimensional, and thus need to be converted back to actual forces and moments
"""

import math
from ..Containers import States
from ..Containers import Inputs
from ..Modeling import VehicleDynamicsModel
from ..Modeling import WindModel
from ..Utilities import MatrixMath
from ..Utilities import Rotations
from ..Constants import VehiclePhysicalConstants as VPC
import ece163.Constants.MAVLINKConstants as mvc



#the idea here is that we can set use_wind to false to mimic the behavior
#we expect students to code in Lab2.  STUDENTS SHOULD NOT HAVE THIS VARIABLE,
# OR IMPLEMENT THIS FEATURE
#use_wind = False
use_wind = True 

class VehicleAerodynamicsModel():	
	"""
	:ivar state: the current state of the vehicle, as an instance of States.vehicleState
	:ivar dot:  the current time-derivative of the state, as an instance of States.vehicleState
	:ivar dT:  the timestep that this object uses when Update()ing.
	"""
	def __init__(self, 
			  initialSpeed=VPC.InitialSpeed,
			  initialHeight=VPC.InitialDownPosition,
			  ):
		"""
		Initialization of the internal classes which ar used to track the vehicle aerodynamics and dynamics.

		:param initialSpeed: defaults to VPC.InitialSpeed
		:param initialHeight: defaults to VPC.InitialDownPosition
		:return: none
		"""

		self.vehicleDynamics = VehicleDynamicsModel.VehicleDynamicsModel()
		if use_wind:
			self.windModel = WindModel.WindModel()
		self.vehicleDynamics.state.pn = VPC.InitialNorthPosition
		self.vehicleDynamics.state.pe = VPC.InitialEastPosition
		self.vehicleDynamics.state.pd = VPC.InitialDownPosition
		self.vehicleDynamics.state.pd = initialHeight
		self.vehicleDynamics.state.u = initialSpeed
		self.initialSpeed = initialSpeed
		self.initialHeight = initialHeight
		return

	def Update(self, controls):
		"""
		Function that uses the current state (internal), wind (internal), and controls (inputs) to calculate the forces,
		and then do the integration	of the full 6-DOF non-linear equations of motion. Wraps the VehicleDynamicsModel class
		as well as the windState internally. The Wind and the vehicleState are maintained internally.

		:param controls: controlInputs class (Throttle, Elevator, Aileron, Rudder)
		:return: none, state is updated internally
		"""
		if use_wind:
			self.windModel.Update()
			Faero = self.updateForces(self.vehicleDynamics.state,  controls, wind = self.windModel.Wind)
		else:
			Faero = self.updateForces(self.vehicleDynamics.state,  controls)
		self.vehicleDynamics.Update(Faero)
		return

	def reset(self):
		"""
		Resets module to its original state so it can run again

		:return: none
		"""
		stateZero = States.vehicleState()
		if use_wind:
			self.windModel.reset()
		self.vehicleDynamics.state = stateZero
		self.vehicleDynamics.dot = stateZero
		self.vehicleDynamics.state.pd = self.initialHeight
		self.vehicleDynamics.state.u = self.initialSpeed
		return

	def getVehicleState(self):
		"""
		Wrapper function to return vehicle state form module

		:return: vehicle state class
		"""
		return self.vehicleDynamics.state

	def setVehicleState(self, state):
		"""
		Wrapper function to set the vehicle state from outside module

		:param state: class of vehicleState
		:return: none
		"""
		self.vehicleDynamics.state = state
		return

	def getVehicleDynamicsModel(self):
		"""
		Wrapper function to return the vehicle dynamics model handle

		:return: vehicleDynamicsModel, from VehicleDynamicsModel class
		"""
		return self.vehicleDynamics

	def getWindModel(self):		
		"""
		Wrapper function to return the windModel

		:return: windModel, from windModel class
		"""
		return self.windModel
	
	def setWindModel(self, windModel):
		"""
		Wrapper function to set the windModel

		:param windModel: from windModel class
		:return: none
		"""
		self. windModel = windModel
		


# 	def getWindState(self):
# 		"""
# 		Wrapper function to return wind state from module

# 		:return: wind state class
# 		"""
# 		return self.windModel.Wind

# 	def setWindModel(self, Wn=0.0, We=0.0, Wd=0.0, drydenParamters=VPC.DrydenNoWind):
# 		"""
# 		def setWindModel(self, Wn=0.0, We=0.0, Wd=0.0, drydenParamters=VPC.DrydenNoWind):
# 		Wrapper function that will inject constant winds and gust parameters into the wind model using the constant wind
# 		in the inertial frame (steady wind) and gusts that are stochastically derived in the body frame using the Dryden
# 		wind gust models.

# 		:param Wn: Steady wind in inertial North direction [m/s]
# 		:param We: Steady wind in inertial East direction [m/s]
# 		:param Wd: Steady wind in inertial Down direction [m/s], should usually be zero
# 		:param drydenParamters: model of Dryden parameters taken from constants
# 		:return: none
# 		"""
# 		self.windModel.Wind.Wn = Wn
# 		self.windModel.Wind.We = We
# 		self.windModel.Wind.Wd = Wd
# 		self.windModel.CreateDrydenTransferFns(self.vehicleDynamics.dT, VPC.InitialSpeed, drydenParamters)
# 		return

	def updateForces(self, state,  controls, wind = None):
		"""
		Function to update all of the aerodynamic, propulsive, and gravity forces and moments. All calculations required
		to update the forces are included. state is updated with new values for airspeed, angle of attack, and sideslip
		angles (see class definition for members)

		:param state: current vehicle state
		:param controls: current vehicle control surface deflections
		:param wind: current environmental wind.  If not specified, defaults to 0 windspeed
		:return: total forces, forcesMoments class
		"""
		Ftotal = Inputs.forcesMoments()

		if wind:
			Va, alpha, beta = self.CalculateAirspeed(state, wind)
			state.Va = Va
			state.alpha = alpha
			state.beta = beta
		else:
			Va = math.hypot(state.u, state.v, state.w)
			alpha = math.atan2(state.w, state.u)
			try:
				beta = math.asin(state.v/Va)
			except ZeroDivisionError:
				beta = 0
		state.Va = Va
		state.alpha = alpha
		state.beta = beta

		Fgravity = self.gravityForces(state)
		Faero = self.aeroForces(state)
		Fcontrols = self.controlForces(state, controls)
		Ftotal.Fx = Fgravity.Fx + Faero.Fx + Fcontrols.Fx
		Ftotal.Fy = Fgravity.Fy + Faero.Fy + Fcontrols.Fy
		Ftotal.Fz = Fgravity.Fz + Faero.Fz + Fcontrols.Fz
		Ftotal.Mx = Fgravity.Mx + Faero.Mx + Fcontrols.Mx
		Ftotal.My = Fgravity.My + Faero.My + Fcontrols.My
		Ftotal.Mz = Fgravity.Mz + Faero.Mz + Fcontrols.Mz
		return Ftotal

	def gravityForces(self, state):
		"""
		Function to project gravity forces into the body frame. Uses the gravity constant g0 from physical constants and
		the vehicle mass. Fg = m * R * [0 0 g0]'

		:param state: current vehicle state (need the rotation matrix)
		:return: gravity forces, forcesMoments class
		"""
		Fgravity = Inputs.forcesMoments()
		mg = [[0.], [0.], [VPC.mass * VPC.g0]]
		Fg = MatrixMath.multiply(state.R, mg)
		Fgravity.Fx = Fg[0][0]
		Fgravity.Fy = Fg[1][0]
		Fgravity.Fz = Fg[2][0]
		Fgravity.Mx = 0.
		Fgravity.My = 0.
		Fgravity.Mz = 0.
		return Fgravity

	def aeroForces(self, state):
		"""
		Function to calculate the Aerodynamic Forces and Moments using the linearized simplified force model and the
		stability derivatives in VehiclePhysicalConstants.py file. Specifically does not include forces due to control
		surface deflection. Requires airspeed (Va) in [m/s], angle of attack (alpha) in [rad] and sideslip angle (beta)
		in [rad] from the state.

		:param state: current vehicle state (need the velocities)
		:return: Aerodynamic forces, forcesMoments class
		"""
		Faero = Inputs.forcesMoments()

		QS = 0.5 * VPC.rho * state.Va ** 2 * VPC.S
		CLa, CDa, CMa = self.CalculateCoeff_alpha(state.alpha)
		if math.isclose(state.Va, 0.0):
			c_2Va = 1.0
			b_2Va = 1.0
		else:
			c_2Va = 0.5 * VPC.c * state.q / state.Va
			b_2Va = 0.5 * VPC.b / state.Va
		Lift = QS * (CLa + VPC.CLq * c_2Va)
		Drag = QS * (CDa + VPC.CDq * c_2Va)
		Faero.My = QS * VPC.c * (CMa + VPC.CMq * c_2Va)
		Faero.Fy = QS * (VPC.CY0 + VPC.CYbeta * state.beta + b_2Va * (VPC.CYp * state.p + VPC.CYr * state.r))
		Faero.Mx = QS * VPC.b * (VPC.Cl0 + VPC.Clbeta * state.beta + b_2Va * (VPC.Clp * state.p + VPC.Clr * state.r))
		Faero.Mz = QS * VPC.b * (VPC.Cn0 + VPC.Cnbeta * state.beta + b_2Va * (VPC.Cnp * state.p + VPC.Cnr * state.r))

		FXZ = MatrixMath.multiply([[math.cos(state.alpha), -math.sin(state.alpha)],
										 [math.sin(state.alpha), math.cos(state.alpha)]],
										[[-Drag], [-Lift]])
		Faero.Fx = FXZ[0][0]
		Faero.Fz = FXZ[1][0]
		return Faero

	def controlForces(self, state, controls):
		"""
		Function to calculate aerodynamic forces from control surface deflections (including throttle) using the linearized
		aerodynamics and simplified thrust model. Requires airspeed (Va) in [m/s] and angle of attack (alpha) in [rad] both
		from state.Va and state.alpha respectively.

		:param state: current vehicle state (need the velocities)
		:param controls: inputs to aircraft - controlInputs()
		:return: Control surface forces, forcesMoments class
		"""
		Fcontrols = Inputs.forcesMoments()

		alpha = state.alpha
		QS = 0.5 * VPC.rho * state.Va ** 2 * VPC.S
		Fcontrols.Fy = QS * (VPC.CYdeltaA * controls.Aileron + VPC.CYdeltaR * controls.Rudder)
		Fcontrols.Mx = QS * VPC.b * (VPC.CldeltaA * controls.Aileron + VPC.CldeltaR * controls.Rudder)
		Fcontrols.Mz = QS * VPC.b * (VPC.CndeltaA * controls.Aileron + VPC.CndeltaR * controls.Rudder)
		Fcontrols.My = QS * VPC.c * VPC.CMdeltaE * controls.Elevator

		Fprop, Mprop = self.CalculatePropForces(state.Va, controls.Throttle)
		Fcontrols.Mx += Mprop

		dLift = QS * VPC.CLdeltaE * controls.Elevator
		dDrag = QS * VPC.CDdeltaE * controls.Elevator
		dFXZ = MatrixMath.multiply([[math.cos(alpha), -math.sin(alpha)], [math.sin(alpha), math.cos(alpha)]],
										 [[-dDrag], [-dLift]])

		Fcontrols.Fx = dFXZ[0][0] + Fprop
		Fcontrols.Fz = dFXZ[1][0]
		return Fcontrols

	def CalculateAirspeed(self, state, wind):
		"""
		Calculates the total airspeed, as well as angle of attack and side-slip angles from the wind and current state.
		Needed for further aerodynamic force calculations. Va, wind speed [m/s], alpha, angle of attack [rad], and beta,
		side-slip angle [rad] are returned from the function. The state must be updated outside this function.

		Note: when total wind speed (math.hypot(wind.Wn, wind.We, wind.Wd)) is zero, set gamma wind to 0 (otherwise it is
		undefined because arcsin is greater than 1.), Also, check is the total airspeed (Va) is zero before calculating the
		sideslip angle beta, if so, set beta to 0.0

		:param state: current vehicle state (need the velocities)
		:param wind: current wind state (global and gust)
		:return: Va, wind speed [m/s], alpha, angle of attack [rad], and beta, side-slip angle [rad]
		"""
		Chiw = math.atan2(wind.We, wind.Wn)
		Ws = math.hypot(wind.Wn, wind.We, wind.Wd)
		if math.isclose(Ws,0.0):
			gammaw = 0.0
		else:
			gammaw = -math.asin(wind.Wd/Ws)
		Razel = Rotations.euler2DCM(Chiw, gammaw, 0.0)
		WgustInertial = MatrixMath.multiply(MatrixMath.transpose(Razel), [[wind.Wu], [wind.Wv], [wind.Ww]])
		Vw = MatrixMath.multiply(state.R, MatrixMath.add(WgustInertial, [[wind.Wn], [wind.We], [wind.Wd]]))
		Vr = MatrixMath.subtract([[state.u], [state.v], [state.w]], Vw)
		Va = math.hypot(Vr[0][0], Vr[1][0], Vr[2][0])
		alpha = math.atan2(Vr[2][0], Vr[0][0])  # angle of attack
		if math.isclose(Va,0.):
			beta = 0.0
		else:
			beta = math.asin(Vr[1][0] / Va)  # side-slip angle
		return Va, alpha, beta

	def CalculateCoeff_alpha(self, alpha):
		"""
		Function to calculate the Coefficient of Lift and Drag (and Moment) as a function of angle of attack. Angle of attack (alpha)
		in [rad] is contained within the state.alpha and updated within the CalculateAirspeed function. For pre-stall lift and drag, use simple
		linear model for lift: CL = CL0 + CLalpha * alpha
		
		and for the pre-stall drag use the parabolic form: CD = CDp + (CL(alpha))^2/(pi*AR*e), where CL(alpha) is the pre-stall lift above. 

		For post-stall, use the flat-plate models for lift and drag: CL = 2 * sin(alpha) * cos(alpha) and CD = 2 sin^2(alpha). Use the exponential
		blending function (sigma) outlined in the book to blend pre- and post-stall lift and drag, with parameters of M and alpha0 taken 
		from VehiclePhysicalParameters.py

		Note that for CM use the same model throughout: CM = CM0 + CMalpha * alpha.

		:param alpha, Angle of Attack [rad]
		:return: Coefficient of Lift, CL_alpha (unitless), Coefficient of Drag, CD_alpha (unitless), Coefficient of Moment, CM_alpha (unitless)
		"""
		# CL_alpha_poststall = math.copysign(math.sin(alpha) ** 2 * math.cos(alpha), alpha) # from book, wrong formula
		CL_alpha_poststall = 2.0 * math.sin(alpha) * math.cos(alpha)
		CD_alpha_poststall = 2.0 * math.sin(alpha) ** 2
		CL_alpha_prestall = VPC.CL0 + VPC.CLalpha * alpha
		CD_alpha_prestall = VPC.CDp + (CL_alpha_prestall) ** 2 / (math.pi * VPC.e * VPC.AR)
		sigma = (1 + math.exp(-VPC.M * (alpha - VPC.alpha0)) + math.exp(VPC.M * (alpha + VPC.alpha0))) / \
				((1 + math.exp(-VPC.M * (alpha - VPC.alpha0))) * (1 + math.exp(VPC.M * (alpha + VPC.alpha0))))
		CL_alpha = (1 - sigma) * CL_alpha_prestall + sigma * CL_alpha_poststall
		CD_alpha = (1 - sigma) * CD_alpha_prestall + sigma * CD_alpha_poststall
		CM_alpha = VPC.CM0 + VPC.CMalpha * alpha
		return CL_alpha, CD_alpha, CM_alpha


	def CalculatePropForces(self, Va, Throttle):
		"""
		Function to calculate the propeller forces and torques on the aircraft. Uses the fancy propeller model that
		parameterizes the torque and thrust	coefficients of the propeller using the advance ratio. See
		ECE163_PropellerCheatSheet.pdf for details. Note: if the propo speed \Omega is imaginary, then set it to 100.0

		:param Va, the vehicle airspeed [m/s]
		:param Throttle: Throttle input [0-1]
		:return: Fx_prop [N], Mx_prop [N-m]
		"""

		if mvc.airframeMode == mvc.defaultAirframe:
			Vin = VPC.V_max * Throttle
			a = VPC.C_Q0 * VPC.rho * VPC.D_prop ** 5 / (4.0 * math.pi ** 2)
			b = VPC.C_Q1 * VPC.rho * VPC.D_prop ** 4 * Va / (2 * math.pi) + VPC.KQ**2 / VPC.R_motor
			c = VPC.C_Q2 * VPC.rho * VPC.D_prop ** 3 * Va ** 2 - VPC.KQ * Vin / VPC.R_motor + VPC.KQ * VPC.i0

			try:
				Omega_op = (-b + math.sqrt(b ** 2 - 4 * a * c)) / (2 * a)
			except:
				# print("Crashed Propeller Model doing square root (imaginary), Va = {}, dT = {}, b = {}, c = {}".format(Va, DeltaT, b, c))
				Omega_op = 100.0
			J_op = 2. * math.pi * Va / (Omega_op * VPC.D_prop)
			CTprop = VPC.C_T2 * J_op ** 2 + VPC.C_T1 * J_op + VPC.C_T0
			CQprop = VPC.C_Q2 * J_op ** 2 + VPC.C_Q1 * J_op + VPC.C_Q0

			Fx_prop = VPC.rho * Omega_op ** 2 * VPC.D_prop ** 4 * CTprop / (4. * math.pi ** 2)
			Mx_prop = -VPC.rho * Omega_op ** 2 * VPC.D_prop ** 5 * CQprop / (4. * math.pi ** 2)
			return Fx_prop, Mx_prop
		elif mvc.airframeMode == mvc.explorerAirframe:
			Vin = VPC.V_max * Throttle
			Omega_op = 100.0
			J_op = 2. * math.pi * Va / (Omega_op * VPC.D_prop)
			Fx_prop = VPC.rho * Omega_op ** 2 * VPC.D_prop ** 4 * VPC.C_T0 / (4. * math.pi ** 2)
			Mx_prop = -VPC.rho * Omega_op ** 2 * VPC.D_prop ** 5 * VPC.C_Q0 / (4. * math.pi ** 2)
			return Fx_prop, Mx_prop
	def getVPC(self):
		return VPC

