/* 
 * File:   RC_Servo.h
 * Author: Margaret Silva
 * 
 * Library used to control pulse width. Needed to work with gimbal servos
 *
 * Created on May 28, 2023, 4:08 PM
 */

#ifndef RCSERVO_H
#define	RCSERVO_H

#include <stdint.h>

/*******************************************************************************
 * PUBLIC #DEFINES                                                            *
 ******************************************************************************/

#define RC_SERVO_MIN_PULSE 600
#define RC_SERVO_CENTER_PULSE 1500 // 1500
#define RC_SERVO_MAX_PULSE 2400

#define YAW_MAX_ANGLE 90.0
#define YAW_MIN_ANGLE -90.0

#define PITCH_MAX_ANGLE 15.0
#define PITCH_MIN_ANGLE -15.0

#define ROLL_MAX_ANGLE 15.0
#define ROLL_MIN_ANGLE -15.0

#define YAW_SERVO SERVO_PWM_1
#define PITCH_SERVO SERVO_PWM_2
#define ROLL_SERVO SERVO_PWM_3

enum {
    SERVO_PWM_1, // OC1, pin 3
    SERVO_PWM_2, // OC2, pin 5
    SERVO_PWM_3 // OC3, pin 6
};


/*******************************************************************************
 * PUBLIC FUNCTIONS                                                           *
 ******************************************************************************/

/**
 * @Function RCServo_Init(uint8_t output_channel)
 * @param output_channel - pwm pin to init
 * @return SUCCESS or ERROR
 * @brief initializes hardware required and set it to the CENTER PULSE */
int RCServo_Init(uint8_t output_channel);

/**
 * @Function int RCServo_SetPulse(unsigned int inPulse)
 * @param inPulse, integer representing number of microseconds
 * @return SUCCESS or ERROR
 * @brief takes in microsecond count, converts to ticks and updates the internal variables
 * @warning This will update the timing for the next pulse, not the current one */
int RCServo_SetPulse(unsigned int inPulse, uint8_t output_channel);

/**
 * @Function int RCServo_SetPulseFromAngle(float angle, uint8_t output_channel)
 * @param angle - angle in degrees to set rc servo to
 *        output_channel - RC servo to output to
 * @return SUCCESS or ERROR
 * @brief takes in microsecond count, converts to ticks and updates the internal variables
 * @warning This will update the timing for the next pulse, not the current one */
int RCServo_SetPulseFromAngle(float angle, uint8_t output_channel);

/**
 * @Function int RCServo_GetPulse(void)
 * @param None
 * @return Pulse in microseconds currently set */
unsigned int RCServo_GetPulse(uint8_t output_channel);

/**
 * @Function int RCServo_GetRawTicks(void)
 * @param None
 * @return raw timer ticks required to generate current pulse. */
unsigned int RCServo_GetRawTicks(uint8_t output_channel);

/**
 * @Function unsigned int RCServo_MapDegreesToPulse(float angle)
 * @param angle - angle in degrees to set rc servo to
 * @return required pulse width */
unsigned int RCServo_MapDegreesToPulse(float angle);

#endif	/* RCSERVO_H */



