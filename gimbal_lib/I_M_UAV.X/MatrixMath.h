/* 
 * File:   MatrixMath.h
 * Author: Margaret Silva
 * Note: This is an adapted version of the matrix math library I wrote in CSE 13E,
 *      with added functions to deal with vector operations
 *
 * Created on May 15, 2023, 2:41 PM
 */

#ifndef MATRIX_MATH_H
#define MATRIX_MATH_H

/**
 * @file
 *
 * @section DESCRIPTION
 *
 * This file implements a basic 3x3 matrix math library. Basic matrix operations are provided along
 * with the matrix inverse function (though that function cannot handle singular matrices).
 *
 * Matrices are defined in row-major order, so that the matrix:
 *   0 1 2
 *   3 4 5
 *   6 7 8
 * is represented by the array `float mat[3][3] = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};`.
 */

/**
 * FP_DELTA defines the tolerance for testing equality for floating-point numbers. 
 * Used within MatrixEquals() 
 * Also used to compare scalar return values, such as the result of MatrixTrace().
 */
#define FP_DELTA 0.0001

/**
 * To avoid using magic numbers, use this macro to iterate over 3 dimensions:
 */
#define DIM 3


/******************************************************************************
 * Matrix Display:
 *****************************************************************************/
/**
 * MatrixPrint displays a 3x3 array to standard output with clean, readable, 
 * consistent formatting.  
 * @param: mat, pointer to a 3x3 float array
 * @return: none
 * The printed matrix should be aligned in a grid when called with positive or
 *  negative numbers.  It should be able to display at least FP_DELTA precision, and
 *  should handle numbers as large as 999.0 or -999.0
 */
void MatrixPrint(float mat[3][3]);


/******************************************************************************
 * Matrix - Matrix Operations
 *****************************************************************************/

/**
 * MatrixEquals checks if the two matrix arguments are equal (to within FP_DELTA).
 * @param: mat1, pointer to a 3x3 float array
 * @param: mat2, pointer to a 3x3 float array
 * @return: TRUE if and only if every element of mat1 is within FP_DELTA of the corresponding element of mat2,  otherwise return FALSE
 * Neither mat1 nor mat2 is modified by this function.
 */
int MatrixEquals(float mat1[3][3], float mat2[3][3]);

/**
 * MatrixAdd performs an element-wise matrix addition operation on two 3x3 matrices and 
 * "returns" the result by modifying the third argument.
 * @param: mat1, pointer to a summand 3x3 matrix
 * @param: mat2, pointer to a summand 3x3 matrix
 * @param: result, pointer to a 3x3 matrix that is modified to contain the sum of mat1 and mat2.
 * @return:  None
 * mat1 and mat2 are not modified by this function.  result is modified by this function.
 */
void MatrixAdd(float mat1[3][3], float mat2[3][3], float result[3][3]);

/**
 * MatrixSub performs an element-wise matrix subtraction operation on two 3x3 matrices and 
 * "returns" the result by modifying the third argument.
 * @param: mat1, pointer to a summand 3x3 matrix
 * @param: mat2, pointer to a summand 3x3 matrix
 * @param: result, pointer to a 3x3 matrix that is modified to contain the subtractions of mat2 from mat1.
 * @return:  None
 * mat1 and mat2 are not modified by this function.  result is modified by this function.
 */
void MatrixSub(float mat1[3][3], float mat2[3][3], float result[3][3]);

/**
 * MatrixMultiply performs a matrix-matrix multiplication operation on two 3x3
 * matrices and "returns" the result by modifying the third argument.
 * @param: mat1, pointer to left factor 3x3 matrix
 * @param: mat2, pointer to right factor 3x3 matrix
 * @param: result, pointer to matrix that is modified to contain the matrix product of mat1 and mat2.
 * @return: none
 * mat1 and mat2 are not modified by this function.  result is modified by this function.
 */
void MatrixMultiply(float mat1[3][3], float mat2[3][3], float result[3][3]);


/******************************************************************************
 * Vector - Vector Operations
 *****************************************************************************/

/**
 * VectorAdd performs an element-wise vector addition operation on two 3x1 vectors and 
 * "returns" the result by modifying the third argument.
 * @param: v1, pointer to a summand 3x1 vector
 * @param: v2, pointer to a summand 3x1 vector
 * @param: result, pointer to a 3x1 vector that is modified to contain the sum of v1 and v2.
 * @return:  None
 * v1 and v2 are not modified by this function.  result is modified by this function.
 */
void VectorAdd(float v1[3], float v2[3], float result[3]);

/**
 * VectorSub performs an element-wise vector subtraction operation on two 3x1 vectors and 
 * "returns" the result by modifying the third argument.
 * @param: v1, pointer to a 3x1 vector
 * @param: v2, pointer to a 3x1 vector
 * @param: result, pointer to a 3x1 vector that is modified to contain the subtraction of v2 from v1.
 * @return:  None
 * v1 and v2 are not modified by this function.  result is modified by this function.
 */
void VectorSub(float v1[3], float v2[3], float result[3]);

/**
 * VectorCross performs a cross product operation on two 3x1 vectors and 
 * "returns" the result by modifying the third argument.
 * @param: v1, pointer to a 3x1 vector
 * @param: v2, pointer to a 3x1 vector
 * @param: result, pointer to a 3x1 vector that is modified to contain the cross product of v1 and v2.
 * @return:  None
 * v1 and v2 are not modified by this function.  result is modified by this function.
 */
void VectorCross(float v1[3], float v2[3], float result[3]);

/******************************************************************************
 * Matrix - Vector Operations
 *****************************************************************************/

/**
 * MatrixVectorMultiply performs a matrix-vector multiplication operation on a 3x3 matrix
 * and a 3x1 vector and "returns" the result by modifying the third argument.
 * @param: mat, pointer to left factor 3x3 matrix
 * @param: v, pointer to right factor 3x1 vector
 * @param: result, pointer to matrix that is modified to contain the matrix product of mat and v.
 * @return: none
 * mat and v are not modified by this function.  result is modified by this function.
 */
void MatrixVectorMultiply(float mat[3][3], float v[3], float result[3]);


/******************************************************************************
 * Matrix - Scalar Operations
 *****************************************************************************/

/**
 * MatrixScalarAdd performs the addition of a matrix and a scalar.  Each element of the matrix is increased by x.
 * The result is "returned"by modifying the third argument.
 * @param: x, a scalar float
 * @param: mat, pointer to a 3x3 matrix
 * @param: result, pointer to matrix that is modified to contain mat + x.
 * @return: none
 * x and mat are not modified by this function.  result is modified by this function.
 */
void MatrixScalarAdd(float x, float mat[3][3], float result[3][3]);

/**
 * MatrixScalarAdd performs the multiplication of a matrix and a scalar.
 * Each element of the matrix is multiplied x.
 * The result is "returned"by modifying the third argument.
 * @param: x, a scalar float
 * @param: mat, pointer to a 3x3 matrix
 * @param: result, pointer to matrix that is modified to contain mat + x.
 * @return: none
 * x and mat are not modified by this function.  result is modified by this function.
 */
void MatrixScalarMultiply(float x, float mat[3][3], float result[3][3]);


/******************************************************************************
 * Vector - Scalar Operations
 *****************************************************************************/

/**
 * VectorScalarMultiply performs the multiplication of a vector and a scalar.
 * Each element of the matrix is multiplied x.
 * The result is "returned"by modifying the third argument.
 * @param: x, a scalar float
 * @param: vec, pointer to a 3x1 vector
 * @param: result, pointer to vector that is modified to contain vec * x.
 * @return: none
 * x and vec are not modified by this function.  result is modified by this function.
 */
void VectorScalarMultiply(float x, float v[3], float result[3]);

/******************************************************************************
 * Unary Matrix Operations
 *****************************************************************************/

/**
 * MatrixTrace calculates the trace of a 3x3 matrix.
 * @param: mat, a pointer to a 3x3 matrix
 * @return: the trace of mat
 */
float MatrixTrace(float mat[3][3]);

/**
 * MatrixTranspose calculates the transpose of a matrix and "returns" the
 * result through the second argument.
 * @param: mat, pointer to a 3x3 matrix
 * @param: result, pointer to matrix that is modified to transpose of mat
 * mat is not modified by this function.  result is modified by this function.
 */
void MatrixTranspose(float mat[3][3], float result[3][3]);

/**
 * MatrixSubmatrix finds a submatrix of a 3x3 matrix that is 
 * formed by removing the i-th row and the j-th column.  The 
 * submatrix is "returned" by modifying the final argument.
 * 
 * @param: i, a row of the matrix, INDEXING FROM 0
 * @param: j, a column of the matrix, INDEXING FROM 0
 * @param: mat, a pointer to a 3x3 matrix
 * @param: result, a pointer to a 2x2 matrix
 * @return: none
 * 
 * mat is not modified by this function.  Result is modified by this function.
 */
void MatrixSubmatrix(int i, int j, float mat[3][3], float result[2][2]);

/**
 * MatrixDeterminant calculates the determinant of a 3x3 matrix 
 * and returns the value as a float.
 * @param: mat, a pointer to a 3x3 matrix
 * @return: the determinant of mat
 * mat is not modified by this function.
 * */
float MatrixDeterminant(float mat[3][3]);


/* MatrixInverse calculates the inverse of a matrix and
 * "returns" the result by modifying the second argument.
 * @param: mat, a pointer to a 3x3 matrix
 * @param: result, a pointer to a 3x3 matrix that is modified to contain the inverse of mat
 * @return: none
 * mat is not modified by this function.  result is modified by this function.
 */
void MatrixInverse(float mat[3][3], float result[3][3]);

/******************************************************************************
 * Vector Unary Operations
 *****************************************************************************/
/**
 * VectorNorm returns the magnitude (norm) of the vector taken as argument
 * @param: v, pointer to 3x1 vector
 * @return: magnitude of vector
 * v is not modified by this function
 */
float VectorNorm(float v[3]);

/**
 * VectorSet creates a vector using the input float as values
 * @param: v0, first element of vector
 * @param: v1, second element of vector
 * @param: v2, third element of vector
 * @param: vec, pointer to 3x1 vector equal to [v0, v1, v2]
 * @return: magnitude of vector
 * v0, v1, and v2 are not modified by this function. vec is
 */
void VectorSet(float v0, float v1, float v2, float vec[3]);

#endif // MATRIX_MATH_H


