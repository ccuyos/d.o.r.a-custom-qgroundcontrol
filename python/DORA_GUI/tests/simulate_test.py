import sys
sys.path.append('..')
import random

import ece163.Simulation.Simulate
import ece163.Containers.States


class testModel:
	def __init__(self):
		self.state = ece163.Containers.States.vehicleState()
		self.otherState = ece163.Containers.States.vehicleState()

	def update(self, dT, pn, pe, pd):
		self.state.pn += pn
		self.state.pe += pd
		self.state.pd += pe
		self.state.yaw += random.random()
		self.state.pitch += random.random()
		self.state.roll += dT
		self.otherState.pd += 5

	def getState(self):
		return self.state

	def reset(self):
		self.state = ece163.Containers.States.vehicleState()
		self.otherState.pd = 0

	def getOtherVar(self):
		return self.otherState

class testSim(ece163.Simulation.Simulate.Simulate):
	def __init__(self):
		super().__init__()
		self.underlyingModel = testModel() # we instantiate the used model here
		self.inputNames.extend(['pn', 'pe', 'pd']) # we need the names of the inputs for export
		self.variableList.append((self.underlyingModel.getState, 'state',
									['pn', 'pe', 'pd', 'yaw', 'pitch', 'roll'])) # and the variables we care about
		self.variableList.append((self.underlyingModel.getOtherVar, 'otherState', ['pd']))

	def takeStep(self, pn, pe, pd):
		self.time += self.dT

		self.underlyingModel.update(self.dT, pn, pe, pd)
		self.recordData([pn, pe, pd])
		return




simClass = testSim()


for i in range(10):
	simClass.takeStep(1, 2, 3)
print(len(simClass.takenData))
simClass.reset()
for i in range(3):
	simClass.takeStep(1, 2, 3)
print(len(simClass.takenData))
simClass.exportToCSV('testData.csv')
simClass.exportToPickle('testData.pickle')
# print(simClass.takenData)
