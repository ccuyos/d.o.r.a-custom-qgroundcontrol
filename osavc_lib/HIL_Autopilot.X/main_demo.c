/* 
 * File:   main_demo.c
 * Author: Margaret Silva
 * Brief: main file for demonstrating data transfer functionality 
 *
 * Created on March 15, 2023, 2:13 PM
 */


/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

// standard libraries
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/attribs.h>  //for ISR definitions
#include "xc.h"
#include "p32mx795f512l.h"

// DORA libraries
#include "AirframeConstants.h"
#include "StabilityController.h"
#include "StateEstimation.h"
#include "PD.h"
#include "PI.h"

// OSAVC libraries
#include "inc/SerialM32.h"
#include "Board.h"
#include "System_timer.h"
#include "Radio_serial.h"
#include "common/mavlink.h"
#include "NEO_M8N.h"
#include "RC_RX.h"
#include "RC_servo.h"
#include "ICM_20948.h"
#include "AS5047D.h"
#include "PID.h"
#include "RC_servo.h"

/*******************************************************************************
 * #DEFINES                                                                    *
 ******************************************************************************/
#define HEARTBEAT_PERIOD 1000 //1 sec interval for hearbeat update
#define SENDING_PERIOD 20 // 20 ms between 
#define CONTROL_PERIOD 20 //Period for control loop in msec #20
#define ENCODER_PERIOD 10 //re-initiate encoder at 100 Hz
#define CONTROL_FREQUENCY (1000/CONTROL_PERIOD) //frequency in Hz
#define ENCODER_MAX_CTS 16384.0 //2^14 counts/rev for the encoder
#define ENCODER_TWO_PI 0.000383495197  //counts to 2 pi conversion = 2pi/0x4fff
#define WHEEL_RADIUS 0.0325 //meters
#define GPS_PERIOD 100 //10 Hz update rate
#define BUFFER_SIZE 1024
#define UINT_16_MAX 0xffff
#define KNOTS_TO_MPS 0.5144444444 //1 meter/second is equal to 1.9438444924406 knots
#define RAW 1
#define SCALED 2
#define CTS_2_USEC 1  //divide by 2 scaling of counts to usec with right shift
#define NEW_STATE_DATA 1
#define NO_NEW_STATE_DATA 0

//enum TestStates {
//    WAITING_FOR_TEST, PID_TEST, PI_TEST, PD_TEST, FULL_CONTROLLER_TEST
//} TestStates;

// used to determine what data is being sent in an HIL actuator commands packet
#define HIL_RECEIVE_STATE 128
#define HIL_RECEIVE_COMMANDS 256

// used to control printing speed of messages
#define CONSOLE_OUTPUT_FREQUENCY 50

// use this to switch between sending data over serial/over radio
#define GET_CHAR() get_char()
#define PUT_CHAR(c) put_char(c)
#define DATA_AVAILABLE() data_available()

#define SAFE_HIGH 1790
#define SAFE_LOW 1000

/*******************************************************************************
 * VARIABLES                                                                   *
 ******************************************************************************/
mavlink_system_t mavlink_system = {
    1, // System ID (1-255)
    MAV_COMP_ID_AUTOPILOT1 // Component ID (a MAV_COMPONENT value)
};

// gains array from simulator
static struct controller_gains simulator_gains = DEFAULT_CONTROLLER_GAINS;

// current vehicle state from simulator
static struct vehicle_state simulator_state = DEFAULT_VEHICLE_STATE;

// controller commands
static float X_c = 0;
static float h_c = 100;
static float Va_c = 20;

// new state flag, used to command controller to only update when new state data is received
static uint8_t new_state_received = FALSE;

enum RC_channels {
    AIL,
    ELE,
    THR,
    RUD,
    HASH,
    SAFE,
    SWITCH_A,
    SWITCH_B,
    SWITCH_C,
    SWITCH_D,
    SWITCH_E
}; //map to the car controls from the RC receiver

enum mav_output_type {
    USB,
    RADIO
};

enum flight_mode {
    MANUAL, AUTONOMOUS
};

const uint16_t RC_raw_fs_scale = RC_RAW_TO_FS;

RCRX_channel_buffer RC_channels[CHANNELS];
static uint8_t pub_RC_servo = FALSE;
static uint8_t pub_RC_signals = TRUE;
static uint16_t ail_prev = 0;
static uint16_t ele_prev = 0;
static uint16_t thr_prev = 0;
static uint16_t rud_prev = 0;
static int safe = 69;
static enum flight_mode mode = MANUAL;

/*******************************************************************************
 * FUNCTION PROTOTYPES                                                         *
 ******************************************************************************/

/**
 * @function RC_channels_init(void)
 * @param none
 * @brief set all RC channels to RC_RX_MID_COUNTS
 * @author Aaron Hunter
 */
void RC_channels_init(void);

/**
 * @function checkRadioEvents(void)
 * @param none
 * @returns none
 * @brief looks for messages sent over the radio serial port to OSAVC
 * @author Margaret Silva
 */
void checkRadioEvents(void);

/**
 * @Function publish_controller_output(float output, char controller_type[10])
 * @param none
 * @brief invokes mavlink helper to send controller output over radio
 * @author aaron hunter
 */
void publish_controller_output(float output, char controller_type[10]);

// send actuator commands to the simulator
void publish_actuator_cmds(struct controller_outputs actuators);

/**
 * @Function publish_status_text(void)
 * @param none
 * @brief invokes mavlink helper to send out status text
 * @author aaron hunter
 */
void publish_status_text(char text[50]);

/**
 * @Function publish_heartbeat(void)
 * @param none
 * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
 * @author aaron hunter
 */
void publish_heartbeat(void);

/**
 * @Function calc_pw(uint16_t raw_counts)
 * @param raw counts from the radio transmitter (11 bit unsigned int)
 * @return pulse width in microseconds
 * @brief converts the RC input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author aahunter
 * @modified <Your Name>, <year>.<month>.<day> <hour> <pm/am> */
static uint16_t calc_pw(uint16_t raw_counts);

/**
 * @Function calc_pw_from_angle(float angle, float min, float max)
 * @param angle - desired angle (radians)
 * @param min - minimum angle (radians)
 * @param max - maximum angle (radians)
 * @return pulse width in microseconds
 * @brief converts the angle input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author Margaret Silva
 */
static uint16_t calc_pw_from_angle(float angle, float min, float max);

/**
 * @Function set_control_output(void)
 * @param control - controller output struct
 * @return none
 * @brief converts RC input signals to pulsewidth values and sets the actuators
 * (servos and ESCs) to those values
 * @author Aaron Hunter
 */
void set_control_output(struct controller_outputs controls);

///**
// * @function publish_RC_signals_raw(void)
// * @param none
// * @brief scales raw RC signals
// * @author Aaron Hunter
// */
void publish_RC_signals_raw(void);

/**
 * @function check_RC_events(void)
 * @param none
 * @brief checks for RC messages and stores data in RC channel buffer
 * @author Aaron Hunter
 */
void check_RC_events();

/*******************************************************************************
 * FUNCTIONS                                                                   *
 ******************************************************************************/

/**
 * @function RC_channels_init(void)
 * @param none
 * @brief set all RC channels to RC_RX_MID_COUNTS
 * @author Aaron Hunter
 */
void RC_channels_init(void) {
    uint8_t i;
    for (i = 0; i < CHANNELS; i++) {
        RC_channels[i] = RC_RX_MID_COUNTS;
    }
}

/**
 * @function checkRadioEvents(void)
 * @param none
 * @returns none
 * @brief looks for messages sent over the radio serial port to OSAVC
 * @author Margaret Silva
 */
void checkRadioEvents() {
    uint8_t channel = MAVLINK_COMM_0;
    uint8_t msg_byte;
    mavlink_message_t msg_rx;
    mavlink_status_t msg_rx_status;

    mavlink_hil_actuator_controls_t hil_sim_state;
    mavlink_named_value_float_t named_float;
    mavlink_statustext_t status_text;
    mavlink_hil_controls_t hil_controls_ping_pong;

    struct controller_outputs act_out;

    if (DATA_AVAILABLE()) {
        msg_byte = GET_CHAR();
        if (mavlink_parse_char(channel, msg_byte, &msg_rx, &msg_rx_status)) {
            switch (msg_rx.msgid) {
                case MAVLINK_MSG_ID_NAMED_VALUE_FLOAT:
                    // used to pass in controller gains from simulator
                    mavlink_msg_named_value_float_decode(&msg_rx, &named_float);
                    //printf("\nReceived param of name %s and value %f from Ground Control\r\n", named_float.name, named_float.value);
                    if (strcmp(named_float.name, "p_roll") == 0) {
                        simulator_gains.kp_roll = named_float.value;
                    } else if (strcmp(named_float.name, "d_roll") == 0) {
                        simulator_gains.kd_roll = named_float.value;
                    } else if (strcmp(named_float.name, "i_roll") == 0) {
                        simulator_gains.ki_roll = named_float.value;
                    } else if (strcmp(named_float.name, "p_side") == 0) {
                        simulator_gains.kp_rudder = named_float.value;
                    } else if (strcmp(named_float.name, "i_side") == 0) {
                        simulator_gains.ki_rudder = named_float.value;
                    } else if (strcmp(named_float.name, "p_course") == 0) {
                        simulator_gains.kp_course = named_float.value;
                    } else if (strcmp(named_float.name, "i_course") == 0) {
                        simulator_gains.ki_course = named_float.value;
                    } else if (strcmp(named_float.name, "p_pitch") == 0) {
                        simulator_gains.kp_pitch = named_float.value;
                    } else if (strcmp(named_float.name, "d_pitch") == 0) {
                        simulator_gains.kd_pitch = named_float.value;
                    } else if (strcmp(named_float.name, "p_alt") == 0) {
                        simulator_gains.kp_altitude = named_float.value;
                    } else if (strcmp(named_float.name, "i_alt") == 0) {
                        simulator_gains.ki_altitude = named_float.value;
                    } else if (strcmp(named_float.name, "p_tSpeed") == 0) {
                        simulator_gains.kp_speedFromThrottle = named_float.value;
                    } else if (strcmp(named_float.name, "i_tSpeed") == 0) {
                        simulator_gains.ki_speedFromThrottle = named_float.value;
                    } else if (strcmp(named_float.name, "p_eSpeed") == 0) {
                        simulator_gains.kp_speedFromElevator = named_float.value;
                    } else if (strcmp(named_float.name, "i_eSpeed") == 0) {
                        simulator_gains.ki_speedFromElevator = named_float.value;
                        initController();
                        setControllerGains(simulator_gains);
                        //printf("\n Set controller gains. Result: \r\n");
                        //print_control_gains(getControllerGains());
                    } else {
                        //printf("\n Received named float: name=%s value=%f \r\n", named_float.name, named_float.value);
                    }
                    break;
                case MAVLINK_MSG_ID_HIL_ACTUATOR_CONTROLS:
                    // used to pass in state data from simulator to controller
                    mavlink_msg_hil_actuator_controls_decode(&msg_rx, &hil_sim_state);
                    switch (hil_sim_state.flags) {
                        case HIL_RECEIVE_STATE:
                            setCurrentState(hil_sim_state.controls);
                            new_state_received = TRUE;
                            break;
                        case HIL_RECEIVE_COMMANDS:
                            X_c = hil_sim_state.controls[0];
                            h_c = hil_sim_state.controls[1];
                            Va_c = hil_sim_state.controls[2];
                            break;
                        default:
                            break;
                    }
                    break;
                case MAVLINK_MSG_ID_HIL_CONTROLS:
                    // used for debug to validate that hil controls are being sent properly
                    break;
                    mavlink_msg_hil_controls_decode(&msg_rx, &hil_controls_ping_pong);
                    act_out.d_aileron = hil_controls_ping_pong.roll_ailerons;
                    act_out.d_elevator = hil_controls_ping_pong.pitch_elevator;
                    act_out.d_rudder = hil_controls_ping_pong.yaw_rudder;
                    act_out.d_throttle = hil_controls_ping_pong.throttle;
                    //publish_actuator_cmds(act_out);
                    //printf("\nSent actuator commands in response\r\n");
                    break;
                case MAVLINK_MSG_ID_STATUSTEXT:
                    // debug message
                    mavlink_msg_statustext_decode(&msg_rx, &status_text);
                    //printf("\nStatus text status msg %s received from Ground Control\r\n", status_text.text);
                    //publish_status_text();
                    break;
                default:
                    break;
            }
        }
    }
}

/**
 * @Function publish_controller_output(float output, char controller_type[10])
 * @param none
 * @brief invokes mavlink helper to send controller output over radio
 * @author aaron hunter
 */
void publish_controller_output(float output, char controller_type[10]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_named_value_float_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            0,
            controller_type,
            output);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

// send actuator commands to the simulator

void publish_actuator_cmds(struct controller_outputs actuators) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    mavlink_msg_hil_controls_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            0, // time (unused)
            actuators.d_aileron,
            actuators.d_elevator,
            actuators.d_rudder,
            actuators.d_throttle,
            0.0, 0.0, 0.0, 0.0, // unused aux commands
            0,
            0
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_status_text(void)
 * @param none
 * @brief invokes mavlink helper to send out status text
 * @author aaron hunter
 */
void publish_status_text(char text[50]) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t severity = 5;
    uint16_t id = 0;
    uint8_t chunk_seq = 0;
    mavlink_msg_statustext_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            severity,
            text,
            id,
            chunk_seq
            );
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function publish_heartbeat(void)
 * @param none
 * @brief invokes mavlink helper to generate heartbeat and sends out via the radio
 * @author aaron hunter
 */
void publish_heartbeat(void) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t mode = MAV_MODE_FLAG_MANUAL_INPUT_ENABLED | MAV_MODE_FLAG_SAFETY_ARMED;
    uint32_t custom = 0;
    uint8_t state = MAV_STATE_STANDBY;
    mavlink_msg_heartbeat_pack(mavlink_system.sysid
            , mavlink_system.compid,
            &msg_tx,
            MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC,
            mode,
            custom,
            state);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        PUT_CHAR(msg_buffer[index]);
    }
}

/**
 * @Function calc_pw(uint16_t raw_counts)
 * @param raw counts from the radio transmitter (11 bit unsigned int)
 * @return pulse width in microseconds
 * @brief converts the RC input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author aahunter
 * @modified <Your Name>, <year>.<month>.<day> <hour> <pm/am> */
static uint16_t calc_pw(uint16_t raw_counts) {
    int16_t normalized_pulse; //converted to microseconds and centered at 0
    uint16_t pulse_width; //servo output in microseconds
    normalized_pulse = (raw_counts - RC_RX_MID_COUNTS) >> CTS_2_USEC;
    pulse_width = normalized_pulse + RC_SERVO_CENTER_PULSE;
    return pulse_width;
}

/**
 * @Function calc_pw_from_angle(float angle, float min, float max)
 * @param angle - desired angle (radians)
 * @param min - minimum angle (radians)
 * @param max - maximum angle (radians)
 * @return pulse width in microseconds
 * @brief converts the angle input into the equivalent pulsewidth output for servo
 * and ESC control
 * @author Margaret Silva
 */
static uint16_t calc_pw_from_angle(float angle, float min, float max){
    uint16_t pulse_width; //servo output in microseconds
    
    pulse_width = (uint16_t)(((angle - min)*(RC_SERVO_MAX_PULSE - RC_SERVO_MIN_PULSE))/(max - min));
    pulse_width = pulse_width + RC_SERVO_MIN_PULSE;
    
    return pulse_width;
}

/**
 * @Function set_control_output(void)
 * @param control - controller output struct
 * @return none
 * @brief converts RC input signals to pulsewidth values and sets the actuators
 * (servos and ESCs) to those values
 * @author Aaron Hunter
 */
void set_control_output(struct controller_outputs controls) {

    char message[BUFFER_SIZE];
    uint8_t msg_len = 0;
    int index;
    int hash, ail, ele, thr, rud;
    int hash_check;
    const int tol = 4;
    int INTOL;

    /* Check switch state*/
    /*if switch state == up*/
    /* Manual control enabled */
    /* if switch state == down*/
    /* Autonomous mode enabled*/
    /* get RC commanded values*/

    safe = RC_channels[SAFE]; //1807 when ON, 240 when OFF
    hash = RC_channels[HASH]; //1807 when ON, 240 when OFF
    ail = RC_channels[AIL];
    ele = RC_channels[ELE];
    thr = RC_channels[THR];
    rud = RC_channels[RUD];

    hash_check = (thr >> 2) + (ail >> 2) + (ele >> 2) + (rud >> 2);
    if (abs(hash_check - hash) <= tol) {
        INTOL = TRUE;
        // check if mode switching occurs
        if (safe <= SAFE_LOW) {
            mode = MANUAL;
        } else if (safe >= SAFE_HIGH) {
            mode = AUTONOMOUS;
        }

        /* send commands to motor outputs*/
        if (mode == MANUAL) {
            RC_servo_set_pulse(calc_pw(ail), SERVO_PWM_1);
            RC_servo_set_pulse(calc_pw(ele), SERVO_PWM_2);
            RC_servo_set_pulse(calc_pw(thr), SERVO_PWM_3);
            RC_servo_set_pulse(calc_pw(rud), SERVO_PWM_4);
        } else {
            printf("I am autonomous! \r\n");
#ifdef USE_AUTONOMOUS_CONTROLS  
            RC_servo_set_pulse(calc_pw_from_angle(controls.d_aileron, MIN_AILERON_ANGLE, MAX_AILERON_ANGLE), SERVO_PWM_1);
            RC_servo_set_pulse(calc_pw_from_angle(controls.d_elevator, MIN_ELEVATOR_ANGLE, MAX_ELEVATOR_ANGLE), SERVO_PWM_2);
            RC_servo_set_pulse(calc_pw_from_angle(controls.d_throttle, MIN_THROTTLE, MAX_THROTTLE), SERVO_PWM_3);
            RC_servo_set_pulse(calc_pw_from_angle(controls.d_rudder, MIN_RUDDER_ANGLE, MAX_RUDDER_ANGLE), SERVO_PWM_4);
#endif
        }

    } else {
        INTOL = FALSE;
        msg_len = sprintf(message, "%d, %d, %d, %d, %d, %d, %d \r\n", thr, ail, ele, rud, hash, hash_check, INTOL);
        for (index = 0; index < msg_len; index++) {
            Radio_put_char(message[index]);
        }
    }

}

///**
// * @function publish_RC_signals_raw(void)
// * @param none
// * @brief scales raw RC signals
// * @author Aaron Hunter
// */

void publish_RC_signals_raw(void) {
    mavlink_message_t msg_tx;
    uint16_t msg_length;
    uint8_t msg_buffer[BUFFER_SIZE];
    uint16_t index = 0;
    uint8_t RC_port = 0; //first 8 channels 
    uint8_t rssi = 255; //unknown--may be able to extract from receiver
    mavlink_msg_rc_channels_raw_pack(mavlink_system.sysid,
            mavlink_system.compid,
            &msg_tx,
            Sys_timer_get_msec(),
            RC_port,
            RC_channels[0],
            RC_channels[1],
            RC_channels[2],
            RC_channels[3],
            RC_channels[4],
            RC_channels[5],
            RC_channels[6],
            RC_channels[7],
            rssi);
    msg_length = mavlink_msg_to_send_buffer(msg_buffer, &msg_tx);
    for (index = 0; index < msg_length; index++) {
        Radio_put_char(msg_buffer[index]);
    }
}

/**
 * @function check_RC_events(void)
 * @param none
 * @brief checks for RC messages and stores data in RC channel buffer
 * @author Aaron Hunter
 */
void check_RC_events() {
    if (RCRX_new_cmd_avail()) {
        RCRX_get_cmd(RC_channels);
    }
}

/*******************************************************************************
 * MAIN                                                                        *
 ******************************************************************************/

void main() {
    // system values
    uint32_t cur_time = 0;
    uint32_t warmup_time = 250; //time in ms to allow subsystems to stabilize (IMU))
    uint32_t heartbeat_start_time = 0;
    uint32_t control_start_time = 0;

    static struct controller_outputs act_out = DEFAULT_CONTROLLER_OUTPUTS;

    // initialize board, serial, and radio
    Board_init(); //board configuration
    Serial_init(); //start debug terminal (USB)
    Sys_timer_init(); //start the system timer
    /*small delay to get all the subsystems time to get online*/
    while (cur_time < warmup_time) {
        cur_time = Sys_timer_get_msec();
    }
    Radio_serial_init(); //start the radios

    RCRX_init(); //initialize the radio control system
    RC_channels_init(); //set channels to midpoint of RC system
    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_1); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_2); // start the servo subsystem
    RC_servo_init(ESC_UNIDIRECTIONAL_TYPE, SERVO_PWM_3); // start the servo subsystem
    RC_servo_init(RC_SERVO_TYPE, SERVO_PWM_4); // start the servo subsystem

    printf("\r\n\nDemo application %s, %s \r\n", __DATE__, __TIME__);

//    float cur_angle = 0;
//    uint16_t out = calc_pw_from_angle(cur_angle, MIN_AILERON_ANGLE, MAX_AILERON_ANGLE);
//    printf("0 deg: %d \r\n", out);
//    cur_angle = MIN_AILERON_ANGLE;
//    out = calc_pw_from_angle(cur_angle, MIN_AILERON_ANGLE, MAX_AILERON_ANGLE);
//    printf("-25 deg: %d \r\n", out);
//    cur_angle = MAX_AILERON_ANGLE;
//    out = calc_pw_from_angle(cur_angle, MIN_AILERON_ANGLE, MAX_AILERON_ANGLE);
//    printf("25 deg: %d \r\n", out);
//    while(1);
    
    initController();
    initStateEstimation();

    while (1) {
        cur_time = Sys_timer_get_msec();
        checkRadioEvents();
        check_RC_events();

        // publish heartbeat
        if (cur_time - heartbeat_start_time >= HEARTBEAT_PERIOD) {
            heartbeat_start_time = cur_time; //reset the timer
            publish_heartbeat();
        }

        if (cur_time - control_start_time >= CONTROL_PERIOD) {
            control_start_time = cur_time; //reset control loop timer
            set_control_output(act_out); // set actuator outputs

            /*publish high speed sensors*/
            if (pub_RC_signals == TRUE) {
                publish_RC_signals_raw();
                //printf("Channels: %d" );
            }
        }

        if (new_state_received == TRUE) {
            // only update controller when a new state is received from the sim
            act_out = updateController(getCurrentState(), X_c, h_c, Va_c);
            publish_actuator_cmds(act_out);
            new_state_received = FALSE;
        }
    }
}