'''
    Author: Margaret Silva
    This file contains the functions used to create the linearized perturbation models from the 
    stability derivatives and the non-linear model
'''

import math
from ece163.Modeling import VehicleAerodynamicsModel
from ece163.Constants import VehiclePhysicalConstants as VPC
from ece163.Containers import States
from ece163.Containers import Inputs
from ece163.Containers import Linearized
from ece163.Utilities import MatrixMath

def CreateTransferFunction(trimState, trimInputs):
    ''' Fills in parameters for and returns a transfer function '''
    # calculate Va, alpha, and beta from current state
    trimState.Va = math.hypot(trimState.u, trimState.v, trimState.w)
    trimState.alpha = math.atan2(trimState.w, trimState.u)         # angle of attack
    # prevent divide by zero error
    try:
        trimState.beta = math.asin(trimState.v/trimState.Va)
    except ZeroDivisionError:
        trimState.beta = 0.0

    # params used for roll angle transfer function
    a_phi1 = -0.5*VPC.rho*(trimState.Va**2)*VPC.S*VPC.b*VPC.Cpp*(VPC.b/(2.0*trimState.Va))
    a_phi2 = 0.5*VPC.rho*(trimState.Va**2)*VPC.S*VPC.b*VPC.CpdeltaA

    # params used for sideslip transfer function
    a_beta1 = -1.0*((VPC.rho*trimState.Va*VPC.S)/(2*VPC.mass))*VPC.CYbeta
    a_beta2 = ((VPC.rho*trimState.Va*VPC.S)/(2*VPC.mass))*VPC.CYdeltaR

    # params used for pitch angle transfer function
    a_theta1 = -1.0*((VPC.rho*(trimState.Va**2)*VPC.c*VPC.S)/(2.0*VPC.Jyy))*VPC.CMq*(VPC.c/(2.0*trimState.Va))
    a_theta2 = -1.0*((VPC.rho*(trimState.Va**2)*VPC.c*VPC.S)/(2.0*VPC.Jyy))*VPC.CMalpha
    a_theta3 = ((VPC.rho*(trimState.Va**2)*VPC.c*VPC.S)/(2.0*VPC.Jyy))*(VPC.CMdeltaE)

    # params used for airspeed transfer function
    C_D = VPC.CD0 + VPC.CDalpha*trimState.alpha + VPC.CDdeltaE*trimInputs.Elevator
    dTp_dVa = dThrust_dVa(trimState.Va, trimInputs.Throttle)
    a_V1 = ((VPC.rho*(trimState.Va)*VPC.S)/VPC.mass)*(C_D) - (1.0/VPC.mass)*dTp_dVa
    a_V2 = dThrust_dThrottle(trimState.Va, trimInputs.Throttle)/VPC.mass
    a_V3 = VPC.g0*math.cos(trimState.pitch - trimState.alpha)

    # calculate gamma
    gamma = trimState.pitch - trimState.alpha

    fn = Linearized.transferFunctions(Va_trim = trimState.Va, alpha_trim = trimState.alpha, 
        beta_trim = trimState.beta, gamma_trim = gamma, theta_trim = trimState.pitch, phi_trim = trimState.roll, 
        a_phi1 = a_phi1, a_phi2 = a_phi2, 
        a_beta1 = a_beta1, a_beta2 = a_beta2, 
        a_theta1 = a_theta1, a_theta2 = a_theta2, a_theta3 = a_theta3, 
        a_V1 = a_V1, a_V2 = a_V2, a_V3 = a_V3)

    return fn


def dThrust_dThrottle(Va, Throttle, epsilon=0.01):
    ''' Calculates the numerical partial derivative of propellor thrust to change in throttle setting '''
    VAM = VehicleAerodynamicsModel.VehicleAerodynamicsModel()
    a, c = VAM.CalculatePropForces(Va, Throttle+epsilon)    # use the force from the calculation, not the moment
    b, c = VAM.CalculatePropForces(Va, Throttle)
    dT_dThrottle = (a-b)/epsilon
    return dT_dThrottle


def dThrust_dVa(Va, Throttle, epsilon=0.5):
    ''' Calculates the numerical partial derivative of propellor thrust to change in airspeed '''
    VAM = VehicleAerodynamicsModel.VehicleAerodynamicsModel()
    a, c = VAM.CalculatePropForces(Va+epsilon, Throttle)
    b, c = VAM.CalculatePropForces(Va, Throttle)
    dT_dVa = (a-b)/epsilon
    return dT_dVa
