/* 
 * File:   PD.h
 * Author: Margaret Silva
 * Brief: Interface to PI controller module
 * Uses code from Aaron Hunter's code from the OSAVC PID library to create a 
 * PD controller module
 *
 * Created on February 14, 2023, 2:03 PM
 */

/*******************************************************************************
 * #INCLUDES                                                                   *
 ******************************************************************************/

#include "PD.h" // The header file for this source file. 
#include <stdio.h>

/*******************************************************************************
 * PRIVATE #DEFINES                                                            *
 ******************************************************************************/


/*******************************************************************************
 * PRIVATE TYPEDEFS                                                            *
 ******************************************************************************/

/*******************************************************************************
 * PRIVATE FUNCTIONS PROTOTYPES                                                 *
 ******************************************************************************/

/*******************************************************************************
 * PUBLIC FUNCTION IMPLEMENTATIONS                                             *
 ******************************************************************************/

/**
 * @Function PD_init(*pd);
 * @param *pd, pointer to PD_controller type
 * @brief initializes the PD_controller struct
 * @note computes the c0, c1, c2 constants of the controller and initializes
 * error array
 * @author Aaron Huter,
 * @modified */
void PD_init(PD_controller *pd) {
    int i;
    pd->u = 0;
    pd->u_calc = 0;
    /* initialize error*/
    for (i = 0; i < 3; i++) {
        pd->error[i] = 0;
    }
}

/**
 * @Function PD_update(PD_controller *pd, float reference, float measurement)
 * @param *pd, pointer to PD_controller type
 * @param, reference, the current process setpoint
 * @param measurmeent, the current process measurement
 * @param dot, the derivative of the current process measurement
 * @brief implements a standard parallel PD
 * @note derivative filtering is not implemented
 * @author Aaron Hunter,
 * @modified  */
void PD_update(PD_controller *pd, float reference, float measurement, float dot) {
    pd->error[2] = pd->error[1];
    pd->error[1] = pd->error[0];
    pd->error[0] = reference - measurement;
    /* compute new output */
    pd->u_calc = pd->trim + pd->kp * pd->error[0] - pd->kd * dot;
    /* clamp outputs within actuator limits*/
    if (pd->u_calc > pd->u_max) {
        pd->u = pd->u_max;
    }
    if (pd->u_calc < pd->u_min) {
        pd->u = pd->u_min;
    } else {
        pd->u = pd->u_calc;
    }
}

/*******************************************************************************
 * PRIVATE FUNCTION IMPLEMENTATIONS                                            *
 ******************************************************************************/


#ifdef PD_TESTING
#include "Board.h"
#include "SerialM32.h"

void main(void) {
    PD_controller controller;
    controller.dt = .02;
    controller.kp = 1;
    controller.ki = 0.5;
    controller.kd = 0.2;
    controller.u_max = 2000;
    controller.u_min = -2000;

    float ref;
    float y;



    Board_init();
    Serial_init();
    printf("PD test harness %s, %s\r\n", __DATE__, __TIME__);
    PD_init(&controller);
    printf("Controller initialized to: \r\n");
    printf("dt %f\r\n", controller.dt);
    printf("kp: %f\r\n", controller.kp);
    printf("ki: %f\r\n", controller.ki);
    printf("kd: %f\r\n", controller.kd);
    printf("max output: %f\r\n", controller.u_max);
    printf("min output: %f\r\n", controller.u_min);




}
#endif //PD_TESTING
