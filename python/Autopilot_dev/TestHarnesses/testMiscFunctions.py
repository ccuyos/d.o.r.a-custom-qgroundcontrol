'''
    File: testMiscFunctions.py
    Author: Margaret Silva
    Notes: Contains tests for functions that are also used in the c implementation
'''

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Controls.VehicleEstimator as Estimator
import ece163.Controls.VehicleClosedLoopControl as VCLC
import ece163.Containers.Controls as Controls
import ece163.Utilities.MatrixMath as MM
import ece163.Utilities.Rotations as Rotations
import matplotlib.pyplot as plt


def printSensors(sensor_vec, var_name):
    print(var_name+".x =", sensor_vec[0], ";")
    print(var_name+".y =", sensor_vec[1], ";")
    print(var_name+".z =", sensor_vec[2], ";")



test_controls = VCLC.VehicleClosedLoopControl()
control_gains = Controls.controlGains(3.0561, 0.001, 0.043177, .5, .05, 5.0968, 2.5484, -13.1825, 
				      -0.7931, 0.07743, 0.030975, 1.493, 1.1466, -0.5206, -0.1)
test_controls.setControlGains(controlGains=control_gains)
test_dynamics = test_controls.getVehicleAerodynamicsModel()
# init estimation
test_estimate = Estimator.VehicleEstimator(VPC.dT, test_dynamics)
test_estimate.setFilterGains(Kp_att_a=5, Kp_att_m=15, Kp_alt=.8, Kp_spd=.11, Kp_course=.5, Ki_att_a=.5, Ki_att_m=1, Ki_spd=.11) 

print("Testing Rexp function")
test_mat = test_estimate.R_exp([[1], [2], [3]], 0.01)
MM.matrixPrint(test_mat)
print()

test_mat = test_estimate.R_exp([[.01], [-10], [33.547]], 0.01)
MM.matrixPrint(test_mat)
print()


print("Testing Attitude estimation")
print("gyros:")
printSensors(test_estimate.getGyroValues(), "imu_data.gyro")
print("acc:")
printSensors(test_estimate.getAccelValues(), "imu_data.acc")
print("mag:")
printSensors(test_estimate.getMagValues(), "imu_data.mag")

test_estimate.update()
print("gyros:")
printSensors(test_estimate.getGyroValues(), "imu_data.gyro")
print("acc:")
printSensors(test_estimate.getAccelValues(), "imu_data.acc")
print("mag:")
printSensors(test_estimate.getMagValues(), "imu_data.mag")

test_estimate.update()
print("gyros:")
printSensors(test_estimate.getGyroValues(), "imu_data.gyro")
print("acc:")
printSensors(test_estimate.getAccelValues(), "imu_data.acc")
print("mag:")
printSensors(test_estimate.getMagValues(), "imu_data.mag")

test_estimate.update()
print("gyros:")
printSensors(test_estimate.getGyroValues(), "imu_data.gyro")
print("acc:")
printSensors(test_estimate.getAccelValues(), "imu_data.acc")
print("mag:")
printSensors(test_estimate.getMagValues(), "imu_data.mag")
# biases, rates, R_hat = test_estimate.compFilterAttitude(test_estimate.getAllSensors())
# print("biases:", biases)
# print("rates:", rates)
# print("angles:", Rotations.dcm2Euler(R_hat))
# print("dcm:")
# MM.matrixPrint(R_hat)



