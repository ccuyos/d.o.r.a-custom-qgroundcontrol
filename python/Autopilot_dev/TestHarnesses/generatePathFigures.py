'''
    File: testChapter8.py
    Author: Margaret Silva
    Notes: Contains tests for functions within the vehicle estimation file
'''

#%% Initialization of test harness and helpers:

import math

import sys
sys.path.append("..") #python is horrible, no?

import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Controls.VehicleEstimator as Estimator
import ece163.Controls.VehicleClosedLoopControl as VCLC
import ece163.Containers.Controls as Controls
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits import mplot3d
import numpy as np

# butterfly pattern n = 15000
bf = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[500], [0], [-100]], [[0], [500], [-100]], [[500], [500], [-100]], [[0], [0], [-100]]]

# snake pattern n = 18000
sn = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[500], [500], [-100]], [[500], [1000], [-100]], [[0], [1000], [-100]], [[0], [500], [-100]], [[-500], [500], [-100]], [[-500], [1000], [-100]]]

# square pattern n = 21000
# sq = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[0], [200], [-100]], [[0], [1200], [-100]], [[-1000], [1200], [-100]], [[-1000], [200], [-100]], [[0], [200], [-100]]]

# zig zag n = 10000
zz = [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]], [[0], [200], [-100]], [[400], [0], [-100]], [[800], [200], [-100]], [[1200], [0], [-100]], [[1600], [200], [-100]]]

all_seqs = [bf, sn, zz]
names = ["Butterfly Sequence", "Snake Sequence", "Zig Zag Sequence"]
idx = 0

plt.figure 
for seq in all_seqs:
    idx += 1
    waypoints_n = [seq[i][0][0] for i in range(len(seq))]
    waypoints_e = [seq[i][1][0] for i in range(len(seq))]
    waypoints_d = [seq[i][2][0] for i in range(len(seq))]
    way_col = [i +1 for i in range(len(waypoints_e))]

    norm = mpl.colors.Normalize(vmin=min(way_col), vmax=max(way_col))
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.Blues)
    cmap.set_array([])

    plt.subplot(1, 3, idx)
    plt.scatter(waypoints_e, waypoints_n, c="orange", s = 40, label="waypoints")

    for i in range(0, len(waypoints_e)):
        if i == 0:
            plt.plot(waypoints_e[i:i+2], waypoints_n[i:i+2], 'b--', label="path")
        else:
            plt.plot(waypoints_e[i:i+2], waypoints_n[i:i+2], 'b--')

    i = 0
    for x, y in zip(waypoints_e, waypoints_n):
        i += 1
        if names[idx - 1] == "Square Sequence":
            if (i == 2):
                plt.text(x, y, "2&6", color="black", fontsize=12)
            if (i == 6):
                pass
            else:
                plt.text(x, y, str(i), color="black", fontsize=12)
        elif names[idx - 1] == "Butterfly Sequence":
            if (i == 1):
                plt.text(x, y, "1&5", color="black", fontsize=12)
            if (i == 5):
                pass
            else:
                plt.text(x, y, str(i), color="black", fontsize=12)
        else:
            plt.text(x, y, str(i), color="black", fontsize=12)
    plt.title(names[idx - 1])
    plt.ylabel("north position (m)")
    plt.xlabel("east position (m)")
    plt.legend()
plt.show()