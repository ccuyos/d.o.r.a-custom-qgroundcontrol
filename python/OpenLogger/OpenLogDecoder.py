import pandas as pd
import datetime as dt

start_count = 1
end_count = 1
enum_data = 1
sep = ',' 
placeHolder = 'NaN'
idx_timestamp = 1 #index of timestamp
idx_sa = 2 #index of sensor availability
idx_sensor_start = 3

BARO = 1
GPS = 2
IMU = 4
ALL = 7
imuCount = 11
gpsCount = 4
baroCount = 2

imuFlag = 0
gpsFlag = 0
baroFlag = 0

DEBUG = 0 
DEPLOY = 1

MODE = DEPLOY

def checkSensorAvailable(checkParam, sensorParam):
    return int(checkParam) & sensorParam

def generateEmptySensor(measureCount):
    return sep.join([placeHolder for i in range(measureCount)])
def generateValidSensor(buffer):
    return sep.join(buffer)

def parseRow(row):
    stringOut = ""
    counter = 0
    #Split the components into a list
    sRow = row.split(sep)
    #List slice each relevant component
    timeStamp = str(int("0x{}".format(sRow[idx_timestamp]),0))
    sensorCheck = sRow[idx_sa]
    imuBuffer = ""
    gpsBuffer = ""
    baroBuffer = ""
    baroAvailable = checkSensorAvailable(int(sensorCheck), BARO)
    gpsAvailable = checkSensorAvailable(int(sensorCheck), GPS)
    imuAvailable= checkSensorAvailable(int(sensorCheck), IMU)

    #Generate empty string depending on which one is not available
    if (imuAvailable):
        imuFlag = 1
        counter += imuCount
        pointer = idx_sensor_start
        imuBuffer = sRow[pointer: idx_sensor_start + counter]
    else:
        imuFlag = 0
    if (gpsAvailable):
        gpsFlag = 1
        pointer = idx_sensor_start + counter
        gpsBuffer = sRow[pointer: pointer + gpsCount]
        counter += gpsCount
    else:
        gpsFlag = 0
    if (baroAvailable):
        baroFlag = 1
        pointer =  idx_sensor_start + counter
        baroBuffer = sRow[pointer: pointer + baroCount]
        counter += baroCount
    else:
        baroFlag = 0
        

    # Normalize buffers back to string
    if (imuFlag):
        imuBuffer = generateValidSensor(imuBuffer)
    else:
        imuBuffer = generateEmptySensor(imuCount)
    if (gpsFlag):
        gpsBuffer = generateValidSensor(gpsBuffer)
    else:
        gpsBuffer = generateEmptySensor(gpsCount)
    if (baroFlag):
        baroBuffer = generateValidSensor(baroBuffer)
    else:
        baroBuffer = generateEmptySensor(baroCount)
    

    logBuffer = [timeStamp, sensorCheck, imuBuffer, gpsBuffer, baroBuffer]
    stringOut += timeStamp + sep + imuBuffer + sep + gpsBuffer + sep + baroBuffer
    return stringOut

if __name__ == "__main__":
    compileTime = dt.datetime.now().date().strftime("%Y_%m_%d")
    print("OpenLogDecoder.py compiled on {}".format(dt.datetime.now()))
    flightDate = '5_10_2023'
    flightLogStamp = '_F'
    flightCount = '1'
    flightfiletype = '.TXT' 
    fd  = pd.read_fwf(flightDate + flightLogStamp + flightCount + flightfiletype)
    fd  = fd.reset_index()
    
    #Start a new .txt file for parsed data
    flightCleanStamp = '_C'
    newFile = flightDate +flightCleanStamp + flightCount + flightfiletype
    txtParsed = open(newFile, "w+")
    for index, row in fd.iterrows():
        if (MODE == DEBUG):
            if (start_count < index):
                parseRow(row[enum_data])
            if (index > end_count):
                break
        elif (MODE == DEPLOY):
            txtParsed.write(parseRow(row[enum_data]) + '\r')
    txtParsed.close()
    print("Completed decoding {} on {}".format(newFile,dt.datetime.now()))