"""
Test harness to check that the perturbation models are being correctly computed.
"""

import math
import sys
sys.path.append('..')

import ece163.Controls.VehicleTrim as VehicleTrim
import ece163.Containers.States as States
import ece163.Containers.Inputs as Inputs
import ece163.Containers.Linearized as Linearized
import ece163.Controls.VehiclePerturbationModels as VehiclePerturbationModels
import ece163.Utilities.MatrixMath as MatrixMath

trimControls = Inputs.controlInputs()
trimState = States.vehicleState()
transferFunction = Linearized.transferFunctions()
stateSpace = Linearized.stateSpace()

vTrim = VehicleTrim.VehicleTrim()
Vastar = 25.0
Gammastar = math.radians(2.0)
Kappastar = 0.0

check = vTrim.computeTrim(Vastar, Kappastar, Gammastar)
if check:
	print("Optimization successful")
else:
	print("Model converged outside of valid inputs, change parameters and try again")

print("Controls:")
trimControls = vTrim.getTrimControls()
print(trimControls)

print("State:")
trimState = vTrim.getTrimState()
print(trimState)

transferFunction = VehiclePerturbationModels.CreateTransferFunction(trimState, trimControls)
print(transferFunction)

stateSpace = VehiclePerturbationModels.CreateStateSpace(trimState, trimControls)
print("A_lateral:")
MatrixMath.matrixPrint(stateSpace.A_lateral)
print("B_lateral:")
MatrixMath.matrixPrint(stateSpace.B_lateral)

print("A_longitudinal:")
MatrixMath.matrixPrint(stateSpace.A_longitudinal)
print("B_longitudinal:")
MatrixMath.matrixPrint(stateSpace.B_longitudinal)

