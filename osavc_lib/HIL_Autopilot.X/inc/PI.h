/* 
 * File:   PI.h
 * Author: Margaret Silva
 * Brief: Interface to PI controller module
 * Uses code from Aaron Hunter's code from the OSAVC PID library to create a 
 * PI controller module
 *
 * Created on February 14, 2023, 2:03 PM
 */

#ifndef PI_H
#define	PI_H

/*******************************************************************************
 * PUBLIC #INCLUDES                                                            *
 ******************************************************************************/
// standard libraries
#include <stdint.h>

/*******************************************************************************
 * PUBLIC #DEFINES                                                             *
 ******************************************************************************/

/*******************************************************************************
 * PUBLIC TYPEDEFS                                                             *
 ******************************************************************************/
typedef struct PI_controller {
    float dt; //loop update time (sec)
    float kp; // proportional gain
    float ki; // integral gain
    float u_max; // output upper bound
    float u_min; //output lower bound
    float u_calc; // calculated output
    float trim; // trim input
    float u; // output returned (may be different due to actuator limits)
    float c0; // pre-computed constants
    float c1; 
    float c2; 
    float error[3]; // explicit array of last three error values
} PI_controller;

/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 ******************************************************************************/

/**
 * @Function PI_init(*pi);
 * @param *pi, pointer to PI_controller type
 * @brief initializes the PI_controller struct
 * @note computes the c0, c1, c2 constants of the controller and initializes
 * error array
 * @author Aaron Huter,
 * @modified */
void PI_init(PI_controller *pi);



/**
 * @Function PI_update(PI_controller *pi, float reference, float measurement)
 * @param *pi, pointer to PI_controller type
 * @param, reference, the current process setpoint
 * @param measurmeent, the current process measurement
 * @brief implements a standard parallel PI
 * @note derivative filtering is not implemented
 * @author Aaron Hunter,
 * @modified 2/24/23 */
void PI_update(PI_controller *pi, float reference, float measurement);



/**
 * @Function PI_reset_integrator(PI_controller *pi)
 * @param *pi, pointer to PI_controller type
 * @brief resets the integrator of this pi controller to 0
 * @author Margaret Silva
 * @modified  2/24/23 */
void PI_reset_integrator(PI_controller *pi);


#endif	/* PI_H */

