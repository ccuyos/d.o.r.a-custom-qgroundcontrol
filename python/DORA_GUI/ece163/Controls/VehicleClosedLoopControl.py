"""
File that implements the closed loop control using the successive loop closure structure described in Beard Chapter 6.
In addition to implementation of the control loops, the file includes classes for PD control (where the derivative is
available as a direct input), PI control with anti-windup and a reset, and a PID loop with anti-windup (where the
derivative is again available directly).
"""

import math
import sys

sys.path.append('..')

import ece163.Containers.Inputs as Inputs
import ece163.Containers.Controls as Controls

import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Modeling.VehicleAerodynamicsModel as VehicleAerodynamicsModule

## DORA Addition ##
import ece163.Constants.MAVLINKConstants as mvc


class PDControl():
	def __init__(self, kp=0.0, kd=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
		"""__init__(self, kp=0.0, kd=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0)
		Functions which implement the PD control with saturation where the derivative is available as a separate input
		to the function. The output is: u = u_ref + Kp * error - Kd * \dot{error} limited between lowLimit and highLimit.

		:param kp: proportional gain
		:param kd: derivative gain
		:param trim: trim output (added the the loop computed output)
		:param lowLimit: lower limit to saturate control
		:param highLimit: upper limit to saturate control
		:return: none
		"""
		self.kp = kp
		self.kd = kd
		self.trim = trim
		self.lowLimit = lowLimit
		self.highLimit = highLimit
		return

	def setPDGains(self, kp=0.0, kd=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
		"""setPDGains(self, kp=0.0, kd=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0)
		Function to set the gains for the PD control block (including the trim output and the limits)

		:param kp: proportional gain
		:param kd: derivative gain
		:param trim: trim output (added the the loop computed output)
		:param lowLimit: lower limit to saturate control
		:param highLimit: upper limit to saturate control
		:return: none
		"""
		self.kp = kp
		self.kd = kd
		self.trim = trim
		self.lowLimit = lowLimit
		self.highLimit = highLimit
		return

	def Update(self, command=0.0, current=0.0, derivative=0.0):
		"""Update(self, command=0.0, current=0.0, derivative=0.0)
		Calculates the output of the PD loop given the gains and limits from instantiation, and using the command, actual,
		and derivative inputs. Output is limited to between lowLimit and highLimit from instantiation.

		:param command: reference command
		:param current: actual output (or sensor)
		:param derivative: derivative of the output or sensor
		:return: u [control] limited to saturation bounds
		"""
		error = command - current
		u = self.kp * error - self.kd * derivative + self.trim
		if u > self.highLimit:
			u = self.highLimit
		elif u < self.lowLimit:
			u = self.lowLimit
		return u

	def __repr__(self):
		return "{0.__name__}(kp={1.kp}, kd={1.kd}, trim={1.trim}, lowLimit={1.lowLimit}, highLimit={1.highLimit}".format(type(self), self)



class PIControl():
	def __init__(self, dT=VPC.dT, kp=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
		"""__init__(self, dT=VPC.dT, kp=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0)
		Functions which implement the PI control with saturation where the integrator has both a reset and an anti-windup
		such that when output saturates, the integration is undone and the output forced the output to the limit.
		The output is: u = u_ref + Kp * error + Ki * \integral{error} limited between lowLimit and highLimit.

		:param dT: time step [s], required for integration
		:param kp: proportional gain
		:param ki: integral gain
		:param trim: trim input
		:param lowLimit: low saturation limit
		:param highLimit: high saturation limit
		:return: none
		"""
		self.accumulator = 0.0	# holds the integration state
		self.prevError = 0.0	# holds the previous error for trapezoidal integration
		self.dT = dT
		self.kp = kp
		self.ki = ki
		self.trim = trim
		self.lowLimit = lowLimit
		self.highLimit = highLimit
		return

	def setPIGains(self, dT=VPC.dT, kp=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
		"""setPIGains(self, dT=VPC.dT, kp=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0)
		Function to set the gains for the PI control block (including the trim output and the limits)

		:param dT: time step [s], required for integration
		:param kp: proportional gain
		:param ki: integral gain
		:param trim: trim input
		:param lowLimit: low saturation limit
		:param highLimit: high saturation limit
		:return: none
		"""
		self.accumulator = 0.0	# holds the integration state
		self.prevError = 0.0	# holds the previous error for trapezoidal integration
		self.dT = dT
		self.kp = kp
		self.ki = ki
		self.trim = trim
		self.lowLimit = lowLimit
		self.highLimit = highLimit
		return

	def Update(self, command=0.0, current=0.0):
		"""Update(self, command=0.0, current=0.0)
		Calculates the output of the PI loop given the gains and limits from instantiation, and using the command and current
		or actual inputs. Output is limited to between lowLimit and highLimit from instantiation. Integration for the integral
		state is done using trapezoidal integration, and anti-windup is implemented such that if the output is out of limits,
		the integral state is not updated (no additional error accumulation).

		:param command: reference command
		:param current: current output or sensor
		:return: u [output] limited to saturation bounds
		"""
		error = command - current
		self.accumulator += 0.5 * self.dT * (error + self.prevError)	# trapezoidal integration
		u = self.kp * error + self.ki * self.accumulator + self.trim
		if u > self.highLimit:
			self.accumulator -= 0.5 * self.dT * (error + self.prevError)	# remove trapezoidal integration
			u = self.highLimit
		elif u < self.lowLimit:
			self.accumulator -= 0.5 * self.dT * (error + self.prevError)
			u = self.lowLimit
		self.prevError = error
		return u

	def resetIntegrator(self):
		"""
		Function to reset the integration state to zero, used when switching modes or otherwise resetting the integral state.

		:return: none
		"""
		self.accumulator = 0.0
		self.prevError = 0.0
		return

	def __repr__(self):
		return "{0.__name__}(accumulator={1.accumulator}, kp={1.kp}, ki={1.ki}, trim={1.trim}, lowLimit={1.lowLimit}, " \
			   "highLimit={1.highLimit}".format(type(self), self)


class PIDControl():
	def __init__(self, dT=VPC.dT, kp=0.0, kd=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
		"""__init__(self, dT=VPC.dT, kp=0.0, kd=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0)
		Functions which implement the PID control with saturation where the integrator has both a reset and an anti-windup
		such that when output saturates, the integration is undone and the output forced the output to the limit. Function
		assumes that physical derivative is available (e.g.: roll and p), not a numerically derived one. The output is:
		u = u_ref + Kp * error - Kd * \dot{error} + Ki * \integral{error} limited between lowLimit and highLimit.

		:param dT: time step [s], required for integration
		:param kp: proportional gain
		:param kd: derivative gain
		:param ki: integral gain
		:param trim: trim input
		:param lowLimit: low saturation limit
		:param highLimit: high saturation limit
		:return: none
		"""
		self.accumulator = 0.0	# holds the integration state
		self.prevError = 0.0	# holds the previous error for trapezoidal integration
		self.dT = dT
		self.kp = kp
		self.kd = kd
		self.ki = ki
		self.trim = trim
		self.lowLimit = lowLimit
		self.highLimit = highLimit
		return

	def setPIDGains(self, dT=VPC.dT, kp=0.0, kd=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
		"""setPIDGains(self, dT=VPC.dT, kp=0.0, kd=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0)
		Function to set the gains for the PID control block (including the trim output and the limits)

		:param dT: time step [s], required for integration
		:param kp: proportional gain
		:param kd: derivative gain
		:param ki: integral gain
		:param trim: trim input
		:param lowLimit: low saturation limit
		:param highLimit: high saturation limit
		:return: none
		"""
		self.accumulator = 0.0	# holds the integration state
		self.prevError = 0.0	# holds the previous error for trapezoidal integration
		self.dT = dT
		self.kp = kp
		self.kd = kd
		self.ki = ki
		self.trim = trim
		self.lowLimit = lowLimit
		self.highLimit = highLimit
		return

	def Update(self, command=0.0, current=0.0, derivative=0.0):
		"""Update(self, command=0.0, current=0.0, derivative=0.0)
		Calculates the output of the PID loop given the gains and limits from instantiation, and using the command and current
		or actual inputs. Output is limited to between lowLimit and highLimit from instantiation. Integration for the integral
		state is done using trapezoidal integration, and anti-windup is implemented such that if the output is out of limits,
		the integral state is not updated (no additional error accumulation).

		:param command: reference command
		:param current: current output or sensor
		:param derivative: derivative of the output or sensor
		:return: u [output] limited to saturation bounds
		"""
		error = command - current
		self.accumulator += 0.5 * self.dT * (error + self.prevError)	# trapezoidal integration
		u = self.kp * error - self.kd * derivative + self.ki * self.accumulator + self.trim
		if u > self.highLimit:
			self.accumulator -= 0.5 * self.dT * (error + self.prevError)	# remove trapezoidal integration
			u = self.highLimit
		elif u < self.lowLimit:
			self.accumulator -= 0.5 * self.dT * (error + self.prevError)
			u = self.lowLimit
		self.prevError = error
		return u

	def resetIntegrator(self):
		"""
		Function to reset the integration state to zero, used when switching modes or otherwise resetting the integral state.

		:return: none
		"""
		self.accumulator = 0.0
		self.prevError = 0.0
		return

	def __repr__(self):
		return "{0.__name__}(accumulator={1.accumulator}, kp={1.kp}, kd={1.kd}, ki={1.ki}, trim={1.trim}, lowLimit={1.lowLimit}, " \
			   "highLimit={1.highLimit}".format(type(self), self)



class VehicleClosedLoopControl():
	def __init__(self, dT=VPC.dT, rudderControlSource="SIDESLIP"):
		"""
		Class that implements the entire closed loop control using the successive loop closure method of Beard Chapter 6.
		The class contains everything from control loops (PD or PI loops) along with the gains, which when given the
		reference commands will compute the control surfaces, which will then (along with the state) dictate the forces
		and moments, which will them compute the next state. Contains the required state for the altitude hold state machine
		from the enumeration class in Controls.AltitudeStates

		:param dT: time step [s], defaults to VehiclePhysicalParameters.dT
		:param rudderControlSource:  Either "SIDESLIP" or "YAW".  Determines whether the rudder is controlled by sideslip angle (as in Beard) or by yaw angle.
		:return: none
		"""
		self.rudderControlSource = rudderControlSource
		
		self.controlGains = Controls.controlGains()
		self.trimInputs = Inputs.controlInputs()
		self.controlSurfaceOutputs = Inputs.controlInputs()
		self.vehicleAerodynamics = VehicleAerodynamicsModule.VehicleAerodynamicsModel()
		self.vehicleAerodynamics.vehicleDynamics.dT = dT
		self.dT = self.vehicleAerodynamics.vehicleDynamics.dT
		
		self.aileronFromRoll = PIDControl()
		self.rollFromCourse = PIControl()
		self.elevatorFromPitch = PDControl()
		self.throttleFromAirspeed = PIControl()
		self.pitchFromAltitude = PIControl()
		self.pitchFromAirspeed = PIControl()	# For when outside of the altitude hold bounds
		if self.rudderControlSource == "SIDESLIP":
			self.rudderFromSideslip = PIControl()
		elif self.rudderControlSource == "YAW":
			self.rudderFromYawRate = PIControl()
			
		self.mode = Controls.AltitudeStates.HOLDING

		## DORA Addition ##
		self.autopilotThrottle = 0
		self.autopilotElevator = 0
		self.autopilotAileron = 0
		self.autopilotRudder = 0
		return

	def setControlGains(self, controlGains=Controls.controlGains()):
		"""setControlGains(self, controlGains=Controls.controlGains())
		Function to set all of the gains from the controlGains previously computed to the correct places within the various
		control loops to do the successive loop closure (see Beard Chapter 6). Control loop limits are taken from the
		VehiclePhysicalConstants file, trim inputs are taken from self.trimInputs.

		:param controlGains: controlGains class, has the PID loop gains for each loop
		:return: none, values are updated in self.
		"""
		self.controlGains = controlGains
		# lateral autopilot gains
		self.aileronFromRoll.setPIDGains(self.dT,
				self.controlGains.kp_roll,
				self.controlGains.kd_roll,
				self.controlGains.ki_roll,
				self.trimInputs.Aileron,
				VPC.minControls.Aileron,
				VPC.maxControls.Aileron)
		self.rollFromCourse.setPIGains(self.dT,
				self.controlGains.kp_course,
				self.controlGains.ki_course,
				0.0,
				-math.radians(VPC.bankAngleLimit),
				math.radians(VPC.bankAngleLimit))
		
		if self.rudderControlSource == "SIDESLIP":
			self.rudderFromSideslip.setPIGains(self.dT,
				self.controlGains.kp_sideslip,
				self.controlGains.ki_sideslip,
				self.trimInputs.Rudder,
				VPC.minControls.Rudder,
				VPC.maxControls.Rudder)
		elif self.rudderControlSource == "YAW":
			self.rudderFromYawRate.setPIGains(self.dT,
					-self.controlGains.kp_sideslip,
					-self.controlGains.ki_sideslip,
					self.trimInputs.Rudder,
					VPC.minControls.Rudder,
					VPC.maxControls.Rudder)
		# longitudinal autopilot gains
		self.elevatorFromPitch.setPDGains(
				self.controlGains.kp_pitch,
				self.controlGains.kd_pitch,
				self.trimInputs.Elevator,
				VPC.minControls.Elevator,
				VPC.maxControls.Elevator)
		self.pitchFromAltitude.setPIGains(self.dT,
				self.controlGains.kp_altitude,
				self.controlGains.ki_altitude,
				0.0,
				-math.radians(VPC.pitchAngleLimit),
				math.radians(VPC.pitchAngleLimit))
		self.throttleFromAirspeed.setPIGains(self.dT,
				self.controlGains.kp_SpeedfromThrottle,
				self.controlGains.ki_SpeedfromThrottle,
				self.trimInputs.Throttle,
				VPC.minControls.Throttle,
				VPC.maxControls.Throttle)
		self.pitchFromAirspeed.setPIGains(self.dT,
				self.controlGains.kp_SpeedfromElevator,
				self.controlGains.ki_SpeedfromElevator,
				0.0,
				-math.radians(VPC.pitchAngleLimit),
				math.radians(VPC.pitchAngleLimit))
		return

	def getControlGains(self):
		"""
		Wrapper function to extract control gains from the class.

		:return: controlGains,
				class from Controls.controlGains
		"""
		return self.controlGains

	def getVehicleState(self):
		"""
		Wrapper function to extract vehicle state from the class.

		:return: vehicleState, class from States.vehicleState
		"""
		return self.vehicleAerodynamics.vehicleDynamics.state

	def setTrimInputs(self, trimInputs=Inputs.controlInputs()):
		"""
		Wrapper function to inject the trim inputs into the class.

		:param trimInputs: from Inputs.controlInputs (Throttle, Elevator, Aileron, Rudder)
		:return: none
		"""
		self.trimInputs = trimInputs
		return

	def getTrimInputs(self):
		"""
		Wrapper function to get the trim inputs from the class.

		:return: trimInputs, class from Inputs.controlInputs (Throttle, Elevator, Aileron, Rudder)
		"""
		return self.trimInputs

	def setVehicleState(self, state):
		"""
		Wrapper function to inject vehicle state into the class.

		:param state: from States.vehicleState class
		:return: none
		"""
		self.vehicleAerodynamics.vehicleDynamics.state = state
		return

	def getVehicleControlSurfaces(self):
		"""
		Wrapper function to extract control outputs (Throttle, Aileron, Elevator, Rudder) from the class.

		:return: controlInputs, class from Inputs.controlInputs
		"""
		return self.controlSurfaceOutputs

	def getVehicleAerodynamicsModel(self):
		"""
		Wrapper function to extract the internal VehicleAerodynamicsModel in order to access the various function that are
		associated with the Aero model (such as setting and getting the wind state and model)

		:return: VehicleAerodynamicsModel, class from VehicleAerodynamicsModel
		"""
		return self.vehicleAerodynamics

	# def getReferenceCommands(self):
	# 	"""
	# 	Wrapper function to extract reference commands (commandedCourse, commandedAltitude, commandedAirspeed, commandedRoll,
	# 	commandedPitch) from the class.
	#
	# 	:return: referenceCommands class
	# 	"""
	# 	return self.referenceCommands

	def reset(self):
		"""
		Resets the module to run again. Does not overwrite control gains, but does reset the integral states of all of the
		PI control loops.

		:return: none
		"""
		self.vehicleAerodynamics.reset()
		self.aileronFromRoll.resetIntegrator()
		self.rollFromCourse.resetIntegrator()		
		self.throttleFromAirspeed.resetIntegrator()
		self.pitchFromAltitude.resetIntegrator()
		self.pitchFromAirspeed.resetIntegrator()
		if self.rudderControlSource == "SIDESLIP":
			self.rudderFromSideslip.resetIntegrator()
		elif self.rudderControlSource == "YAW":
			self.rudderFromYawRate.resetIntegrator()
		return

	def UpdateControlCommands(self, referenceCommands, state):
		"""UpdateControlCommands(self, referenceCommands, state)
		Function that implements the full closed loop controls using the commanded inputs of airspeed, altitude, and
		course (chi). Calls all of the submodules below it to implement the full flight dynamics under closed loop control.
		Note that the internal commandedPitch and commandedRoll of the reference commands are altered by this function, and
		this is used to track the reference commands by the simulator.

		You will need to add or subtract 360 degrees (2*pi) from you internal course (chi) when the error is outside of +/- 180
		degrees (pi radians). There is never a course error of more than 180 degrees, so if you do see such an error, it is
		because signed angles and reciprocal headings (e.g.: -180 and +180 are pointed in the same direction).
		:param referenceCommands: high level autopilot commands (note: altered by function)
		:param state: vehicle state (either actual or estimated)
		:return: controlSurfaceOutputs: vehicle control inputs from class Inputs->controlInputs
		"""
		# lateral autopilot (course and sideslip command ailerons and rudder)
		controlSurfaceOutputs = Inputs.controlInputs()
		
		# course->roll->aileron command:
		courseError = referenceCommands.commandedCourse - state.chi
		if courseError >= math.pi:
			state.chi += 2.0 * math.pi
		if courseError <= -math.pi:
			state.chi -= 2.0 * math.pi
		rollCommand = self.rollFromCourse.Update(referenceCommands.commandedCourse, state.chi)
		aileronCommand = self.aileronFromRoll.Update(rollCommand, state.roll, state.p)
		
		#rudder command:
		if self.rudderControlSource == "SIDESLIP":
			rudderCommand = self.rudderFromSideslip.Update(0.0, state.beta)
		elif self.rudderControlSource == "YAW":
			if math.isclose(state.Va, 0.0):
				rCommand = math.tan(rollCommand) * VPC.g0
			else:
				rCommand = math.sin(rollCommand) * math.cos(state.pitch) * VPC.g0 / state.Va
			rudderCommand = self.rudderFromYawRate.Update(rCommand, state.r)
		
		# longitudinal autopilot:
		#handle state machine and get throttleCommand and pitchCommand:
		altitude = -state.pd
		if altitude > (referenceCommands.commandedAltitude + VPC.altitudeHoldZone):
			# print("DESCENDING")
			# Too high, descending mode
			# if not in DESCENDING state, reset integrator
			if self.mode is not Controls.AltitudeStates.DESCENDING:
				self.mode = Controls.AltitudeStates.DESCENDING
				self.pitchFromAirspeed.resetIntegrator()
			throttleCommand = VPC.minControls.Throttle
			pitchCommand = self.pitchFromAirspeed.Update(referenceCommands.commandedAirspeed, state.Va)
		elif altitude  < (referenceCommands.commandedAltitude - VPC.altitudeHoldZone):
			# print("ASCENDING")
			# Too low, climbing mode
			# if not in CLIMBING state, reset integrator
			if self.mode is not Controls.AltitudeStates.CLIMBING:
				self.mode = Controls.AltitudeStates.CLIMBING
				self.pitchFromAirspeed.resetIntegrator()
			throttleCommand = VPC.maxControls.Throttle
			pitchCommand = self.pitchFromAirspeed.Update(referenceCommands.commandedAirspeed, state.Va)
		else:
			# print("HOLDING")
			if self.mode is not Controls.AltitudeStates.HOLDING:
				self.mode = Controls.AltitudeStates.HOLDING
				# print("RESET PFA")
				self.pitchFromAltitude.resetIntegrator()
				
			throttleCommand = self.throttleFromAirspeed.Update(referenceCommands.commandedAirspeed, state.Va)
			# print("Va=",state.Va)
			pitchCommand = self.pitchFromAltitude.Update(referenceCommands.commandedAltitude, altitude)
			# print("pitchCommand=",pitchCommand)
		#pitchcommand to elevator command:
		elevatorCommand = self.elevatorFromPitch.Update(pitchCommand, state.pitch, state.q)

		#assemble outputs:

		if mvc.HIL_mode == mvc.simMode:
			controlSurfaceOutputs.Throttle = throttleCommand
			controlSurfaceOutputs.Elevator = elevatorCommand
			controlSurfaceOutputs.Aileron = aileronCommand
			controlSurfaceOutputs.Rudder = rudderCommand
		elif mvc.HIL_mode == mvc.autopilotMode:
			controlSurfaceOutputs.Throttle = self.autopilotThrottle
			controlSurfaceOutputs.Elevator = self.autopilotElevator
			controlSurfaceOutputs.Aileron = self.autopilotAileron
			controlSurfaceOutputs.Rudder = self.autopilotRudder

		
		#internal loop set points
		referenceCommands.commandedPitch = pitchCommand
		referenceCommands.commandedRoll = rollCommand
		return controlSurfaceOutputs

	def Update(self, referenceCommands=Controls.referenceCommands()):
		"""Update(self, referenceCommands=Controls.referenceCommands)
		Function that wraps the UpdateControlCommands and feeds it the correct state (estimated or otherwise). Updates the
		vehicle state internally using the vehicleAerodynamics.Update command.
		"""
		self.controlSurfaceOutputs = self.UpdateControlCommands(referenceCommands, self.vehicleAerodynamics.vehicleDynamics.state)
		self.vehicleAerodynamics.Update(self.controlSurfaceOutputs)
		return

	## DORA Update ##
	def UpdateAutopilotCommands(self, thr, ele, ail, rud):
		self.autopilotThrottle = thr
		self.autopilotElevator = ele
		self.autopilotAileron = ail
		self.autopilotRudder = rud