clc
clear all
close all


actualYaw = 1;
actualPitch = 2;
actualRoll = 3;
estYaw = 4;
estPitch = 5;
estRoll = 6;

data = importdata('Pitch_t2.txt');
dT = 0.1;
[a,b] = size(data);
time = (1:a)*dT;


figure
hold on
plot(time, data(:,actualPitch)+5)
plot(time, data(:, estPitch))
xlabel('time (s)')
ylabel('angle (deg)')
title('Actual vs Estimate for Pitch Angle using Gimbal')
legend('Actual', 'Estimate')

data2 = importdata('Roll_t1.txt');
dT = 0.1;
[a,b] = size(data2);
time = (1:a)*dT;

figure
hold on
plot(time, data2(:, actualRoll))
plot(time, data2(:, estRoll))
xlabel('time (s)')
ylabel('angle (deg)')
title('Actual vs Estimate for Roll Angle using Gimbal')
legend('Actual', 'Estimate')

data3 = importdata('Yaw_t1.txt');
dT = 0.1;
[a,b] = size(data3);
time = (1:a)*dT;

figure
hold on 
plot(time, data3(:, actualYaw))
plot(time, data3(:, estYaw))
xlabel('time (s)')
ylabel('angle (deg)')
title('Actual vs Estimate for Yaw Angle using Gimbal')
legend('Actual', 'Estimate')


