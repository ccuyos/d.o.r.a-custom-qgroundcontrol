'''
    Author: Margaret Silva
    This file contains the classes and function needed to create a closed loop vehicle controller
'''

import math
import sys
import ece163.Containers.Inputs as Inputs
import ece163.Containers.Controls as Controls
import ece163.Constants.VehiclePhysicalConstants as VPC
import ece163.Modeling.VehicleAerodynamicsModel as VehicleAerodynamicsModule
import ece163.Sensors.SensorsModel as SensorsModel
import ece163.Controls.VehicleEstimator as Estimation
import ece163.Utilities.MatrixMath as MM
import datetime


class VehicleClosedLoopControl():

    def __init__(self, dT=VPC.dT, rudderControlSource='YAW DAMPING', stateSource='ESTIMATED', airspeedCtrl='Off'):
        ''' Initializes vehicleCloseLoopControl instance '''
        # aerodynamics model
        self.VAM = VehicleAerodynamicsModule.VehicleAerodynamicsModel()
        # state estimator
        self.estimator = Estimation.VehicleEstimator(dT=dT, aeroModel=self.VAM)
        self.estimator.setFilterGains()
        self.stateSource = stateSource
        # toggle control of airspeed, use trim values when not controlled
        self.airspeedCtrl = airspeedCtrl
        # control gains
        self.controlGains = Controls.controlGains()
        # trim controls, output controls
        self.trimControls = Inputs.controlInputs()
        self.outputControls = Inputs.controlInputs()
        # modes, timestep
        self.dT = dT
        self.rudderConrolSource = rudderControlSource
        self.mode = Controls.AltitudeStates.HOLDING
        # PI, PD, and PID controllers
        self.rollFromCourse = PIControl()
        self.rudderFromSideslip = PIControl()
        self.rudderFromYawDamping = PIControl()
        self.throttleFromAirspeed = PIControl()
        self.pitchFromAltitude = PIControl()
        self.pitchFromAirspeed = PIControl()
        self.elevatorFromPitch = PDControl()
        self.aileronFromRoll = PIDControl()
        # expermental rudder controller
        self.rud_xi = 0
        self.k_rudd = 0.5
        self.p_wo = 0.05
        # waypoint array and index storage
        self.Waypoints = []
        self.waypoint_idx = 0
        # debug
        self.debug = 0
        self.file_name = "rc_data_{}.txt".format(datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"))

    def Update(self, referenceCommands=Controls.referenceCommands()):
        ''' 
            Updates the vehicle state internally using the vehicleAerodynamics.Update command and the 
            UpdateControlCommands function  
        '''
        if self.stateSource == 'TRUE':
            self.outputControls = self.UpdateControlCommands(referenceCommands, self.getVehicleState())
        else:
            self.estimator.update()
            self.outputControls = self.UpdateControlCommands(referenceCommands, self.estimator.getStateEstimate())
        self.VAM.Update(self.outputControls)

    def UpdateWithWaypoints(self):
        '''
            Computes height and course command needed to follow waypoint series and passes them to the update 
            function
            returns commanded course and height for debugging purposes
        '''
        # debug values
        waypoint_swap = 0

        # choose tunable params
        X_inf = math.pi / 2
        dist = MM.subtract(self.Waypoints[self.waypoint_idx + 1], self.Waypoints[self.waypoint_idx - 1])
        dist = 0.5*math.hypot(dist[0][0], dist[1][0], dist[2][0])
        try:
            Kpath = 1/dist
        except:
            Kpath = 0.01

        # choose which waypoint to follow

        # find position
        if self.stateSource == 'TRUE':
            pos = [[self.getVehicleState().pn], [self.getVehicleState().pe], [self.getVehicleState().pd]]
            X = self.getVehicleState().chi
        else:
            pos = [[self.estimator.getStateEstimate().pn], [self.estimator.getStateEstimate().pe], [self.estimator.getStateEstimate().pd]]
            X = self.estimator.getStateEstimate().chi

        # X = self.getVehicleState().chi

        while X > 2*math.pi:
            X -= 2*math.pi
        while X < -2*math.pi:
            X += 2*math.pi

        r = self.Waypoints[self.waypoint_idx - 1]
        # check if you've passed the most recent waypoint
        q_minus = MM.vectorNorm(MM.subtract(self.Waypoints[self.waypoint_idx], self.Waypoints[self.waypoint_idx - 1]))
        q_i = MM.vectorNorm(MM.subtract(self.Waypoints[self.waypoint_idx + 1], self.Waypoints[self.waypoint_idx]))
        n = MM.vectorNorm(MM.add(q_minus, q_i))
        # this determines if you've passed the waypoint
        decide = MM.multiply(MM.transpose(MM.subtract(pos, self.Waypoints[self.waypoint_idx])), n)
        if decide[0][0] >= 0:
            # increment waypoint index if you have
            if self.waypoint_idx < len(self.Waypoints) - 2:
                self.waypoint_idx += 1
            # self.waypoint_idx = (self.waypoint_idx + 1) % (len(self.Waypoints))
            # if self.waypoint_idx >= (len(self.Waypoints) - 1):
            #     self.Waypoints[-1] = pos
            #     self.Waypoints = [self.Waypoints[-i - 1] for i in range((len(self.Waypoints)))]
            #     print()
            #     for w in self.Waypoints:
            #         print("[{:.2f}, {:.2f}, {:.2f}]".format(w[0][0], w[1][0], w[2][0]), end="| ")
            #     print("\n")
            #     self.waypoint_idx = 1
            #     # self.waypoint_idx-=1
                print("idx", self.waypoint_idx, end="| ")
                print("position [{:.2f}, {:.2f}, {:.2f}]".format(pos[0][0], pos[1][0], pos[2][0]), end="| ")
                w = self.Waypoints[self.waypoint_idx - 1]
                print("last wpt [{:.2f}, {:.2f}, {:.2f}]".format(w[0][0], w[1][0], w[2][0]), end="| ")
                print("next wpt [{:.2f}, {:.2f}, {:.2f}]".format(self.Waypoints[self.waypoint_idx][0][0], self.Waypoints[self.waypoint_idx][1][0], self.Waypoints[self.waypoint_idx][2][0]))
                waypoint_swap = 1
            # print(decide)
        q = q_minus

        # calculate commanded course and altitude to reach said waypoint

        error = MM.subtract(pos, r)
        k = [[0], [0], [1]]
        n = MM.vectorNorm(MM.crossProduct(q, k)) # vector normal to the plane created by the path vector and the downwards vector k
        
        # s = e_p - (e_p . n) n
        ep_dot_n = MM.dotProduct(error, n)[0][0]
        ep_dot_n_times_n = MM.scalarMultiply(ep_dot_n, n)
        s = MM.subtract(error, ep_dot_n_times_n) 

        # compute commanded altitude
        try:
            hc = -r[2][0] - math.sqrt(s[0][0]**2 + s[1][0]**2) * ((q[2][0])/ (math.sqrt(q[0][0]**2 + q[1][0]**2)))
        except:
            hc = -r[2][0]

        # compute commanded course
        Xq = math.atan2(q[1][0], q[0][0])
        # X_true = self.getVehicleState().chi
        # if (abs((Xq - X) - (Xq - X_true)) > math.pi):
        #     print("true:", X_true, "est:", X, "Xq:", Xq)
        while (Xq - X) < -math.pi:
            Xq += 2*math.pi
        while (Xq - X) > math.pi:
            Xq -= 2*math.pi

        epy = -math.sin(Xq) * (pos[0][0] - r[0][0]) + math.cos(Xq) * (pos[1][0] - r[1][0])
        Xc = Xq - X_inf * (2/math.pi) * math.atan(Kpath * epy)

        # pass commands to update
        commands = Controls.referenceCommands(Xc, hc, 15)
        self.Update(commands)

        return math.degrees(Xc), -hc, waypoint_swap


    def UpdateControlCommands(self, referenceCommands, state):
        ''' Implements the full closed loop controls using the commanded inputs of airspeed, altitude, and course '''
        # make sure commanded course is in the range [-pi, pi]
        if (referenceCommands.commandedCourse - state.chi) >= math.pi:
            state.chi += 2*math.pi
        elif (referenceCommands.commandedCourse - state.chi) <= -math.pi:
            state.chi -= 2*math.pi

        # lateral controller
        rollCmd = self.rollFromCourse.Update(referenceCommands.commandedCourse, state.chi)
        referenceCommands.commandedRoll = rollCmd
        if self.rudderConrolSource == 'SIDESLIP':
            rudderCmd = self.rudderFromSideslip.Update(0.0, state.beta)
        else:
            # if math.isclose(math.cos(state.pitch), 0, abs_tol=1e-12):
            #     rc = VPC.g0 * math.tan(rollCmd)
            # else:
            #     rc = (1/math.cos(state.pitch))*(state.q*math.sin(state.roll) + state.r*math.cos(state.roll))
            # rudderCmd = self.rudderFromYawDamping.Update(rc, state.r)

            # if math.isclose(state.Va, 0, abs_tol=1e-12):
            #     rc = VPC.g0 * math.tan(rollCmd)
            # else:
            #     rc = math.sin(rollCmd)*math.cos(state.pitch) * VPC.g0/ state.Va
            #     # rc = (VPC.g0 * math.tan(rollCmd))/referenceCommands.commandedAirspeed
            # rudderCmd = self.rudderFromYawDamping.Update(rc, state.r)

            temp = self.dT*(-self.p_wo*self.rud_xi + self.k_rudd * state.r)
            self.rud_xi += self.dT*(-self.p_wo*self.rud_xi + self.k_rudd * state.r)
            rudderCmd = -self.p_wo*self.rud_xi + self.k_rudd*state.r
            if (rudderCmd > VPC.maxControls.Rudder):
                rudderCmd = VPC.maxControls.Rudder
                self.rud_xi -= temp
            elif (rudderCmd < VPC.minControls.Rudder):
                rudderCmd = VPC.minControls.Rudder
                self.rud_xi -= temp


        aileronCmd = self.aileronFromRoll.Update(rollCmd, state.roll, state.p)
        # f = open(self.file_name, 'a')
        # f.write("/*"+str(self.debug)+"*/ rollCmd = {:.20f}; pitch= {:.20f}; Va = {:.20f}; r = {:.20f};".format(rollCmd, state.pitch, state.Va, state.r))
        # f.write("X_c={:.20f}; chi={:.20f};".format(referenceCommands.commandedCourse, state.chi))
        # f.write("cur_roll={:.20f}; p={:.20f};".format(state.roll, state.p))
        # # f.write('''printf("in - rollCmd: %f, pitch: %f, Va: %f, r: %f\\n", rollCmd, pitch, Va, r); \n rc = rcController(rollCmd, pitch, Va, r, &rudder_yaw_damping); \nprintf("out - rc: %f, throttle: %f\\n", rc, rudder_yaw_damping.u); ''')
        # f.write('''PI_update(&course, X_c, chi); \n''')
        # f.write("PID_update(&roll, course.u, cur_roll, p);")
        # f.write('''rc = rcController(course.u, pitch, Va, r, &rudder_yaw_damping); \n''')
        
        # f.write('''printf("{} out - rollCmd: %f, aileron: %f, rudder: %f\\n", course.u, roll.u, rudder_yaw_damping.u);'''.format(self.debug))
        # f.write('''printf("expected - rollCmd {:.7f}, aileron: {:.7f}, rudder: {:.7f}\\n");'''.format(rollCmd, aileronCmd, rudderCmd))
        # f.write("delay(500000);")
        # f.write("\n")
        # print("/*"+str(self.debug)+"*/ rollCmd = {:.20f}; pitch= {:.20f}; Va = {:.20f}; r = {:.20f};".format(rollCmd, state.pitch, state.Va, state.r))
        # print('''printf("in - rollCmd: %f, pitch: %f, Va: %f, r: %f\\n", rollCmd, pitch, Va, r); \n rc = rcController(rollCmd, pitch, Va, r, &rudder_yaw_damping); \nprintf("out - rc: %f, throttle: %f\\n", rc, rudder_yaw_damping.u); ''')
        # print('''printf("expected {:.20f}\\n");'''.format(rudderCmd))
        # print()
        # self.debug += 1

        # longitudinal controller
        upper_thresh = referenceCommands.commandedAltitude + VPC.altitudeHoldZone
        lower_thresh = referenceCommands.commandedAltitude - VPC.altitudeHoldZone
        # determine if mode should be changed
        if (self.mode == Controls.AltitudeStates.DESCENDING):
            if (-state.pd > lower_thresh) and (-state.pd < upper_thresh):
                self.mode = Controls.AltitudeStates.HOLDING
                self.pitchFromAltitude.resetIntegrator()
        elif (self.mode == Controls.AltitudeStates.HOLDING):
            if (-state.pd > upper_thresh):
                self.pitchFromAirspeed.resetIntegrator()
                self.mode = Controls.AltitudeStates.DESCENDING
            if (-state.pd < lower_thresh):
                self.pitchFromAirspeed.resetIntegrator()
                self.mode = Controls.AltitudeStates.CLIMBING
        elif (self.mode == Controls.AltitudeStates.CLIMBING):
            if (-state.pd > lower_thresh) and (-state.pd < upper_thresh):
                self.pitchFromAltitude.resetIntegrator()
                self.mode = Controls.AltitudeStates.HOLDING
        else:
            # the value of self.mode is not recognized
            raise ValueError
        
        # use controllers according to state
        if (self.mode == Controls.AltitudeStates.DESCENDING):
            if self.airspeedCtrl == 'On':
                pitchCmd = self.pitchFromAirspeed.Update(referenceCommands.commandedAirspeed, state.Va)
            else:
                pitchCmd = self.pitchFromAltitude.Update(referenceCommands.commandedAltitude, -state.pd)
            throttleCmd = VPC.minControls.Throttle
            elevatorCmd = self.elevatorFromPitch.Update(pitchCmd, state.pitch, state.q)
        elif (self.mode == Controls.AltitudeStates.HOLDING):
            pitchCmd = self.pitchFromAltitude.Update(referenceCommands.commandedAltitude, -state.pd)
            if self.airspeedCtrl == 'On':
                throttleCmd = self.throttleFromAirspeed.Update(referenceCommands.commandedAirspeed, state.Va)
            else:
                # throttleCmd = self.trimControls.Throttle
                throttleCmd = 0.7
            elevatorCmd = self.elevatorFromPitch.Update(pitchCmd, state.pitch, state.q)
        elif (self.mode == Controls.AltitudeStates.CLIMBING):
            if self.airspeedCtrl == 'On':
                pitchCmd = self.pitchFromAirspeed.Update(referenceCommands.commandedAirspeed, state.Va)
            else:
                pitchCmd = self.pitchFromAltitude.Update(referenceCommands.commandedAltitude, -state.pd)
            throttleCmd = VPC.maxControls.Throttle
            elevatorCmd = self.elevatorFromPitch.Update(pitchCmd, state.pitch, state.q)
        else:
            # the value of self.mode is not recognized
            raise ValueError
        referenceCommands.commandedPitch = pitchCmd
        
        # pack controls into correct data type
        return Inputs.controlInputs(throttleCmd, aileronCmd, elevatorCmd, rudderCmd)

    def getControlGains(self):
        ''' Returns the current control gains '''
        return self.controlGains

    def getTrimInputs(self):
        ''' Returns the current trim inputs '''
        return self.trimControls

    def getVehicleAerodynamicsModel(self):
        ''' Returns stored vehicle aerodyanmics model '''
        return self.VAM

    def getVehicleControlSurfaces(self):
        ''' Returns the current control surface outputs '''
        return self.outputControls

    def getVehicleState(self):
        ''' Returns current vehicle state store in vehicle aerodynamics model '''
        return self.VAM.getVehicleState()
    
    def getStateEstimate(self):
        ''' Returns the current state estimate '''
        return self.estimator.getStateEstimate()
    
    def reset(self):
        ''' Resets all controller integrators, as well as vehicle aerodynamics model '''
        self.VAM.reset()
        self.estimator.reset()
        self.rollFromCourse.resetIntegrator()
        self.rudderFromSideslip.resetIntegrator()
        self.rudderFromYawDamping.resetIntegrator()
        self.throttleFromAirspeed.resetIntegrator()
        self.pitchFromAltitude.resetIntegrator()
        self.pitchFromAirspeed.resetIntegrator()
        self.aileronFromRoll.resetIntegrator()
        # expermental rudder controller
        self.rud_xi = 0
        # debug
        self.debug = 0
        self.file_name = "rc_data_{}.txt".format(datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"))

    def setEstimatorGains(self, Kp_att_a=5, Kp_att_h=10, Kp_alt=0.8, Kp_spd=.1, Ki_att_a=None, Ki_att_h=None, 
                       Ki_alt=None, Ki_spd=None):
        ''' Set the gains of the internal state estimation '''
        self.estimator.setFilterGains(Kp_att_a, Kp_att_h, Kp_alt, Kp_spd, Ki_att_a, Ki_att_h, Ki_alt, Ki_spd)

    def setControlGains(self, controlGains=Controls.controlGains()):
        ''' Sets the control gains, both in the stored control gains and in the controllers individually '''
        self.controlGains = controlGains
        # set control gains, as well as upper/lower limits and trim inputs
        self.rollFromCourse.setPIGains(dT=self.dT, kp=controlGains.kp_course, ki=controlGains.ki_course, trim=0.0,
                                       lowLimit=-math.radians(VPC.bankAngleLimit), 
                                       highLimit=math.radians(VPC.bankAngleLimit))
        if self.rudderConrolSource == 'SIDESLIP':
            self.rudderFromSideslip.setPIGains(dT=self.dT, kp=controlGains.kp_sideslip, ki=controlGains.ki_sideslip, 
                                        trim=self.trimControls.Rudder, 
                                        lowLimit=VPC.minControls.Rudder, 
                                        highLimit=VPC.maxControls.Rudder)
        else:
            self.rudderFromYawDamping.setPIGains(dT=self.dT, kp=-controlGains.kp_sideslip, ki=-controlGains.ki_sideslip, 
                                        trim=self.trimControls.Rudder, 
                                        lowLimit=VPC.minControls.Rudder, 
                                        highLimit=VPC.maxControls.Rudder)
            self.k_rudd = controlGains.kp_sideslip
            self.p_wo = controlGains.ki_sideslip
        self.throttleFromAirspeed.setPIGains(dT=self.dT, kp=controlGains.kp_SpeedfromThrottle, 
                                        ki=controlGains.ki_SpeedfromThrottle, trim=self.trimControls.Throttle, 
                                        lowLimit=VPC.minControls.Throttle, 
                                        highLimit=VPC.maxControls.Throttle)
        self.pitchFromAltitude.setPIGains(dT=self.dT, kp=controlGains.kp_altitude, 
                                        ki=controlGains.ki_altitude, trim=0.0, 
                                        lowLimit=-math.radians(VPC.pitchAngleLimit), 
                                        highLimit=math.radians(VPC.pitchAngleLimit))
        self.pitchFromAirspeed.setPIGains(dT=self.dT, kp=controlGains.kp_SpeedfromElevator, 
                                        ki=controlGains.ki_SpeedfromElevator, trim=0.0, 
                                        lowLimit=-math.radians(VPC.pitchAngleLimit), 
                                        highLimit=math.radians(VPC.pitchAngleLimit))
        self.elevatorFromPitch.setPDGains(kp=controlGains.kp_pitch, kd=controlGains.kd_pitch,
                                        trim=self.trimControls.Elevator, 
                                        lowLimit=VPC.minControls.Elevator,
                                        highLimit=VPC.maxControls.Elevator)
        self.aileronFromRoll.setPIDGains(dT=self.dT, 
                                        kp=controlGains.kp_roll, kd=controlGains.kd_roll, ki=controlGains.ki_roll,
                                        trim=self.trimControls.Aileron,
                                        lowLimit=VPC.minControls.Aileron, highLimit=VPC.maxControls.Aileron)

    def setTrimInputs(self, trimInputs=Inputs.controlInputs(Throttle=0.5, Aileron=0.0, Elevator=0.0, Rudder=0.0)):
        ''' Sets the trim control inputs '''
        self.trimControls = trimInputs

    def setVehicleState(self, state):
        ''' Sets the current vehicle state stored in the vehicle aerodynamics model '''
        self.VAM.setVehicleState(state)

    def setWaypointArray(self, Waypoints):
        ''' Sets a new array for the UAV to follow '''
        # find position
        # find position
        if self.stateSource == 'TRUE':
            pos = [[self.getVehicleState().pn], [self.getVehicleState().pe], [self.getVehicleState().pd]]
        else:
            pos = [[self.estimator.getStateEstimate().pn], [self.estimator.getStateEstimate().pe], [self.estimator.getStateEstimate().pd]]
        self.Waypoints = [pos] + Waypoints #+ [[[VPC.InitialNorthPosition], [VPC.InitialEastPosition], [VPC.InitialDownPosition]]]
        self.waypoint_idx = 1



class PIDControl():

    def __init__(self, dT=VPC.dT, kp=0.0, kd=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
        ''' Sets internal controller values to those passed as arguments '''
        self.dT = dT
        self.kp = kp
        self.kd = kd
        self.ki = ki
        self.trim = trim
        self.lowLimit = lowLimit
        self.highLimit = highLimit
        self.integral = 0.0
        self.prevError = 0.0

    def Update(self, command=0.0, current=0.0, derivative=0.0):
        ''' Calculates the output of the PID loop '''
        curError = command - current
        self.integral += (self.dT/2)*(curError + self.prevError)
        u = self.trim + self.kp*curError - self.kd*derivative + self.ki*self.integral
        # anti windup
        if u > self.highLimit:
            u = self.highLimit
            self.integral -= (self.dT/2)*(curError + self.prevError)    # deaccumulate
        elif u < self.lowLimit:
            u = self.lowLimit
            self.integral -= (self.dT/2)*(curError + self.prevError)    # deaccumulate
        # record error and return PID loop command
        self.prevError = curError
        return u

    def resetIntegrator(self):
        ''' Reset integration state to zero, for use when switching modes or resetting integral state '''
        self.integral = 0

    def setPIDGains(self, dT=VPC.dT, kp=0.0, kd=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
        ''' Function to set the gains for the PID control block (including the trim output and the limits) '''
        self.dT = dT
        self.kp = kp
        self.kd = kd
        self.ki = ki
        self.trim = trim
        self.lowLimit = lowLimit
        self.highLimit = highLimit


    
class PDControl():

    def __init__(self, kp=0.0, kd=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
        ''' Sets internal controller values to those passed as arguments '''
        self.kp = kp
        self.kd = kd
        self.trim = trim
        self.lowLimit = lowLimit
        self.highLimit = highLimit

    def Update(self, command=0.0, current=0.0, derivative=0.0):
        ''' Calculates output of PD loop '''
        curError = command - current
        u = self.trim + self.kp*curError - self.kd*derivative
        # saturation
        if u > self.highLimit:
            u = self.highLimit
        elif u < self.lowLimit:
            u = self.lowLimit
        return u
    
    def setPDGains(self, kp=0.0, kd=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
        ''' Function to set the gains for the PD control block (including the trim output and the limits) '''
        self.kp = kp
        self.kd = kd
        self.trim = trim
        self.lowLimit = lowLimit
        self.highLimit = highLimit



class PIControl():

    def __init__(self, dT=VPC.dT, kp=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
        ''' Sets internal controller values to those passed as arguments '''
        self.dT = dT
        self.kp = kp
        self.ki = ki
        self.trim = trim
        self.lowLimit = lowLimit
        self.highLimit = highLimit
        self.integral = 0.0
        self.prevError = 0.0

    def Update(self, command=0.0, current=0.0):
        ''' Calculates the output of the PI loop '''
        curError = command - current
        self.integral += (self.dT/2)*(curError + self.prevError)
        u = self.trim + self.kp*curError + self.ki*self.integral
        # anti windup
        if u > self.highLimit:
            u = self.highLimit
            self.integral -= (self.dT/2)*(curError + self.prevError)    # deaccumulate
        elif u < self.lowLimit:
            u = self.lowLimit
            self.integral -= (self.dT/2)*(curError + self.prevError)    # deaccumulate
        # record error and return PID loop command
        self.prevError = curError
        return u
    
    def resetIntegrator(self):
        ''' Reset integration state to zero, for use when switching modes or resetting integral state '''
        self.integral = 0

    def setPIGains(self, dT=VPC.dT, kp=0.0, ki=0.0, trim=0.0, lowLimit=0.0, highLimit=0.0):
        ''' Function to set the gains for the PI control block (including the trim output and the limits) '''
        self.dT = dT
        self.kp = kp
        self.ki = ki
        self.trim = trim
        self.lowLimit = lowLimit
        self.highLimit = highLimit
