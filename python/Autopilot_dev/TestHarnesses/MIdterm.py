'''
    Author: Margaret Silva

    Code related to midterm 
'''

#%% Imports
import sys
sys.path.append("..") #python is horrible, no?

from ece163 . Utilities import MatrixMath as mm
from ece163.Constants import VehiclePhysicalConstants as VPC
import math
import matplotlib.pyplot as plt

#%% Question 3

# constants
Va = 15
tau = 0.01
T_tot = 6
n_steps = int(T_tot/tau)

# data arrays
t_data = [i*tau for i in range(n_steps)]
pitch_data_id = [0 for i in range(n_steps)]
pitch_data_delayed = [0 for i in range(n_steps)]
pitch_c = [math.radians(5.73) for i in range(n_steps)]

# calculated params
c1 = (.5*VPC.rho*(Va**2)*VPC.S*VPC.c)/VPC.Jyy
b2 = c1*VPC.CMdeltaE

a1 = -VPC.CMq*c1*(VPC.c/(2*Va))
a2 = -VPC.CMalpha*c1

Kp = -8

print("b2:", b2, "a1:", a1, "a2:", a2)

print("DC gain", (Kp*b2)/(a2+ Kp*b2))

# transfer functions to simulate ideal system
A_id = [[ - a1 , - (a2 + Kp*b2) ] ,[1 , 0]]
B_id = [[1] , [0]]
C_id = [[0 , b2 ]]

x = [[0], [0]]
for i in range(n_steps):
    pitch_data_id[i] = mm.multiply(C_id, x)[0][0]
    u = pitch_c[i]
    # calculate derivative :
    x_dot = mm . add (
     mm . multiply (A_id ,x) ,
    mm . scalarMultiply (u ,B_id))
    # forward euler update :
    x = mm . add (x ,mm . scalarMultiply ( tau , x_dot ) )

# transfer functions to simulate delayed system
A_dl = [[ - ((1/tau) + a1) , - ((a1/tau) + a2 - Kp*b2), -((a2 + Kp*b2)/tau) ] ,[1 , 0, 0], [0, 1, 0]]
B_dl = [[1] , [0], [0]]
C_dl = [[0 , Kp*b2, (Kp*b2)/tau ]]

x = [[0], [0], [0]]
for i in range(n_steps):
    pitch_data_delayed[i] = mm.multiply(C_dl, x)[0][0]
    u = pitch_c[i]
    # calculate derivative :
    x_dot = mm . add (
     mm . multiply (A_dl ,x) ,
    mm . scalarMultiply (u ,B_dl))
    # forward euler update :
    x = mm . add (x ,mm . scalarMultiply ( tau , x_dot ) )


plt.subplot(2, 2, 1)
plt.plot(t_data, pitch_data_id, label="ideal pitch data")
plt.xlabel("Time (seconds)")
plt.ylabel("Pitch angle (radians)") 
plt.legend()

plt.subplot(2, 2, 2)
plt.plot(t_data, pitch_data_delayed, label="delayed pitch data")
plt.xlabel("Time (seconds)")
plt.ylabel("Pitch angle (radians)")
plt.legend()

plt.subplot(2, 1, 2)
plt.plot(t_data, pitch_data_id, label="ideal pitch data")
plt.plot(t_data, pitch_data_delayed, label="delayed pitch data")
plt.xlabel("Time (seconds)")
plt.ylabel("Pitch angle (radians)")
plt.legend()

plt.suptitle("Problem 3 Part e - Graphs of controller using ideal and delayed pitch data")
plt.show()


#%% Problem 4
CDp = 0.025
a_opt_time = (math.sqrt(3*CDp*math.pi*VPC.AR*VPC.e) - VPC.CL0)/(VPC.CLalpha)
Cl_opt_time = VPC.CL0 + VPC.CLalpha*a_opt_time
CD_opt_time = CDp + (1/(math.pi*VPC.AR*VPC.e))*(Cl_opt_time**2)
gamma_opt_time = math.atan2(CD_opt_time, Cl_opt_time)
Va_opt_time = math.sqrt((2*VPC.mass*VPC.g0*math.cos(gamma_opt_time))/(VPC.rho*VPC.S*Cl_opt_time))

print("alpha opt (time):", math.degrees(a_opt_time))
print("gamma opt (time):", math.degrees(gamma_opt_time), gamma_opt_time)
print("Va opt (time):", Va_opt_time)

a_opt_dist = (math.sqrt(CDp*math.pi*VPC.e*VPC.AR) - VPC.CL0)/VPC.CLalpha
CL_opt_dist = VPC.CL0 + VPC.CLalpha*(a_opt_dist)
CD_opt_dist = CDp + (1/(math.pi*VPC.AR*VPC.e))*(CL_opt_dist**2)
gamma_opt_dist = math.atan2(CD_opt_dist, CL_opt_dist)
Va_opt_dist = math.sqrt((2*VPC.mass*VPC.g0*math.cos(gamma_opt_dist))/(VPC.rho*VPC.S*CL_opt_dist))

print("alpha opt (dist):", math.degrees(a_opt_dist))
print("gamma opt (dist):", math.degrees(gamma_opt_dist), gamma_opt_dist)
print("Va opt (dist):", Va_opt_dist)

print("h dot (time)", Va_opt_time*math.sin(gamma_opt_time))
print("h dot (dist)", Va_opt_dist*math.sin(gamma_opt_dist))
print((Va_opt_dist*math.sin(gamma_opt_dist))/ (Va_opt_time*math.sin(gamma_opt_time)))

print("Df/H0 (time)", 1/math.tan(gamma_opt_time))
print("Df/H0 (dist)", 1/math.tan(gamma_opt_dist))
print((1/math.tan(gamma_opt_dist))/ (1/math.tan(gamma_opt_time)))


# %%
